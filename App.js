/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View,TouchableOpacity } from 'react-native';
import BoxShadow from './app/components/shadow/BoxShadow';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {

componentWillMount(){
  
}
  render() {

    const shadowOpt = {
      width: 100,
      height: 90,
      color: "#000000",
      border: 15,
      // radius: 5,
      opacity: 0.5,
      x: 0,
      y: 5,
      style: { marginVertical: 5 }
    }

    return (
      <TouchableOpacity onPress={()=>{
      }} style={styles.container}>

        <BoxShadow setting={shadowOpt} >
          <View style={{ width: 100, height: 100, borderRadius:0,backgroundColor:'#fff' }} />
        </BoxShadow>
        <View >
        <BoxShadow style={{}} setting={shadowOpt} >
          <View style={{ width: 100, height: 100, borderRadius:0,backgroundColor:'#fff' }} />
        </BoxShadow>
        </View>
    
        {/* <BoxShadow setting={shadowOpt} >
          <View style={{ width: 100, height: 100, backgroundColor: '#fff' }} />

        </BoxShadow> */}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

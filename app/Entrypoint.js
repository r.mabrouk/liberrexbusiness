/**
 * React Native App
 * Everthing starts from the entrypoint
 */
import React, { Component } from 'react';
import { ActivityIndicator, View, ImageBackground } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react';
import Navigator from '../app/navigation';
import configureStore from 'app/store/configureStore';
import Footer from './components/Footer/Footer';
import Toast,{ setToast } from './components/toast/toast';
import { images } from './config';
import { Card } from './components/card/card';
import { Height } from './components/utils/dimensions';
import SideBar from './components/sideBar';
import { drowerSideBar } from './components/sideBar';
import Loading from './screens/Loading';
const { persistor, store } = configureStore();

export default class Entrypoint extends Component {
    constructor(props) {
        super(props)
   
    }
    componentWillMount() {


        //  this.openBottomMenu.onAnimated(true)
        setTimeout(()=>{
            setToast(this._Toast)  
        },3000)
    }
    render() {
        console.disableYellowBox = true;
        return (
            <Provider store={store}>
                <PersistGate
                    loading={<ActivityIndicator />}
                    persistor={persistor}
                >

                    <SideBar ref={(refs) => drowerSideBar(refs)} >

                        <Navigator />


                    </SideBar>





                    {/* <Footer /> */}
                    <Toast   ref={(toast)=>this._Toast=toast} duration={1000} height={100}  />
                </PersistGate>
            </Provider>
        );
    }
}

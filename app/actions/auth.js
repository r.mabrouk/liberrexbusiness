/*
 * Reducer actions related with login
 */

import * as types from './types';


export function socialLoginRequest(user_data){
    return {
        type: types.SOCIAL_LOGIN_REQUEST,
        user_data
    };
}

export function socialLoginResponse(response){
    return {
        type: types.SOCIAL_LOGIN_RESPONSE,
        response
    };
}
export function socialLoginFailed(){
    return {
        type: types.SOCIAL_LOGIN_FAILED
    };
}

// login actions
export function requestLogin(user_data) {
    return {
        type: types.LOGIN_REQUEST,
        user_data
    };
}
export function loginFailed() {
    return {
        type: types.LOGIN_FAILED
    };
}
export function onLoginResponse(response) {
    return {
        type: types.LOGIN_RESPONSE,
        response
    };
}
export function enableLoader() {
    return {
        type: types.LOGIN_ENABLE_LOADER
    };
}
export function disableLoader() {
    return {
        type: types.LOGIN_DISABLE_LOADER
    };
}


// get countries 
export function getCountriesRequest() {
    return {
        type: types.GET_COUNTRIES_REQUEST,
    };
}
export function getCountriesFailed() {
    return {
        type: types.GET_COUNTRIES_FAILED
    };
}
export function getCountriesResponse(response) {
    return {
        type: types.GET_COUNTRIES_RESPONSE,
        response
    };
}


// check email
export function checkEmailRequest(data,email) {
    return {
        type: types.CHECK_EMAIL_REQUEST,
        data,
        email
    };
}
export function checkEmailFailed() {
    return {
        type: types.CHECK_EMAIL_FAILED
    };
}
export function checkEmailResponse(response) {
    return {
        type: types.CHECK_EMAIL_RESPONSE,
        response
    };
}



// search industry
export function searchIndustryRequest(keyword) {
    return {
        type: types.SEARCH_INDUSTRIES_REQUEST,
        keyword
    };
}
export function searchIndustryFailed() {
    return {
        type: types.SEARCH_INDUSTRIES_FAILED
    };
}

export function searchIndustryResponse(response) {
    return {
        type: types.SEARCH_INDUSTRIES_RESPONSE,
        response
    };
}
// get industry
export function getAllIndustryRequest() {
    return {
        type: types.GET_ALL_INDUSTRIES_REQUEST,
    };
}
export function getAllIndustryFailed() {
    return {
        type: types.GET_ALL_INDUSTRIES_FAILED
    };
}

export function getAllIndustryResponse(response) {
    return {
        type: types.GET_ALL_INDUSTRIES_RESPONSE,
        response
    };
}


// sign up actions   ----------------------------------------------------->

export function requestSignUp(user_data,social) {
    return {
        type: types.SIGNUP_REQUEST,
        user_data,
        social
    };
}
export function SignUpFailed() {
    return {
        type: types.SIGNUP_FAILED
    };
}

export function onSignUpResponse(response) {
    return {
        type: types.SIGNUP_RESPONSE,
        response
    };
}



// sign up forgot passsword ----------------------------------------------------->
export function requestResetPassword(email) {
    return {
        type: types.RESET_PASSWORD_REQUEST,
        email
    };
}
export function onResetPasswordFailed() {
    return {
        type: types.RESET_PASSWORD_FAILED
    };
}
export function onResetPasswordResponse(response) {
    return {
        type: types.RESET_PASSWORD_RESPONSE,
        response
    };
}

// request Reset Password With Code ----------------------------------------------------->
export function requestResetPasswordWithCode(data) {
    return {
        type: types.RESET_PASSWORD_REQUEST_WITH_CODE,
        data
    };
}
export function resetPasswordWithCodeFailed() {
    return {
        type: types.RESET_PASSWORD_FAILED_WITH_CODE
    };
}
export function onResetPasswordWithCodeResponse(response) {
    return {
        type: types.RESET_PASSWORD_RESPONSE_WITH_CODE,
        response
    };
}


// logout ----------------------------------------------------->

export function requestLogout() {
    return {
        type: types.LOGOUT_REQUEST,
    };
}
export function LogoutFailed() {
    return {
        type: types.LOGOUT_FAILED
    };
}
export function LogoutResponse() {
    return {
        type: types.LOGOUT_RESPONSE,
        
    };
}



export function setDefaultQueue(id) {
    return {
        type: types.SET_DEFAULT_QUEUE,
        defaultQueue:id
        
    };
}
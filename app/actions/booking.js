


import * as types from './types';

// get booking list actions ---------------------------->

export function getBookingListRequest(reset,keyword,page) {
    return {
        type: types.GET_BOOKING_LIST_REQUEST,
        reset,
        page,
        keyword
    };
}
export function getBookingListFailed() {
    return {
        type: types.GET_BOOKING_LIST_FAILED
    };
}
export function getBookingListResponse(response) {
    return {
        type: types.GET_BOOKING_LIST_RESPONSE,
        response
    };
}


// create booking list actions ---------------------------->

export function createBookingRequest(data) {
    return {
        type: types.CREATE_BOOKING_REQUEST,
        data
    };
}
export function createBookingFailed() {
    return {
        type: types.CREATE_BOOKING_FAILED
    };
}
export function createBookingResponse(response, index) {
    return {
        type: types.CREATE_BOOKING_RESPONSE,
        response, index
    };
}


// delete booking list actions ---------------------------->

export function deleteBookingRequest(id,index,onCloseModal) {
    return {
        type: types.DELETE_BOOKING_REQUEST,
        id, index,
        onCloseModal
    };
}
export function deleteBookingFailed() {
    return {
        type: types.DELETE_BOOKING_FAILED
    };
}
export function deleteBookingResponse(response, index) {
    return {
        type: types.DELETE_BOOKING_RESPONSE,
        response, index
    };
}



// update booking list actions ---------------------------->

export function updateBookingRequest(data, id, index) {
    return {
        type: types.UPDATE_BOOKING_REQUEST,
        id, index,
        data
    };
}
export function updateBookingFailed() {
    return {
        type: types.UPDATE_BOOKING_FAILED
    };
}
export function updateBookingResponse(response, index) {
    return {
        type: types.UPDATE_BOOKING_RESPONSE,
        response, index
    };
}



// filter booking list actions ---------------------------->

export function filterBookingRequest(date) {
    return {
        type: types.FILTER_BOOKING_REQUEST,
        date
    };
}
export function filterBookingFailed() {
    return {
        type: types.FILTER_BOOKING_FAILED
    };
}
export function filterBookingResponse(response) {
    return {
        type: types.FILTER_BOOKING_RESPONSE,
        response
    };
}








// get booking requests list actions ---------------------------->

export function getBookingRequestListRequest(reset, keyword, page) {
    return {
        type: types.GET_BOOKING_REQUEST_LIST_REQUEST,
        reset, keyword, page
    };
}
export function getBookingRequestListFailed() {
    return {
        type: types.GET_BOOKING_REQUEST_LIST_FAILED
    };
}
export function getBookingRequestListResponse(response) {
    return {
        type: types.GET_BOOKING_REQUEST_LIST_RESPONSE,
        response
    };
}


// approve booking requests actions ---------------------------->

export function approveBookingRequest(id, index, onCloseModal) {
    return {
        type: types.APPROVE_BOOKING_REQUEST_REQUEST,
        id, index, onCloseModal
    };
}
export function approveBookingRequestFailed() {
    return {
        type: types.APPROVE_BOOKING_REQUEST_FAILED
    };
}
export function approveBookingRequestResponse(response, index) {
    return {
        type: types.APPROVE_BOOKING_REQUEST_RESPONSE,
        response, index
    };
}

// delete booking list actions ---------------------------->

export function deleteBookingRequestRequest(id,index,onCloseModal) {
    return {
        type: types.DELETE_BOOKING_REQUEST_REQUEST,
        id,index,onCloseModal
    };
}
export function deleteBookingRequestFailed() {
    return {
        type: types.DELETE_BOOKING_REQUEST_FAILED
    };
}
export function deleteBookingRequestResponse(response, index) {
    return {
        type: types.DELETE_BOOKING_REQUEST_RESPONSE,
        response, index
    };
}







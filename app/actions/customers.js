/*
 * Reducer actions related with login
 */

import * as types from './types';


// get all customerts actions   ---------------------------->
export function resetCustomersList() {
    return {
        type: types.RESET_CUSTOMERS_LIST,
    };
}
export function getAllCustomersRequest(reset, keyword,page) {
    return {
        type: types.GET_ALL_CUSTOMERS_REQUEST,
        keyword,
        page,
        reset
    };
}
export function getAllCustomersFailed() {
    return {
        type: types.GET_ALL_CUSTOMERS_FAILED
    };
}
export function getAllCustomersResponse(response) {
    return {
        type: types.GET_ALL_CUSTOMERS_RESPONSE,
        response
    };
}
export function SelectCustomer(customer) {
    return {
        type: types.SELECTED_CUSTOMER_TO_APPOINTMENT,
        customer
    };
}


// get all vip customerts actions   ---------------------------->
export function resetCustomersVipList() {
    return {
        type: types.RESET_CUSTOMERS_VIP_LIST,
    };
}
export function getAllCustomersVipRequest(reset, keyword,page) {

    return {
        type: types.GET_ALL_CUSTOMERS_VIP_REQUEST,
        keyword,
        page,
        reset
    };
}
export function getAllCustomersVipFailed() {
    return {
        type: types.GET_ALL_CUSTOMERS_VIP_FAILED
    };
}
export function getAllCustomersVipResponse(response) {
    return {
        type: types.GET_ALL_CUSTOMERS_VIP_RESPONSE,
        response
    };
}

// get all blackList customerts actions   ---------------------------->
export function resetCustomersBlackList() {
    return {
        type: types.RESET_CUSTOMERS_BLACKLIST_LIST,
    };
}
export function getAllCustomersBlacklistRequest(reset, keyword,page) {
    return {
        type: types.GET_ALL_CUSTOMERS_BLACKLIST_REQUEST,
        keyword,page,
        reset
    };
}
export function getAllCustomersBlacklistFailed() {
    return {
        type: types.GET_ALL_CUSTOMERS_BLACKLIST_FAILED
    };
}
export function getAllCustomersBlacklistResponse(response) {
    return {
        type: types.GET_ALL_CUSTOMERS_BLACKLIST_RESPONSE,
        response
    };
}




// get a customerts actions ---------------------------->

export function getACustomersRequest() {
    return {
        type: types.GET_A_CUSTOMERS_REQUEST
    };
}
export function getACustomersFailed() {
    return {
        type: types.GET_A_CUSTOMERS_FAILED
    };
}
export function getACustomersResponse(response) {
    return {
        type: types.GET_A_CUSTOMERS_RESPONSE,
        response
    };
}


// create new customer action ---------------------------->

export function createCustomerRequest(customer_data,onCloseModal) {
    return {
        type: types.CREATE_CUSTOMER_REQUEST,
        customer_data,
        onCloseModal
    };
}
export function createCustomerFailed() {
    return {
        type: types.CREATE_CUSTOMER_FAILED
    };
}
export function createCustomerResponse(response) {
    return {
        type: types.CREATE_CUSTOMER_RESPONSE,
        response
    };
}


// update customer action ---------------------------->

export function updateCustomerRequest(customer_data,editCustomerModal,index,keyOfReducer) {
    return {
        type: types.UPDATE_CUSTOMER_REQUEST,
        customer_data,
        editCustomerModal,
        index,
        keyOfReducer
    };
}
export function updateCustomerFailed() {
    return {
        type: types.UPDATE_CUSTOMER_FAILED
    };
}
export function updateCustomerResponse(response,keyOfReducer) {
    return {
        type: types.UPDATE_CUSTOMER_RESPONSE,
        response,
        keyOfReducer
    };
}

// delete customer action ---------------------------->

export function deleteCustomerRequest(id, index, loader, onCloseModal, keyOfReducer) {
    return {
        type: types.DELETE_CUSTOMER_REQUEST,
        customer_id:id,
        index, loader, onCloseModal, keyOfReducer
    };
}
export function deleteCustomerFailed(loader) {
    return {
        type: types.DELETE_CUSTOMER_FAILED,
        loader
    };
}
export function deleteCustomerResponse(index,loader,keyOfReducer) {
    return {
        type: types.DELETE_CUSTOMER_RESPONSE,
        index,
        loader,
        keyOfReducer
    };
}


// remove  customer from book action ---------------------------->

export function removeCustomerFromBookRequest(id, index, onCloseModal,books_id,loader,keyOfReducer) {
    return {
        type: types.REMOVE_CUSTOMER_FROM_BOOK_REQUEST,
        id,index,onCloseModal,
        books_id,loader,keyOfReducer
    };
}
export function removeCustomerFromBookFailed(loader) {
    return {
        type: types.REMOVE_CUSTOMER_FROM_BOOK_FAILED,
        loader
    };
}
export function removeCustomerFromBookResponse(response,loader,keyOfReducer) {
    return {
        type: types.REMOVE_CUSTOMER_FROM_BOOK_RESPONSE,
        response,
        loader,
        keyOfReducer
    };
}




//add Customer To books---------------------------->

export function addCustomerToBooksRequest(id, index,loader,books_id,onCloseModal,keyOfReducer) {
    return {
        type: types.ADD_CUSTOMER_TO_BOOKS_REQUEST,
        id,index,onCloseModal,
        loader,
        books_id,
        keyOfReducer
    };
}
export function addCustomerToBooksFailed(loader) {
    return {
        type: types.ADD_CUSTOMER_TO_BOOKS_FAILED,
        loader
    };
}
export function addCustomerToBooksResponse(response,loader,books_id,keyOfReducer) {
    return {
        type: types.ADD_CUSTOMER_TO_BOOKS_RESPONSE,
        response,
        loader,
        books_id,
        keyOfReducer
    };
}


// get all Customer Feedback---------------------------->

export function getAllCustomerFeedbackRequest() {
    return {
        type: types.GET_ALL_CUSTOMER_FEEDBACK_REQUEST,
    };
}
export function getAllCustomerFeedbackFailed(loader) {
    return {
        type: types.GET_ALL_CUSTOMER_FEEDBACK_FAILED,
        loader
    };
}
export function getAllCustomerFeedbackResponse(response) {
    return {
        type: types.GET_ALL_CUSTOMER_FEEDBACK_RESPONSE,
        response
    };
}


// filter Customer Feedback---------------------------->

export function filterCustomerFeedbackRequest(filter_data) {
    return {
        type: types.FILTER_CUSTOMER_FEEDBACK_REQUEST,
        filter_data
    };
}
export function filterCustomerFeedbackFailed() {
    return {
        type: types.FILTER_CUSTOMER_FEEDBACK_FAILED,
        
    };
}
export function filterCustomerFeedbackResponse(response) {
    return {
        type: types.FILTER_CUSTOMER_FEEDBACK_RESPONSE,
        response
    };
}







import * as types from './types';

export function getPlacesRequest(place_name) {
    return {
        type: types.GET_PLACES_FROM_GOOGLE_API_REQUEST,
        place_name
    };
}
export function getPlacesFailed() {
    return {
        type: types.GET_PLACES_FROM_GOOGLE_API_FAILED
    };
}
export function getPlacesResponse(response) {
    return {
        type: types.GET_PLACES_FROM_GOOGLE_API_RESPONSE,
        response
    };
}
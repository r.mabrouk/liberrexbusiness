
import * as types from './types';


export function getHomeStatsRequest() {
    return {
        type: types.GET_HOME_STATS_REQUEST,
    };
}
export function getHomeStatsFailed() {
    return {
        type: types.GET_HOME_STATS_FAILED
    };
}
export function getHomeStatsResponse(response) {
    return {
        type: types.GET_HOME_STATS_RESPONSE,
        response
    };
}


export function filterHomeReportsRequest() {
    return {
        type: types.FILTER_HOME_REPORTS_REQUEST,
    };
}
export function filterHomeReportsFailed() {
    return {
        type: types.FILTER_HOME_REPORTS_FAILED
    };
}
export function filterHomeReportsResponse(response) {
    return {
        type: types.FILTER_HOME_REPORTS_RESPONSE,
        response
    };
}



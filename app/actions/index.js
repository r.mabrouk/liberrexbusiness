// export action creators
import * as authActions from './auth';
import * as navigationActions from './navigationActions';
import * as queueActions from './queue';
import * as servicesActions from './services';

export const ActionCreators = Object.assign(
    // {},
    authActions,
    navigationActions,
    queueActions,
    servicesActions
);


import * as types from './types';


// get All Services Request

export function getMembershipPlansRequest() {
    return {
        type: types.GET_MEMBERSHIP_PLAN_REQUEST
    };
}
export function getMembershipPlansFailed() {
    return {
        type: types.GET_MEMBERSHIP_PLAN_FAILED
    };
}
export function getMembershipPlansResponse(response,selectedPackagesIndex) {
    return {
        type: types.GET_MEMBERSHIP_PLAN_RESPONSE,
        response,
        selectedPackagesIndex
    };
}

export function setPlanSelectedAction(selectedPackagesIndex) {
    return {
        type: types.SET_PLAN_SELECTED,
        selectedPackagesIndex
    };
}


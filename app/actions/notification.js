
import * as types from './types';


// get All Services Request

export function getNotificationListRequest() {
    return {
        type: types.GET_ALL_NOTIFICATION_REQUEST,

    };
}
export function getNotificationListFailed() {
    return {
        type: types.GET_ALL_NOTIFICATION_FAILED
    };
}
export function getNotificationListResponse(response) {
    return {
        type: types.GET_ALL_NOTIFICATION_RESPONSE,
        response
    };
}

import * as types from './types';

// get All Services Request

export function addnewPaymentCardRequest(data, saved, CloseModal) {
    return {
        type: types.ADD_NEW_PAYMENT_CARD_REQUEST,
        data, saved,
        CloseModal
    };
}
export function addnewPaymentCardFailed() {
    return {
        type: types.ADD_NEW_PAYMENT_CARD_FAILED
    };
}
export function addnewPaymentCardResponse(response) {
    return {
        type: types.ADD_NEW_PAYMENT_CARD_RESPONSE,
        response
    };
}

// get payment cards list 

export function getPaymentCardsRequest(data, saved) {
    return {
        type: types.GET_PAYEMENT_CARDS_REQUEST,
        data, saved
    };
}
export function getPaymentCardsFailed() {
    return {
        type: types.GET_PAYEMENT_CARDS_FAILED
    };
}
export function getPaymentCardsResponse(response) {
    return {
        type: types.GET_PAYEMENT_CARDS_RESPONSE,
        response
    };
}

// delete payment card list 

export function deletePaymentCardRequest(id, index) {
    return {
        type: types.DELETE_PAYEMENT_CARD_REQUEST,
        id, index
    };
}
export function deletePaymentCardFailed() {
    return {
        type: types.DELETE_PAYEMENT_CARD_FAILED
    };
}
export function deletePaymentCardResponse(index) {
    return {
        type: types.DELETE_PAYEMENT_CARD_RESPONSE,
        index
    };
}


// update payment card list 

export function updateAccountMembershipRequest(data,detailsForNextScreen) {
    return {
        type: types.UPDATE_ACCOUNT_MEMBERSHIP_REQUEST,
        data,
        detailsForNextScreen
    };
}
export function updateAccountMembershipFailed() {
    return {
        type: types.UPDATE_ACCOUNT_MEMBERSHIP_FAILED
    };
}
export function updateAccountMembershipResponse(response) {
    return {
        type: types.UPDATE_ACCOUNT_MEMBERSHIP_RESPONSE,
        response
    };
}







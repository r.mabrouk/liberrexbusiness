import * as types from './types';





// -------------- add queue ----------------
export function addQueue(body) {
    return {
        type: types.ADD_QUEUE,
        body
    };
}
export function addQueueResponse(response) {
    return {
        type: types.ADD_QUEUE_RESPONSE,
        response
    };
}
export function addQueueFailed() {
    return {
        type: types.ADD_QUEUE_FAILED
    };
}






// -------------- selected queue -------------
export function selectedQueue(id,selectedQueue) {
    return {
        type: types.SELECTED_QUEUE,
        id,
        selectedQueue
    };
}


// --------------- queue waiting list ------------
export function getQueueWaitingList() {
    return {
        type: types.GET_QUEUE_WAITING_LIST,
    };
}
export function getQueueWaitingListResponse(response) {
    return {
        type: types.GET_QUEUE_WAITING_LIST_RESPONSE,
        response
    };
}
export function getQueueWaitingListFailed() {
    return {
        type: types.GET_QUEUE_WAITING_LIST_FAILED
    };
}


// --------------- queue pending list ------------
export function getQueuePendingList() {
    return {
        type: types.GET_QUEUE_PENDING_LIST,
    };
}
export function getQueuePendingListResponse(response) {
    return {
        type: types.GET_QUEUE_PENDING_LIST_RESPONSE,
        response
    };
}
export function getQueuePendingListFailed() {
    return {
        type: types.GET_QUEUE_PENDING_LIST_FAILED
    };
}


// --------------- approve queue request ------------
export function approveQueueRequest(queue_request_id, onCloseModal) {
    return {
        type: types.APPROVE_QUEUE_REQUEST,
        queue_request_id,
        onCloseModal
    };
}
export function approveQueueRequestResponse(response) {
    return {
        type: types.APPROVE_QUEUE_REQUEST_RESPONSE,
        response
    };
}
export function approveQueueRequestFailed() {
    return {
        type: types.APPROVE_QUEUE_REQUEST_FAILED
    };
}



// --------------- decline queue request ------------
export function declineQueueRequest(queue_request_id, onCloseModal) {
    return {
        type: types.DECLINE_QUEUE_REQUEST,
        queue_request_id,
        onCloseModal
    };
}
export function declineQueueRequestResponse(response) {
    return {
        type: types.DECLINE_QUEUE_REQUEST_RESPONSE,
        response
    };
}
export function declineQueueRequestFailed() {
    return {
        type: types.DECLINE_QUEUE_REQUEST_FAILED
    };
}
 
// ----------- selected customer ------------
export function selectedCustomer(customer) {
    return {
        type: types.SELECTED_CUSTOMER,
        customer
    };
}

// --------------- redirect queue customer ------------
export function redirectCustomer(selectedQueueID) {
    return {
        type: types.REDIRECT_CUSTOMER,
        selectedQueueID
    };
}
export function redirectCustomerResponse(response) {
    return {
        type: types.REDIRECT_CUSTOMER_RESPONSE,
        response
    };
}
export function redirectCustomerFailed() {
    return {
        type: types.REDIRECT_CUSTOMER_FAILED
    };
}


// -------------- add customer to queue ----------------
export function addCustomerToQueue(body,onCloseModal) {
    return {
        type: types.ADD_CUSTOMER_TO_QUEUE,
        body,
        onCloseModal
    };
}
export function addCustomerToQueueResponse(response) {
    return {
        type: types.ADD_CUSTOMER_TO_QUEUE_RESPONSE,
        response
    };
}
export function addCustomerToQueueFailed() {
    return {
        type: types.ADD_CUSTOMER_TO_QUEUE_FAILED
    };
}



// -------------- add customer to queue ----------------
export function cancelCustomerInWaiting(customer_id, onCloseModal) {
    return {
        type: types.CANCEL_CUSTOMER_IN_WAITING,
        customer_id,
        onCloseModal
    };
}
export function cancelCustomerInWaitingResponse(response) {
    return {
        type: types.CANCEL_CUSTOMER_IN_WAITING_RESPONSE,
        response
    };
}
export function cancelCustomerInWaitingFailed() {
    return {
        type: types.CANCEL_CUSTOMER_IN_WAITING_FAILED
    };
}


// -------------- call next customer in queue ----------------
export function callNextCustomer(queue_id, onCloseModal) {
    return {
        type: types.CALL_NEXT_CUSTOMER,
        queue_id,
        onCloseModal
    };
}
export function callNextCustomerResponse(response) {
    return {
        type: types.CALL_NEXT_CUSTOMER_RESPONSE,
        response
    };
}
export function callNextCustomerFailed() {
    return {
        type: types.CALL_NEXT_CUSTOMER_FAILED
    };
}


// -------------- Swap customer ranks ----------------
export function swapCustomerRanks(customer_1, customer_2) {
    return {
        type: types.SWAP_CUSTOMER_RANKS,
        customer_1,
        customer_2
    };
}
export function swapCustomerRanksResponse(response) {
    return {
        type: types.SWAP_CUSTOMER_RANKS_RESPONSE,
        response
    };
}
export function swapCustomerRanksFailed() {
    return {
        type: types.SWAP_CUSTOMER_RANKS_FAILED
    };
}



// -------------- get queue list ----------------
export function getQueueList(firstTime) {
    return {
        type: types.GET_QUEUE_LIST,
        firstTime: !!firstTime
    };
}
export function getQueueListResponse(response) {
    return {
        type: types.GET_QUEUE_LIST_RESPONSE,
        response
    };
}
export function getQueueListFailed() {
    return {
        type: types.GET_QUEUE_LIST_FAILED
    };
}



// -------------- add queue ----------------
export function getQueueRequest(queue_id) {
    return {
        type: types.GET_A_QUEUE_REQUEST,
        queue_id
    };
}
export function getQueueResponse(response) {
    return {
        type: types.GET_A_QUEUE_RESPONSE,
        response
    };
}
export function getQueueFailed() {
    return {
        type: types.GET_A_QUEUE_FAILED
    };
}


// -------------- update queue ----------------
export function updateQueueRequest(queue_id,newQueue) {
    return {
        type: types.UPDATE_A_QUEUE_REQUEST,
        queue_id,
        newQueue
    };
}
export function updateQueueResponse(response) {
    return {
        type: types.UPDATE_A_QUEUE_RESPONSE,
        response
    };
}
export function updateQueueFailed() {
    return {
        type: types.UPDATE_A_QUEUE_FAILED
    };
}


// -------------- update queue ----------------
export function deleteQueueRequest(queue_id) {
    return {
        type: types.DELETE_A_QUEUE_REQUEST,
        queue_id
    };
}
export function deleteQueueResponse(response) {
    return {
        type: types.DELETE_A_QUEUE_RESPONSE,
        response
    };
}
export function deleteQueueFailed() {
    return {
        type: types.DELETE_A_QUEUE_FAILED
    };
}


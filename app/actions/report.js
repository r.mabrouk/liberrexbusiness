
import * as types from './types';

// -------------- get queue list ----------------
export function getReportsRequest() {
    return {
        type: types.GET_REPORTS_LIST_REQUEST
    };
}
export function getReportsResponse(response) {
    return {
        type: types.GET_REPORTS_LIST_RESPONSE,
        response
    };
}
export function getReportsFailed() {
    return {
        type: types.GET_REPORTS_LIST_FAILED
    };
}


// -------------- get queue list ----------------
export function filterReportRequest(keywords) {
    return {
        type: types.FILTER_REPORT_REQUEST,
        keywords
    };
}

export function filterReportResponse(response) {
    return {
        type: types.FILTER_REPORT_RESPONSE,
        response
    };
}
export function filterReportFailed() {
    return {
        type: types.FILTER_REPORT_FAILED
    };
}


import * as types from './types';


//  get all Rules list

export function getRulesListRequest(reset, keyword, page) {
    return {
        type: types.GET_RULES_LIST_REQUEST,
        reset,
        keyword,
        page
    };
}
export function getRulesListFailed() {
    return {
        type: types.GET_RULES_LIST_FAILED
    };
}
export function getRulesListResponse(response) {
    return {
        type: types.GET_RULES_LIST_RESPONCE,
        response
    };
}


//  create Rules 
export function createRuleRequest(data) {
    return {
        type: types.CREATE_RULE_REQUEST,
        data
    };
}
export function createRuleFailed() {
    return {
        type: types.CREATE_RULE_FAILED
    };
}
export function createRuleResponse(response) {
    return {
        type: types.CREATE_RULE_RESPONCE,
        response
    };
}


// update Rules 
export function updateRuleRequest(data, id, index) {
    return {
        type: types.UPDATE_RULE_REQUEST,
        id,
        index,
        data
    };
}
export function updateRuleFailed() {
    return {
        type: types.UPDATE_RULE_FAILED
    };
}
export function updateRuleResponse(response, index) {
    return {
        type: types.UPDATE_RULE_RESPONCE,
        response,
        index
    };
}


// delete Rules 
export function deleteRuleRequest(id, index) {
    return {
        type: types.DELETE_RULE_REQUEST,
        id, index
    };
}
export function deleteRuleFailed() {
    return {
        type: types.DELETE_RULE_FAILED
    };
}
export function deleteRuleResponse(response, index) {
    return {
        type: types.DELETE_RULE_RESPONCE,
        response,
        index
    };
}




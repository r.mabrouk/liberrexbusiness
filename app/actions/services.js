
import * as types from './types';


// get All Services Request

export function getAllServicesRequest(resetServices,keyword,page) {
    return {
        type: types.GET_ALL_SERVICES_REQUEST,
        resetServices,
        keyword,
        page
    };
}
export function getAllServicesFailed() {
    return {
        type: types.GET_ALL_SERVICES_FAILED
    };
}
export function getAllServicesResponse(response) {
    return {
        type: types.GET_ALL_SERVICES_RESPONSE,
        response
    };
}



// delete Service Request
export function deleteServiceRequest(service_id,index,onCloseModal) {
    return {
        type: types.DELETE_SERVICE_REQUEST,
        service_id,
        index,
        onCloseModal
    };
}
export function deleteServiceFailed() {
    return {
        type: types.DELETE_SERVICE_FAILED
    };
}
export function deleteServiceResponse(response,index) {
    return {
        type: types.DELETE_SERVICE_RESPONSE,
        response,index
    };
}



// update Service Request
export function updateServiceRequest(service_id,service_data,index,onCloseModal) {
    return {
        type: types.UPDATE_SERVICE_REQUEST,
        service_id,service_data,index,onCloseModal
    };
}
export function updateServiceFailed() {
    return {
        type: types.UPDATE_SERVICE_FAILED
    };
}
export function updateServiceResponse(response,index) {
    return {
        type: types.UPDATE_SERVICE_RESPONSE,
        response,
        index
    };
}



// create Service Request
export function createServiceRequest(service_data,onCloseModal) {
    return {
        type: types.CREATE_SERVICE_REQUEST,
        service_data,onCloseModal
    };
}
export function createServiceFailed() {
    return {
        type: types.CREATE_SERVICE_FAILED
    };
}
export function createServiceResponse(response) {
    return {
        type: types.CREATE_SERVICE_RESPONSE,
        response
    };
}



// --------------- Get a service ------------
export function getService(service_id, firstTime) {
    return {
        type: types.GET_SERVICE,
        service_id,
        firstTime,
    };
}
export function getServiceResponse(response) {
    return {
        type: types.GET_SERVICE_RESPONSE,
        response
    };
}
export function getServiceFailed() {
    return {
        type: types.GET_SERVICE_FAILED
    };
}
// create Service Request
export function getIndustryServicesRequest(reset,keyword,page) {
    return {
        type: types.GET_INDUSTRY_SERVICES_REQUEST,
        keyword,
        reset,
        page
    };
}
export function getIndustryServicesFailed() {
    return {
        type: types.GET_INDUSTRY_SERVICES_FAILED
    };
}
export function getIndustryServicesResponse(response) {
    return {
        type: types.GET_INDUSTRY_SERVICES_RESPONSE,
        response
    };
}




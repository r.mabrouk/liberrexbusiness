
import * as types from './types';


// get All team Members

export function getAllTeamMembersRequest(resetServices) {
    return {
        type: types.GET_ALL_TEAM_MEMBER_REQUEST,
        resetServices
    };
}
export function getAllTeamMembersFailed() {
    return {
        type: types.GET_ALL_TEAM_MEMBER_FAILED
    };
}
export function getAllTeamMembersResponse(response) {
    return {
        type: types.GET_ALL_TEAM_MEMBER_RESPONSE,
        response
    };
}



// create team Members

export function createTeamMemberRequest(data) {
    return {
        type: types.CREATE_TEAM_MEMBER_REQUEST,
        data
    };
}
export function createMemberFailed() {
    return {
        type: types.CREATE_TEAM_MEMBER_FAILED
    };
}
export function createMemberResponse(response) {
    return {
        type: types.CREATE_TEAM_MEMBER_RESPONSE,
        response
    };
}


// update team Members

export function updateMemberRequest(id,data,index) {
    return {
        type: types.UPDATE_TEAM_MEMBER_REQUEST,
        data,
        id,
        index
    };
}
export function updateMemberFailed() {
    return {
        type: types.UPDATE_TEAM_MEMBER_FAILED
    };
}
export function updateMemberResponse(response,index) {
    return {
        type: types.UPDATE_TEAM_MEMBER_RESPONSE,
        response,
        index
    };
}



// delete team Members

export function deleteMemberRequest(id,index) {
    return {
        type: types.REMOVE_TEAM_MEMBER_REQUEST,
        id,
        index
    };
}
export function deleteMemberFailed() {
    return {
        type: types.REMOVE_TEAM_MEMBER_FAILED
    };
}
export function deleteMemberResponse(index) {
    return {
        type: types.REMOVE_TEAM_MEMBER_RESPONSE,
        index
    };
}




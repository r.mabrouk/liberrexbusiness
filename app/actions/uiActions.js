/*
 * Reducer actions related with login
 */
import * as types from './types';
import { setlanguage } from '../local';

export function setAppLanguage(lang) {
    setlanguage(lang)
    return {
        type: types.SET_APP_LANGUAGE,
        lang
    };
}
export function getAppLanguage() {
    return {
        type: types.GET_APP_LANGUAGE
    };
}

export function set_UserLoginData(login_data) {
    return {
        type: types.SET_USER_LOGIN_DATA,
        login_data
    };
}
export function setMenuState(menuState) {
    return {
        type: types.SET_MENU_STATE,
        menuState
    };
}





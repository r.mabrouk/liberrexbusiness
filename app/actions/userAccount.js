

// update user account

import * as types from './types';

export function updateUserAccountRequest(data) {
    return {
        type: types.UPDATE_USER_ACCOUNT_REQUEST,
        data
    };
}
export function updateUserAccountFailed() {
    return {
        type: types.UPDATE_USER_ACCOUNT_FAILED
    };
}
export function updateUserAccountResponse(response) {
    return {
        type: types.UPDATE_USER_ACCOUNT_RESPONCE,
        response
    };
}

// update password

export function updatePasswordRequest(data) {
    return {
        type: types.UPDATE_PASSWORD_REQUEST,
        data
    };
}
export function updatePasswordFailed() {
    return {
        type: types.UPDATE_PASSWORD_FAILED
    };
}
export function updatePasswordResponse(response) {
    return {
        type: types.UPDATE_PASSWORD_RESPONCE,
        response
    };
}



// update Business Info

export function updateBusinessInfoRequest(data) {
    return {
        type: types.UPDATE_BUSINESS_INFO_REQUEST,
        data
    };
}
export function updateBusinessInfoFailed() {
    return {
        type: types.UPDATE_BUSINESS_INFO_FAILED
    };
}
export function updateBusinessInfoResponse(response) {
    return {
        type: types.UPDATE_BUSINESS_INFO_RESPONCE,
        response
    };
}

// update Business location
export function updateBusinessLocationRequest(data) {
    return {
        type: types.UPDATE_BUSINESS_LOCATION_REQUEST,
        data
    };
}
export function updateBusinessLocationFailed() {
    return {
        type: types.UPDATE_BUSINESS_LOCATION_FAILED
    };
}
export function updateBusinessLocationResponse(response) {
    return {
        type: types.UPDATE_BUSINESS_LOCATION_RESPONCE,
        response
    };
}

// update Business working dayes
export function updateBusinessWorkingDayesRequest(data) {
    return {
        type: types.UPDATE_BUSINESS_WORKING_DAYES_REQUEST,
        data
    };
}
export function updateBusinessWorkingDayesFailed() {
    return {
        type: types.UPDATE_BUSINESS_WORKING_DAYES_FAILED
    };
}
export function updateBusinessWorkingDayesResponse(response) {
    return {
        type: types.UPDATE_BUSINESS_WORKING_DAYES_RESPONCE,
        response
    };
}





// update app settings
export function updateAppSettingsRequest(data) {
    return {
        type: types.UPDATE_APP_SETTING_REQUEST,
        data
    };
}
export function updateAppSettingsFailed() {
    return {
        type: types.UPDATE_APP_SETTING_FAILED
    };
}
export function updateAppSettingsResponse(response) {
    return {
        type: types.UPDATE_APP_SETTING_RESPONSE,
        response
    };
}






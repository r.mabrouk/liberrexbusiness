/* App config for apis
 */
const ApiConstants = {
    BASE_URL: 'https://api.liberrex.com/api/business/',
    IMAGE_BASE_URL: "https://cloud.liberrex.com",
    LOGIN: 'account/auth',
    LOG_OUT: "account/logout",
    RESET_PASSWORD: "account/password/reset",
    update_password: "account/password/reset/update",
    SIGNUP: "account/register",
    INDUSTRIES: 'industry',
    SESRCH_INDUSTR: "industry/filter/",
    COUNTRIES: "countries",
    SOCIAL_LOGIN: "account/auth/social",
    //customers
    CUSTOMER: 'customer?page=',
    CUSTOMER_BOOK: 'customer/book/',
    SEARCH_CUSTOMER: "customer/filter/",
    DELETE_CUSTOMER: '/delete',
    UPDATE_CUSTOMER: "/update",
    GET_A_CUSTOMERS: "customer/",
    CREATE_CUSTOMER: "customer/create",

    CUSTOMER_FEEDBACK: "feedback",
    FILTER_CUSTOMER_FEEDBACK: "feedback/filter",
    // services
    SERVICES: (page) => `service?page=${page}`,
    SERVICES_UPDATE: (service_id) => `service/${service_id}/update`,
    SERVICES_DELETE: (service_id) => `service/${service_id}/delete`,
    CREATE_SERVICES: `service/create`,
    FILTER_SERVICES: (keyword, page) => `service/filter/${keyword}?page=${page}`,
    CREATE_SERVICE: "service/create",
    GET_SERVICE: (serviceID) => `service/${serviceID}`,

    // get queue list
    GET_QUEUE_LIST: 'queue/summary?page=',

    INDUSTRIES_SERVICES: (page) => `helpers/service?page=${page}`,
    INDUSTRIES_SERVICES_FILTER: (page, keyword) => `helpers/service/filter/${keyword}?page=${page}`,


    // add queue
    ADD_QUEUE: 'queue/create',

    // notification
    NOTIFICATION: "notification",

    // queue waiting lsit
    GET_QUEUE_WAITING_LIST: (ID) => `queue/${ID}/waitinglist`,

    // queue pending lsit
    GET_QUEUE_PENDING_LIST: (ID) => `queue/${ID}/requests`,

    // approve queue request
    APPROVE_QUEUE_REQUEST: (queue_id, queue_request_id) => `queue/${queue_id}/request/${queue_request_id}/approve`,

    // decline queue request
    DECLINE_QUEUE_REQUEST: (queue_id, queue_request_id) => `queue/${queue_id}/request/${queue_request_id}/decline`,

    // add customer to queue
    ADD_CUSTOMER_TO_QUEUE: (queue_id) => `queue/${queue_id}/push`,

    // redirect queue
    REDIRECT_CUSTOMER: (currentQueueID, customer_id, selectedQueueID) => `queue/${currentQueueID}/redirect/${customer_id}/${selectedQueueID}`,

    // cancel customer in waiting
    CANCEL_CUSTOMER_IN_WAITING: (currentQueueID, customer_id) => `queue/${currentQueueID}/revoke/${customer_id}`,

    // call next customer
    CALL_NEXT_CUSTOMER: (queue_id) => `queue/${queue_id}/call`,

    // swap customer ranks
    SWAP_CUSTOMER_RANKS: (queue_id, customer_1, customer_2) => `queue/${queue_id}/swap/${customer_1}/${customer_2}`,

    // teamMembers
    teamMembers: "team",
    //MEMBERSHIP_PLANS
    GET_MEMBERSHIP_PLANS: "package",


    // rules
    RULES: () => `rules`,
    UPDATE_RULE: (rule_id) => `rules/${rule_id}/update`,
    DELETE_RULE: (rule_id) => `rules/${rule_id}/delete`,
    CREATE_RULE: () => `rules/create`,
    UPDATE_USER_ACCOUNT: "account/update",
    UPDATE_PASSWORD: "account/password/update",

    // home
    GET_HOME_STATS: "statistic/home",
    FILTER_HOME_REPORTS: `statistic/home/filter`,
    //payment
    ADD_PAYMENT_CARD: `billing/create`,
    GET_PAYMENT_CARDS: "billing",
    DELETE_PAYMENT_CARD: (billing_method_id) => `billing/${billing_method_id}/delete`,
    UPDATE_ACCOUNT_MEMBERSHIP: "membership/update",
    // update business

    UPDATE_BUSINESS_INFO: 'update',
    UPDATE_BUSINESS_WORKING_DAYES: 'workingdays/update',
    UPDATE_BUSINESS_LOCATION: 'location/update',
    UPDATE_APP_SETTINGS: "account/setting/update",
    // booking
    GET_ALL_BOOKING: (page) => `booking?page=${page}`,
    CREATE_BOOKING: `booking/create`,
    UPDATE_BOOKING: (id) => `booking/${id}/update`,
    DELETE_BOOKING: (id) => `booking/${id}/delete`,
    FILTER_BOOKING: (page, filter) => `booking/filter/${filter}?page=${page}`,
    GET_BOOKING_REQUEST: (page) => `booking/request?page=${page}`,
    APPROVE_BOOKING_REQUEST: (id) => `booking/request/${id}/approve`,
    DELETE_BOOKING_REQUEST: (id) => `booking/request/${id}/decline`,

    REPORT_LIST: `statistic`,
    FILTER_REPORT: `statistic/filter`,

    UPDATE_QUEUE: (queue_id) => `queue/${queue_id}/update`,
    GET_QUEUE: (queue_id) => `queue/${queue_id}`,
    DELETE_QUEUE: (queue_id) => `queue/${queue_id}/delete`,

    CHECK_EMAIL:(email)=>`account/checkemail/${email}`
};

export default ApiConstants;

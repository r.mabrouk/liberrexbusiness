// General api to access data
import ApiConstants from './ApiConstants';
import { getlanguage } from '../local';
export default function api(path, params, method, token, formdata) {
    console.log(getlanguage() , "    optionsoxxxxxxptionsoptionsoptions")

    let options;
    options = {
        headers: {
            'X-Timezone-Offset':new Date().getTimezoneOffset(),
            'Content-Language': getlanguage(),
            'Content-Type': formdata ? 'multipart/form-data' :
                'application/json',
            ...(token && { "Authorization": "Bearer " + token })        },
        method: method,
        ...(params && { body: formdata ? params : JSON.stringify(params) })
    };
    console.log(options, "optionsoptionsoptionsoptions")
    return fetch(ApiConstants.BASE_URL + path, options)
        .then(resp => resp.json())
        .then(json => json)
        .catch(error => error);
}


let signup = {
    user: {
        username: "ddd",
        email: "raaa@sss.com",
        password: "12333",
    },
    business: {
        legalname: "dddd",
        logo_or_photo: "base64",
        industry_id: 1,
        address: "string",
        address2: "sssss",
        country_id: 1,
        city_id: 1,
        location: {
            lat: "10.123213",
            lng: "5.31313123"
        }

    }
}
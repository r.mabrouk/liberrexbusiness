import Api from '..';
import ApiConstants from '../ApiConstants';

//get booking  list
export function getAllBookingList({ token, page,keyword }) {
    let url=""
    if (keyword){
        url= ApiConstants.FILTER_BOOKING(page,keyword)

    }
    else
    url= ApiConstants.GET_ALL_BOOKING(page)
    

    return Api(
        url,
        null,
        'GET',
        token
    );
}

//create booking request
export function createBooking({ data, token }) {
    return Api(
        ApiConstants.CREATE_BOOKING,
        data,
        'PUT',
        token
    );
}


//update booking request
export function updateBooking({ data, token, id }) {
    return Api(
        ApiConstants.UPDATE_BOOKING(id),
        data,
        'POST',
        token
    );
}


//delete booking request
export function deleteBooking({ token, id }) {
    return Api(
        ApiConstants.DELETE_BOOKING(id),
        null,
        'DELETE',
        token
    );
}


//filter booking request
export function filterBookingList({ token, page, filter }) {
    return Api(
        ApiConstants.FILTER_BOOKING(page, filter),
        null,
        'GET',
        token
    );
}




//get booking request 
export function getBookingRequestList({ token,page }) {
    return Api(
        ApiConstants.GET_BOOKING_REQUEST(page),
        null,
        'GET',
        token
    );
}


//approve booking request 
export function approveBookingRequest({ token, id }) {
    return Api(
        ApiConstants.APPROVE_BOOKING_REQUEST(id),
        null,
        'POST',
        token
    );
}


//delete booking request 
export function deleteBookingRequest({ token, id }) {
    return Api(
        ApiConstants.DELETE_BOOKING_REQUEST(id),
        null,
        'DELETE',
        token
    );
}

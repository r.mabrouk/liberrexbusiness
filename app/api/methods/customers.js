import Api from '..';
import ApiConstants from '../ApiConstants';


export function getAllCustomerBooksSearch({ token, page, keyword, book_id }) {
    return Api(
        ApiConstants.CUSTOMER_BOOK + book_id + '/filter/' + keyword + '?page=' + page,
        null,
        'GET',
        token
    );
}

export function getAllCustomerBooks({ token, page, book_id, keyword }) {
    let url = null
    if (keyword)
        url = ApiConstants.CUSTOMER_BOOK + book_id + '/filter/' + keyword + '?page=' + page
    else
        url = ApiConstants.CUSTOMER_BOOK + book_id + '?page=' + page

    return Api(
        url,
        null,
        'GET',
        token
    );
}


export function getAllCustomersSearch({ token, page, keyword }) {
    return Api(
        ApiConstants.SEARCH_CUSTOMER + keyword + "?page=" + page,
        null,
        'GET',
        token
    );
}

export function getAllCustomers({ token, page ,keyword}) {
    let url=null
    if (keyword)
        url=ApiConstants.SEARCH_CUSTOMER + keyword + "?page=" + page
        else
        url=ApiConstants.CUSTOMER + page
    return Api(
        url,
        null,
        'GET',
        token
    );
}

export function getACustomers({ customer_id }) {
    return Api(
        ApiConstants.CUSTOMER + customer_id,
        null,
        'GET',
        null
    );
}


export function deleteCustomer({ customer_id, token }) {
    return Api(
        ApiConstants.GET_A_CUSTOMERS + customer_id + '/delete',
        null,
        'DELETE',
        token
    );
}

export function updateCustomer({ customer_data, token }) {
    return Api(
        ApiConstants.GET_A_CUSTOMERS + customer_data.id + "/update",
        customer_data,
        'POST',
        token
    );
}

export function createCustomer({ customet_data, token }) {
    return Api(
        ApiConstants.CREATE_CUSTOMER,
        customet_data,
        'PUT',
        token
    );
}

export function removeCustomerFromBook({ customer_id, book_id, token }) {
    return Api(
        ApiConstants.CUSTOMER_BOOK + book_id + "/remove/" + customer_id,
        null,
        'DELETE',
        token
    );
}


export function addCustomerToBooks({ customer_id, book_id, token }) {
    return Api(
        ApiConstants.CUSTOMER_BOOK + book_id + "/add/" + customer_id,
        null,
        'PUT',
        token
    );
}


export function getAllCustomerFeedback({ token }) {
    return Api(
        ApiConstants.CUSTOMER_FEEDBACK,
        null,
        'GET',
        token
    );
}
export function filterCustomerFeedback({ filter, token }) {
    return Api(
        ApiConstants.FILTER_CUSTOMER_FEEDBACK,
        filter,
        'POST',
        token
    );
}






import Api from '..';

export function GetPlaces({ place_name }) {

    let baseUrl = 'https://maps.googleapis.com/maps/api/place/textsearch/';
    baseUrl += `json?query=${place_name}`;
    baseUrl += `&key=AIzaSyCfsUP_8yM1dRP_PAjzcnJT7Zmrz3LesVk`;
    return fetch(baseUrl, {
        method: 'GET'
    })
        .then(resp => resp.json())
        .then(json => json)
        .catch(error => error);
}
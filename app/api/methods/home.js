import Api from '..';
import ApiConstants from '../ApiConstants';


export function getHomeStats({ token }) {
    return Api(
        ApiConstants.GET_HOME_STATS,
        null,
        'GET',
        token
    );
}


export function filterHomeReports({ token,keyword }) {
    return Api(
        ApiConstants.FILTER_HOME_REPORTS,
        keyword,
        'POST',
        token
    );
}

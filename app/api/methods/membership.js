import Api from '..';
import ApiConstants from '../ApiConstants';


export function getMembershipPlans({ token }) {
    return Api(
        ApiConstants.GET_MEMBERSHIP_PLANS,
        null,
        'GET',
        token
    );
}

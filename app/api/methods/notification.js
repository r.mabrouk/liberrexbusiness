import Api from '..';
import ApiConstants from '../ApiConstants';


export function getNotificationList({ token }) {
    return Api(
        ApiConstants.NOTIFICATION,
        null,
        'GET',
        token
    );
}

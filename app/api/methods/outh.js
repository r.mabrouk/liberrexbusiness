import Api from '..';
import ApiConstants from '../ApiConstants';

export function loginUser({ user_data }) {
    return Api(
        ApiConstants.LOGIN,
        user_data,
        'POST',
        null
    );
}

export function socialLogin({ user_data }) {
    return Api(
        ApiConstants.SOCIAL_LOGIN,
        user_data,
        'POST',
        null
    );
}


export function signUp({ user_data, social }) {
    let url = ApiConstants.SIGNUP + (social ? social : "")
    return Api(
        url,
        user_data,
        'POST',
        null,
        "formdata"
    );
}

export function reset_password({ email }) {
    return Api(
        ApiConstants.RESET_PASSWORD,
        email,
        'POST',
        null
    );
}
export function sendNewPassword({ data }) {
    return Api(
        ApiConstants.update_password,
        data,
        'POST',
        null
    );
}

export function logout({ token }) {
    return Api(
        ApiConstants.LOG_OUT,
        null,
        'POST',
        token
    );
}
export function getAllIndustries() {
    return Api(
        ApiConstants.INDUSTRIES,
        null,
        'GET',
        null
    );
}

export function searchIndustr({ keyword }) {
    return Api(
        ApiConstants.SESRCH_INDUSTR + keyword,
        null,
        'GET',
        null
    );
}


export function getCountries() {
    return Api(
        ApiConstants.COUNTRIES,
        null,
        'GET',
        null
    );
}

export function checkEmail({email}) {
    return Api(
        ApiConstants.CHECK_EMAIL(email),
        null,
        'GET',
        null
    );
}


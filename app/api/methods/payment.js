import Api from '..';
import ApiConstants from '../ApiConstants';


export function updateAccountMembership({ token, data }) {
    return Api(
        ApiConstants.UPDATE_ACCOUNT_MEMBERSHIP,
        data,
        'POST',
        token
    );
}


export function getPaymentCards({ token }) {
    return Api(
        ApiConstants.GET_PAYMENT_CARDS,
        null,
        'GET',
        token
    );
}



export function deletePaymentCard({ token, billing_method_id }) {
    return Api(
        ApiConstants.DELETE_PAYMENT_CARD(billing_method_id),
        null,
        'DELETE',
        token
    );
}


export function AddPaymentCard({ token, data }) {

    return Api(
        ApiConstants.ADD_PAYMENT_CARD,
        data,
        'PUT',
        token
    );
}


import Api from '..';
import ApiConstants from '../ApiConstants';

export  function getQueueListMethod({token,page}) {
    return Api(
        ApiConstants.GET_QUEUE_LIST+page,
        null,
        'GET',
        token
    );
}


export  function addQueueMethod({token,body}) {
    return Api(
        ApiConstants.ADD_QUEUE,
        body,
        'PUT',
        token
    );
}


export  function getQueueWaitingListMethod({token,QueueID}) {
    return Api(
        ApiConstants.GET_QUEUE_WAITING_LIST(QueueID),
        null,
        'GET',
        token
    );
}

export  function getQueuePendingListMethod({token,QueueID}) {
    return Api(
        ApiConstants.GET_QUEUE_PENDING_LIST(QueueID),
        null,
        'GET',
        token
    );
}

export  function approveQueueRequestMethod({token,queue_id, queue_request_id}) {
    return Api(
        ApiConstants.APPROVE_QUEUE_REQUEST(queue_id, queue_request_id),
        null,
        'POST',
        token
    );
}

export  function declineQueueRequestMethod({token,queue_id, queue_request_id}) {
    return Api(
        ApiConstants.DECLINE_QUEUE_REQUEST(queue_id, queue_request_id),
        null,
        'POST',
        token
    );
}


export  function addCustomerToqueueMethod({token,queue_id, body}) {
    return Api(
        ApiConstants.ADD_CUSTOMER_TO_QUEUE(queue_id),
        body,
        'PUT',
        token
    );
}


export  function redirectQueueMethod({token, currentQueueID, customer_id, selectedQueueID}) {
    return Api(
        ApiConstants.REDIRECT_CUSTOMER(currentQueueID,customer_id,selectedQueueID),
        null,
        'POST',
        token
    );
}

export  function cancelCustomerInWaitingMethod({token, currentQueueID, customer_id}) {
    return Api(
        ApiConstants.CANCEL_CUSTOMER_IN_WAITING(currentQueueID,customer_id),
        null,
        'DELETE',
        token
    );
}


export  function callNextCustomerMethod({token, queue_id,}) {
    return Api(
        ApiConstants.CALL_NEXT_CUSTOMER(queue_id),
        null,
        'POST',
        token
    );
}


export  function swapCustomerRanksMethod({token, queue_id,customer_1, customer_2}) {
    return Api(
        ApiConstants.SWAP_CUSTOMER_RANKS(queue_id,customer_1, customer_2),
        null,
        'POST',
        token
    );
}

export  function getQueue({token, queue_id}) {
    return Api(
        ApiConstants.GET_QUEUE(queue_id),
        null,
        'GET',
        token
    );
}

export  function deleteQueue({token, queue_id}) {
    return Api(
        ApiConstants.DELETE_QUEUE(queue_id),
        null,
        'DELETE',
        token
    );
}
export  function updateQueue({token, queue_id,newQueue}) {
    return Api(
        ApiConstants.UPDATE_QUEUE(queue_id),
        newQueue,
        'POST',
        token
    );
}

import Api from '..';
import ApiConstants from '../ApiConstants';

export  function getReportList({token}) {
    return Api(
        ApiConstants.REPORT_LIST,
        null,
        'GET',
        token
    );
}

export  function filterReport({token,keywords}) {
    return Api(
        ApiConstants.FILTER_REPORT,
        keywords,
        'POST',
        token
    );
}
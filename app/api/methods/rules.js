import Api from '..';
import ApiConstants from '../ApiConstants';

export function getRulesList({ token, page }) {
    return Api(
        ApiConstants.RULES(),
        null,
        'GET',
        token
    );
}

export function updateRule({ token, rule_id, rule_data }) {
    return Api(
        ApiConstants.UPDATE_RULE(rule_id),
        rule_data,
        'POST',
        token
    );
}

export function createRule({ token, rule_data }) {
    return Api(
        ApiConstants.CREATE_RULE(),
        rule_data,
        'PUT',
        token
    );
}


export function deleteRule({ token, rule_id }) {
    return Api(
        ApiConstants.DELETE_RULE(rule_id),
        null,
        'DELETE',
        token
    );
}

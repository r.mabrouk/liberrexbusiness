import Api from '..';
import ApiConstants from '../ApiConstants';


export function getAllServices({ token, page, keyword }) {
    if (keyword)
    return Api(ApiConstants.FILTER_SERVICES(keyword,page), null, 'GET', token);
    else
    return Api(ApiConstants.SERVICES(page), null, 'GET', token);

}


export function createService({ token, service_data }) {
    return Api(
        ApiConstants.CREATE_SERVICES,
        service_data,
        'PUT',
        token
    );
}

export function updateService({ token, service_id, service_data }) {
    console.log("updateServiceResponseupdateService", token, service_id, service_data)

    return Api(
        ApiConstants.SERVICES_UPDATE(service_id),
        service_data,
        'POST',
        token
    );
}


export function deleteService({ token, service_id }) {
    return Api(
        ApiConstants.SERVICES_DELETE(service_id),
        null,
        'DELETE',
        token
    );
}

export function getServiceMethod({ token, service_id }) {
    return Api(
        ApiConstants.GET_SERVICE(service_id),
    );
}

// get Industry Services
export function getIndustryServices({ token ,keyword,page}) {
    url=""
    if (!keyword) 
    url=ApiConstants.INDUSTRIES_SERVICES(page)
    else
    url=ApiConstants.INDUSTRIES_SERVICES_FILTER(page,keyword)
    return Api(
        url,
        null,
        'GET',
        token
    );
}


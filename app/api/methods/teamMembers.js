import Api from '..';
import ApiConstants from '../ApiConstants';


export function getAllTeamMembers({ token }) {
    return Api(
        ApiConstants.teamMembers,
        null,
        'GET',
        token
    );
}


export function deleteTeamMembers({ id,token }) {
    return Api(
        ApiConstants.teamMembers+'/'+id+"/delete",
        null,
        'DELETE',
        token
    );
}

export function updateTeamMembers({ id,data,token }) {
    return Api(
        ApiConstants.teamMembers+'/'+id+"/update",
        data,
        'POST',
        token
    );
}

export function createTeamMembers({ data,token }) {
    return Api(
        ApiConstants.teamMembers+'/create/',
        data,
        'PUT',
        token
    );
}


// update user account



import Api from '..';
import ApiConstants from '../ApiConstants';


export function updateAppSettings({ data,token }) {
    return Api(
        ApiConstants.UPDATE_APP_SETTINGS,
        data,
        'POST',
        token
    );
}

export function updateUserAccount({ data,token }) {
    return Api(
        ApiConstants.UPDATE_USER_ACCOUNT,
        data,
        'POST',
        token,
        'formData'
    );
}
export function updatePassword({ data,token }) {
    return Api(
        ApiConstants.UPDATE_PASSWORD,
        data,
        'POST',
        token
    );
}




export function updateBusinessInfo({ data,token }) {
    return Api(
        ApiConstants.UPDATE_BUSINESS_INFO,
        data,
        'POST',
        token,
        'formData'
    );
}



export function updateBusinessWorkingDayes({ data,token }) {
    return Api(
        ApiConstants.UPDATE_BUSINESS_WORKING_DAYES,
        data,
        'POST',
        token
    );
    
}

export function updateBusinessLocation({ data,token }) {
    return Api(
        ApiConstants.UPDATE_BUSINESS_LOCATION,
        data,
        'POST',
        token
    );
}



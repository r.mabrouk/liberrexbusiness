


import React, { Component } from 'react';
import {
    View,

} from 'react-native';
import { ColorsList } from '../PropsStyles/colors'
import { getWidth, getHeight, Width } from '../utils/dimensions';
import StarRating from 'react-native-star-rating';
import { Card } from '../card/card';
import { HeaderSection } from '../headerSection/headerSection';

import {
    LineChart,
    BarChart
} from 'react-native-chart-kit'
import { _Text } from '../text/text';
import { _string } from '../../local';

type Props = {
    type: "BarChar",
    zIndex: Number,
    title: String,
    labels: Array,
    data: Array
}




const Charts = (props: Props) => {
    let { type,
        zIndex,
        title,
        labels,
        data
    } = props
    return (
        <Card style={{ width: Width, height: getHeight(195), backgroundColor: 'white', alignItems: 'center', zIndex: zIndex }} >

            {/* //line chart */}
            <View
                style={{ width: Width, alignItems: 'center' }}>
                <View
                    style={{
                       
                        width: '92%',
                        flexDirection: 'row',
                        justifyContent: 'space-between'

                    }}>

                    {type == 'linechart' &&
                    <View style={{
                        marginLeft:-getWidth(15),width:'100%'
                    }}>

                    <LineChart
                    fromZero
                        onDataPointClick={() => {
                        }}
                        data={{
                            labels: labels,
                            datasets: [{
                                data: data
                            }]
                        }}
                        withShadow
                        
                        width={getWidth(345)} // from react-native
                        height={getHeight(175)}
                        // yAxisLabel={'$'}
                        chartConfig={{
                            strokeWidth: 1,
                            backgroundColor: 'green',
                            backgroundGradientFrom: 'white',
                            backgroundGradientTo: 'white',
                            decimalPlaces: 0, // optional, defaults to 2dp
                            color: (opacity = 1) => ColorsList.blue,
                        }}
                        bezier
                    />
                    </View>
                    }
                    {type == 'piechart' && <BarChart
                    fromZero
                        onDataPointClick={() => {
                        }}
                        data={{
                            labels: labels,
                            datasets: [{
                                data: data
                            }]
                        }}
                        withShadow
                        width={getWidth(345)} // from react-native
                        height={getHeight(175)}
                        // yAxisLabel={'$'}
                        chartConfig={{
                            strokeWidth: 1,
                            backgroundColor: 'gray',
                            backgroundGradientFrom: 'white',
                            backgroundGradientTo: 'white',
                            decimalPlaces: 0, // optional, defaults to 2dp
                            color: (opacity = 1) => ColorsList.blue,
                        }}
                        bezier
                    />}
                    {type == 'star' && <Card style={{ width: getWidth(345), justifyContent: 'space-between' }}>
                        {labels.map((item, index) => {
                            return <CustomerSatisfaction value={data[index]} rating={item} />
                        })}
                    </Card>}

                </View>
            </View>
        </Card>
    )
}
const CustomerSatisfaction = ({ rating, value }) => {
    return (
        <Card style={{ flexDirection: 'row', width: getWidth(345), paddingVertical: getHeight(5), justifyContent: 'space-between'}}>
            <StarRating
                disabled={true}
                maxStars={5}
                rating={rating}
                starSize={getWidth(19)}
                emptyStar={'star'}
                fullStar={'star'}
                iconSet={'FontAwesome'}
                fullStarColor='#FBBC05'
                emptyStarColor={ColorsList.rgbaOrange(.2)}
                containerStyle={{ width: getWidth(120)}}
                starStyle={{}}
                selectedStar={{}}
            />
            <_Text style={{ fontSize: 'size15', fontFamily: 'bold', paddingHorizontal: getWidth(10), color: 'darkGray1' }}>{value}  <_Text style={{ fontSize: 'small', fontFamily: 'bold', color: 'lightGray' }}>{_string("report.customers")}</_Text></_Text>
        </Card>
    )
}
export default Charts
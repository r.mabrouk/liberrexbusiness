import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text, ScrollView,
    TextInput
} from 'react-native';
import { _Text } from '../../components/text/text';
import { Card } from '../../components/card/card';
import { Icons } from '../../config';
import { ColorsList } from '../PropsStyles';
import { getWidth, getHeight, Width } from '../utils/dimensions';
import { RFValue } from 'react-native-responsive-fontsize'
type KeyboardType = "default" | "email-address" | "numeric" | "phone-pad";
type KeyboardTypeIOS =
    | "ascii-capable"
    | "numbers-and-punctuation"
    | "url"
    | "number-pad"
    | "name-phone-pad"
    | "decimal-pad"
    | "twitter"
    | "web-search";
type KeyboardTypeAndroid = "visible-password";
type KeyboardTypeOptions = KeyboardType | KeyboardTypeAndroid | KeyboardTypeIOS;

export type ReturnKeyType = 'done' | 'go' | 'next' | 'search' | 'send';
export type ReturnKeyTypeAndroid = 'none' | 'previous';
export type ReturnKeyTypeIOS = 'default' | 'google' | 'join' | 'route' | 'yahoo' | 'emergency-call';
export type ReturnKeyTypeOptions = ReturnKeyType | ReturnKeyTypeAndroid | ReturnKeyTypeIOS;
type CustomTextInputProps = {
    value: String,
    isNotTitle: Boolean,
    onChangeText: Function,
    KeyboardType: KeyboardTypeOptions,
    marginTop: Number,
    password: Boolean,
    Width: Number,
    returnKeyType: ReturnKeyTypeOptions,
    maxLength: Number,
    onSubmitEditing:Function

}
class CustomTextInput extends Component<CustomTextInputProps>  {
    focus=()=>{
        this.input.focus()
    }

    render(){
        return (
            <Card style={{ width: this.props.Width || Width, justifyContent: 'center', alignItems: 'center', marginTop: this.props.marginTop, paddingBottom: getHeight(15) }}>
                {/*                
                    {!props.isNotTitle && <Card style={{ flexDirection: 'row', width:props.Width?props.Width-getWidth(15): getWidth(345) }}>
                        <Icons name={props.headerIcon} width={getWidth(16)} height={getWidth(18)} />
                        <_Text style={{ fontSize: 'medium15Px', fontFamily: 'bold', color: 'darkGray1', marginLeft: getWidth(5), marginBottom: 5 }}>{props.headerText}</_Text>
                    </Card>} */}
    
    
                {!this.props.isNotTitle && <Card style={{ flexDirection: 'row', width: this.props.Width ? this.props.Width : getWidth(345), alignItems: 'center', paddingHorizontal: this.props.padding ?this.props.padding : 0 }}>
                    <Icons name={this.props.headerIcon} width={getWidth(18)} height={getWidth(18)} />
                    <_Text style={{ fontSize: 'medium15Px', fontFamily: 'bold', color: 'darkGray1', marginLeft: getWidth(5) }}>{this.props.headerText}</_Text>
                </Card>}
    
                <Card style={styles.inputContainer(this.props.Width ? this.props.Width - getWidth(30) : getWidth(345))}>
                    <TextInput
                    onSubmitEditing={this.props.onSubmitEditing}
                    ref={(ref)=>this.input=ref}
                    maxLength={this.props.maxLength}
                        returnKeyType={this.props.returnKeyType}
                        editable={!this.props.showonly}
                        keyboardType={this.props.KeyboardType}
                        placeholder={this.props.placeholder}
                        style={{ width: '90%', fontSize: RFValue(13), color: ColorsList.darkGray1 }}
                        value={this.props.value}
                        secureTextEntry={this.props.password}
                        onChangeText={this.props.onChangeText}
                    />
                    <Icons name={this.props.leftIcon} height={getWidth(12)} width={getWidth(12)} />
                </Card>
            </Card>
        );
    }
   
}
const styles = {
    inputContainer: (width) => (
        {
            flexDirection: 'row',
            width: width,
            marginTop: getHeight(10),
            backgroundColor: 'white',
            shadow: 6, height: getHeight(41),
            alignItems: 'center',
            borderRadius: 3,
            zIndex: 30,
            justifyContent: 'space-between',
            paddingHorizontal: getWidth(13),
            overflow: 'visible'
        }
    )

}

export default CustomTextInput



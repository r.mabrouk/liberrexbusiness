import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import PropTypes from 'prop-types';
import { images, metrics, Icons } from '../../config';
import { Button } from '../../components/button/button';
import { RequestsStatusItem } from '../../components/requestsStatusItem/requestsStatusItem';
import { Header } from '../../components/header/header';
import { Card } from '../../components/card/card';
import { _Text } from '../../components/text/text';
import { getWidth, getHeight } from '../../components/utils/dimensions';
import Swiper from 'react-native-swiper'
import HomeHeader from '../../components/HomeHeader/HomeHeader';
import { SvgIcon } from '../../config/svgIcon';
import styles from './styles'

export default class CustomerCardInfo extends Component {
    
    render() {
        let {
            usericon,
            name,
            email,
            leftIcon
        } = this.props
        return (
            <View style={{width:'100%',marginTop:15}}>

                <Card style={styles.cardInfo}>
                    <Card style={styles.infoContainer}>
                        <Card style={{ flexDirection: 'row', }}>
                            <Icons name={usericon}  width={getWidth(42)} height={getWidth(42)} />
                            <Card style={{ flexDirection: 'column', marginLeft: 13 }}>
                                <_Text style={styles.name}>{name}</_Text>

                                <Card style={styles.emailContainer}>
                                    <Icons name="email" width={getWidth(15)} height={getWidth(15)} />
                                    <_Text style={{ marginLeft: 5 }}>{email}</_Text>
                                </Card>

                            </Card>
                        </Card>
                        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Icons name={leftIcon} width={getWidth(18)} height={getWidth(18)} />
                        </TouchableOpacity>

                    </Card>
                </Card>

                <Card style={styles.selected}>
                    <SvgIcon name='doneGreen' width={getWidth(18)} height={getWidth(18)} />
                    <_Text style={styles.selectedText}>Select This Customer</_Text>
                </Card>
            </View>
        );
    }
}


import { images, metrics, Icons } from '../../config';
import { getWidth, getHeight } from '../../components/utils/dimensions';

const styles = {
    cardInfo: {
        width: '100%',
        height: getHeight(72),
        shadow: 3.5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    infoContainer:
        { flexDirection: 'row', width: '90%', justifyContent: 'space-between' },
    name:
        { fontSize: 'medium15Px', color: 'black', fontWeight: 'bold', marginBottom: 7, },
    emailContainer:
    {
        flexDirection: 'row', justifyContent: 'center', alignItems: 'center'
    },
    selected: {

        width: '100%',
        height: getHeight(44),
        shadow: 3,
        backgroundColor: 'white',
       
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'

    },
    selectedText:{
        fontSize: 'small13Px', fontWeight: 'bold', color: 'black', marginLeft: 5,
    }
}
export default styles
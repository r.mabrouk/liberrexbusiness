import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { getWidth, getHeight } from '../utils/dimensions';
import { Card } from '../card/card';
import { Icons } from '../../config';
import { _Text } from '../text/text';
import { Button } from '../button/button';


type Props = {
    onEditCustomer: Function,
    onPress: Function
}
export default class CustomerCardWithGreenButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        let { name, email, onPress, onEditCustomer,firstItem } = this.props
        return (
            <Button
                style={{
                    width: getWidth(375), height: getHeight(58), backgroundColor: 'white',
                    alignItems: 'center', justifyContent: 'space-between', shadow: 6,
                    flexDirection: 'row',
                    marginTop:firstItem?0: getHeight(15)
                }}
                onPress={onPress} >
                {/* <Card style={styles.Done}> */}
                {/* <Icons name='doneGreen' width={getWidth(18)} height={getWidth(18)} /> */}
                {/* </Card> */}
                <Card style={{ flexDirection: 'row', alignItems: 'center', width: getWidth(375 - 96), paddingLeft: getWidth(15) }}>
                    <Icons name={'user'} width={getWidth(28)} height={getWidth(28)} />
                    <Card style={{  paddingHorizontal: getWidth(10) }}>
                        <_Text style={styles.username}>{name}</_Text>
                        <_Text style={styles.email}>({email})</_Text>
                    </Card>
                </Card>
  
            </Button>

        );
    }
}


const styles = {
    Done: {
        height: getHeight(58),
        width: getWidth(48),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        shadow: 6
    },
    username: { fontSize: 'size15', fontFamily: 'bold', color: 'darkGray1' },
    email: { fontSize: 'size12', marginTop: getHeight(5),fontFamily:'medium',color:'lightGray1' },

}
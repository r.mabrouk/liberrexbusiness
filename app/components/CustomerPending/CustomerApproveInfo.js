import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import PropTypes from 'prop-types';
import { images, metrics, Icons } from '../../config';

import { Card } from '../card/card';
import { _Text } from '../text/text';
import moment from 'moment'

import styles from './styles'
import { getHeight, Width, getWidth } from '../utils/dimensions';


class CustomerApproveInfo extends Component {

    render() {
        let {
            usericon,
            name,
            leftIcon,
            time,
            service
        } = this.props
        return (
            <TouchableOpacity
            activeOpacity={.8}
            onPress={this.props.onPress}
            >
            <Card style={styles.cardInfo(getHeight(15),this.props.zIndex)}>
               

                <Card style={{ flexDirection: 'row', marginTop: getHeight(12), width: getWidth(330) }}>
                    <Icons name={'user'} width={getWidth(42)} height={getWidth(42)} />
                    <Card style={{ flexDirection: 'column', marginLeft: getWidth(10) }}>
                        <_Text style={styles.name}>{name}</_Text>
                        <Card style={styles.subContainer}>
                            <Card style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Icons name='grid' width={getWidth(15)} height={getWidth(15)} />
                                <_Text style={{ marginLeft: getWidth(5), fontSize: 'size12', fontFamily: 'medium', color: 'lightGray1' }}>{service != null ? service.slice(0, 10) + "..." : "No serivers"}</_Text>
                            </Card>
                            <Card style={{ flexDirection: 'row', marginLeft: getWidth(10), alignItems: 'center' }}>
                                <Icons name='clock' width={getWidth(15)} height={getWidth(15)} />
                                <_Text style={{ marginLeft: getWidth(5), fontSize: 'size12', fontFamily: 'medium', color: 'lightGray1' }}>{this.getFormatDate(time)}</_Text>
                            </Card>
                        </Card>

                    </Card>
                </Card>

                <TouchableOpacity
                    onPress={this.props.onPress}
                    style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                    <Icons name={'more'} width={getWidth(18)} height={getWidth(18)} color={'lightGray'} />
                </TouchableOpacity>

            </Card>
            </TouchableOpacity>


        );
    }
    getFormatDate=(date)=>{
        return moment(date).format("DD MMM,YYYY HH:mm")
    }
}

export default CustomerApproveInfo
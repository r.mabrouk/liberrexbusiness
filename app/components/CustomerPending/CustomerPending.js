import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import PropTypes from 'prop-types';
import { images, metrics, Icons } from '../../config';
import { Button } from '../../components/button/button';
import { RequestsStatusItem } from '../../components/requestsStatusItem/requestsStatusItem';
import { Header } from '../../components/header/header';
import { Card } from '../../components/card/card';
import { _Text } from '../../components/text/text';
import { getWidth, getHeight, Width } from '../../components/utils/dimensions';
import moment from 'moment'

import styles from './styles'


class CustomerPending extends Component {

    render() {
        let {
            usericon,
            name,
            withButtons,
            leftIcon,
            time,
            service,
            onApprovedBookingRequest,
            onDeclineBookingRequest,
            loadingApproveBookingRequest,
            loadingDeleteBookingRequest
        } = this.props
        return (
            <Card style={{ width: Width, justifyContent: 'center', alignItems: 'center', marginBottom: getHeight(15), shadow: 6, backgroundColor: 'white' }}>

                <Card style={styles.cardInfo(0)}>
                    <Card style={{ flexDirection: 'row', marginTop: getHeight(12), width: getWidth(330) }}>
                        <Icons name={'user'}  width={getWidth(42)} height={getWidth(42)} />
                        <Card style={{ flexDirection: 'column', marginLeft: getWidth(10) }}>
                            <_Text style={styles.name}>{name}</_Text>
                            <Card style={styles.subContainer}>
                                <Card style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Icons name='grid' width={getWidth(15)} height={getWidth(15)} />
                                    <_Text style={{ marginLeft: getWidth(5), fontSize: 'size12', fontFamily: 'medium', color: 'lightGray1' }}>{service != null ? service.slice(0, 10) + "..." : "No serivers"}</_Text>
                                </Card>
                                <Card style={{ flexDirection: 'row', marginLeft: getWidth(10), alignItems: 'center' }}>
                                    <Icons name='clock' width={getWidth(15)} height={getWidth(15)} />
                                    <_Text style={{ marginLeft: getWidth(5), fontSize: 'size12', fontFamily: 'medium', color: 'lightGray1' }}>{moment(time).format("DD MMM,YYYY HH:mm")}</_Text>
                                </Card>
                            </Card>

                        </Card>
                    </Card>

                    <TouchableOpacity
                        onPress={this.props.onPress}
                        style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Icons name={'more'} width={getWidth(18)} height={getWidth(18)} color={'lightGray'} />
                    </TouchableOpacity>
                </Card>


                <Card style={styles.crads}>

                    <Button
                        loaderColor={"darkGray1"}
                        isLoading={loadingApproveBookingRequest}
                        onPress={onApprovedBookingRequest} style={styles.Button1}>
                        <Icons name={'doneGreen'} width={getWidth(18)} height={getWidth(18)} />
                        <_Text style={styles.optionsText}>Approve</_Text>
                    </Button>
                    <Button
                        isLoading={loadingDeleteBookingRequest}
                        loaderColor={"darkGray1"}
                        onPress={onDeclineBookingRequest} style={styles.Button2}>
                        <Icons name={'cancel'} width={getWidth(18)} height={getWidth(18)} />
                        <_Text style={styles.optionsText}>Decline</_Text>
                    </Button>

                </Card>
            </Card>
        );
    }
}

export default CustomerPending
import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import PropTypes from 'prop-types';
import { images, metrics, Icons } from '../../config';

import { Card } from '../card/card';
import { _Text } from '../text/text';


import styles from './styles'
import { getWidth } from '../utils/dimensions';


class CustomerPendingInfo extends Component {

    render() {
        let {
            usericon,
            name,
            leftIcon,
            time,
            service
        } = this.props
        return (
            <Card style={{ width: '100%',justifyContent:'center',alignItems:'center',height:76,shadow:6}}>

                <Card style={styles.cardInfo}>
                    <Card style={styles.infoContainer}>
                        <Card style={{ flexDirection: 'row', }}>
                            <Icons name={usericon}  width={getWidth(42)} height={getWidth(42)} />
                            <Card style={{ flexDirection: 'column', marginLeft: 13 }}>
                                <_Text style={styles.name}>{name}</_Text>

                                <Card style={styles.subContainer}>
                                    <Card style={{ flexDirection: 'row' }}>
                                        <Icons name='grid' width={getWidth(18)} height={getWidth(18)} />
                                        <_Text style={{ marginLeft: 5, fontSize: 'small1' }}>{service}</_Text>
                                    </Card>
                                    <Card style={{ flexDirection: 'row', marginLeft: 10 }}>
                                        <Icons name='clock' width={getWidth(18)} height={getWidth(18)} />
                                        <_Text style={{ marginLeft: 4, fontSize: 'small1' }}>{time}</_Text>
                                    </Card>
                                </Card>

                            </Card>
                        </Card>
                        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                            <Icons name={leftIcon} width={getWidth(18)} height={getWidth(18)} />
                        </TouchableOpacity>

                    </Card>
                </Card>

               
            </Card>
        );
    }
}

export default CustomerPendingInfo
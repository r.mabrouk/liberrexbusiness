import { images, metrics, Icons } from '../../config';
import { getWidth, getHeight, Width } from '../../components/utils/dimensions';
import { ColorsList } from '../PropsStyles';
const styles = {
    cardInfo: (padding,zIndex)=>({
        width: Width,
        height: getHeight(72),
        shadow: 6,
        marginBottom: padding,
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor:ColorsList.white,
        paddingHorizontal:getWidth(15),

    }),
    name:
    {
        fontSize: 'size15',
        color: 'darkGray1',
        fontWeight: 'bold',
    },
    subContainer:
    {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop:getHeight(12),
        justifyContent:'space-between',
        width:getWidth(240)
    },
    crads: {

        width: '100%',
        height: getHeight(44),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
        backgroundColor:'green'
        // shadow:5

    },
    options: {
        height: getHeight(44),
        shadow: 5,
        width: metrics.screenWidth / 3,
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

    },
    optionsText: {
        fontSize: 'small13Px',
        fontWeight: 'bold',
        marginLeft: 5,

    },
    Button1: {
        height: getHeight(44),
        shadow: 5,
        width: Width/2,
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex:10,

    },
    Button2: {
        height: getHeight(44),
        shadow: 5,
        width: Width/2,
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    CardInfo:{
        width: '100%',
        height: getHeight(72),
        shadow: 6,
        zIndex:5,
        // backgroundColor:'red',
        // marginBottom: getHeight(15),
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    
   }


}

export default styles
import React, { Component } from 'react';
import {
    View, FlatList
} from 'react-native';
import styles from './styles'
import { Button } from '../../components/button/button';
import { _Text } from '../../components/text/text';
import { Card } from '../../components/card/card';
import { getHeight, getWidth, Width } from '../../components/utils/dimensions';
import { PopupModal } from '../../components/popupModal/popupModal';
import { _string, getlanguage } from '../../local';
import { CheckBox } from '../../components/checkBox/checkBox';
import { ColorsList } from '../../components/PropsStyles';
// import { FlatList } from 'react-native-gesture-handler';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import EmptyData from '../emptyData/emptyData';
import NavigationService from '../../navigation/NavigationService';
import { Icons } from '../../config';

class CallNextModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedQueue: -1
        };
    }
    onAnimated = (state) => {
        this.popupAddModal.onAnimated(state)
    }

    render() {
        let { selectedServicesID, selectedServicesNames } = this.state
        let { queueList, loadingQueueList, onQueueSelect } = this.props
        return (
            <PopupModal
                height={getHeight(360)}
                duration={500}
                zIndex={30}
                ref={_popupAddModal => this.popupAddModal = _popupAddModal}>
                <Card style={styles.popupContainer}>
                    <View style={styles.popupHeader}>
                        <Icons name='user' width={getWidth(28)} height={getWidth(28)} />
                        <_Text
                            style={styles.popupHeaderText}>{_string("SelectQueue").toLocaleUpperCase()}</_Text>
                    </View>
                    <View style={{ width: Width, height: 1, backgroundColor: ColorsList.rgbaBlack(0.04), marginTop: getHeight(10) }}></View>

                    <View style={{ height: getHeight((queueList.length < 1) && !loadingQueueList ? 240 : 0), width: '100%', paddingTop: getHeight((queueList.length < 1) && !loadingQueueList ? 60 : 0) }}>

                        <EmptyData
                            imageHeight={getWidth(100)}
                            imageWidth={getWidth(100)}
                            height={getWidth(100)}
                            onPress={() => {
                                this.onAnimated()
                                NavigationService.navigate('QueueEdit')
                            }}
                            show={(queueList.length < 1) && !loadingQueueList} />
                    </View>
                    <View style={{ height: getHeight((queueList.length < 1) && !loadingQueueList ? 0 : 240), width: '100%' }}>
                        <FlatList
                            data={queueList}
                            // onEndReached={onEndReachedQueueList}
                            // onEndReachedThreshold={0.1}
                            renderItem={({ item, index }) => {
                                return (
                                    <QueueItem
                                        queue_name={item.queue.title}
                                        onChecked={() => {
                                            this.setState({ selectedQueue: item.queue.id })
                                            // onQueueSelect({ id: item.queue.id, name: item.queue.title })
                                        }}
                                        checked={this.state.selectedQueue == item.queue.id} />
                                )
                            }}
                            ListFooterComponent={() => {
                                return (
                                    <LoaderLists isLoading={loadingQueueList} />
                                )
                            }}
                        />

                    </View>
                    <Button
                        disabled={(queueList.length < 1)}
                        isLoading={this.props.loadingCallNext}
                        onPress={() => { this.props.onCallNextPress(this.state.selectedQueue) }}
                        iconName='next'
                        text={_string("footer.call_next")}
                        iconStyle={{ width: getWidth(18), height: getHeight(18) }}
                        style={styles.approveButton}
                    />
                </Card>

            </PopupModal>
        );
    }
}
const QueueItem = ({ checked, onChecked, queue_name }) => {
    return (
        <Button
            onPress={onChecked}
            style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: getHeight(15),
                paddingHorizontal: getWidth(15),
                borderBottomWidth: 1,
                borderColor: ColorsList.rgbaBlack(0.04)
            }}>
            <CheckBox
                fontSize={'size13'}
                text={queue_name}
                onChecked={onChecked}
                checked={checked} />
        </Button>
    )
}

export default CallNextModal
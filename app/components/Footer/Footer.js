import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Card } from '../../components/card/card';
import { getHeight, getWidth } from '../../components/utils/dimensions';
import { metrics, Icons } from '../../config';
import { Button } from '../../components/button/button';
import LinearGradient from 'react-native-linear-gradient'
import { Colors, GradientColors, GradientColorsList } from '../../components/PropsStyles/colors'
import { _Text } from '../../components/text/text';
import styles from './styles'
import { _string } from '../../local';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';
import CallNextModal from './CallNextModal';


class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    getQueuList = (firstTime) => {
        this.props.getQueueList(firstTime)
    }
    onCallNextCustomer = () => {
        if (this.props.defaultQueue)
            this.props.callNextCustomer(this.props.defaultQueue, this.callNext.onAnimated)
        else
            this.callNext.onAnimated(true)

    }
    callNextCustomer = (queue_id) => {
        this.props._setDefaultQueue(queue_id)
        this.props.callNextCustomer(queue_id, this.callNext.onAnimated)
    }

    componentDidMount() {
        this.getQueuList()
    }


    render() {
        const {
            navigation: { state: { index, routes } },
            style,
            activeTintColor,
            inactiveTintColor,
            renderIcon,
            jumpTo
        } = this.props;
        return (
            // <View style={{
            //     flexDirection: 'row',
            //     height: 50,
            //     width: '100%',
            //     ...style
            // }}>
            //     {
            //         routes.map((route, idx) => (
            //   <TouchableOpacity onPress={() => 
            //   {
            //     jumpTo(route.key)
            //   }

            //   } style={{width:100,height:60,backgroundColor:'red',margin:5}}>

            //                 {/* {renderIcon({
            //                     route,
            //                     focused: index === idx,
            //                     tintColor: index === idx ? activeTintColor : inactiveTintColor
            //                 })} */}
            //   </TouchableOpacity>
            //         ))
            //     }
            // </View>

            <View style={styles.container}>
                <Button onPress={() => {
                    jumpTo(routes[0].key)
                    console.log(index)

                }} style={styles.footerCard1(index === 0 ? 20 : 0)}
                    iconName={'home'}
                    iconStyle={{ width: getWidth(22), height: getHeight(22), color: index != 0 ? 'gray1' : null }}

                />

                <Button onPress={() => {

                    jumpTo(routes[1].key)
                    console.log(index)

                }} style={styles.footerCard2(index === 1 ? 20 : 0)}
                    iconName={'queueTab'}
                    iconStyle={{ width: getWidth(22), height: getHeight(22), color: index != 1 ? 'gray1' : null }}

                />


                <Card style={styles.nextButton}>
                    <TouchableOpacity
                        onPress={() => { this.onCallNextCustomer() }}
                        onLongPress={() => { this.callNext.onAnimated(true) }}
                        style={styles.TouchableLinearGradient} activeOpacity={0.8}>
                        <LinearGradient
                            style={styles.TouchableLinearGradient}
                            locations={[0, 1]}
                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }}
                            colors={GradientColorsList['blue']}>
                        </LinearGradient>
                        <Icons name={'next'} width={getWidth(22)} height={getHeight(22)} />
                        <_Text style={{ color: 'white', fontSize: 'size11', fontFamily: 'bold' }}>{_string("footer.call_next").toLocaleUpperCase()}</_Text>
                    </TouchableOpacity>
                </Card>


                {/* End Next Button */}

                <Button onPress={() => {
                    jumpTo(routes[2].key)
                    console.log(index)

                }} style={styles.footerCard2(index === 2 ? 20 : 0)}
                    iconName={'appointmentTab'}
                    iconStyle={{ width: getWidth(22), height: getHeight(22), color: index != 2 ? 'gray1' : null }}
                />

                <Button onPress={() => {
                    jumpTo(routes[3].key)
                    console.log(index)

                }} style={styles.footerCard1(index === 3 ? 20 : 0)}
                    iconName={'settings'}
                    iconStyle={{ width: getWidth(22), height: getHeight(22), color: index != 3 ? 'gray1' : null }}
                />
                {/* -------------- call next model -------------- */}
                <CallNextModal
                    ref={(callNext) => this.callNext = callNext}
                    queueList={this.props.QueueList}
                    // selectedQueueID={this.props.selectedQueueID}
                    loadingQueueList={this.props.loadingQueueList}
                    onCallNextPress={this.callNextCustomer}
                    loadingCallNext={this.props.loadingCallNextCustomer}
                />
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        QueueList: state.queueReducer.QueueList,
        loadingQueueList: state.queueReducer.loadingQueueList,
        loadingCallNextCustomer: state.queueReducer.loadingCallNextCustomer,
        defaultQueue: state.authReducer.defaultQueue,

    };
}

function mapDispatchToProps(dispatch) {
    return {
        getQueueList: (firstTime) => dispatch(ActionCreators.getQueueList(firstTime)),
        callNextCustomer: (queue_id, onCloseModal) => dispatch(ActionCreators.callNextCustomer(queue_id, onCloseModal)),
        _setDefaultQueue: (id) => dispatch(ActionCreators.setDefaultQueue(id)),
    };
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Footer);
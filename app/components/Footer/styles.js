import { getHeight, getWidth, Width } from "../utils/dimensions";
import { metrics } from "../../config";

const styles = {
    container: {
        width: Width,
        height: getHeight(48),
        //backgroundColor: 'red',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end'
     
    },
    footerCard1:(zIndex)=>(
        {
            width: getWidth(72),
            shadow: 3,
            height: getHeight(48),
            backgroundColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',
            zIndex:zIndex
        }
    ) ,
    footerCard2:(zIndex)=>(
       {
        width: getWidth(72),
        shadow: 6,
        height: getHeight(48),
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex:zIndex
    }  
    ),
    nextButton: {
        width: getWidth(87),
        shadow: 8,
        height: getHeight(58),
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius:3,borderTopRightRadius:3
    },
    TouchableLinearGradient: {
        width: '100%', height: '100%', position: 'absolute',
        borderTopLeftRadius:3,borderTopRightRadius:3,
         alignItems: 'center',
         justifyContent: 'center',
    },
    popupContainer: {
        // height: '100%',
        width: '100%',
        alignItems: 'center'
    },
    popupHeader: {
        flexDirection: 'row', width: getWidth(345),
        justifyContent: 'center', alignItems: 'center',
        marginTop:getHeight(20),
      
    },
    popupHeaderText: {

        marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily:'bold',
    },
    approveButton:{
        height: getHeight(44),
        width: getWidth(345),
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        textColor:'white' ,
        colors:'blue',
        fontSize: 'medium15Px',
        fontFamily: 'bold'
    },
}

export default styles


import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Button } from '../../components/button/button';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { Card } from '../../components/card/card';
import { images, metrics, Icons } from '../../config';
import LinearGradient from 'react-native-linear-gradient'
import CustomerPendingInfo from '../../components/CustomerPending/CustomerPending'
import CustomerApproveInfo from '../../components/CustomerPending/CustomerApproveInfo'
import { Colors, GradientColors, GradientColorsList } from '../../components/PropsStyles/colors'
import { getHeight, getWidth } from '../utils/dimensions';
class HeaderButtons extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentSelected: 0
        };
    }




    onSelected = (index) => {
        this.setState({ currentSelected: index })
        this.props.onSelected(index)
    }
    render() {

        let { leftButtonIcon, leftButtonText, rightButtonIcon, rightButtonText } = this.props

        return (
            <Card
                style={styles.container}>

                <Card style={styles.TwoButtons}>

         


                    <Button
                    iconStyle={{ width: getWidth(20),height:getHeight(20) }}
                        text={leftButtonText}
                        iconName={this.state.currentSelected === 0?leftButtonIcon+'white':leftButtonIcon}
                        style={this.state.currentSelected === 0 ? styles.blueButton : styles.whiteButton}
                        onPress={() => { this.onSelected(0) }}
                        
                    />



                    <Button
                    iconStyle={{ width: getWidth(20),height:getHeight(20) }}
                    text={rightButtonText}
                        iconName={this.state.currentSelected === 1?rightButtonIcon+'white':rightButtonIcon}
                        style={this.state.currentSelected === 1 ? styles.blueButton : styles.whiteButton}
                        onPress={() => { this.onSelected(1) }}
                    />

                </Card>


            </Card>
        );
    }
}


export default HeaderButtons;

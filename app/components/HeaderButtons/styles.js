import { getHeight, getWidth } from "../utils/dimensions";
import { metrics } from "../../config";

const styles = {
    container: {
        width: '100%',
        height: getHeight(44),
        marginBottom: 2,
        shadow: 6,
        backgroundColor: 'white'

    },
    TwoButtons: {
        flexDirection: 'row',
        width: '100%',
        backgroundColor: 'white',
        height: 44,
        shadow: 6,
        justifyContent: 'center',
    },
    blueButton: {
        width: getWidth(375/2),
        //marginTop: 15,
        textColor: '#fff',
        shadow: 6,
        height: getHeight(44),
        colors: 'blue',
        //borderRadius: 5,
        marginLeft: 5,
        fontSize: 'small13Px'
    },
    whiteButton: {
        width: getWidth(375/2),
        //marginTop: 15,
        textColor: '#555',
        shadow: 6,
        height: getHeight(44),
        backgroundColor: 'white',
        // borderRadius: 5,
        marginLeft: 5,
        fontSize: 'small13Px'
    }


}
export default styles
import React, { Component } from 'react';
import { View, Text,Image } from 'react-native';
import { Card } from '../../components/card/card'
import LinearGradient from 'react-native-linear-gradient'
import { Colors, GradientColors, GradientColorsList } from '../../components/PropsStyles/colors'
import { _Text } from '../../components/text/text';
import styles from './styles'
import ApiConstants from '../../api/ApiConstants';
import { images } from '../../config';
import { _string } from '../../local';
export default class HomeHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    let{name,image} = this.props
    return (

     
        <Card style={styles.container}>
      
        <View style={styles.headerContent}>
            <Image source={!image?images.user_profile:{uri:ApiConstants.IMAGE_BASE_URL+ image}} style={styles.avatarImg} />
            <View style={{flexDirection:'row'}}>
            <_Text style={styles.greeting}>{_string("Welcome")+','} </_Text>
            <_Text style={{fontSize:'size15',color:'white',fontFamily: 'bold'}} >{name}</_Text>
            </View>
        </View>
    </Card>
    );
  }
}

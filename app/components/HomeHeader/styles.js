import { StyleSheet } from 'react-native';
import {  getHeight, Width, getWidth } from '../../components/utils/dimensions';
import {RFValue} from 'react-native-responsive-fontsize'

const styles = {
    container: {
        height: getHeight(77),
        width: Width,
       marginTop:-getHeight(30),
        shadow: 6,
        colors: 'blue',

    },
    headerContent: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: getHeight(25),
        flexDirection: 'row',

    },
    greeting: {
        color: "white",
        marginLeft: 10,
        fontSize:'size15',
        fontFamily:'regular'
    },
    avatarImg: {
        width: getWidth(45),
        height: getWidth(45),
        borderRadius: getWidth(45/2),
    },
    name: {
        color: 'white',

    }
}
export default styles;
import * as React from 'react';
import {
    View,ActivityIndicator
} from 'react-native';
import { ColorsList } from '../PropsStyles/colors'
import { getHeight, Width } from '../utils/dimensions';
type LoaderListsProps = {
    isLoading:Boolean,
}
function LoaderLists(props: LoaderListsProps) {
    if (props.isLoading) {
        return (
            <View style={{
                width: Width,
                height: getHeight(60),
                alignItems: 'center',
                justifyContent: 'center'
            }}>
                <ActivityIndicator color={ColorsList.darkGray1} size={'large'} />
            </View>
        )
    }
    else
    return <View />
}



export { LoaderLists };

import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Button } from '../../components/button/button';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { images, metrics, Icons } from '../../config';
import ImagePicker from 'react-native-image-picker';
import HeaderButtons from '../../components/HeaderButtons/HeaderButtons'
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { ScrollView } from 'react-native-gesture-handler';
import { getHeight, getWidth } from '../../components/utils/dimensions';
import { Input } from '../../components/input/input';

export default class Notification extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }


    getTimeAgo = (time) => {
        let times = new Date(time.replace(" ", "T")).getTime()
        let timeAgo = (new Date().getTime() - times) / 1000
        let _time = 0
        let timing = ""
        if (timeAgo > 1440) {
            timing = 'Day'
            _time = timeAgo / 60 / 24 / 60
        }
        else if (timeAgo >= 60) {
            timing = 'Hour'
            _time = timeAgo / 60
        }
        else if (timeAgo < 60) {
            timing = 'minute'
            _time = timeAgo
        }
        return Math.floor(_time) + " " + timing + " ago"
    }

    render() {
        let { title, subtitle, time } = this.props
        return (

            <Card style={styles.notificationCard} >
                {/* <Card style={styles.Icon}> */}
                <Icons name='notificationGradient' width={getWidth(42)} height={getWidth(42)} />
                {/* </Card> */}
                <Card style={{
                    width: getWidth(300), marginHorizontal: getWidth(10)
                }}>
                    <_Text style={styles.title}>{title}</_Text>
                    <_Text style={styles.subtitle}>{subtitle}</_Text>
                    <_Text style={styles.time}>{this.getTimeAgo(time)}</_Text>
                </Card>
            </Card>

        );
    }
}


import { StyleSheet } from 'react-native';
import { getHeight, Width, getWidth } from '../../components/utils/dimensions';
import { ColorsList } from '../PropsStyles';

const styles = {
    notificationCard: {
        width: Width,
        backgroundColor: 'white',
        shadow: 6,
        height: getHeight(86),
        alignItems: 'center',
        // justifyContent: 'center',
        marginTop: getHeight(15),
        flexDirection: 'row',
        // backgroundColor:'red',
        paddingHorizontal:getWidth(10)
    },
    Icon: {
        //width: metrics.screenWidth * .2,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: 'red'
    },
    title: {
        fontSize: 'size13',
        fontFamily: 'bold',
        color: 'darkGray1'
    },
    subtitle: {

        fontSize: 'size12',
        fontFamily: 'medium',
        color: 'darkGray1',
        marginTop: getHeight(5)

    },
    time: {
        fontSize: 'size12',
        fontFamily: 'medium',
        marginTop: getHeight(5),
        color:'lightGray1'

    }
};

export default styles;

import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Card } from '../card/card';
import { _Text } from '../text/text';
import { Icons } from '../../config';
import styles from './styles'
import { Button } from '../button/button';
import { getHeight, getWidth } from '../utils/dimensions';

export default class PaymentCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    let { title, onPress, rightIcon, leftIcon, loadingDeletePaymentCard ,onDeleteCard,isNotReomveCard} = this.props
    return (
      <Card style={styles.paymentContainer}>
        <View style={styles.paymentCard} activeOpacity={.85}
          onPress={onPress}
        >
          <View style={styles.iconAndCardName}>
            <Icons name={leftIcon} color='gray' width={getWidth(18)} height={getWidth(18)} />
            <_Text
              style={styles.paymentText}
            >{title}</_Text>
          </View>
         {isNotReomveCard&&<Button
          style={{width:getWidth(15),height:getWidth(15)}}
          isLoading={loadingDeletePaymentCard} 
          onPress={onDeleteCard}
          iconName={rightIcon} iconStyle={{width: getWidth(15),height:getHeight(15)}} />}

        </View>


      </Card>
    );
  }
}

import { StyleSheet } from 'react-native';
import { getHeight, getWidth } from '../../components/utils/dimensions';

const styles = {
    iconAndCardName: {
        flexDirection: 'row',
        alignItems:'center'

    },

    paymentContainer: {
        width: getWidth(345),
        height: getHeight(44),
        backgroundColor: 'white',
        shadow: 6,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: getHeight(15),
        borderRadius: 3,
        paddingHorizontal:getWidth(13)
    },
    paymentCard: {
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        height: '100%'
    },
    paymentText: {
        fontFamily: "bold",
        fontSize: 'size13',
        color: 'darkGray1',
        paddingHorizontal: getWidth(5)

    }

};

export default styles;

import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Button } from '../../components/button/button';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { images, metrics, Icons } from '../../config';
import LinearGradient from 'react-native-linear-gradient'
import CustomerPendingInfo from '../../components/CustomerPending/CustomerPending'
import CustomerApproveInfo from '../../components/CustomerPending/CustomerApproveInfo'
import { Colors, GradientColors, GradientColorsList } from '../../components/PropsStyles/colors'
import { Width, Height, getHeight } from '../utils/dimensions';
import SwiperTabButtons from '../swiperTabButtons/swiperTabButtons';
class PenddingandApprove extends Component {
    constructor(props) {
        super(props);
        this.state = {
            approve: true,
            pending: false
        };
    }
    _approve = () => {

        this.setState({
            approve: true,
            pending: false

        })
    }




    _pendding = () => {
        this.setState({
            approve: false,
            pending: true

        })
    }
    render() {

        let { buttons } = this.props

        let { requestsList, CustomerList, CustomerPendingList } = require("../../config/LocalDatabase")
        return (
            <View
                style={styles.container}>

                <View style={{
                            alignItems: 'center',
                            width: Width,
                            height:'100%'
                        }}>
                    <SwiperTabButtons tabButons={[
                        { iconsName: 'Check', label: 'Approved' },
                        { iconsName: 'Pending', label: 'Pending' }]} >

                {/* {this.state.approve && */}
                
                    
                    {CustomerPendingList.map((item, index) =>
                        <CustomerApproveInfo
                            name={item.name}
                            usericon={item.usericon}
                            leftIcon={item.leftIcon}
                            time={item.time}
                            service={item.service}
                        />)}
            
            {/* } */}

                {/* {this.state.pending &&  */}
           
                    {CustomerPendingList.map((item, index) =>
                        <CustomerPendingInfo
                            name={item.name}
                            usericon={item.usericon}
                            leftIcon={item.leftIcon}
                            time={item.time}
                            service={item.service}
                        />)}
            {/* } */}
            </SwiperTabButtons> 
             </View>
            </View>
        );
    }
}



export default PenddingandApprove;

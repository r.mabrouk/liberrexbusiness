import { StyleSheet } from 'react-native';
import { metrics } from '../../config';
import { Height, getHeight } from '../utils/dimensions';

const styles ={
    container: {
        
        justifyContent: 'center',
        alignItems: 'center',
       width:metrics.screenWidth,
       height:getHeight(500)
    },
   
};

export default styles;

import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { Card } from '../../components/card/card';
import { getHeight, getWidth } from '../../components/utils/dimensions';
import { Icons } from '../../config';
import { _Text } from '../../components/text/text';
import ServiceCard from '../../components/ServiceCard/ServiceCard';
import { Button } from '../../components/button/button';
import styles from './styles'
export default class Foooter extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        let {onPress,name} = this.props
        return (
            <Card style={styles.container} >
                <Card style={styles.exitButton}>
                    <TouchableOpacity style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }}  onPress={onPress}>
                        <Image source={require('../../assets/img/exit.png')} style={{ width: 18, height: 18, tintColor: 'gray' }} resizeMode='cover' />
                    </TouchableOpacity>
                </Card>

                <Card style={styles.HeaderSection}>
                    <Card style={styles.HeaderIconContainer}>
                        <Icons name='user' width={getWidth(22)}
                        height={getWidth(22)}
                        />
                    </Card>
                    <_Text style={styles.HeaderTitle}>ADD</_Text>
                    <_Text style={styles.name}>{" "+name+" "}</_Text>
                    <_Text style={styles.HeaderTitle}>TO QUEUE 1</_Text>
                </Card>

                <Card style={styles.appointmentService}>
                    <Card style={{ flexDirection: 'row', width: '90%', marginTop: getHeight(5), marginBottom: getHeight(7), }}>
                        <Icons name='grid' width={getWidth(18)} height={getWidth(18)} />
                        <_Text style={styles.optionTitle}>
                            Queue Service:</_Text>

                    </Card>
                    <Card style={styles.Service}>
                        <ServiceCard name={'Hair Cut'} style={styles.ServiceCard} />
                        <ServiceCard name={'Hair Cut'} style={styles.ServiceCard} />
                    </Card>
                    <Card style={styles.Service}>
                        <ServiceCard name={'Hair Cut'} style={styles.ServiceCard} />
                        <ServiceCard name={'Hair Cut'} style={styles.ServiceCard} />
                    </Card>
                </Card>

                <Button

                    iconStyle={{ color: '#fff', width: getWidth(25),height:getHeight(25), marginBottom: 5, }}
                    text='Add Customer'
                    iconName='done'
                    style={styles.loginButton}
                />

            </Card>
        );
    }
}


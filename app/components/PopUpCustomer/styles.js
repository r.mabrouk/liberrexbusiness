import { getHeight } from "../utils/dimensions";

const styles = {
    container: {
        backgroundColor: 'rgba(255,255,255,.95)',
        height: getHeight(286),
        width: '100%',
        position: 'absolute',
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
      
    },
    HeaderSection: {
        flexDirection: 'row',
        width: '90%',
        justifyContent: 'center',
        alignItems: 'center',
        height: getHeight(50),
        marginTop: getHeight(15),

    },
    exitButton: {
        position: 'absolute',
        top: 0,
        shadow: 5,
        width: 40,
        height: 40,
        backgroundColor: 'white',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        right: 20
    },
    name: {

        color: 'lightBlue',
        fontSize: 'medium15Px',
        fontWeight: 'bold',
    },
    HeaderTitle: {
        color: 'black',
        fontSize: 'medium15Px',
        fontWeight: 'bold',
    },
    HeaderIconContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 7,
    },
    ServiceCard: {
        flexDirection: 'row', justifyContent: 'flex-start',
        backgroundColor: 'white', shadow: 5,
        height: 41,
        width: '45%',
        alignItems: 'center',
        paddingLeft: 20,
        borderRadius: 3

    },
    Service: {
        flexDirection: 'row',
        backgroundColor: 'white',
        width: '95%',
        height: 50,
        marginTop: 5,
        justifyContent: 'space-around'
    },
    appointmentService: {
        // marginTop: getHeight(10),
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    optionTitle: {
        fontSize: 'medium15Px',
        fontWeight: 'bold',
        marginLeft: 7,
        color: 'darkGray',

    },
    loginButton: {
        width: '90%',
        marginTop: 5,
        textColor: '#fff',
        shadow: 6,
        height: 50,
        colors: 'blue',
        borderRadius: 5,


    }

}

export default styles

import * as React from 'react'
import { Colors, GradientColors } from "./colors";
import { FontsType } from '../../config/fontStyle';


export type ButtonProps = {
    fontFamily:FontsType
    fontSize:number,
    width: number,
    height: number,
    borderRadius: number,
    shadow: number,
    backgroundColor: Colors | string,
    flexDirection: 'row' | 'column'|'row-reverse',
    borderWidth: number,
    borderColor: Colors | String,
    textColor: Colors | String,
    marginTop: number,
    colors: GradientColors ,
    position:'absolute'|'relative',
    alignItems: "flex-start" | "flex-end" | "center" | "stretch" | "baseline",
    justifyContent?: "flex-start" | "flex-end" | "center" | "space-between" | "space-around" | "space-evenly"
}
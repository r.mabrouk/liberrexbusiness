import { Colors, GradientColors } from "./colors";
import {StyleSheet} from 'react-native'
export type CardProps = {
    iconName: 'email' | String,
    width: Number,
    height: Number,
    borderRadius: Number,
    shadow: number,
    backgroundColor: Colors|String,
    flexDirection: 'row' | 'column' | String,
    children?: React.ReactNode,
    borderWidth: Number,
    borderColor: String,
    marginTop: Number,
    overflow: 'hidden' | "scroll" | "visible",
    alignItems: "flex-start" | "flex-end" | "center" | "stretch" | "baseline",
    justifyContent?: "flex-start" | "flex-end" | "center" | "space-between" | "space-around" | "space-evenly",
    colors:GradientColors
}


export const defaultPropsCard = {
    width: 200,
    height: 50,
    shadow: 0,
    overflow: 'hidden',
    flexDirection: 'row',
    borderWidth: 0,
    borderColor: '#aaa',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    white:'#fff'
}

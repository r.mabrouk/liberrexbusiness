import { Colors, GradientColors } from "./colors";

export type CercleProgressProps = {
    children: React.ReactNode,
    colors: GradientColors,
    color: Colors | String,
    size: Number,
    backgroundColor: String,
    CercleWidth: Number,
    duration: Number,
    backgroundWidth: Number,
    startfrom: 'top' | 'right' | 'left' | 'bottom',
    rotate: Boolean,
    totalValue: Number,
     usedValue: Number
  }
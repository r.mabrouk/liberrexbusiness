
export type Colors = 'gray' | 'red' | 'green' | 'gray1' | 'darkGray' | 'black' | 'lightGray' | 'gray0' | 'white' | 'darkGray1' | 'orange'
export const ColorsList = {
    red: 'red',
    gray: 'gray',
    gray1: '#A6ADB3',
    darkGray1: '#333333',
    green: '#34A853',
    darkGray: '#272727',
    black: '#333333',
    lightGray: '#C9C9C9',
    lightGray1:'#8F8F8F',
    gray0: '#A6ADB3',
    white: '#fff',
    orange: '#FBBC05',
    blue: '#408AF4',
    lightBlue: '#56abf8',
    rgbaLightGray: (opacity) => 'rgba(51,51,51,' + opacity + ')',
    rgbaGray: (opacity) => 'rgba(21,21,21,' + opacity + ')',
    rgbaWhite: (opacity) => 'rgba(255,255,255,' + opacity + ')',
    rgbaRed: (opacity) => 'rgba(234,67,53,' + opacity + ')',
    rgbaOrange: (opacity) => 'rgba(251,188,5,' + opacity + ')',
    rgbaGreen: (opacity) => 'rgba(52,168,83,' + opacity + ')',
    rgbaBlack: (opacity) => 'rgba(0,0,0,' + opacity + ')'
}
export type GradientColors = 'blue' | 'red' | 'green'
export const GradientColorsList = {
    red: ['#408AF4', '#55D8FF'],
    blue: ['#408AF4', '#55D8FF'],
    green: ['#408AF4', '#55D8FF'],
}

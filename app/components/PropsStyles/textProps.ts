import { Colors } from "./colors";
import { FontsType, FontsSize } from "../../config/fontStyle";

export type TextProps = {
color:Colors,
fontFamily:FontsType,
fontSize:FontsSize,
paddingHorizontal:Number
}
import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';

import { images, metrics, Icons } from '../../config';

import LinearGradient from 'react-native-linear-gradient'
import { Colors, GradientColors, GradientColorsList } from '../PropsStyles/colors'
import { Card } from '../../components/card/card';
import { _Text } from '../../components/text/text';
import { getWidth, getHeight } from '../utils/dimensions';
import { CheckBox } from '../checkBox/checkBox';



type ServiceCardProps = {
    onChecked: Function,
    text: String,
    checked: Boolean
}

export default ServiceCard = (props: ServiceCardProps) => {
    return (
        <Card
            style={styles.card(props.zIndex)}>
            <TouchableOpacity onPress={()=>props.onChecked&&props.onChecked(props.checked)} style={{
                flexDirection: 'row', width: '100%',
                height: '100%', justifyContent: 'flex-start', alignItems: 'center'
            }} activeOpacity={.8} >
                <CheckBox fontSize={'size13'} text={props.text} checked={props.checked} onChecked={()=>props.onChecked&&props.onChecked(props.checked)} />
            </TouchableOpacity>
        </Card>
    );
}
const styles = {
    card:zIndex=>( {
        flexDirection: 'row',
        backgroundColor: 'white', 
        shadow: 6,
        height: getHeight(41),
        width: getWidth(167.5),
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: getWidth(17),
        borderRadius: 3,
        zIndex:zIndex,

        
    }),
    circle: {
        width: 15, height: 15, borderRadius: 7.5,
        alignItems: 'center', justifyContent: 'center'
    },
    ServiceName: {
        marginLeft: 10,
        fontFamily: 'regular', fontSize: 'small13Px', fontWeight: '200', color: 'black'
    },
}
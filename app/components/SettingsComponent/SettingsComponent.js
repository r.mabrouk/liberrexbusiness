import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import styles from './styles';
import { images, metrics, Icons } from '../../config';
import { Card } from '../../components/card/card';
import { _Text } from '../../components/text/text';
import { getWidth, getHeight } from '../utils/dimensions';
type SettingsComponentProps ={
    onPress:Function
}
export default class SettingsComponent extends Component<SettingsComponentProps> {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        let { title, leftIcon,onPress } = this.props
        return (
            <Card style={styles.optionCard(this.props.width)} >
                <TouchableOpacity  style={styles.option} activeOpacity={0.8} onPress={onPress}>
                    <View style={{ flexDirection: 'row' }}>
                        <Icons color={'lightGray1'} name={leftIcon} width={getWidth(18)} height={getWidth(18)} />
                        <_Text style={styles.optionText}>{title}</_Text>
                    </View>


                    <View>
                        <Icons name={'arrow'} width={getWidth(12)} height={getHeight(12)} />
                    </View>


                </TouchableOpacity>
            </Card>
        );
    }
}

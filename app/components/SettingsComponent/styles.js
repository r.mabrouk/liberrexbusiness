import { getHeight, getWidth } from "../utils/dimensions";

const styles = {

optionCard:(width)=>( {

    backgroundColor: 'white', shadow: 3, width:width|| getWidth(280), marginTop: getHeight(10),
    height: getHeight(44), justifyContent: 'center', alignItems: 'center',
    borderRadius:3,

}),
option: {

    flexDirection: 'row', width: '100%', height: '100%',
    justifyContent: 'space-between', alignItems: 'center',paddingHorizontal:getWidth(15)


},
optionText:
    { fontSize: "size13", fontFamily: 'bold', marginLeft: 5,color:'darkGray1' }
}

export default styles

import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import PropTypes from 'prop-types';
import { Button } from '../../components/button/button';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { images, metrics, Icons } from '../../config';

import ImagePicker from 'react-native-image-picker';
import HeaderButtons from '../../components/HeaderButtons/HeaderButtons'
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import { getHeight, getWidth } from '../../components/utils/dimensions';
import { Input } from '../../components/input/input';
import Notification from '../../components/Notification/Notification';
import CustomerCardInfo from '../../components/CustomerCardInfo/CustomerCardInfo';
import CustomerApproveInfo from '../../components/CustomerPending/CustomerApproveInfo';
import ServiceCard from '../../components/ServiceCard/ServiceCard';
import { QueueItem } from '../../components/queueItem/queueItem';
import LinearGradient from 'react-native-linear-gradient'
import { Colors, GradientColors, GradientColorsList, ColorsList } from '../../components/PropsStyles/colors'
import { CheckBox } from '../checkBox/checkBox';
import { _string } from '../../local';

export default class TimeCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            flag: props.isSelected===1?true:false,
        };
    }


    _selected = () => {
        let newFlag = !this.state.flag
        this.setState({ flag: newFlag })
        if (this.props.onDayPress) {
            this.props.onDayPress(newFlag)
        }
    }
componentWillReceiveProps=(props)=>{
this.setState({flag:props.isSelected===1?true:false})
}
    render() {
        console.log(this.props.isSelected,"isSelected")
        let { name, onPress } = this.props
        return (

            <Card style={{
                flexDirection: 'row', width: getWidth(345),
                height: getHeight(41), backgroundColor: 'white', shadow: 6,paddingHorizontal:getWidth(10),alignItems:'center',justifyContent:'space-between',marginTop:getHeight(10),borderRadius:3
            }}>
                <CheckBox fontSize='size13' text={name} checked={this.state.flag} onChecked={this._selected} />

                <TouchableOpacity style={{
                    flexDirection: 'row', 
                    // backgroundColor: 'red',
                    height: getHeight(41), width: getWidth(210),
                    justifyContent: 'flex-end',
                    alignItems: 'center'
                }} onPress={onPress}>
                    <_Text style={{
                        fontSize: 'size13', color: 'darkGray1',
                        fontFamily: 'medium', marginRight: getWidth(10),
                    }}><_Text style={{fontFamily:'medium',fontSize:'size13',color:ColorsList.rgbaGray(.6)}}>{_string("From")}  </_Text>{`${this.props.from ? this.props.from : "9:00 am"}`} <_Text style={{fontFamily:'medium',fontSize:'size13',color:ColorsList.rgbaGray(.6)}}>{_string("To")}</_Text> {`${this.props.to ? this.props.to : "9:00 pm"}`}</_Text>
                    <Icons name='arrowDown' width={getWidth(12)} height={getWidth(12)} />
                </TouchableOpacity>
            </Card>
        )
    }
}

const styles = {
    card: {
        flexDirection: 'row',
        backgroundColor: 'white',
        height: 41,
        width: metrics.screenWidth * .45,
        alignItems: 'center',
        paddingLeft: 20,
        borderRadius: 3
    },
    circle: {
        width: 15, height: 15, borderRadius: 7.5,
        alignItems: 'center', justifyContent: 'center'
    },
    ServiceName: {
        marginLeft: 5,
        fontSize: 'medium15Px', fontWeight: '200', color: 'black'
    },

}
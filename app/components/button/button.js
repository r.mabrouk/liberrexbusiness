import * as React from 'react';
import {
    Text,
    TouchableOpacity, ActivityIndicator, View, Modal
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient'
import { Card } from '../card/card';
import { Icons, IconsName } from '../../config';
import { Colors, GradientColors, GradientColorsList, ColorsList } from '../PropsStyles/colors'
import { ButtonProps } from '../PropsStyles/buttonProps'
import { fontsSize, fonts } from '../../config/fontStyle';
import { getWidth, getHeight } from '../utils/dimensions';
import Loading from '../../screens/Loading';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

type ButtonPropsIn = {
    onPress: Function,
    text: String,
    iconName: IconsName | String,
    icon: Function,
    iconStyle: {
        color: Colors | String,
        size: Number,
        rotate: Boolean
    },
    style: ButtonProps,
    children: React.ReactNode,
    loaderColor: Colors | String,
    isLoading: Boolean,
    rightIcon: IconsName | String,
    activeOpacity: Number

}
function get_font(fontName) {
    return fonts[fontName]
}
function get_font_size(fontsize) {
    console.log("get_font_size  ", fontsize)
    return RFValue(fontsSize[fontsize]) || fontsSize[fontsize]
}
function get_font_color(color) {
    return ColorsList[color] || color
}
function Button(props: ButtonPropsIn) {
    let style = props.style
    return (
        <Card style={props.style}  >
            <TouchableOpacity
            disabled={props.disabled}
                onPress={props.onPress}
                activeOpacity={props.activeOpacity || .8}
                style={{
                    width: '100%',
                    height: '100%',
                    alignItems: style.alignItems || 'center',
                    justifyContent: style.justifyContent || 'center',
                    flexDirection: style.flexDirection || 'row'
                }}>
                {/* {props.style.colors && <LinearGradient
                        style={{ width: '100%', height: '100%', position: 'absolute' }}
                        locations={[0, 1]}
                        start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }}
                        colors={getColor(props.style.colors)}>
                    </LinearGradient>} */}

                <Icons
                    rotate={props.iconStyle && props.iconStyle.rotate}
                    width={props.iconStyle ? props.iconStyle.width : getWidth(12)}
                    height={props.iconStyle ? props.iconStyle.height : getHeight(12)}


                    color={props.iconStyle && props.iconStyle.color}
                    icon={props.icon}
                    name={props.iconName} />
                {props.text ? <Text
                    numberOfLines={props.numberOfLines}
                    style={{ color: get_font_color(props.style.textColor), paddingHorizontal: 5, fontSize: get_font_size(props.style.fontSize), fontFamily: get_font(props.style.fontFamily) }}>
                    {props.text}
                </Text> : null}
                {props.children}

                {props.rightIcon &&
                    <View style={{ position: 'absolute', right: 0 }}>

                        <Icons
                            height={getHeight(12)}
                            width={getWidth(12)}
                            color={props.iconStyle && props.iconStyle.color}
                            icon={props.icon}
                            name={props.rightIcon}
                            rotate={props.iconStyle && props.iconStyle.rotate}
                        />
                    </View>

                }
            </TouchableOpacity>

            <Loading isLoading={props.isLoading ? props.isLoading : false} />
        </Card>
    );
}

function getColor(colorName) {
    return GradientColorsList[colorName] || colorName
}

export { Button };

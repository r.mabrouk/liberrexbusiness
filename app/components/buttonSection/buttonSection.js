import * as React from 'react';
import {
    View,
    TouchableOpacity
} from 'react-native';
import { Card } from '../card/card';
import { _Text } from '../text/text';
import { Icons } from '../../config'
import styles from './styles';
import { Button } from '../button/button';
import { getWidth } from '../utils/dimensions';


export type ButtonSectionItemProps = {
    iconName1: String,
    iconName2: String,
    text1: String,
    text2: String,
    leftIcon: String,
    onPress: Function
}


function ButtonSectionItem(props: ButtonSectionItemProps) {
    return (
        <View style={styles.container}>
            <Card style={styles.cardView}>
                <View style={{ width: getWidth(20) }}>
                    <Icons name={props.iconName1} width={getWidth(18)} height={getWidth(18)} />
                </View>
                <_Text style={styles.textStyle('medium15Px')}>{props.text1}</_Text>

            </Card>
            <Card style={styles.cardContainer}>
                <TouchableOpacity style={styles.buttonStyle}
                    activeOpacity={0.8}
                    onPress={() => { }}>

                    <View style={{ flexDirection: 'row' }}>
                        {props.leftIcon &&
                            <View style={{ width: getWidth(20) }}>
                                <Icons name={props.leftIcon} width={getWidth(18)} height={getWidth(18)} />
                            </View>}
                        <_Text style={styles.textStyle('small13Px')}>{props.text2}</_Text>
                    </View>

                    <View style={styles.iconView}>
                        <Icons name={props.iconName2} width={getWidth(18)} height={getWidth(18)} />
                    </View>
                </TouchableOpacity>
            </Card>

        </View>

    );
}

ButtonSectionItem.defaultProps = {

}

export { ButtonSectionItem };
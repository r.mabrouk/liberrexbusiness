import { getHeight, getWidth } from "../utils/dimensions";

export default styles = {
    container: {
        width: getWidth(350),
        height: getHeight(75),
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardView: {
        flexDirection: 'row',
        width: getWidth(345)
    },
    textStyle: (size) => ({
        fontSize: size,
        fontWeight: 'bold',
        color: 'darkGray1'
    }),
    cardContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(345),
        height: getHeight(41),
        backgroundColor: 'white',
        shadow: 6,
        borderRadius: 3,
        marginTop: getHeight(10)
    },
    buttonStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: getWidth(345),
        paddingHorizontal: getWidth(10),
        alignItems: 'center'
    },
    iconView: {
        marginTop: getHeight(10),
        marginLeft: getWidth(10)
    }
}
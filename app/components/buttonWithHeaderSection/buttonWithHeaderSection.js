import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text, ScrollView,
    TextInput
} from 'react-native';
import { _Text } from '../../components/text/text';
import { Card } from '../../components/card/card';
import { Icons,IconsName } from '../../config';
import { ColorsList } from '../PropsStyles';
import { getWidth, getHeight, Width } from '../utils/dimensions';
import { Button } from '../button/button';
type KeyboardType = "default" | "email-address" | "numeric" | "phone-pad";
type KeyboardTypeIOS =
    | "ascii-capable"
    | "numbers-and-punctuation"
    | "url"
    | "number-pad"
    | "name-phone-pad"
    | "decimal-pad"
    | "twitter"
    | "web-search";
type KeyboardTypeAndroid = "visible-password";
type KeyboardTypeOptions = KeyboardType | KeyboardTypeAndroid | KeyboardTypeIOS;


type ButtonWithHeaderSectionProps = {
    value: String,
    isNotTitle: Boolean,
    onPress: Function,
    KeyboardType: KeyboardTypeOptions,
    marginTop: Number,
    password: Boolean,
    headerIcon:IconsName,
    headerText:String

}
const ButtonWithHeaderSection = (props: ButtonWithHeaderSectionProps) => {
    return (
        <Card style={{ width: Width, justifyContent: 'center', alignItems: 'center', marginTop: props.marginTop, paddingBottom: getHeight(15) }}>
            {!props.isNotTitle && <Card style={{ flexDirection: 'row', width: getWidth(345),alignItems:'center' }}>
                <Icons name={props.headerIcon} width={getWidth(18)}  height={getWidth(18)} />
                <_Text style={{ fontSize: 'medium15Px', fontFamily: 'bold', color: 'darkGray1', marginLeft: getWidth(5) }}>{props.headerText}</_Text>
            </Card>}
            <Button onPress={props.onPress} text={props.value} numberOfLines = {props.numberOfLines} iconName='arrowDown' style={styles.inputContainer(props.textColor)}>
            </Button>
        </Card>
    );
}

const styles = {
    inputContainer: (textColor) => (
        {
            flexDirection: 'row-reverse',
            width: getWidth(345),
            marginTop: getHeight(10),
            backgroundColor: 'white',
            shadow: 6, height: getHeight(41),
            alignItems: 'center',
            borderRadius: 3,
            zIndex: 30,
            justifyContent: 'space-between',
            paddingHorizontal: getWidth(13)
            ,overflow:'visible' ,
            fontFamily:'medium',
            fontSize:'size13'
        })
}

export default ButtonWithHeaderSection



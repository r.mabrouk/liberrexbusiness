import {StyleSheet, Platform} from 'react-native';
import * as defaultStyle from '../../style';
import { getWidth } from '../../../../utils/dimensions';
import { ColorsList } from '../../../../PropsStyles';
import { fontsSize, fonts } from '../../../../../config/fontStyle';
import {RFValue} from 'react-native-responsive-fontsize'

const STYLESHEET_ID = 'stylesheet.calendar.header';

export default function(theme={}) {
  const appStyle = {...defaultStyle, ...theme};
  return StyleSheet.create({
    header: {
      flexDirection: 'row',
      justifyContent: 'space-between',

      marginTop: 6,
      alignItems: 'center'
    },
    monthText: {
      fontSize:RFValue(fontsSize.medium15Px) ,
      fontFamily: fonts.bold,
      // fontWeight: appStyle.textMonthFontWeight,
      color: ColorsList.black,
      marginHorizontal: getWidth(15)
    },
    arrow: {
      // padding: 10,
      ...appStyle.arrowStyle
    },
    arrowImage: {
      ...Platform.select({
        ios: {
          tintColor: ColorsList.gray1
        },
        android: {
          tintColor: appStyle.arrowColor
        }
      })
    },
    week: {
      marginTop: 7,
      flexDirection: 'row',
      justifyContent: 'space-around'
    },
    dayHeader: {
      marginTop: 2,
      marginBottom: 7,
      width: 32,
      textAlign: 'center',
      fontSize:RFValue(fontsSize.size12),
      fontFamily: fonts.regular,
      // fontWeight: appStyle.textDayHeaderFontWeight,
      color: ColorsList.rgbaGray(.54)
    },
    ...(theme[STYLESHEET_ID] || {})
  });
}

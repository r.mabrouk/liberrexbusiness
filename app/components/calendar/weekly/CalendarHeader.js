import React, { Component } from "react";
import PropTypes from "prop-types";
import { Text, View } from "react-native";

import styles from "./Calendar.style.js";
import { _Text } from "../../text/text.js";
import { _string } from "../../../local/index.js";

class CalendarHeader extends Component {
  static propTypes = {
    calendarHeaderFormat: PropTypes.string.isRequired,
    calendarHeaderContainerStyle: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number
    ]),
    calendarHeaderStyle: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number
    ]),
    datesForWeek: PropTypes.array.isRequired,
    allowHeaderTextScaling: PropTypes.bool
  };

  shouldComponentUpdate(nextProps) {
    return JSON.stringify(this.props) !== JSON.stringify(nextProps);
  }

  //Function that formats the calendar header
  //It also formats the month section if the week is in between months
  formatCalendarHeader(datesForWeek, calendarHeaderFormat) {
    if (!datesForWeek || datesForWeek.length === 0) {
      return "";
    }

    let firstDay = datesForWeek[0];
    let lastDay = datesForWeek[datesForWeek.length - 1];
    let monthFormatting = "";
    //Parsing the month part of the user defined formating
    if ((calendarHeaderFormat.match(/Mo/g) || []).length > 0) {
      monthFormatting = "Mo";
    } else {
      if ((calendarHeaderFormat.match(/M/g) || []).length > 0) {
        for (
          let i = (calendarHeaderFormat.match(/M/g) || []).length;
          i > 0;
          i--
        ) {
          monthFormatting += "M";
        }
      }
    }

    if (firstDay.month() === lastDay.month()) {
      return _string("month_names")[firstDay.format("MMM")]+firstDay.format("YYYY");
    } else if (firstDay.year() !== lastDay.year()) {
      return `${_string(firstDay.format("MMM"))} / ${_string(lastDay.format("MMM"))+lastDay.format("YYYY")}`;
    }

    return `${
      monthFormatting.length > 1 ? _string("month_names")[firstDay.format("MMM")] : ""
    } ${monthFormatting.length > 1 ? "/" : ""} ${_string("month_names")[lastDay.format("MMM")]+lastDay.format("YYYY")
      
    }`;
  }

  render() {
    const headerText = this.formatCalendarHeader(
      this.props.datesForWeek,
      this.props.calendarHeaderFormat
    );
    return (
      <View style={this.props.calendarHeaderContainerStyle}>
        <_Text
          style={{fontFamily:'bold',fontSize:'size15',color:'darkGray1'}}
        >
          {headerText}
        </_Text>
      </View>
    );
  }
}

export default CalendarHeader;

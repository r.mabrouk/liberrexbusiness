import {StyleSheet, Platform} from 'react-native';
import * as defaultStyle from '../../style';
import { getWidth } from '../../../../utils/dimensions';
import { ColorsList } from '../../../../PropsStyles';
import { fontsSize, fonts } from '../../../../../config/fontStyle';

const STYLESHEET_ID = 'stylesheet.calendar.header';

export default function(theme={}) {
  const appStyle = {...defaultStyle, ...theme};
  return StyleSheet.create({
    header: {
      flexDirection: 'row',
      justifyContent: 'space-between',

      marginTop: 6,
      alignItems: 'center'
    },
    monthText: {
      fontSize: fontsSize.medium15Px,
      fontFamily: fonts.bold,
      // fontWeight: appStyle.textMonthFontWeight,
      color: ColorsList.black,
      marginHorizontal: getWidth(15)
    },
    arrow: {
      // padding: 10,
      ...appStyle.arrowStyle
    },
    arrowImage: {
      ...Platform.select({
        ios: {
          tintColor: ColorsList.gray1
        },
        android: {
          tintColor: appStyle.arrowColor
        }
      })
    },
    week: {
      marginTop: 7,
      flexDirection: 'row',
      justifyContent: 'space-around'
    },
    dayHeader: {
      marginTop: 2,
      marginBottom: 7,
      width: 32,
      textAlign: 'center',
      fontSize: appStyle.textDayHeaderFontSize,
      fontFamily: appStyle.textDayHeaderFontFamily,
      fontWeight: appStyle.textDayHeaderFontWeight,
      color: appStyle.textSectionTitleColor
    },
    ...(theme[STYLESHEET_ID] || {})
  });
}

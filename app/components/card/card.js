import * as React from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image
} from 'react-native';
import { Colors, ColorsList, GradientColorsList } from '../PropsStyles/colors'
import { CardProps } from '../PropsStyles'
import { getWidth, getHeight } from '../utils/dimensions';
import LinearGradient from 'react-native-linear-gradient';
import { fonts } from '../../config/fontStyle';

function getColor(colorName) {
    return ColorsList[colorName] || colorName
}
type CardPropsIn = {
    style: CardProps
}


function Card(props: CardPropsIn) {
    let style = props.style
    return (
        <View style={[
            IosShadow[props.style.shadow], 
            { zIndex: props.style.zIndex,position:props.style.position ,
                right:props.style.right,left:props.style.left,top:props.style.top}
        ]
        }>

            <View
                style={[
                    {
                      
                        elevation: props.style.shadow==6?2.5:props.style.shadow>=1?2.5:0 ,
                        overflow: 'hidden',
                        backgroundColor:'white'
                    },
                    { ...style }, {
                        borderColor: getColor(style.borderColor),
                        backgroundColor: getColor(style.backgroundColor),
                        shadowColor: '#000',
                          top:0,
                        //   right:0,
                          left:0,
                        
                    },]}>



                        
                {props.style.colors && <LinearGradient
                    style={{ width: '100%', height: '100%', position: 'absolute' }}
                    locations={[0, 1]}
                    start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }}
                    colors={GradientColorsList['blue']}>
                </LinearGradient>}
                {props.children}


            </View>

        </View>

    );

}
let IosShadow = {
    1: { shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.18, shadowRadius: 1.0 },
    2: { shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.20, shadowRadius: 1.41 },
    3: { shadowOffset: { width: 0, height: 3 }, shadowOpacity: 0.1, shadowRadius: 4.65 },
    4: { shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.1, shadowRadius: 2.62 },
    5: { shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.15, shadowRadius: 10 },
    6: { shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.12, shadowRadius: 8},
    7: { shadowOffset: { width: 0, height: 3 }, shadowOpacity: 0.29, shadowRadius: 4.65 },
    8: { shadowOffset: { width: 0, height: 4 }, shadowOpacity: 0.30, shadowRadius: 4.65 },
    9: { shadowOffset: { width: 0, height: 4 }, shadowOpacity: 0.32, shadowRadius: 5.46 },
    10: { shadowOffset: { width: 0, height: 5 }, shadowOpacity: 0.34, shadowRadius: 6.27 },
    11: { shadowOffset: { width: 0, height: 5 }, shadowOpacity: 0.36, shadowRadius: 6.68 },
    12: { shadowOffset: { width: 0, height: 6 }, shadowOpacity: 0.37, shadowRadius: 7.49 },
    13: { shadowOffset: { width: 0, height: 6 }, shadowOpacity: 0.39, shadowRadius: 8.30 },
    14: { shadowOffset: { width: 0, height: 7 }, shadowOpacity: 0.41, shadowRadius: 9.11 },
    15: { shadowOffset: { width: 0, height: 7 }, shadowOpacity: 0.43, shadowRadius: 9.51 },
    16: { shadowOffset: { width: 0, height: 8 }, shadowOpacity: 0.44, shadowRadius: 10.32 },
    17: { shadowOffset: { width: 0, height: 8 }, shadowOpacity: 0.46, shadowRadius: 11.14 },
    18: { shadowOffset: { width: 0, height: 9 }, shadowOpacity: 0.48, shadowRadius: 11.95 },
    19: { shadowOffset: { width: 0, height: 9 }, shadowOpacity: 0.50, shadowRadius: 12.35 },
    20: { shadowOffset: { width: 0, height: 10 }, shadowOpacity: 0.51, shadowRadius: 13.16 },
    21: { shadowOffset: { width: 0, height: 10 }, shadowOpacity: 0.53, shadowRadius: 13.97 },
    22: { shadowOffset: { width: 0, height: 11 }, shadowOpacity: 0.55, shadowRadius: 14.78 },
    23: { shadowOffset: { width: 0, height: 11 }, shadowOpacity: 0.57, shadowRadius: 15.19 },
    24: { shadowOffset: { width: 0, height: 12 }, shadowOpacity: 0.58, shadowRadius: 16.00 },
    // 25: { shadowOffset: { width: 0, height: 12 }, shadowOpacity: 0.59, shadowRadius: 16.00 },
    // 26: { shadowOffset: { width: 0, height: 13 }, shadowOpacity: 0.61, shadowRadius: 16.00 },
}




Card.defaultProps = {
    style: {
        shadow: 0,
        overflow: 'hidden',
        flexDirection: 'column',
        borderWidth: 0,
        borderColor: '#aaa',
        backgroundColor: '#fff',
        alignItems: 'center',
        // justifyContent: 'center'
    }


}

export { Card };

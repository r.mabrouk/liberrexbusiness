import React from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  Easing
} from 'react-native';
import Circular from './circular';
const AnimatedProgress = Animated.createAnimatedComponent(Circular);
export default class AnimatedCircularProgress extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      fillAnimation: new Animated.Value(props.prefill)
    }
  }
  componentDidMount() {
    this.animate();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.fill !== this.props.fill) {
      this.animate();
    }
  }

  reAnimate(prefill, toVal, dur, ease) {
    this.setState({
      fillAnimation: new Animated.Value(prefill)
    }, () => this.animate(toVal, dur, ease));
  }

  animate(toVal, dur, ease) {
    const toValue = toVal || this.props.fill;
    const duration = dur || this.props.duration;
    const easing = ease || this.props.easing;

    const anim = Animated.timing(this.state.fillAnimation, {
      toValue,
      easing,
      duration,
    });
    anim.start(this.props.onAnimationComplete);

    return anim;
  }

  render() {
    const { fill, prefill, ...other } = this.props;

    return (
      <AnimatedProgress
        rotation={90}
        {...other}
        fill={this.state.fillAnimation}
      />
    );
  }
}

AnimatedCircularProgress.propTypes = {
  ...Circular.propTypes,
  prefill: PropTypes.number,
  duration: PropTypes.number,
  easing: PropTypes.func,
  onAnimationComplete: PropTypes.func,
  colors: PropTypes.array,
  color: PropTypes.string
};

AnimatedCircularProgress.defaultProps = {
  duration: 500,
  easing: Easing.out(Easing.ease),
  prefill: 0
};

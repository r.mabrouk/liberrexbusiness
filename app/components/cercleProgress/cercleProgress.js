import React from 'react';
import { View, Easing } from 'react-native';
import AnimatedCircularProgress from './AnimatedCircularProgress';
import { ColorsList, GradientColorsList,CercleProgressProps } from '../PropsStyles';
import { Colors, GradientColors } from '../PropsStyles'
// type CercleProgressProps = {
//   children: React.ReactNode,
//   colors: GradientColors,
//   color: Colors | String,
//   size: Number,
//   backgroundColor: String,
//   CercleWidth: Number,
//   duration: Number,
//   backgroundWidth: Number,
//   startfrom: 'top' | 'right' | 'left' | 'bottom',
//   rotate: Boolean,
//   totalValue: Number,
//    usedValue: Number
// }
function CercleProgress(props: CercleProgressProps) {
  return (
    <View>
      <View style={{
        width: props.size, height: props.size,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        borderRadius: props.size / 2,
        overflow: 'hidden'
      }}>
        {props.children}
      </View>

      <View style={{
        transform: [
          {
            rotate: rotation(props.startfrom)
          },
          { rotateX: props.rotate ? '180deg' : '0deg' }]
      }}>

        <AnimatedCircularProgress
          colors={getColors(props.colors) ||
            [getColor(props.color)]}
          size={props.size}
          width={props.CercleWidth}
          duration={props.duration}
          easing={Easing.elastic()}
          fill={getfill(props.totalValue, props.usedValue)}
          tintColor="#EA4335"
          onAnimationComplete={() => { }}
          backgroundColor={props.backgroundColor}
          backgroundWidth={props.backgroundWidth}
        />
      </View>

    </View>


  );
};
function getColor(colorName) {
  return ColorsList[colorName] || colorName
}
function getColors(colorsName) {
  return GradientColorsList[colorsName] || colorsName
}
function rotation(position) {
  switch (position) {
    case 'top':
      return '-90deg'
    case 'right':
      return '-0deg'
    case 'bottom':
      return '90deg'
    case 'left':
      return '180deg'
    default:
      return '180deg'
  }
}
function getfill(totalValue, usedValue) {
  return (usedValue / totalValue) * 100
}
CercleProgress.defaultProps = {
  startfrom: 'top',
  size: 100,
  color: 'gray',
  backgroundColor: "#EAEAEA",
  backgroundWidth: 8,
  CercleWidth: 8,
  duration: 1000,
  totalValue:5,
  usedValue: 5
}

export default CercleProgress;

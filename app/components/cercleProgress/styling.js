import { Dimensions } from 'react-native'
let { width, height } = Dimensions.get('window')
export default {
  container: {
    width: width * 0.58,
    height: width * 0.58,
    marginTop: height * .015,
    borderRadius: width * 0.58 / 2,
    elevation: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2687D5'
  }
};

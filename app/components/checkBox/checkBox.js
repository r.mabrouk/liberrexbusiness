import * as React from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity
} from 'react-native';
import { Colors, ColorsList } from '../PropsStyles/colors'
import { CardProps } from '../PropsStyles'
import { getWidth, getHeight } from '../utils/dimensions';
import { Button } from '../button/button';
import styles from './styles';
import { _Text } from '../text/text';
import { FontsType,FontsSize } from '../../config/fontStyle';

function getColor(colorName) {
    return ColorsList[colorName] || colorName
}
type CheckBoxPropsIn = {
    checked: Boolean,
    onChecked: Function,
    text:String,
    fontSize:FontsSize,
    textColor:Colors
}
class CheckBox extends React.Component<CheckBoxPropsIn>  {

    constructor(props) {
        super(props)
        this.state = {
            checked: props.checked
        }
    }
    onChecked = () => {
        let { checked } = this.state

        this.setState({ checked: !checked }, () => {
            this.props.onChecked(checked)
        })
    }
    render() {
        // let { checked } = this.state
        let {checked}=this.props
        return (
            <TouchableOpacity activeOpacity={.8} onPress={this.onChecked} style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Button
                    onPress={this.onChecked}
                    iconStyle={{ width: getWidth(7),height:getHeight(7),color: 'white'}}
                    iconName={checked ? 'Check' : undefined}
                    style={styles.container(checked)} />
                {this.props.text&&<_Text style={{ paddingHorizontal: getWidth(10), color:this.props.textColor|| 'darkGray1', fontSize:this.props.fontSize||'size15',fontFamily:'medium' }} >{this.props.text}</_Text>}
            </TouchableOpacity>


        );
    }


}


export { CheckBox };

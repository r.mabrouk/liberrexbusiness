import { getWidth } from "../utils/dimensions";

export default styles = {
    container: (checked) => ({
        backgroundColor: '#E5E7E8',
        colors: checked ? 'blue' : undefined, width: getWidth(15), height: getWidth(15), borderRadius: getWidth(7.5)
    })
}

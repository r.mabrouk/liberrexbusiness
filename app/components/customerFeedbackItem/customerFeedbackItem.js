import * as React from 'react';
import {
    View, TouchableOpacity
} from 'react-native';
import { CardProps } from '../PropsStyles'
import { _Text } from '../text/text';
import { Icons } from '../../config'
import styles from './styles';
import { Button } from '../button/button';
import { Rating } from '../rating/rating';
import { Card } from '../card/card';
import { getHeight, getWidth } from '../utils/dimensions';

export type CustomerFeedbackItemProps = {
    style: CardProps,
    onPress: Function,
    icon: String,
    rating: Boolean,
    feedback: String,
    fullname: String
}


function CustomerFeedbackItem(props: CustomerFeedbackItemProps) {

    return (
        <Card
            style={styles.container}
        >

            <TouchableOpacity
                style={[styles.container, props.style]}
                activeOpacity={0.8}
                onPress={props.onPress}
            >
                <View style={styles.userView}>
                    <View style={styles.iconAndText}>
                        <Icons
                            name='user'
                         width={getWidth(30)}
                         height={getWidth(30)}
                        />
                        <_Text
                            style={styles.queueText} >
                            {props.fullname}
                        </_Text>
                    </View>

                    <Button style={styles.buttonStyle}

                                       iconStyle={{ width: getWidth(18),height:getHeight(18) }}

                        iconName='more'
                    />
                </View>
                <_Text
                    style={styles.textStyle} >
                    {props.feedback}
                </_Text>

                <Rating rating={props.rating} style={{ marginTop: getHeight(10) }} />

            </TouchableOpacity>
        </Card>

    );
}
CustomerFeedbackItem.defaultProps = {

}

export { CustomerFeedbackItem };
import { metrics } from "../../config";
import { getHeight, getWidth } from "../utils/dimensions";

export default styles = {
    container: {
        width: getWidth(375),
        height: getHeight(111),
        shadow: 6,
        backgroundColor: 'white',
        marginBottom: getHeight(15),
        flexDirection: 'column',
        alignItems:'center',
        paddingHorizontal:getWidth(15)
    },
    userView: {
        height: getHeight(28),
        width:getWidth(345),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',marginTop:getHeight(15)
    },
    iconAndText: {
        flexDirection: 'row',
        alignItems:'center'
    },
    iconStyle: {
        width: getWidth(30)
    },
    queueTextView: {
        width: '80%',
        height: '100%',
        justifyContent: 'center',
        marginHorizontal: '2%'
    },
    queueText: {
        fontSize: 'size15',
        fontFamily: 'bold',
        color: 'darkGray1',
        paddingHorizontal:getWidth(10)
    },
    buttonStyle: {
        width: getWidth(18),
        height: "95%"
    },
    textView: {
        width: getWidth(345),
        height: getHeight(37),
        justifyContent: 'center'
    },
    textStyle: {
        fontSize: 'size12',
        fontFamily: 'medium',
        color: 'lightGray1',
        marginTop:getHeight(10),
        width:getWidth(345),
        textAlign:'left'
    }
}
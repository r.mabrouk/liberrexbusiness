import React, { Component } from 'react'
import { View, Image } from 'react-native'
import { _Text } from '../text/text'
import { images } from '../../config'
import { getWidth, getHeight } from '../utils/dimensions'
import { Button } from '../button/button'
import { _string } from '../../local'

type Props = {
    show: Boolean,
    message:String,
    onPress:Function,
    height:Number,
    imageHeight:Number,
    imageWidth:Number,
}

const EmptyData = (props: Props) => {
    let {onPress,message,show,height,imageHeight,imageWidth}=props
    return (
    show ? <View style={{ width: '100%', height:height?height: '70%', alignItems: 'center', justifyContent: 'center',position:props.position? 'absolute':undefined}}>
            <Image style={{ width:imageWidth|| getWidth(250), height:imageHeight|| getHeight(150) }} resizeMode='contain' source={images.emptyData} />
            <_Text style={{ fontFamily: 'bold', color: 'blue', fontSize: 'size18',marginTop:getHeight(15) }}>{_string("TheListIsEmptyNow")}</_Text>
            {onPress&&<Button
            onPress={onPress}
            style={{ fontFamily: 'regular', textColor: 'gray0', fontSize: 'size15',height:getHeight(20),marginTop:getHeight(5) }}
            text={_string("AddNew")}
            />}
        </View> : null
    )
}

export default EmptyData
import * as React from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image,
    Text
} from 'react-native';
import { Colors, ColorsList } from '../PropsStyles/colors'
import { Card } from '../card/card';
import { CardProps } from '../PropsStyles'
import { Icons, images, metrics } from '../../config';
import { fonts } from '../../config/fontStyle';
import { _Text } from '../text/text';
import { IconsName } from '../../config'
import { Button } from '../button/button';
import { styles } from './styles';
import { getWidth, getHeight } from '../utils/dimensions';
export type HeaderProps = {
    style: CardProps,
    children: React.ReactChild,
    requestType?: String,
    iconName: IconsName,
    leftIconStyle:{
        color: Colors | String,
    size: Number,
    },
    onNotification:Function,
    onBack:Function

}

function Header(props: HeaderProps) {
    return (
        <Card  {...props} >
            <View
                style={styles.container}>
                <Image
                    source={images.header}
                    style={styles.imageBackground} />
                <View
                    style={styles.contentView}>
                    <Button
                    onPress={props.onBack}
                        iconName={props.iconName||'arrowBack'}
                        iconStyle={props.leftIconStyle?props.leftIconStyle:{ width: getWidth(22),height:getHeight(22)}}
                        style={styles.button} />
                    <Image
                        source={images.logo}
                        style={styles.logo} />
                    <Button
                       onPress={props.onNotification}
                        iconName='notificationIcon'
                        iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                        style={styles.button} />
                </View>
            </View>
        </Card>
    );
}
Header.defaultProps = {
}

export { Header };

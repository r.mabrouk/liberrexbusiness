import * as React from 'react';
import {
    StyleSheet
} from 'react-native';

import { metrics } from '../../config';
import { Width, getWidth, getHeight } from '../utils/dimensions';
export const styles = StyleSheet.create({
container:{ width: Width, height: getHeight(105), alignItems: 'center', justifyContent: 'center' },
imageBackground:{ width: '100%', resizeMode: 'contain', position: 'absolute' },
contentView:{ flexDirection: 'row', width:Width, justifyContent: 'space-between', alignItems: 'center'},
button:{ width: getWidth(50), height: getWidth(50),paddingBottom:getHeight(6)},
logo:{ width: getWidth(156), resizeMode: 'contain' ,height:getHeight(32)}
})
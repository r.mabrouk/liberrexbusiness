import * as React from 'react';
import {
    View
} from 'react-native';
import { Colors, ColorsList } from '../PropsStyles/colors'
import { CardProps } from '../PropsStyles'
import { Card } from '../card/card';
import { Icons } from '../../config';
import { _Text } from '../text/text';
import { Button } from '../button/button';
import { IconsName } from '../../config'
import { ButtonProps, TextProps } from '../PropsStyles'
import styles from './styles';
import { getHeight, getWidth } from '../utils/dimensions';
function getColor(colorName) {
    return ColorsList[colorName] || colorName
}
type HeaderSectionProps = {
    onPress:Function,
    text: String,
    style: CardProps,
    iconName: IconsName,
    buttonStyle: ButtonProps,
    iconButton: IconsName,
    textStyle: TextProps,
    withButton: Boolean,
    textButton: String,
    iconStyle:{
        color: Colors | String,
        size: Number
    },
    iconSize:Number,
    isLoading:Boolean
}
// example 
// <HeaderSection withButton textButton='Save' text='YOUR REQUESTS' iconName='requests' iconButton='email' />
function HeaderSection(props: HeaderSectionProps) {
    return (
        
        <Card  {...props} >
            <View
                style={styles.container}>
                <Icons

                    name={props.iconName}
                    width={props.iconSize?props.iconSize:getWidth(28)}
                    height={props.iconSize?props.iconSize:getWidth(28)} 
                    />
                <_Text
                    numberOfLines={1}
                    style={props.textStyle} >
                    {props.text}
                </_Text>
            </View>
            {props.withButton &&
                <Button
                isLoading={props.isLoading}
                onPress={props.onPress}

                    iconStyle={ props.iconStyle||{ color: 'white',width: getWidth(15),height:getHeight(15) }}
                    text={props.textButton}
                    iconName={props.iconButton}
                    style={props.buttonStyle} />}
        </Card>
    );
}
HeaderSection.defaultProps = {
    style: {
        width: getWidth(375),
        flexDirection: 'row',
        height: getHeight(50),
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal:getWidth(15)
    },
    textStyle: {
        fontSize: 'size17',
        fontFamily: 'bold',
        color: 'darkGray1',
        paddingHorizontal: getWidth(10),
    },
    buttonStyle: {
        backgroundColor: 'gray1',
        width: getWidth(110),
        height:getHeight(35) ,
        colors: 'blue',
        textColor: 'white',
        borderRadius: 3,
        fontFamily:'bold',
        fontSize:'size13'
    }

}

export { HeaderSection };

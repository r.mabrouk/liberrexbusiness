import React, { Component } from 'react';
import {
    View,
    TextInput
} from 'react-native';
import { Card } from '../card/card';
import styles from './styles';
import { Icons } from '../../config';
import { CardProps, defaultPropsCard } from '../PropsStyles/cardProps'
import { IconsProps } from '../../config/icons'
import { getWidth, getHeight } from '../utils/dimensions';
import { fontsSize, fonts } from '../../config/fontStyle';
import { Button } from '../button/button';
import { ColorsList, Colors } from '../PropsStyles';
import get_responsive_font_size from '../utils/fontResponsive';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";


export type ReturnKeyType = 'done' | 'go' | 'next' | 'search' | 'send';
export type ReturnKeyTypeAndroid = 'none' | 'previous';
export type ReturnKeyTypeIOS = 'default' | 'google' | 'join' | 'route' | 'yahoo' | 'emergency-call';
export type ReturnKeyTypeOptions = ReturnKeyType | ReturnKeyTypeAndroid | ReturnKeyTypeIOS;

type KeyboardType = "default" | "email-address" | "numeric" | "phone-pad";
type KeyboardTypeIOS =
    | "ascii-capable"
    | "numbers-and-punctuation"
    | "url"
    | "number-pad"
    | "name-phone-pad"
    | "decimal-pad"
    | "twitter"
    | "web-search"
type KeyboardTypeAndroid = "visible-password";
type KeyboardTypeOptions = KeyboardType | KeyboardTypeAndroid | KeyboardTypeIOS;

type Props = {
    iconName: 'email' | "send" | String,
    verticalLine: Boolean,
    style: CardProps,

    iconName: 'email' | "send" | String,
    width: Number,
    height: Number,
    radius: Number,
    shadow: number,
    background: String,
    placeholder: String,
    placeholderColor: Colors,
    showIcon: Boolean,
    keyboardType: KeyboardTypeOptions,
    secureTextEntry: Boolean,
    onChangeText: Function,
    value: String,
    iconStyle: IconsProps,
    onRightPress: Function,
    autoFocus:Boolean,
    returnKeyType:ReturnKeyTypeOptions,
    onSubmitEditing:Function,
    maxLength:Number
}
function get_font(fontName) {
    return fonts[fontName]
}
function get_font_size(fontsize) {
    return get_responsive_font_size(fontsSize[fontsize])
}
function get_font_color(color) {
    return ColorsList[color] || color

}
class Input extends Component<Props> {
constructor(props){
    super(props)    
}

focus=()=>{
    this.input.focus()
}
render(){
    let {props}=this
    return(
<Card
            style={props.style}
        //    {...props.style}
        >

            <View style={{ width: props.onRightPress ? '85%' : '100%', flexDirection: 'row' }}>
                {props.showIcon && <View style={styles.iconView}>
                    <Icons name={props.iconName}
                        width={props.iconStyle ? props.iconStyle.width : getWidth(15)}
                        height={props.iconStyle ? props.iconStyle.height : getHeight(15)}
                        size={props.iconStyle ? props.iconStyle.size : 15} />
                </View>}
                {props.verticalLine &&
                    <View style={styles.verticalLine} />}
                <View
                    style={styles.inputView}>
                    <TextInput
                    maxLength={this.props.maxLength}
                    onSubmitEditing={props.onSubmitEditing}
                    ref={(ref)=>this.input=ref}
                    returnKeyType={props.returnKeyType}
                    // onFocus={}
                        autoFocus={props.autoFocus}
                        value={props.value}
                        onChangeText={props.onChangeText}
                        secureTextEntry={props.secureTextEntry}
                        keyboardType={props.keyboardType}
                        placeholderTextColor={get_font_color(props.placeholderColor) || ColorsList.rgbaBlack(.54)}
                        placeholder={props.placeholder}

                        style={{ flex: 1, paddingVertical: 0, fontSize: RFValue(fontsSize.size13), fontFamily: fonts.medium, color: ColorsList.rgbaBlack(1) }} />
                </View>
            </View>

            {props.onRightPress && <Button onPress={props.onRightPress} iconName={'eye'} iconStyle={{ color: ColorsList.rgbaGray(.5), width: getWidth(12), height: getHeight(12) }} style={{ height: '100%', width: getWidth(35), justifyContent: 'flex-end', alignItems: 'center', justifyContent: 'center' }}>
                {/* <Icons name={props.iconName} size={props.iconStyle?props.iconStyle.size:20} /> */}
            </Button>}

        </Card>
    )
}
}

Input.defaultProps = {
    style: defaultPropsCard,
    placeholder: 'default',
    placeholderColor: '#aaa',
    showIcon: true
}

export { Input };

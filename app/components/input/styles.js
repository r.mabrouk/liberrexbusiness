import { getWidth } from "../utils/dimensions";

export default styles = {
    iconView: {
        // width: '6%',
        height: '100%',
        justifyContent: 'center',
        alignItems:'center',
        marginLeft:getWidth(13)
    },
    inputView: {
       width: '100%',
        height: '100%',
        justifyContent: 'center',
        paddingLeft:getWidth(5)
    },
    verticalLine:{ height: '60%', width: 1, backgroundColor: '#aaa' }
}
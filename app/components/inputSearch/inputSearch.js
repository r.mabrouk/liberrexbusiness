import { Input } from "../input/input";

import React from 'react'
import styles from "./styles";
import { getWidth, getHeight } from "../utils/dimensions";

type InputSearchProps={
    placeholder:String,
    onChangeText:Function,
}
export const InputSearch=(props:InputSearchProps)=>{
    return(
        <Input
        onChangeText={props.onChangeText}
        style={styles.search(props.marginTop)}
        iconName={'search'}
        placeholder={props.placeholder}
        iconStyle={{ width: getWidth(15),height:getHeight(15) }}

        verticalLine={false}

    />
    )
}
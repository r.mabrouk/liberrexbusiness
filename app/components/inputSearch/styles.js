import { getWidth, getHeight } from "../utils/dimensions";

export default styles={
    search:(marginTop)=>(
        {
            width: getWidth(345),
            height: getHeight(41),
            shadow: 6, backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
            marginTop: marginTop||getHeight(15),
            borderRadius: 3
        }
    ) ,
}
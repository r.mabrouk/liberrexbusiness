import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Animated } from 'react-native';
import { keyframes, stagger } from 'popmotion'
import { ColorsList } from '../PropsStyles';
import { getWidth } from '../utils/dimensions';

const COUNT = 5
const initialPhase = { scale: 0, opacity: 1 }
const constructAnimations = () => [...Array(COUNT).keys()].map(() => (initialPhase))
export default class MapMarkerWithAnimation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animations: constructAnimations()
        };
    }
    animatioCircles = () => {
        const keyframes_ = Array(COUNT).fill(
            keyframes({
                values: [initialPhase, { scale: 2, opacity: 0 }],
                duration: 4000,
                loop: Infinity,
                yoyo: Infinity
            })
        )
        stagger(keyframes_, 4000 / 5).start(animations => {
            this.setState({ animations })
        })
    }
    componentWillMount() {
        this.animatioCircles()
    }
    render() {
        return (
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                {this.state.animations.map(({ scale, opacity }, index) => {
                    return (<Animated.View style={{ borderRadius: getWidth(50), opacity: opacity, backgroundColor: ColorsList.blue, transform: [{ scale: scale }], width: getWidth(100), height: getWidth(100), position: 'absolute' }}>

                    </Animated.View>)
                })}
                <View style={{ width:  getWidth(25), height:  getWidth(25), borderRadius:getWidth(25/2), backgroundColor: ColorsList.blue, borderColor: 'white'}}></View>
            </View>
        );
    }
}

import * as React from 'react';
import {
    View
} from 'react-native';
import { Card } from '../card/card';
import { CardProps } from '../PropsStyles'
import { _Text } from '../text/text';
import { Icons } from '../../config'
import styles from './styles';
import { Button } from '../button/button';
import { getWidth, getHeight } from '../utils/dimensions';


export type PendingItemProps = {
    style: CardProps,
    onPress: Function,
    name: String,
    service: String,
    date: String,
    loadingApproveQueueRequest: Boolean,
    loadingDeclineQueueRequest: Boolean,
}


function PendingItem(props: PendingItemProps) {
    return (
        <Card style={styles.container} >
            <Card
                style={styles.buttonStyle}
            >
                <View
                    style={styles.viewIcon}>
                    <Icons
                        name='user'
                      width={getWidth(42)}
                      height={getWidth(42)}
                    />
                </View>
                <View style={styles.dateContainerStyle}>
                    <View style={styles.queueTextView}>
                        <_Text
                            numberOfLines={1}
                            style={styles.queueText} >
                            {props.name}
                        </_Text>
                    </View>
                    <View style={styles.dateAndTimeStyle}>

                        <View
                            style={styles.queueStyle}>
                            <Icons
                                name='grid'
                                width={getWidth(18)}
                                height={getWidth(18)}
                            />
                            <_Text
                                numberOfLines={1}
                                style={styles.textQueue.timeText} >
                                {props.service}
                            </_Text>
                        </View>
                        <View
                            style={styles.timeContainer}>
                            <Icons
                                name='clock'
                                width={getWidth(18)} height={getWidth(18)}
                            />
                            <_Text
                                numberOfLines={1}
                                style={styles.textQueue.timeText} >
                                {props.date}
                            </_Text>
                        </View>
                    </View>
                </View>
                <Button
                    style={styles.viewIcon}
                    iconStyle={{ width: getWidth(18),height:getHeight(18) }}

                    iconName='more'
                    onPress={props.onPress}
                />
            </Card>

            <Card
                style={styles.statusCards} >
                <StatusItem
                    zIndex={25}
                    text='Approve'
                    iconName='doneGreen'
                    onPress={props.onApprovePress}
                    isLoading={props.loadingApproveQueueRequest}
                />
                <StatusItem
                    zIndex={2}
                    text='Decline'
                    iconName='cancel'
                    onPress={props.onDeclinePress}
                    isLoading={props.loadingDeclineQueueRequest}
                />
                <StatusItem
                    zIndex={23}
                    text='Redirect'
                    iconName='redirect'
                    onPress={props.onRedirectPress}
                    isLoading={false}
                />
            </Card>
        </Card>

    );
}
const StatusItem = ({ text, iconName, onPress, zIndex, isLoading }) => {
    return (
        <Button
            loaderColor='blue'
            isLoading={isLoading}
            style={styles.statusButton(zIndex)}
            iconStyle={{ width: getWidth(18),height:getHeight(18) }}

            text={text}
            iconName={iconName}
            onPress={onPress}
        />
    )
}
PendingItem.defaultProps = {
    status: 'Pending',
    name: 'Remon Nabil',
    service: 'Hair Cut',
    date: '25 June,2019 4:00PM',
    loadingApproveQueueRequest: false,
    loadingDeclineQueueRequest: false,
}

export { PendingItem };
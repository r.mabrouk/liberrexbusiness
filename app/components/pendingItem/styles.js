import { metrics } from "../../config";
import { getHeight, getWidth, Width } from "../utils/dimensions";

export default styles = {
    container: {
        width: getWidth(375),
        height: getHeight(116),
        shadow:6,
        backgroundColor: 'white',
        marginBottom: getHeight(15),
    },
    buttonStyle: {
        width: "100%",
        height:getHeight(72) ,
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 0.2,
        borderBottomColor: '#C9C9C9',
    },
    dateContainerStyle: {
        width: '70%',
        height: '90%',
        justifyContent: 'flex-start',
        // backgroundColor: 'blue'
    },
    dateAndTimeStyle: {
        width: '100%',
        height: '40%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop:getHeight(7),
        // backgroundColor: 'black'
    },
    queueStyle: {
        width: '38%',
        flexDirection: 'row',
        // justifyContent: 'space-between',
        alignItems: 'center',
        // backgroundColor: 'green'
    },
    statusButton:(zIndex)=>( {
        width: Width/3,
        height: getHeight(44),
        shadow: 3,
        zIndex:zIndex,
        // borderWidth:1,
        // borderColor:'lightGray1',
        backgroundColor:'white',
        textColor: 'darkGray1',
        fontFamily: 'bold',
        alignItems:'center',justifyContent:'center'
    }),
    textQueue: {
        numText: {
            fontSize: 'regular',
            fontFamily: 'bold',
            color: 'blue'
        },
        statusText: {
            fontSize: 'small13Px',
            fontFamily: 'italic',
            color: 'Gray0'
        },
        timeText: {
            marginLeft: '2%',
            width: '87%',
            fontSize: 'small13Px',
            fontFamily: 'regular',
            color: 'lightGray1',
            // backgroundColor: 'white',
        }
    },
    timeContainer: {
        width: '61%',
        // justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        // backgroundColor: 'yellow'
    },
    statusCards: {
        width: "100%",
        height: getHeight(44),
        flexDirection: 'row'
    },
    queueText: {
        fontSize: 'regular',
        fontFamily: 'bold',
        color: 'darkGray1'
    },
    viewIcon: {
        width:getWidth(61),
        height: "100%",
        justifyContent: 'center',
        alignItems: 'center',
    },
    queueTextView: {
        width: '100%',
        height: '35%',
        // alignItems: 'center',
        justifyContent: 'center',
        marginTop: '2%',
        // marginHorizontal: '2%',
        // backgroundColor: 'red',
    }
}
import * as React from 'react';
import {
    Text,
    TouchableOpacity, View, Animated, Easing, Modal, Keyboard
} from 'react-native';
import { ColorsList } from '../PropsStyles';
import { Height, Width, getWidth, getHeight } from '../utils/dimensions';
import BoxShadow from '../shadow/BoxShadow';
import { SvgIcon } from '../../config/svgIcon';
import { styles } from './styles'
import { Button } from '../button/button';
import Toast, { setModalToast } from '../toast/toast';
type PopupModalProps = {
    height: Number,
    duration: Number,
    backgroundColor: String,
    inputHeight: Number,
    inputDuration: Number
}
class PopupModal extends React.Component<PopupModalProps> {
    constructor(props) {
        super(props)
        this.state = {
            animatedMenu: new Animated.Value(-(this.props.height + getWidth(39))),
            opacityView: new Animated.Value(0),
            state: false,
            _height: new Animated.Value(this.props.height),
            topButton: new Animated.Value(0),
        }
    }
    onAnimatedInput = (toValue) => {
        let { _height, topButton } = this.state
        Animated.parallel([
            Animated.timing(_height, {
                toValue: this.props.height + toValue,
                easing: Easing.elastic(),
                duration: this.props.inputDuration
            }),
            Animated.timing(topButton, {
                toValue: -toValue,
                easing: Easing.elastic(),

                duration: this.props.inputDuration
            })
        ]).start()
    }
    componentWillMount = () => {

        Keyboard.addListener('keyboardDidShow', (e) => {
            this.onAnimatedInput(getHeight(this.props.inputHeight))

        })
        Keyboard.addListener('keyboardDidHide', (e) => {
            this.onAnimatedInput(0)

        })
    }
    render() {
        let { animatedMenu, opacityView, _height, topButton } = this.state
        let _opacity = opacityView.interpolate({
            inputRange: [0, 1],
            outputRange: [ColorsList.rgbaBlack(0),
            ColorsList.rgbaBlack(.3)]
        })
        return (

            <Modal visible={this.state.state} transparent={true}
                style={{ backgroundColor: 'rgba(255,255,255.3)' }} backdropOpacity={.5} >

                <TouchableOpacity onPress={() => this.onAnimated()} activeOpacity={1} style={{ width: '100%', height: '100%' }} />

                <Animated.View
                    pointerEvents='box-none'
                    style={[styles.container,
                    { backgroundColor: _opacity, zIndex: this.props.zIndex }]}>

                    <Animated.View
                        style={[styles.contentContainer,
                        { bottom: animatedMenu, height: this.props.height + getWidth(39 / 2) }]}>



                        <Animated.View style={[styles.contentView, { height: _height, backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : ColorsList.white }]}>
                            <View style={styles.content}>
                                {this.props.children}
                                <Toast ref={(toast) => this.ModalToast = toast} duration={1000} height={100} />
                            </View>
                        </Animated.View>


                        <Animated.View style={{
                            position: 'absolute',
                            top: topButton,
                            right: getWidth(15),
                        }} >

                            <Button
                                onPress={() => {
                                    Keyboard.dismiss()
                                    this.onAnimated(false)
                                }}
                                style={styles.closeButton}
                                iconName='cancel1'
                                iconStyle={{ width: getWidth(15), height: getHeight(15) }}
                            />
                        </Animated.View>


                    </Animated.View >

                </Animated.View>


            </Modal>

        )
    }


    onAnimated(status) {
        let { duration, height } = this.props
        let { animatedMenu, opacityView, state } = this.state

        if (status)
            this.setState({ state: status }, () => {
                setTimeout(() => {
                    setModalToast(this.ModalToast)
                }, 300)
            })
        Animated.parallel([
            Animated.timing(animatedMenu, {
                toValue: status ? (-getHeight(58)) :
                    -(this.props.height + (getWidth(100))),
                easing: Easing.elastic(),
                duration: duration ? duration : 1200
            }),
            Animated.timing(opacityView, {
                toValue: status ? 1 : 0,
                duration: duration ? duration : 1200
            })
        ]).start(() => {
            if (!status)
                this.setState({ state: false })
        })
    }
}



export { PopupModal };

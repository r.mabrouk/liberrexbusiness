import { Height, getWidth, Width } from "../utils/dimensions";
import { ColorsList } from "../PropsStyles";


export const styles = {
    container: {
        width: '100%',
        height: Height,
        position: 'absolute'
    },
    contentContainer:
    {
        width: Width,
        
        position: 'absolute'
    },
    closeButton: {
        width: getWidth(39),
        height: getWidth(39),
        borderRadius: getWidth(39 / 2),
        backgroundColor: ColorsList.white,
        shadow: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    contentView:
    {
        width: '100%',
        backgroundColor: ColorsList.white,
        bottom: 0,
        position: 'absolute'
    },
    content: {
        width: '100%',
        height: '100%'
    }

}
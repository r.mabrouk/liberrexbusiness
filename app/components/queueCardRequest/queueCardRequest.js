import * as React from 'react';
import {
    View, TouchableOpacity
} from 'react-native';
import { Card } from '../card/card';
import { CardProps } from '../PropsStyles'
import { _Text } from '../text/text';
import { Icons } from '../../config'
import styles from './styles';
import { getWidth, getHeight } from '../utils/dimensions';
import { _string } from '../../local';
import { Button } from '../button/button';


export type QueueCardRequestProps = {
    style: CardProps,
    onPress: Function,
    onMorePress: Function,
    openCard: Boolean,
    index: Number,
    currentIndex: Number,

    number: String,
    name: String,
    status: String,
    time: Object,
    joined: String,
    service: String,
    timeLeft: Object,
}


function QueueCardRequest(props: QueueCardRequestProps) {
    // let cardOpen = props.index == props.currentIndex && props.openCard
    let cardOpen = props.status == _string("Current")
    let nameLength = `${props.name.length * 5.6}%`
    console.log('${props.name.length * 5.6}%', nameLength)
    return (
        <Card style={styles.container(cardOpen)} >

            <Card style={styles.card(cardOpen)} >
                <View
                    // onPress={() => { props.onPress() }}
                    style={styles.buttonStyle}
                >

                    <View style={{ flexDirection: 'row' }}>
                        <_Text
                            numberOfLines={1}
                            style={styles.textQueue.numText}>
                            {`#${props.number}`}
                        </_Text>
                        <View style={styles.queueTextView}>
                            <_Text
                                numberOfLines={1}
                                style={styles.queueText(props.name.length > 9 ? JSON.stringify({ width: `${props.name.length * 5.5}%` }) : {})}>
                                {props.name}
                            </_Text>
                            <_Text
                                style={styles.textQueue.statusText} >
                                {props.status}
                            </_Text>

                        </View>
                    </View>




                    <View
                        style={styles.timeContainer}>
                        <Icons
                            name='clock'
                            width={getWidth(16)}
                            height={getWidth(16)}
                        />
                        <_Text
                            style={styles.textQueue.timeText} >
                            {/* 01:00 PM */}
                            {`${props.time.hour}:${props.time.minute}:00`}
                        </_Text>
                        <TouchableOpacity
                            onPress={props.onMorePress}>
                            <Icons
                                name='more'
                                width={getWidth(18)}
                                height={getWidth(18)}
                            />
                        </TouchableOpacity>
                    </View>




                </View>

                {cardOpen &&
                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <Card style={styles.cardStyle}>
                            <Icons name={'calendar'} width={getWidth(15)} height={getWidth(15)} />
                            <_Text style={styles.textQueue.expandText}>{`${_string("JoinedQueueAt")} ${props.joined}`}</_Text>
                        </Card>
                        <View style={{ marginTop: getHeight(15) }}>
                            <Card style={styles.cardStyle}>
                                <Icons name={'grid'} width={getWidth(15)} height={getWidth(15)} />
                                <_Text style={styles.textQueue.expandText}>{props.service}</_Text>
                            </Card>
                        </View>

                        <View style={styles.separationLine} />

                        <Card
                            style={styles.movingCard} >

                            <_Text style={{
                                fontFamily:'medium',
                                fontSize:'size13',
                                color:'darkGray1'
                            }}>{_string("TimeleftServiceTime")}</_Text>

                            <View style={styles.timesView}>
                                <TimeItem
                                    time={props.timeLeft.hour}
                                    timing={_string("Hours")} />
                                <TimeItem
                                    time={props.timeLeft.minute}
                                    timing={_string("Minutes")} />
                                {/* <TimeItem
                                    time={'60'}
                                    timing='Seconds' /> */}
                            </View>
                        </Card>
                    </View>}
            </Card>


            {cardOpen && <View style={{ width: '100%', position: 'absolute', alignItems: 'center', top: getHeight(164.5) }}>
                <Button
                    isLoading={props.loadignPushBack}
                    onPress={props.onPushBackPress}
                    style={{
                        width: getWidth(110),
                        height: getHeight(35),
                        textColor: '#fff',
                        shadow: 6,
                        colors: 'blue',
                        borderRadius: 3,
                        fontFamily: 'bold',
                        fontSize: 'size13',
                    }}
                    text={_string("PushBack")}
                    iconName={"next"}
                    iconStyle={{rotate:true,width:getWidth(15),height:getWidth(15)}}
                />
            </View>
            }
        </Card>

    );
}
const TimeItem = ({ time, timing }) => {
    return (
        <_Text
            style={styles.timeItem.textTime}>
            {time + ' '}
            <_Text
                style={styles.timeItem.timingText}>
                {timing}
            </_Text>
        </_Text>
    )
}
QueueCardRequest.defaultProps = {
    status: 'Pending',
}

export { QueueCardRequest };
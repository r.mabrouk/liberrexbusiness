import { metrics } from "../../config";
import { Width, getWidth, getHeight } from "../utils/dimensions";
import { ColorsList } from "../PropsStyles";

export default styles = {
    container: (cardOpen) => ({
        width: getWidth(375),
        height: cardOpen ? getHeight(200) : getHeight(52),
        shadow: 6,
        // backgroundColor: 'white',
        // marginVertical: '3%',
        marginTop: getHeight(15),
        // alignItems: 'center'
    }),
    card: (cardOpen) => ({
        width: '100%',
        height: cardOpen ? getHeight(182) : getHeight(52),
        backgroundColor: 'white',
        // marginVertical: '3%',
        // marginTop: getHeight(12),
        alignItems: 'center'
    }),
    buttonStyle: {
        width: getWidth(345),
        height: getHeight(52),
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        // paddingHorizontal:getWidth(15)
        // borderBottomWidth: 0.2,
        // borderBottomColor: '#C9C9C9'
    },
    timeItem: {
        textTime: {
            fontSize: 'size17',
            fontFamily: 'bold',
            color: 'darkGray1'
        },
        timingText: {
            fontSize: 'size11',
            color: 'lightGray',
            fontFamily:'medium'
        }
    },
    textQueue: {
        numText: {
            fontSize: 'size18',
            fontFamily: 'bold',
            color: 'blue'
        },
        statusText: {
            marginLeft: getWidth(3),
            fontSize: 'size11',
            fontFamily: 'medium',
            color: 'lightGray1',
            // backgroundColor: 'yellow'
        },
        timeText: {
            fontSize: 'small',
            fontFamily: 'regular',
            color: 'lightGray1',paddingHorizontal:getWidth(7)
        },
        expandText: {
            marginLeft: getWidth(5),
            width: '90%',
            fontSize: 'small',
            fontFamily: 'medium',
            color: 'lightGray1',
            // backgroundColor: 'red'
        }
    },
    timeContainer: {
    
        alignItems: 'center',
        flexDirection: 'row',
        // marginHorizontal: '5%',
        // backgroundColor: 'black',
    },
    movingCard: {
        width: getWidth(345),
        height: getHeight(52),
        flexDirection: 'row',
       justifyContent: 'space-between',
        alignItems: "center"
    },
    cardStyle: {
        width: getWidth(345),
        height: getHeight(15),
        flexDirection: 'row',
        alignItems: "center",
    },
    separationLine:{
        width: '100%',
        height: getHeight(1),
        marginTop: getHeight(15),
        backgroundColor: ColorsList.rgbaBlack(0.05)
    },
    timesView: {
        marginLeft: '5%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: getWidth(120),
        height: '70%'
    },
    timesContainerView: {
        width: '95%',
        height: '50%',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    queueText:(textWidth)=>({

        fontSize: 'size15',
        fontFamily: 'bold',
        color: 'darkGray1',
        // backgroundColor:"red"
    }),
    viewIcon: {
        width: '16%',
        height: '100%',
        justifyContent: 'center',
        // alignItems: 'center',
        // backgroundColor: 'blue',

    },
    statusContainer: {
        width: '100%',
        height: '50%',
        shadow: 6,
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    queueTextView: {
    paddingHorizontal:getWidth(10),
        height: '100%',
        alignItems: 'center',
        // justifyContent: 'space-between',
        flexDirection: 'row',
        // backgroundColor: 'red',
    }
}
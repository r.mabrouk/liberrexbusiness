import * as React from 'react';
import {
    View, Text,
    TouchableOpacity
} from 'react-native';
import { Card } from '../card/card';
import { CardProps } from '../PropsStyles'
import { _Text } from '../text/text';
import { IconsName, Icons } from '../../config'
import { Status } from '../status/status';
import styles from './styles';
import { getWidth, getHeight } from '../utils/dimensions';
import { _string } from '../../local';
export type QueueItemProps = {
    style: CardProps,
    children: React.ReactChild,
    status: 'Pending' | 'Wating',
    requestType?: String,
    iconName: IconsName,
    onPress: Function,
    title: String,
    description: String,
    pending: Number,
    rejected: Number,
    total_waiting: Number,
    waiting_time: Object,
    onEditQueue:Function
}


function QueueItem(props: QueueItemProps) {
    return (
        <TouchableOpacity activeOpacity={.95} onPress={props.onPress}>

            <Card style={styles.container}  >
                <Card
                    style={styles.statusContainer} >
                    <View
                        style={styles.viewIcon}>
                        <Icons
                            name='queueIcon2'
                            width={getWidth(42)} height={getHeight(42)} />
                    </View>

                    <View style={styles.queueTextView}>


                        <View style={{ flexDirection: 'row',width:'100%',justifyContent:'space-between',paddingRight:getWidth(15) }}>

                            <_Text

                                numberOfLines={1}
                                style={styles.queueText} >
                                {`${props.title}${props.description ? ` (${props.description})` : ''}`}
                            </_Text>
                            <TouchableOpacity
                                onPress={props.onEditQueue}
                                style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                                <Icons name={'more'} width={getWidth(18)} height={getWidth(18)} color={'lightGray'} />
                            </TouchableOpacity>

                        </View>


                        <View
                            style={styles.statusView}>
                            <Status
                                value={props.pending}
                                status={_string("Pending")} />
                            <Status
                                value={props.rejected}
                                status={_string('Rejected')} />
                            <Status
                                value={props.waiting_time}
                                status={_string('Wating')} />
                        </View>
                    </View>




                </Card>
                <View style={styles.timesContainerView}>
                    <_Text style={styles.totalWaitingTimeText}>
                        {_string("TotalWaitingTime")}
                    </_Text>
                    <View style={styles.timesView}>
                        <TimeItem
                            time={props.total_waiting.hour}
                            timing={_string('Hours')} />
                        <TimeItem
                            time={props.total_waiting.minute}
                            timing={_string('Minutes')} />
                        {/* <TimeItem
                        time={props.total_waiting.second}
                        timing='Seconds' /> */}


                    </View>

                </View>

            </Card>
        </TouchableOpacity>
    );
}
const TimeItem = ({ time, timing }) => {
    return (
        <_Text
            style={styles.timeItem.textTime}>
            {time + ' '}
            <_Text
                style={styles.timeItem.timingText}>
                {timing}
            </_Text>
        </_Text>
    )
}
QueueItem.defaultProps = {

    status: 'Pending',
}

export { QueueItem };

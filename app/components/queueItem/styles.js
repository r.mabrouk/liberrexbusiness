import { getHeight, Width, getWidth } from "../utils/dimensions";

export default styles = {
    timeItem: {
        textTime: {
            fontSize: 'size17',
            fontFamily: 'bold',
            color: 'darkGray1'
        },
        timingText: {
            fontSize: 'size11',
            color: 'lightGray',
            fontFamily:'medium'
        }
    },
    timesView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: getWidth(120)
    },
    totalWaitingTimeText: {
        fontSize: 'size13',
        color: 'darkGray1',
        fontFamily: 'medium',

    },
    container: {
        width: "100%",
        height:getHeight(113),
        shadow: 6,
        marginTop: getHeight(15),
        backgroundColor: 'white',
        alignItems: 'center'
    },
    timesContainerView: {
        width:Width,
        height: '40%',
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal:getWidth(15)
    },
    statusView: {
        width: '80%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 7
    },
    queueText: {
        fontSize: 'size15',
        fontFamily: 'bold',
        color: 'darkGray1',paddingRight:getHeight(10),
        width:getWidth(250)
    },
    viewIcon: {
        width: '20%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    statusContainer: {
        width: '100%',
        height: getHeight(72),
        shadow: 6,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    queueTextView: {
        width: '80%',
        height: '100%',
        justifyContent: 'center'
    }
}
import * as React from 'react';
import {
    View
} from 'react-native';
import { CardProps, ColorsList } from '../PropsStyles'
import { _Text } from '../text/text';
import styles from './styles';
import { getHeight, Width } from '../utils/dimensions';
import StarRating from 'react-native-star-rating';

export type RatingProps = {
    style: CardProps,
    rating:Number
}


function Rating(props: RatingProps) {
    return (
        <View style={[styles.ratingView,props.style]}>
            <StarRating
                disabled={true}
                maxStars={5}
                rating={props.rating}
                starSize={getHeight(18)}
                emptyStar={'star'}
                fullStar={'star'}
                iconSet={'FontAwesome'}
                fullStarColor='#FBBC05'
                emptyStarColor={ColorsList.rgbaOrange(.2)}
                containerStyle={{ width: Width * 0.32 }}
                starStyle={{}}
                selectedStar={{}}
            />
        </View>
    );
}
Rating.defaultProps = {

}

export { Rating };
import { getHeight, getWidth } from '../../components/utils/dimensions';

const styles = {
    ratingView: {
        width: getWidth(345),
        height: getHeight(30),
        marginBottom: getHeight(5)
    }
};
export default styles;
import * as React from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image,
    Text
} from 'react-native';
import { CercleProgressProps, ColorsList } from '../PropsStyles'

import { _Text } from '../text/text';
import { IconsName } from '../../config'
import { getWidth, getHeight } from '../utils/dimensions';
import { Status } from '../status/status';
import CercleProgress from '../cercleProgress/cercleProgress';

type ProgressProps = CercleProgressProps
export type ReportRequestProps = {
    status: 'Accepted' | 'Canceled' | 'Rejected' | 'Waiting' | 'Hours Left' | 'good' | 'average' | 'bad',
    usedValue: Number,
    totalValue: Number,
    requests: Number,
    notPercentage: Boolean,
    withStatusDot: Boolean
}


function ReportRequestItem(props: ReportRequestProps) {
    console.log("dsewdStatus = ", props.status)
    let { cercleProps } = props
    return (
        <View style={{ width: getWidth(108), alignItems: 'center' }}>
            <CercleProgress
                size={getWidth(106)}
                duration={1000}
                {...props}
                backgroundWidth={9}
                CercleWidth={9}
                color={(props.status == 'Canceled') ?
                    'gray0' : (props.status == 'Rejected' || props.status == 'bad') ?
                        'red' : (props.status == 'Hours Left' || props.status == 'average') ?
                            'orange' : (props.status == 'Accepted' || props.status == 'Waiting' || props.status == 'good') ?
                                'green' : 'black'}
            >
                <_Text
                    style={{ fontFamily: 'bold', color: 'darkGray1', fontSize: 'medium15Px' }}>
                    {props.notPercentage ? props.usedValue + '-' + props.totalValue : getPerscent(props.usedValue,
                        props.totalValue) + '%'}
                </_Text>
                <_Text style={{fontFamily:'regular',fontSize:'size15',color:ColorsList.rgbaBlack(.54)}}>{props.status}</_Text>
            </CercleProgress>
            {props.requests && <_Text
                style={{ fontFamily: 'bold', marginTop: getHeight(10) }}>
                {props.requests} Requests
            </_Text>}
            {props.withStatusDot && <Status
                status={props.status == 'Accepted' ?
                    'Wating' : props.status == 'Canceled' ?
                        'Pending' : props.status}
                style={{ marginTop: 3 }}
            />}
        </View>
    );
}
function getPerscent(usedValue, totalValue) {
    return Math.floor((usedValue / totalValue) * 100)
}
ReportRequestItem.defaultProps = {
    status: true,
    style: {

    },
    cercleProps: {

    }
}

export { ReportRequestItem };

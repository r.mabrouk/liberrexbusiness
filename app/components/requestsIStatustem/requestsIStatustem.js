import * as React from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image,
    Text
} from 'react-native';
import { Colors, ColorsList } from '../PropsStyles/colors'
import { Card } from '../card/card';
import { CardProps } from '../PropsStyles'
import { Icons } from '../../config';
import { fonts } from '../../config/fontStyle';
import { _Text } from '../text/text';
import {IconsName} from '../../config'
import { getWidth } from '../utils/dimensions';
export type RequestsIStatustemProps = {
    style: CardProps,
    children: React.ReactChild,
    status: 'Pending' | 'Wating',
    requestType?: String,
    iconName:IconsName
}

function StatusItem({ status, text }) {
    return (
        <View style={{ width: '90%', flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ width: 10, height: 10, backgroundColor: ColorsList[status ? 'green' : 'gray1'], borderRadius: 5 }} />
            <_Text style={{ fontFamily: "regular", color: 'black' }}>  {"15  "}
                <_Text style={{ fontFamily: "regular", fontSize: 'small1', color: 'gray0' }} >
                    {text}
                </_Text>
            </_Text>
        </View>
    )
}
function RequestsIStatustem(props: RequestsIStatustemProps) {
    return (
        <Card  {...props} >
            <Card style={{ width: '100%', shadow: 4, height: '63%', 
            flexDirection: 'row', alignItems: 'center' }} >
                <View style={{ width: '40%', height: '100%', 
                alignItems: 'center', justifyContent: 'center' }} >
                    <Icons name={props.iconName} width={getWidth(32)} height={getWidth(32)} />
                </View>
                <View style={{ width: '60%', height: '60%', justifyContent: 'space-between' }} >
                    <StatusItem
                        text={'Pending'}
                        status={props.status == 'Pending'} />
                    <StatusItem
                        text={'Wating list'}
                        status={props.status == 'Wating'} />
                </View>
            </Card>
            <View style={{ width: '100%', height: '37%', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontFamily: fonts.bold, fontSize: 15 }}>{props.requestType}</Text>
            </View>
        </Card>
    );
}
RequestsIStatustem.defaultProps = {

    status: 'Pending',
}

export { RequestsIStatustem };

import * as React from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image,
    Text, TouchableOpacity
} from 'react-native';
import { Colors, ColorsList } from '../PropsStyles/colors'
import { Card } from '../card/card';
import { CardProps } from '../PropsStyles'
import { Icons } from '../../config';
import { fonts } from '../../config/fontStyle';
import { _Text } from '../text/text';
import { IconsName } from '../../config'
import styles from './styles';
import { Button } from '../button/button';
import { getWidth, getHeight } from '../utils/dimensions';
export type RequestsStatusItemProps = {
    style: CardProps,
    children: React.ReactChild,
    status: 'Pending' | 'Wating',
    requestType?: String,
    iconName: IconsName,
    onPress: Function,
    watingValue: Number,
    pendingValue: Number
}

function StatusItem({ status, text, value,color }) {
    return (
        <View
            style={styles.statusItem.container}>
            <View
                style={styles.statusItem.status(color)} />
            <_Text
                style={styles.statusItem.textValue}>  {`${value} `}
                <_Text
                    style={styles.statusItem.statusText} >
                    {text}
                </_Text>
            </_Text>
        </View>
    )
}

// example
{/* <RequestsStatusItem
iconName='queueIcon'
status='Wating'
requestType={'Queues'}
style={{ width: '47%',
 height: metrics.screenHeight * .17,
 shadow: 5,
  borderRadius: 3,
 backgroundColor: 'white' }} 
/> */}
function RequestsStatusItem(props: RequestsStatusItemProps) {
    return (
        <TouchableOpacity onPress={props.onPress} activeOpacity={.9}>

            <Card
                style={props.style}  >

                <Card
                    style={styles.contentView} >
                    <View
                        style={styles.iconView} >
                        <Icons
                        width={getWidth(42)}
                        height={getWidth(42)}
                            name={props.iconName}
                             />
                    </View>
                    <View
                        style={styles.statusContent} >
                        <StatusItem
                        
                          color={props.Colors[0]}
                            value={props.pendingValue}
                            text={props.titles[0]}
                            status={props.status == 'Pending'} />
                        <StatusItem
                           color={props.Colors[1]}
                           text={props.titles[1]}
                            value={props.watingValue}
                            status={props.status == 'Wating'} />
                    </View>
                </Card>
                <View style={styles.requestTypeView}>
                    <_Text style={styles.requestTypeText} >
                        {props.requestType}</_Text>
                </View>
            </Card>
        </TouchableOpacity>

    );
}
RequestsStatusItem.defaultProps = {

    status: 'Pending',
}

export { RequestsStatusItem };

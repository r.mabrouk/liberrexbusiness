import { ColorsList } from "../PropsStyles";
import { getHeight } from "../utils/dimensions";
export default styles = {
    statusItem: {
        container:
        {
            width: '90%', flexDirection: 'row',
            alignItems: 'center'
        },
        status: (color) => ({
            width: 10,
            height: 10,
            backgroundColor: color,
            borderRadius: 5
        }),
        textValue: {
            fontFamily: "bold",
            color: ColorsList.rgbaBlack(.87),
            fontSize: 'size13',

        },
        statusText: {
            fontFamily: "regular",
            fontSize: 'size11',
            color: ColorsList.rgbaBlack(.54)
        }

    },
    statusContent: {
        width: '60%',
        height: '60%',
        justifyContent: 'space-between'
    },
    iconView: {
        width: '40%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    contentView: {
        width: '100%',
        shadow: 6,
        height:getHeight(72),
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    requestTypeView: {
        width: '100%',
        height: '37%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    requestTypeText: {
        fontFamily: 'bold',
        fontSize: 'size15',
        color:'darkGray1'
    }
}
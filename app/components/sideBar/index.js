
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import SideBarView from './sideBarView';
import { connect } from 'react-redux'
import { requestLogout } from '../../actions/auth';

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  onDrower = () => {
    this.SideBarView.onDrower()
  }

  onLogout=()=>{
    this.props.onLogout()
  }
  render() {
    return (
      <SideBarView
      {...this}
        ref={(SideBarView) => this.SideBarView = SideBarView}
        {...this.props} />
    );
  }
}


function mapStateToProps(state) {
  return {
    user_profile: state.authReducer.user_profile,
    isLogoutLoading:state.loadingReducer.isLogoutLoading,
  }
}
function mapDispatchToProps(Dispatch) {
  return {
    onLogout: () => Dispatch(requestLogout())
  }
}


export let Drower = null
export let drowerSideBar = (statu) => {
  Drower = statu
}

export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(SideBar)





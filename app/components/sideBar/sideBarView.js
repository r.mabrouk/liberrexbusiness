
import React, { Component } from 'react';
import { View, ImageBackground, Image, Animated, Easing, TouchableOpacity } from 'react-native';
import { Width, Height, getWidth, getHeight } from '../utils/dimensions';
import { images } from '../../config';
import { Card } from '../card/card';
import { _Text } from '../text/text';
import { Button } from '../button/button';
import SettingsComponent from '../SettingsComponent/SettingsComponent';
import styles from './styles'
import { connect } from 'react-redux'

import NavigationService from '../../navigation/NavigationService';
import ApiConstants from '../../api/ApiConstants';
import { requestLogout } from '../../actions/auth';
import { _string } from '../../local';
import { isIOs } from '../utils/functions';
import { ActionCreators } from '../../actions';
import { setMenuState } from '../../actions/uiActions';
class SideBarView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            drowerWidth: new Animated.Value(0),
            scaleYScreen: new Animated.Value(1),
            marginTopScreen: new Animated.Value(0),
            state: true
        }
    }
    componentDidMount() {
        // this.onDrower()
        //  this.openBottomMenu.onAnimated(true)

    }


    //     onAnimated(status) {
    //         let {duration,height}=this.props
    //         let { animatedMenu,
    //             opacityView } =
    //             this.state

    //         Animated.parallel([
    //             Animated.timing(animatedMenu, {
    //                 toValue: status ? (0 ) :
    //                     -height,
    //                 easing: Easing.elastic(),
    //                 duration:duration?duration: 1200
    //             })
    //             ,
    //             Animated.timing(opacityView, {
    //                 toValue: status ? 1 : 0,
    //                 duration:duration?duration: 1200
    //             })
    //         ]).start()
    //     }
    // }


    onDrower = () => {
        let { state } = this.state
        this.setState({ state: !this.state.state }, () => {
            this.props._setMenuState(!this.state.state)
            Animated.timing(this.state.drowerWidth, {
                toValue: state ? getWidth(310) : getWidth(0), duration: 400, easing: state ? Easing.elastic() : Easing.back()
            }).start()
            Animated.timing(this.state.scaleYScreen, {
                toValue: state ? .95 : 1, duration: 500, easing: state ? Easing.elastic() : undefined,
            }).start()
            Animated.timing(this.state.marginTopScreen, {
                toValue: state ? Height * .01 : 0, duration: 500
            }).start()
        })

    }

    render() {
        // console.disableYellowBox = true;
        let { name, user_profile, onLogout, isLogoutLoading } = this.props

        let { user, business } = user_profile
        return (

            <ImageBackground
                resizeMode='cover'
                source={images.loginbackground}
                style={{ width: Width, height: Height + (isIOs() ? 0 : getHeight(58)), flexDirection: 'row', zIndex: 0 }}>

                <Animated.View style={{ width: this.state.drowerWidth, alignItems: 'center' }}>

                    <Card style={styles.subHeaderSectionContainer}>
                        <Card style={styles.imageContainer}>
                            <Image source={!user.photo ? images.user_profile : { uri: ApiConstants.IMAGE_BASE_URL + user.photo }}
                                style={{ width: '100%', height: '100%', borderRadius: 43 }} resizeMode="cover" />
                        </Card>
                        <Card style={{ width: getWidth(160), marginLeft: 15 }} >
                            <_Text style={styles.name}>{user.fname + " " + user.lname}</_Text>
                            <_Text style={styles.job}>{user.position}</_Text>
                            <Card style={{ width: '100%', marginTop: getHeight(10) }} >
                                <Button
                                    onPress={() => {
                                        NavigationService.navigate("EditProfile")
                                        this.onDrower()
                                    }}
                                    iconStyle={{ width: getWidth(12), height: getHeight(12) }}
                                    iconName={'editpen'}
                                    text={_string("side_menu.edit_my_Profile")}
                                    style={styles.subHeaderButton}
                                />

                            </Card>
                        </Card>


                    </Card>



                    <Card style={styles.optionsContainer}>

                        <SettingsComponent onPress={() => {
                            NavigationService.navigate("Report")
                            this.onDrower()
                        }} title={_string("side_menu.Statistics_and_Reports")}
                            leftIcon={'report'} />
                        <SettingsComponent
                            onPress={() => {
                                this.onDrower()
                                NavigationService.navigate("NewAppointment")
                            }}
                            title={_string("side_menu.new_appointment")}
                            leftIcon={'appointmentsgray'}
                        />
                        <SettingsComponent
                            onPress={() => {
                                this.onDrower()
                                NavigationService.navigate("QueueEdit")
                            }}
                            title={_string("side_menu.new_queue")}
                            leftIcon={'queueIconGray'}
                        />
                        <SettingsComponent
                            onPress={() => {
                                this.onDrower()
                                NavigationService.navigate("YourService")
                            }}
                            title={_string("side_menu.your_services")}
                            leftIcon={'grid'} />
                        <SettingsComponent
                            onPress={() => {
                                this.onDrower()
                                NavigationService.navigate("CustomerList")
                            }}
                            title={_string("side_menu.customers_list")}
                            leftIcon={'reports'}
                        />
                        <SettingsComponent
                            onPress={() => {
                                NavigationService.navigate("CustomerFeedback")
                                this.onDrower()
                            }}
                            title={_string("side_menu.customers_feedback")}
                            leftIcon={'feedback'}
                        />
                    </Card>

                    <Card
                        style={{ alignItems: 'center', width: getWidth(345), justifyContent: 'center', paddingVertical: getHeight(15), marginTop: getHeight(25) }}>
                        <_Text style={styles.logoutText}>{_string("side_menu.app_version").toLocaleUpperCase()}</_Text>
                        <Button
                            loaderColor={'red'}
                            isLoading={isLogoutLoading}
                            iconName={'login'}
                            onPress={onLogout}
                            text={_string("side_menu.logout")}
                            iconStyle={{ width: getWidth(18), height: getHeight(28) }}

                            style={styles.logoutButton}
                        />
                    </Card>
                </Animated.View>

                <Animated.View style={{
                    width: Width, height: Height + getHeight(58), backgroundColor: 'red', transform: [{ scaleY: this.state.scaleYScreen }],
                    shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.2, shadowRadius: 10, top: this.state.marginTopScreen
                }}>

                    {this.props.children}

                    {!this.state.state && <TouchableOpacity
                        onPress={() => {
                            this.onDrower()

                        }}
                        activeOpacity={1} style={{
                            width: '100%', height: '100%',
                            position: 'absolute'
                        }}>

                    </TouchableOpacity>}
                </Animated.View>
            </ImageBackground>



        );
    }
}

function mapStateToProps(state) {
    return {
        user_profile: state.authReducer.user_profile,
        menuState: state.uiReducer.menuState
    }
}

function mapDispatchToProps(Dispatch) {
    return {
        _setMenuState: (state) => Dispatch(setMenuState(state))
    }
}




export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(SideBarView)





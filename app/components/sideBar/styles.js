import { getHeight, getWidth,Width,Height } from "../utils/dimensions";

export default styles ={
    job: {
        fontSize: 'size11',
        color: 'white',
        fontFamily:'bold',
    },
    addressContainer: {
        flexDirection: 'row',
        marginTop: getHeight(7.5)
    },
    headerButton: {
        backgroundColor: 'white', width: getWidth (120),
        height: getHeight(31),
        fontSize: 'small',
        borderRadius: 3,
        alignItems: "center",
        justifyContent: 'center'

    },
    imageContainer: {
        width: getWidth(110),
         height: getWidth(110), borderRadius: getWidth(55), shadow: 6, backgroundColor: 'white'

    },
    subHeaderSectionContainer: {
        width: getWidth(290),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:getHeight(40),
        marginTop:getHeight(40)

    },
    subHeaderButton: {
        backgroundColor: 'white',
        width: '100%', height: getHeight(31),
        fontSize: 'size11',
        shadow: 6,
        borderRadius: 3, alignItems: "center",
        justifyContent: 'center',
        textColor: 'darkGray1',
        fontFamily:'medium'
    },
    name: {
        fontSize: 'size15',
        fontFamily:'bold',
        color: 'white'
    },
    optionsContainer: {
        width: getWidth(350), alignItems: 'center',
        // marginTop: getHeight(-30),
        paddingBottom: getHeight(20)
    },
    logoutButton: {
        backgroundColor: 'white',
        width: getWidth(Width/2), height: getHeight(44),
        fontSize: 'size15',
        marginTop: getHeight(10),
        shadow: 6,
        borderRadius: 3, alignItems: "center",
        justifyContent: 'center',
        fontFamily:'bold',
        textColor: 'darkGray1'
    },
    logoutText:
        { fontSize: 'small1', color: 'gray' }
}
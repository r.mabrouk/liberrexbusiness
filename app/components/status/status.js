import * as React from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image,
    Text
} from 'react-native';
import { ColorsList } from '../PropsStyles/colors'

import { _Text } from '../text/text';
import { IconsName } from '../../config'
import { getWidth } from '../utils/dimensions';
import { _string } from '../../local';
export type StatusProps = {
    // status: Boolean,
    value: String,
    status: 'Pending' | 'Wating' | 'Rejected',
    style: {
        marginTop: Number
    }
}


function Status(props: StatusProps) {
    return (
        <View style={[{ ...props.style },{ flexDirection: 'row', alignItems: 'center' }]}>
            <View style={{ width: 10, height: 10, backgroundColor: ColorsList[props.status == _string("Rejected") ? 'red' : props.status ==_string("Pending") ? 'gray1' : props.status == _string("Wating")? 'green' : ''], borderRadius: 5, justifyContent: 'center' }} />
            <_Text style={{ fontFamily: "bold", color: 'darkGray1',fontSize:'size13' }}>  {props.value && (props.value)}
                <_Text style={{ fontFamily: "regular", fontSize: 'size11', color: ColorsList.rgbaBlack(.54) }} >
                    {" "+props.status[0].toUpperCase()+props.status.slice(1, props.status.length)+" "}
                </_Text>
            </_Text>
        </View>
    );
}
Status.defaultProps = {
    status: true,
    style: {

    }
}

export { Status };

import { getHeight, Width, getWidth } from "../utils/dimensions";

export default styles = {
    buttonsView: {
        width: Width, height: getHeight(42), flexDirection: 'row', shadow: 6,zIndex:30,backgroundColor:'white'
    },
    button: (childrenLength,
        index, currentIndex,buttonWidth) =>
        ({
             width:buttonWidth?buttonWidth:getWidth(188),

            // width: Width / childrenLength,
            height: '100%',
            shadow:6,
            zIndex:index,
         
            fontFamily:'medium',
            fontSize:'size13',
            backgroundColor: 'white',
            colors: index == currentIndex ?
                'blue' : undefined,
            textColor: index == currentIndex ?
                'white' : 'black'
        })
}
import React, { Component } from 'react';
import { View, FlatList } from 'react-native'
import PropTypes from 'prop-types';
import { _Text } from '../text/text';
import Swiper from 'react-native-swiper'
import { isIOs } from '../utils/functions';
import styles from './styles';
import { Width, getHeight, getWidth } from '../utils/dimensions';
import { Button } from '../button/button';
import { Card } from '../card/card';
import { IconsName } from '../../config'

type SwiperTabButtonsProps = {
    currentTab: Array,
    tabButons: [
        {
            iconsName: IconsName,
            label: string
        }
    ]

}
class SwiperTabButtons extends Component<SwiperTabButtonsProps> {
    constructor(props) {
        super(props);
        this.state = {
            statusList: [

            ],
            openCard: false,
            currentIndex: 0,
        }
    }
    onScroll = (index) => {
        let offset = 0
        if (index == 1)
            offset = getWidth(50)
        if (index == 2)
            offset = getWidth(200)
        this.flatListRef.scrollToOffset({ animated: true, offset: offset });
    }
    render() {

        let { tabButons, currentTab } = this.props
        let { currentIndex } = this.state
        return (
            <View  style={{elevation:2.5}} >
                <Card
                    style={styles.buttonsView}>
                    <FlatList
                    
                        ref={(flatListRef) => this.flatListRef = flatListRef}
                        scrollEnabled={tabButons.length !== 2}
                        extraData={this.state}
                        data={tabButons}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        renderItem={({ item, index }) => {
                            return (
                                <Button
                                    iconStyle={{
                                        color: index == currentIndex ?
                                            'white' : 'gray1',
                                        width: getWidth(15),
                                        height: getWidth(15)
                                    }}

                                    iconName={item.iconsName}
                                    onPress={() => {
                                        if(tabButons.length>2)
                                        this.onScroll(index)
                                        if (this.state.currentIndex != index) {
                                            this._swiper.scrollBy(index - this.state.currentIndex, true);
                                        }
                                    }}
                                    text={item.label}
                                    style={styles.button(this.props.children.length, index, currentIndex, this.props.buttonWidth)} />
                            )
                        }}
                    />
                </Card>
                <Swiper
                    onIndexChanged={(index) => {
                        this.onScroll(index)
                        if (this.props.onIndexChanged) {
                            this.props.onIndexChanged(index)
                        }
                        this.setState({ currentIndex: index })
                    }}
                    loop={false}
                    ref={(_swiper) => this._swiper = _swiper}
                    // dotStyle={{ width: 10, height: 10 }}
                    // activeDotStyle={{ width: 10, height: 10 }}
                    // activeDotColor={'#A0A5AA'}
                    // dotColor={'#E4E6E8'}
                    dot={false}
                    showsPagination={false}
                    style={{ width: !isIOs() ? Width : undefined, alignItems: 'center' }} >
                    {this.props.children.map((component) => (
                        <View style={{ width: Width ,alignItems:'center'}}>
                            {component}
                        </View>

                    ))}
                </Swiper>
            </View>

        );
    }
}


SwiperTabButtons.propTypes = {
    tabButons: [

    ]
};

export default SwiperTabButtons;
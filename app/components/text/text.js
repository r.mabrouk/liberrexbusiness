import * as React from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image,
    Text
} from 'react-native';
import { Colors, ColorsList } from '../PropsStyles/colors'
import { Card } from '../card/card';
import { CardProps } from '../PropsStyles'
import { Icons } from '../../config';
import { fonts, fontsSize } from '../../config/fontStyle';
import { TextProps } from '../PropsStyles/textProps'
import get_responsive_font_size from '../utils/fontResponsive';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

export type TextPropsInClass = {
    style: TextProps,
    children: React.ReactNode,
    numberOfLines:Number
}

function get_font(fontName) {
    return fonts[fontName]
}
function get_font_size(fontsize) {
    return RFValue(fontsSize[fontsize])
}
function get_font_color(color) {
    return ColorsList[color] || color

}

function _Text(props: TextPropsInClass) {
    return (
        <Text numberOfLines={props.numberOfLines} numberOfLines={props.numberOfLines} style={[props.style, {
            fontFamily: get_font(props.style ? props.style.fontFamily : 'Lato-medium'),
            fontSize: props.style && props.style.fontSize ? get_font_size(props.style.fontSize) : undefined,
            color: get_font_color(props.style.color)
        }]}

        >
            {props.children}
        </Text>
    );
}
_Text.defaultProps = {
    style: {
        color: '#000'
    }
}

export { _Text };

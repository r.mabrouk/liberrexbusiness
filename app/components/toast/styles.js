import { Height, getWidth } from "../utils/dimensions";
import { ColorsList } from "../PropsStyles";


export const styles = {
    container: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        alignItems: 'center'
    },
    contentContainer: (state) => (
        {
            backgroundColor: state == "error" ? ColorsList.rgbaRed(.9) : state == 'Check' ?
                ColorsList.rgbaGreen(.9) :
                ColorsList.rgbaOrange(.9)
            ,

            width: '87%',
            alignItems: 'center',
            borderRadius: 100,
            position: 'absolute',
            justifyContent: 'center'
        }),
    closeButton: {
        width: getWidth(39),
        height: getWidth(39),
        borderRadius: getWidth(39 / 2),
        backgroundColor: ColorsList.white,
        shadow: 6,
        position: 'absolute',
        top: 0,
        right: getWidth(39 / 2),
        alignItems: 'center',
        justifyContent: 'center'
    },
    contentView:
    {
        width: '100%',
        bottom: 0,
        position: 'absolute'
    },
    content: {
        width: '100%',
        height: '100%'
    }

}
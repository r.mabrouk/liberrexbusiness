import * as React from 'react';
import {
    Text,
    TouchableOpacity, View, Animated, Easing, Modal
} from 'react-native';
import { ColorsList } from '../PropsStyles';
import { Height, Width, getWidth, getHeight } from '../utils/dimensions';
import BoxShadow from '../shadow/BoxShadow';
import { SvgIcon } from '../../config/svgIcon';
import { styles } from './styles'
import { Button } from '../button/button';
import { _Text } from '../text/text';
import { Icons } from '../../config';

type ToastProps = {
    height: Number,
    duration: Number
}
class Toast extends React.Component<ToastProps> {
    constructor(props) {
        super(props)
        this.state = {
            animatedToast: new Animated.Value(-getHeight(70)),
            opacityView: new Animated.Value(0),
            massage: "",
            state: "Check",
            visible: false
        }
    }


    render() {
        let { animatedToast, opacityView, massage, state } = this.state
        let _opacity = opacityView.interpolate({
            inputRange: [0, 1],
            outputRange: [ColorsList.rgbaBlack(0),
            ColorsList.rgbaBlack(.3)]
        })
        return (
            <Animated.View
                pointerEvents='box-none'
                style={[styles.container,
                { backgroundColor: _opacity, zIndex: 100 }]}>
                <Animated.View
                    style={[styles.contentContainer(state),
                    {
                        bottom: animatedToast,
                        height:
                            getHeight(42), flexDirection: 'row'
                    }]}>
                    <Icons color={'white'} name={state}
                        width={getWidth(15)} height={getWidth(15)} />

                    <_Text style={{
                        fontFamily: 'black',
                        fontSize: 'small1',
                        color: 'white',
                        paddingHorizontal: getWidth(8)
                    }}>{
                            massage}
                    </_Text>

                </Animated.View >
            </Animated.View>

        )
    }

    componentDidMount() {
        global.openToast = this.open
        // this.open()
    }

    open = (massage, state) => {
        let _state = {
            e: "error",
            s: "Check",
            w: 'info'
        }
        this.setState({ massage, state: _state[state], visible: true })
        let { animatedToast,
            opacityView } =
            this.state

        Animated.parallel([
            Animated.timing(animatedToast, {
                toValue: getHeight(70)
                ,
                easing: Easing.elastic(),
                duration: 1000
            })
            ,
            Animated.timing(opacityView, {
                toValue: 0,
                duration: 1000
            })
        ]).start(() => {
            Animated.timing(animatedToast, {
                toValue: -getHeight(70),
                easing: Easing.elastic(),
                duration: 1200, delay: 2000
            }).start()

            Animated.timing(opacityView, {
                toValue: 0,
                duration: 1000, delay: 2000
            }).start(() => {
                this.setState({ visible: false })

            })

        })
    }
}

export let _Toast = {}
export let ModalToast=null
export const setToast = (toast) => {
    _Toast = toast
}
export const setModalToast=(toast)=>{
    ModalToast = toast
}
export default Toast ;

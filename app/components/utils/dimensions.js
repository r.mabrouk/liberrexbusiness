import { Dimensions } from 'react-native'
let { width, height } = Dimensions.get('window')
let DEVICE_WIDTH = 375
let DEVICE_HEIGHT = 667
export function getWidth(_width) {
    let currenttWidth = width
    let DeviceWidth = DEVICE_WIDTH
    let perscentWidth = (_width / DeviceWidth) * 100
    return (currenttWidth / 100) * perscentWidth
}
export function getHeight(_height) {
    let currenttHeight = height
    let deviceHeight = DEVICE_HEIGHT
    let perscentHeight = (_height / deviceHeight) * 100
    return (currenttHeight / 100) * perscentHeight
}

export const Width =getWidth(DEVICE_WIDTH) 
export const FooterHeight =getHeight(58)
export const Height =getHeight(DEVICE_HEIGHT)-FooterHeight
export const SpaceHeight=getHeight(10)


import { Dimensions, StatusBar, Platform } from 'react-native';
const { height } = Dimensions.get('window');

export default function get_responsive_font_size(percent) {
  const deviceHeight = Platform.OS === 'android'
    ? height - StatusBar.currentHeight
    : height;
  const heightPercent = (percent * deviceHeight) / 730;
  return Math.round(heightPercent);
}

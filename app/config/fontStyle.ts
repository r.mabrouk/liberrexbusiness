
export type FontsType =
    'black' |
    'blackItalic' |
    'bold' |
    'boldItalic' |
    'hairline' |
    'hairlineItalic' |
    'italic' |
    'light' |
    'lightItalic' |
    'regular' | 'medium'


export type FontsSize =
    'small' | 'regular' | 'small1' | 'regular17Px' |'size12'| 'size15' | 'medium15Px' | 'small13Px' | 'size14' | 'size13'

const fontsSize = {
    small: 11,
    small1: 12,
    small13Px: 13,
    size14: 14,
    size11: 11,
    size12: 12,
    size13: 13,
    size15: 15,
    size16: 16,
    size17: 17,
    size18: 18,
    size19: 19,
    regular: 18,
    regular17Px: 17,
    medium15Px: 15
}
const fonts = {
    black: "Lato-Black",
    blackItalic: "Lato-BlackItalic",
    bold: "Lato-Bold",
    boldItalic: "Lato-BoldItalic",
    hairline: "Lato-Hairline",
    hairlineItalic: "Lato-HairlineItalic",
    italic: "Lato-medium",
    light: "Lato-Light",
    lightItalic: "Lato-LightItalic",
    regular: "Lato-Regular",
    medium: 'Lato-medium'
}

export {
    fonts,
    fontsSize
}
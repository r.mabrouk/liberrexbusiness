import React from 'react';
import { Image, Text } from 'react-native'
import { metrics } from './metrics';
import { images } from './images';
import { Colors, ColorsList } from '../components/PropsStyles/colors'
import { SvgIcon } from './svgIcon';
export type IconsName = 'logo' | "arrowDown" | "filter" | 'closeIcon' | "star" | "reports" | "cancel" | "calendarArrow" | "appointmentsgray" | "eye" | "notificationGradient" | 'email' | "info" | "error" | "queueEditNew" | "menu" | 'frinshFlag' | "pending" | "Check" | "russainFlag" | "ukFlag" | 'appointments' | 'queueIcon' | 'header' | 'arrowBack' | 'notification' | 'requests' | String
export type IconsProps = {
    name?: IconsName,
    icon: Function,
    color: Colors | String,
    size: Number,
    width: Number,
    height: Number,
    rotate:Boolean
}
function getColor(colorName) {
    return ColorsList[colorName] || colorName
}

function Icons(props: IconsProps) {
    switch (props.name) {
        
        case 'user':
            return <Image source={images[props.name]} style={IconImage(props)} />
        case 'cancel1':
            return <Image source={images[props.name]} style={IconImage(props)} />
        case 'arrow':
            return <Image source={images[props.name]} style={IconImage(props)} />
        case 'menuIcon':
            return <Image source={images[props.name]} style={IconImage(props)} />
        case 'notificationIcon':
            return <Image source={images[props.name]} style={IconImage(props)} />
        case 'queueIcon2':
            return <Image source={images[props.name]} style={IconImage(props)} />
        case 'appointmentsIcon':
            return <Image source={images[props.name]} style={IconImage(props)} />
        case 'logo':
            return <Image source={images[props.name]} style={IconImage(props)} />
        case 'header':
            return <Image source={images[props.name]} style={IconImage(props)} />
        case 'email':
            return <Image source={images[props.name]} style={IconImage(props)} />
        case 'send':
            return <Image source={images[props.name]} style={IconImage(props)} />
        case 'login':
            return <Image source={images[props.name]} style={IconImage(props)} />
        case 'frinshFlag':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'doneGreen':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'more':
            return <SvgIcon name={props.name} width={props.width} height={props.height} />
        case 'search':
            return <SvgIcon name={props.name} width={props.width} height={props.height} />
        case 'ukFlag':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'russainFlag':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'queuegray':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'report':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'reports':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'reportsEdit':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'password':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'facebook':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'location':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'twitter':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'googlepluse':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'arrowDown':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'clock':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'add':
            return <SvgIcon name={'add'} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'addwhite':
            return <SvgIcon name={'addwhite'} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'info':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'signupgray':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'signup':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'next':
            return <SvgIcon name={props.name} width={props.width} height={props.height} rotate={props.rotate} color={getColor(props.color) } />
        case 'legal':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'grid':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'user':
            return <SvgIcon name={props.name} width={props.width} height={props.height} />
        case 'userGray':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'case':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'phone':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'done':
            // return <SvgIcon name={'done'} width={props.width} height={props.height} color={getColor(props.color) } />
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'fullStar':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'paymentCardBlue':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) }/>
        case 'filter':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) }/>
        case 'membership':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'paymentCardGray':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'appointments':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'appointmentsgray':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'queueIcon':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'queueIconGray':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'notification':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'arrowBack':
            return <SvgIcon  rotate={props.rotate} name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'requests':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'list':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'clock':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'more':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'closeIcon':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'clockTime':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'trash':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'star':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'listwhite':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'calendar':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'cancel':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'redirect':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'doneGray':
            return <SvgIcon name={props.name} width={props.width} height={props.height}  color={getColor(props.color) } />
        case 'editpen':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'home':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'queueTab':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'settings':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'gridLinear':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />

        case 'appointmentTab':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />

        case 'rightArrow':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'Check':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />

        case 'Pending':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />

        case 'eye':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'menu':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'queueEditNew':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'error':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'notificationGradient':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'rules':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'membership':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'payments':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'team':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'feedback':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case "appsettings":
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case "calendarArrow":
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'lang':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'rulesBlue':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />
        case 'delete':
            return <SvgIcon name={props.name} width={props.width} height={props.height} color={getColor(props.color) } />

        default:
            return props.icon ? props.icon() : null
    }
}

const IconImage = (props) =>
    ({
        width: props.width,
        height: props.height,
        resizeMode: 'contain',
        tintColor: getColor(getColor(props.color))
            || undefined
    })

Icons.defaultProps = {
    size: metrics.screenWidth * .04
}
export { Icons }
/* App config for images
 */
const images = {
  icons: {
    splash: require('../assets/img/splash.png'),
  },
  splash: require('../assets/img/splash.png'),
  logo: require('../assets/img/logo-lg-blue.png'),
  email: require('../assets/img/email-sm.png'),
  loginbackground: require('../assets/img/login-bg.png'),
  WhiteLogo: require('../assets/img/logo-lg-white.png'),
  send: require('../assets/img/send.png'),
  login: require('../assets/img/login.png'),
  header: require('../assets/img/app-head-bg.png'),
  arrowBack: require('../assets/img/arrowback.png'),
  camera: require('../assets/img/camera.png'),
  done: require('../assets/img/done.png'),
  avatar: require('../assets/img/avatar.jpg'),
  user: require('../assets/img/user.png'),
  hold: require('../assets/img/hold.png'),
  holdgray: require('../assets/img/holdgray.png'),
  loaderLogo: require('../assets/img/logo-liberrex-medium.png'),
  fadeLogo: require('../assets/img/Rectangle.png'),
  emptyData: require('../assets/img/seagull-lendit.jpg'),
  appointmentsIcon: require('../assets/img/appointmentsIcon.png'),
  queueIcon2: require('../assets/img/queueIcon.png'),
  notificationIcon: require('../assets/img/notificationsIcon.png'),
  menuIcon: require('../assets/img/menuIcon.png'),
  user_profile: require('../assets/img/user_profile.png'),
  company_profile: require('../assets/img/company-profile.png'),
  arrow: require('../assets/img/arrow.png'),
  cancel1: require('../assets/img/cancel1.png'),
  
};

export { images };



getFcmToken = () => {

    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
        // user has a device token
    } else {
        // user doesn't have a device token yet
    }
    return fcmToken
}


RequestPermissions=()=>{
    try {
        await firebase.messaging().requestPermission();
        // User has authorised
    } catch (error) {
        // User has rejected permissions
    }
}


notificationOpen=()=>{
    // Set up your listener
firebase.notifications().onNotificationOpened((notificationOpen) => {
    // notificationOpen.action will equal 'test_action'
});

// Build your notification
const notification = new firebase.notifications.Notification()
  .setTitle('Android Notification Actions')
  .setBody('Action Body')
  .setNotificationId('notification-action')
  .setSound('default')
  .android.setChannelId('notification-action')
  .android.setPriority(firebase.notifications.Android.Priority.Max);
// Build an action
const action = new firebase.notifications.Android.Action('test_action', 'ic_launcher', 'My Test Action');
// Add the action to the notification
notification.android.addAction(action);

// Display the notification
firebase.notifications().displayNotification(notification);
}
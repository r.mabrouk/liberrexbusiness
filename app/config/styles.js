import { getHeight, getWidth, Width } from "../components/utils/dimensions";

/*
 * Provides universal color configs used in the app.
 * Provides universal fonts used in the app.
 */
const AppStyles = {
    headerSection: {
        container: {
            shadow: 6,
            zIndex: 15,
            width: Width,
            height: getHeight(67),
            marginTop: -getHeight(30),
            paddingTop: getHeight(15),
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: 'white',
            justifyContent: 'space-between',
            paddingHorizontal: getWidth(15),

        }
    }

};


export { AppStyles };

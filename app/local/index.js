import { } from './en'
import { } from './en'
import { } from './en'

let lang = 'en'
import { connect } from 'react-redux'

export const setlanguage = (_lang) => {
    lang = _lang
}
export const getlanguage = () => {
    return lang
}

export const _string = (key) => {
    const _En = require('./en.json')
    const _Fr = require('./fr.json')
    const _Ru = require('./ru.json')
    const _lang = Object.assign({}, { en: _En, fr: _Fr, ru: _Ru })
    let st = key.split('.')
    if (st.length == 1)
        return _lang[lang][st[0]]
    else if (st.length == 2)
        return _lang[lang][st[0]][st[1]]
    else if (st.length == 3)
        return _lang[lang][st[0]][st[1]][st[2]]

}


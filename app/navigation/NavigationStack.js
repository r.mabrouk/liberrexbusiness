import React from 'react'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Splash from '../../app/screens/Splash';
import Language from '../screens/Language'
import ForgetPassword from '../screens/ForgetPassword'
import QueueRequests from '../screens/QueueRequests';
import NewPassword from '../screens/NewPassword'
import SingUp from '../screens/SignUp'
import Login from '../screens/LoginScreen'
import Location from '../screens/Location/index'
import SignUpBusiness from '../screens/SignUpBusiness'
import SignUpPerson from '../screens/SignUpPerson'
import QueueList from '../screens/QueueList'
import QueueEdit from '../screens/QueueEdit'
import Home from '../screens/Home'
import AppointmentsApproved from '../screens/AppointmentsApproved'
import NewAppointmentCustomer from '../screens/NewAppointmentCustomer/index'
import Settings from '../screens/Settings'
import NewAppointment from '../screens/NewAppointment/index'
import EditProfile from '../screens/EditProfile'
import EditBusiness from '../screens/EditBusiness'
import Notification from '../screens/Notification'
import YourService from '../screens/YourService'
import BusinessRules from '../screens/BusinessRules'
import BusinessRulesCustom from '../screens/BusinessRulesCustom'
import TeamMembers from '../screens/TeamMembers'
import CustomerList from '../screens/CustomerList'
import TeamMembersAdd from '../screens/TeamMembersAdd'
import QueueAddCustomer from '../screens/QueueAddCustomer'
import Payments from '../screens/Payments'
import CustomerFeedback from '../screens/CustomerFeedback';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { _Text } from '../components/text/text';
import Footer from '../components/Footer/Footer';
import Report from '../screens/report';
import AppSettings from '../screens/AppSettings'
import Membership from '../screens/Membership/membership/index';
import MembershipCheckout from '../screens/Membership/MembershipCheckout';
import MembershipCheckoutStatus from '../screens/Membership/MembershipCheckoutStatus';
import QueueRedirect from '../screens/QueueRedirect';

const SettingTab = createStackNavigator({
    Settings: {
        screen: Settings,
        navigationOptions: { header: null}
    },
    EditProfile: {
        screen: EditProfile,
        navigationOptions: { header: null}
    },
    EditBusiness: {
        screen: EditBusiness,
        navigationOptions: { header: null}

    },
    YourService: {
        screen: YourService,
        navigationOptions: { header: null}

    },
    Payments: {
        screen: Payments,
        navigationOptions: { header: null }

    },
    BusinessRules: {
        screen: BusinessRules,
        navigationOptions: { header: null }

    },
    BusinessRulesCustom: {
        screen: BusinessRulesCustom,
        navigationOptions: { header: null }
    },
    TeamMembers: {
        screen: TeamMembers,
        navigationOptions: { header: null}
    },
    TeamMembersAdd: {
        screen: TeamMembersAdd,
        navigationOptions: { header: null }

    },
    CustomerList: {
        screen: CustomerList,
        navigationOptions: { header: null }

    },
    AppSettings: {
        screen: AppSettings,
        navigationOptions: { header: null }
    },
    Report: {
        screen: Report,
        navigationOptions: { header: null }
    },
    Notification: {
        screen: Notification,
        navigationOptions: { header: null }

    },
    MembershipCheckout: {
        screen: MembershipCheckout,
        navigationOptions: { header:null }
    },
    Membership: {
        screen: Membership,
        navigationOptions: { header:null}
    },
    MembershipCheckoutStatus: {
        screen: MembershipCheckoutStatus,
        navigationOptions: { header:null }
    },
}, {

    })

    
const QueueTab = createStackNavigator({
    QueueList: {
        screen: QueueList,
        navigationOptions: { header: null }
    },
    QueueEdit: {
        screen: QueueEdit,
        navigationOptions: { header: null }
    },
    QueueRequests: {
        screen: QueueRequests,
        navigationOptions: { header: null }
    },
    QueueAddCustomer: {
        screen: QueueAddCustomer,
        navigationOptions: { header: null }

    },
    CustomerFeedback: {
        screen: CustomerFeedback,
        navigationOptions: { header: null }
    },
    Notification: {
        screen: Notification,
        navigationOptions: { header: null}

    },
    QueueRedirect: {
        screen: QueueRedirect,
        navigationOptions: { header: null,}
    }

}, {

    })

// AppointmentTab screens .............

const AppointmentTab = createStackNavigator({
    AppointmentsApproved: {
        screen: AppointmentsApproved,
        navigationOptions: { header: null }
    },
    NewAppointment: {
        screen: NewAppointment,
        navigationOptions: { header: null }
    },
    Notification: {
        screen: Notification,
        navigationOptions: { header: null }

    },
    QueueAddCustomer: {
        screen: QueueAddCustomer,
        navigationOptions: { header: null }

    },

}, {

    })

// HomeTab screens .............

const HomeTab = createStackNavigator({
    Home: {
        screen: Home,
        navigationOptions: { header: null }
    }

}, {

    })

const Tabs = createBottomTabNavigator({
    HomeTab: {screen: HomeTab}, 
    QueueTab: {screen: QueueTab},
    AppointmentTab: {screen: AppointmentTab},
    QueueAddCustomessr: {screen: SettingTab}
},
    {
        resetOnBlur: true,
        tabBarComponent: (props) => {

            return (
                <Footer {...props} />
            )
        },
        tabBarOptions: {
            showLabel: false,
            activeTintColor: '#F8F8F8',
            inactiveTintColor: '#586589',
            style: {
                backgroundColor: '#171F33'
            }
        }
    },
)


const RNApp = createStackNavigator(
    {    AppSettings: {
        screen: AppSettings,
        navigationOptions: { header: null }
    },

        AppointmentsApproved: {
            screen: AppointmentsApproved,
            navigationOptions: { header: null}
        },

        report: {
            screen: Report,
            navigationOptions: { header: null}
        },

        Splash: {
            screen: Splash,
            navigationOptions: { header: null}
        },
        Login: {
            screen: Login,
            navigationOptions: { header: null}

        },
        Language: {
            screen: Language,
            navigationOptions: { header: null}
        },

        ForgetPassword: {
            screen: ForgetPassword,
            navigationOptions: { header: null}
        },


        NewPassword: {
            screen: NewPassword,
            navigationOptions: { header: null}
        },

        SingUp: {
            screen: SingUp,
            navigationOptions: { header: null}

        },

        SignUpBusiness: {
            screen: SignUpBusiness,
            navigationOptions: { header: null}
        },
        Location: {
            screen: Location,
            navigationOptions: { header: null}
        },
        SignUpPerson: {
            screen: SignUpPerson,
            navigationOptions: { header: null}
        },

        QueueList: {
            screen: QueueList,
            navigationOptions: { header: null}
        },

        Tabs: {
            screen: Tabs,
            navigationOptions: { header: null}

        },
        NewAppointmentCustomer: {
            screen: NewAppointmentCustomer,
            navigationOptions: { header: null}

        },
        QueueAddCustomer: {
            screen: QueueAddCustomer,
            navigationOptions: { header: null}

        },
        MembershipCheckoutStatus: {
            screen: MembershipCheckoutStatus,
            navigationOptions: { header:null }
        },
        Report: {
            screen: Report,
            navigationOptions: { header: null }
        },
    },
    {
        initialRouteName: 'Splash'
    }
);

export default createAppContainer(RNApp);


/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../../app/actions/types';


let initialuser_profile = {
    "user": {
        "id": 9099001,
        "fname": "Achraf",
        "lname": "Ammar",
        "title": null,
        "email": "achref.amar@gmail.com",
        "address": null,
        "postalcode": null,
        "telephone": "+21655517800",
        "lat": "36.79935662275469",
        "lng": "10.180047309005772",
        "photo": "/images/accounts/9099001.jpg?v=1536316748",
        "type": "",
        "position": "Chirurgien",
        "description": "",
        "prefix": "Dr.",
        "business_id": 1,
        "created_at": "2018-05-21 21:28:42",
        "updated_at": "2018-10-05 15:45:53",
    },
    business: {
        address: "Bab saadoun, Tunis",
        card_brand: "",
        card_last_four: "",
        city: "Colchester",
        country: "United Kingdom",
        created_at: "2018-08-25 00:00:00",
        email: "achref.amar@gmail.com",
        id: 1,
        industry: "1",
        lat: "36.82945177723023",
        lng: "10.185620829749732",
        name: "Ministère de la Santé - Tunisie",
        phone: "0021671577000",
        photo: "/images/accounts/1.png?v=1566849742",
        postal_code: "1900",
        stripe_id: "cus_FhbVOvl8xR80wR",
        trial_ends_at: "0000-00-00 00:00:00",
        type: "GOV",
        updated_at: "2019-08-26 20:05:10",
        website: "http://www.santetunisie.rns.tn/"
    },

    countries: {

    },
    defaultQueue: null

}
const initialState = {
    isLoggedIn: false,
    industryies: [],
    user_profile: initialuser_profile,

    loadingChangeProflie: false,
    loadingChangePassword: false,
    loadingUpdateBusinessInfo: false,
    loadingCheckEmail: false


};

export const authReducer = createReducer(initialState,
    {
        [types.LOGIN_REQUEST](state, action) {
            return {
                ...state,
                username: action.username,
                password: action.password
            };
        },
        [types.LOGIN_RESPONSE](state, action) {
            return {
                ...state,
                user_profile: action.response
            };
        },
        [types.LOGIN_FAILED](state) {
            return {
                ...state,
                isLoggedIn: false
            };
        },
        [types.GET_ALL_INDUSTRIES_RESPONSE](state, action) {
            return {
                ...state,
                industryies: action.response
            };
        },
        [types.SEARCH_INDUSTRIES_RESPONSE](state, action) {
            return {
                ...state,
                industryies: action.response
            };
        },
        [types.SEARCH_INDUSTRIES_REQUEST](state, action) {
            return {
                ...state,
                industryies: []
            };
        },
        [types.GET_COUNTRIES_RESPONSE](state, action) {
            return {
                ...state,
                countries: action.response
            };
        },
        [types.SIGNUP_RESPONSE](state, action) {
            return {
                ...state,
                user_profile: action.response
            };
        },
        [types.LOGOUT_RESPONSE](state, action) {
            return {
                ...state,
                user_profile: initialuser_profile
            };
        },
        [types.SOCIAL_LOGIN_RESPONSE](state, action) {
            return {
                ...state,
                user_profile: action.response
            };
        },


        [types.UPDATE_USER_ACCOUNT_REQUEST](state, action) {
            return {
                ...state,
                loadingChangeProflie: true
            };
        },
        [types.UPDATE_USER_ACCOUNT_FAILED](state, action) {
            return {
                ...state,
                loadingChangeProflie: false
            };
        },
        [types.UPDATE_USER_ACCOUNT_RESPONCE](state, action) {
            let temp = state.user_profile
            temp.user = action.response
            return {
                ...state, loadingChangeProflie: false, user_profile: JSON.parse(JSON.stringify(temp))
            };
        }
        ,
        [types.UPDATE_BUSINESS_WORKING_DAYES_RESPONCE](state, action) {
            let temp = state.user_profile
            temp.working_days = action.response
            return {
                ...state, user_profile: JSON.parse(JSON.stringify(temp))
            };
        }
        ,
        [types.UPDATE_BUSINESS_LOCATION_RESPONCE](state, action) {
            let temp = state.user_profile
            temp.business = action.response
            return {
                ...state, user_profile: JSON.parse(JSON.stringify(temp))
            };
        }
        ,
        [types.UPDATE_BUSINESS_INFO_RESPONCE](state, action) {
            let temp = state.user_profile
            temp.business = action.response
            return {
                ...state, user_profile: JSON.parse(JSON.stringify(temp))
            };
        },
        [types.SET_DEFAULT_QUEUE](state, action) {

            return {
                ...state, defaultQueue: action.defaultQueue
            };
        },



        [types.CHECK_EMAIL_REQUEST](state, action) {
            return {
                ...state, loadingCheckEmail: true
            };
        }
        ,
        [types.CHECK_EMAIL_FAILED](state, action) {
            return {
                ...state, loadingCheckEmail: false
            };
        },
        [types.CHECK_EMAIL_RESPONSE](state, action) {

            return {
                ...state, loadingCheckEmail: false
            };
        }



    });


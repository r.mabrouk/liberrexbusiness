/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {
    loadingBookingList: false,
    loadingCreateBooking: false,
    loadingDeleteBooking: false,
    loadingUpdateBooking: false,
    bookingList: [],
    current_page_bookingList: 1,
    last_page_bookingList: 1,
    bookingRequestList: [],
    current_page_bookingRequestList: 1,
    last_page_bookingRequestList: 1,
    loadingBookingRequestsList: false,
    loadingDeleteBookingRequest: false,
    loadingApproveBookingRequest: false
};

export const bookingReducer = createReducer(initialState,
    {

        //get booking list -----------------------------
        [types.GET_BOOKING_LIST_REQUEST](state, action) {
            if (action.reset) {
                return {
                    ...state,
                    current_page_bookingList: 1,
                    last_page_bookingList: 1,
                    loadingBookingList: true,
                    bookingList: [],

                };
            }
            return {
                ...state,
                loadingBookingList: true
            };
        },
        [types.GET_BOOKING_LIST_FAILED](state, action) {
            return {
                ...state,
                loadingBookingList: false
            };
        },
        [types.GET_BOOKING_LIST_RESPONSE](state, action) {
            return {
                ...state,
                loadingBookingList: false,
                bookingList: state.bookingList.concat(action.response.data),
                current_page_bookingList: action.response.current_page,
                last_page_bookingList: action.response.last_page,
            };
        },


        //create booking  -----------------------------
        [types.CREATE_BOOKING_REQUEST](state, action) {
            return {
                ...state,
                loadingCreateBooking: true
            };
        },
        [types.CREATE_BOOKING_FAILED](state, action) {
            return {
                ...state,
                loadingCreateBooking: false
            };
        },
        [types.CREATE_BOOKING_RESPONSE](state, action) {
            return {
                ...state,
                loadingCreateBooking: false
            };
        },


        //delete booking  -----------------------------
        [types.DELETE_BOOKING_REQUEST](state, action) {
            return {
                ...state,
                loadingDeleteBooking: true
            };
        },
        [types.DELETE_BOOKING_FAILED](state, action) {
            return {
                ...state,
                loadingDeleteBooking: false
            };
        },
        [types.DELETE_BOOKING_RESPONSE](state, action) {
            let temp = state.bookingList
            temp.splice(action.index, 1)
            // if (action.index === 0)
            //     temp = []
            return {
                ...state,
                loadingDeleteBooking: false,
                bookingList: JSON.parse(JSON.stringify(temp)),
            };
        },
        //-----------------------------  update booking  -----------------------------
        [types.UPDATE_BOOKING_REQUEST](state, action) {
            return {
                ...state,
                loadingCreateBooking: true
            };
        },
        [types.UPDATE_BOOKING_FAILED](state, action) {
            return {
                ...state,
                loadingCreateBooking: false
            };
        },
        [types.UPDATE_BOOKING_RESPONSE](state, action) {
            let temp = state.bookingList
            temp[action.index] = action.response
            return {
                ...state,
                loadingCreateBooking: false,
                bookingList: JSON.parse(JSON.stringify(temp)),
            };
        },

        //-----------------------------  get booking requests  -----------------------------
        [types.GET_BOOKING_REQUEST_LIST_REQUEST](state, action) {

            if (action.reset) {
                return {
                    ...state,
                    current_page_bookingList: 1,
                    last_page_bookingList: 1,
                    bookingList: [],
                    loadingBookingRequestsList: true
                };
            }
            return {
                ...state,
                loadingBookingRequestsList: true
            };
        },
        [types.GET_BOOKING_REQUEST_LIST_REQUEST](state, action) {
            return {
                ...state,
                loadingBookingRequestsList: false
            };
        },
        [types.GET_BOOKING_REQUEST_LIST_RESPONSE](state, action) {
            return {
                ...state,
                loadingBookingRequestsList: false,
                bookingRequestList: state.bookingRequestList.concat(action.response.data),
                current_page_bookingRequestList: action.response.current_page,
                last_page_bookingRequestList: action.response.last_page,
            };
        },


        //-----------------------------  approve booking requests  -----------------------------
        [types.APPROVE_BOOKING_REQUEST_REQUEST](state, action) {
            return {
                ...state,
                loadingApproveBookingRequest: true
            };
        },
        [types.APPROVE_BOOKING_REQUEST_FAILED](state, action) {
            return {
                ...state,
                loadingApproveBookingRequest: false
            };
        },
        [types.APPROVE_BOOKING_REQUEST_RESPONSE](state, action) {
            let temp = state.bookingRequestList
            temp.splice(action.index, 1)
            return {
                ...state,
                loadingApproveBookingRequest: false,
                bookingRequestList: temp,
            };
        },
        //-----------------------------  delete booking requests  -----------------------------
        [types.DELETE_BOOKING_REQUEST_REQUEST](state, action) {
            return {
                ...state,
                loadingDeleteBookingRequest: true
            };
        },
        [types.DELETE_BOOKING_REQUEST_FAILED](state, action) {
            return {
                ...state,
                loadingDeleteBookingRequest: false
            };
        },
        [types.DELETE_BOOKING_REQUEST_RESPONSE](state, action) {
            let temp = state.bookingRequestList
            temp.splice(action.index, 1)
            // if (action.index === 0)
            //     temp = []
            return {
                ...state,
                loadingDeleteBookingRequest: false,
                bookingRequestList: JSON.parse(JSON.stringify(temp)),
            };
        },

    });

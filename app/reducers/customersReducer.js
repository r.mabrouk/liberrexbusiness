
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {
    loadingCustomerFeedback: false,
    customer_selecterd_index: -1,
    customer_feedback: [],
    customers_feedback_current_page: 1,
    customers_feedback_pages: 1,
    customer_feedback_statistics: {
        good: 0,
        average: 0,
        bad: 0
    },
    all_customers: [
    ],
    cuurent_page: 1,
    pages: 1,
    vip_customers: [

    ],
    vip_pages: 1,
    vip_cuurent_page: 1,
    blacklist_customers: [

    ],
    blacklist_pages: 1,
    blacklist_cuurent_page: 1,
    selectedCustomer: {

    }

};


export const customersReducer = createReducer(initialState,
    {
        [types.GET_ALL_CUSTOMERS_RESPONSE](state, action) {
            return {
                ...state,
                all_customers: state.all_customers.concat(action.response.data),
                cuurent_page: action.response.current_page ,
                pages: action.response.last_page
            };
        },
        [types.GET_ALL_CUSTOMERS_REQUEST](state, action) {
            if (action.reset)
            return {
                ...state,
                all_customers: [],
                cuurent_page: 1 ,
                pages:1
            };
            else
            return {...state}
        },
        [types.GET_ALL_CUSTOMERS_VIP_REQUEST](state, action) {
            if (action.reset)
            return {
                ...state,
                vip_customers: [],
                vip_cuurent_page: 1 ,
                vip_pages:1
            };
            else
            return {...state}
        },
        [types.GET_ALL_CUSTOMERS_BLACKLIST_REQUEST](state, action) {
            if (action.reset)
            return {
                ...state,
                blacklist_customers: [],
                blacklist_cuurent_page: 1 ,
                blacklist_pages:1
            };
            else
            return {...state}
        },

        
        [types.SELECTED_CUSTOMER_TO_APPOINTMENT](state, action) {
            return {
                ...state,
                selectedCustomer: action.customer
            };
        }
        ,
        [types.RESET_CUSTOMERS_LIST](state, action) {
            return {
                ...state,
                all_customers: [], pages: 1, current_page: 1,
            };
        }
        ,
        [types.GET_ALL_CUSTOMERS_BLACKLIST_RESPONSE](state, action) {
            return {
                ...state,
                blacklist_customers: state.blacklist_customers.concat(action.response.data),
                blacklist_cuurent_page: action.response.current_page,
                blacklist_pages: action.response.last_page
            };
        },
        [types.RESET_CUSTOMERS_LIST](state, action) {
            return {
                ...state,
                all_customers: [], pages: 1, current_page: 1,
            };
        }
        ,
        [types.RESET_CUSTOMERS_VIP_LIST](state, action) {
            return {
                ...state,
                vip_customers: [],
            };
        },
        [types.GET_ALL_CUSTOMERS_VIP_RESPONSE](state, action) {
            return {
                ...state,
                vip_customers: state.vip_customers.concat(action.response.data),
                vip_cuurent_page: action.response.current_page,
                vip_pages: action.response.last_page
            };
        },
        [types.RESET_CUSTOMERS_BLACKLIST_LIST](state, action) {
            return {
                ...state,
                blacklist_customers: [],
            };
        },

        // keyOfReducer
        // update customer
        [types.UPDATE_CUSTOMER_RESPONSE](state, action) {
            let updateCustomer = state[action.keyOfReducer]
            updateCustomer[state.customer_selecterd_index] = action.response
            return {
                ...state,
                [action.keyOfReducer]: updateCustomer
            };
        },
        [types.UPDATE_CUSTOMER_REQUEST](state, action) {
            return {
                ...state,
                customer_selecterd_index: action.index
            };
        },
        [types.REMOVE_CUSTOMER_FROM_BOOK_REQUEST](state, action) {
            return {
                ...state,
                customer_selecterd_index: action.index
            };
        },

        // remove customer from book
        [types.REMOVE_CUSTOMER_FROM_BOOK_RESPONSE](state, action) {
            let temp = state[action.keyOfReducer]
            temp.splice(state.customer_selecterd_index, 1)
            console.log(temp, "updateCustomer", state.customer_selecterd_index + "updateCustomer")
            return {
                ...state,
                [action.keyOfReducer]: temp
            };
        },
        [types.ADD_CUSTOMER_TO_BOOKS_RESPONSE](state, action) {
            // let temp = state[action.keyOfReducer]
            // temp.splice(state.customer_selecterd_index, 1)
            // console.log(temp, "updateCustomer", state.customer_selecterd_index + "keyOfReducer" + action.keyOfReducer)
            return {
                ...state
                ,
                // [action.keyOfReducer]: temp
            };
        },

        [types.ADD_CUSTOMER_TO_BOOKS_REQUEST](state, action) {
            return {
                ...state,
                customer_selecterd_index: action.index
            };
        },


        // delete customer from book
        [types.DELETE_CUSTOMER_RESPONSE](state, action) {
            let temp = state[action.keyOfReducer]
            temp.splice(action.index, 1)
            return {
                ...state,
                [action.keyOfReducer]: temp
            };
        },


        //get all customer feedback
        [types.GET_ALL_CUSTOMER_FEEDBACK_REQUEST](state, action) {
            return {
                ...state,
                loadingCustomerFeedback: true
            };
        },
        [types.GET_ALL_CUSTOMER_FEEDBACK_FAILED](state, action) {
            return {
                ...state,
                loadingCustomerFeedback: false
            };
        },
        [types.GET_ALL_CUSTOMER_FEEDBACK_RESPONSE](state, action) {
            return {
                ...state,
                loadingCustomerFeedback: false,
                customer_feedback_statistics: action.response.statistics,
                customer_feedback: state.customer_feedback.concat(action.response.feedback.data),
                customers_feedback_current_page: action.current_page,
                customers_feedback_pages: action.last_page,

            };
        },


        //filter customer feedback
        [types.FILTER_CUSTOMER_FEEDBACK_REQUEST](state, action) {
            return {
                ...state,
                loadingCustomerFeedback: true, customer_feedback: []
            };
        },
        [types.FILTER_CUSTOMER_FEEDBACK_FAILED](state, action) {
            return {
                ...state,
                loadingCustomerFeedback: false
            };
        },
        [types.FILTER_CUSTOMER_FEEDBACK_RESPONSE](state, action) {
            return {
                ...state,
                loadingCustomerFeedback: false,
                customer_feedback_statistics: action.response.statistics,
                customer_feedback: state.customer_feedback.concat(action.response.feedback.data),
                customers_feedback_current_page: action.current_page,
                customers_feedback_pages: action.last_page,

            };
        },




    });

/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {
     
    places:[]
};

export const  googleMapReducer = createReducer(initialState, 
    {
    [types.GET_PLACES_FROM_GOOGLE_API_RESPONSE](state, action) {
        return {
            ...state,
            places:action.response
        };
    },
});

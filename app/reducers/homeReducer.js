/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {

    loadingHomeState: false,
    homeStats: {
        // "booking_requests": 1,
        // "queue_requests": 0,
        // "bookings": 6,
        // "waiting": 16,
        // "accepted": 60,
        // "rejected": 15,
        // "pending": 25,
        // "action": "getHomeStats",
        // "status": "success"
    }
};
export const homeReducer = createReducer(initialState,
    {
        [types.GET_HOME_STATS_REQUEST](state, action) {
            return {
                ...state,
                loadingHomeState: true
            };
        },
        [types.GET_HOME_STATS_FAILED](state, action) {
            return {
                ...state,
                loadingHomeState: false
            };
        },
        [types.GET_HOME_STATS_RESPONSE](state, action) {
            return {
                ...state,
                loadingHomeState: false,homeStats:action.response
            };
        },



        [types.FILTER_HOME_REPORTS_REQUEST](state, action) {
            return {
                ...state,
                loadingHomeState: true
            };
        },
        [types.FILTER_HOME_REPORTS_FAILED](state, action) {
            return {
                ...state,
                loadingHomeState: false
            };
        },
        [types.FILTER_HOME_REPORTS_RESPONSE](state, action) {
            return {
                ...state,
                loadingHomeState: false,homeStats:action.response
            };
        }

    });

/* 
 * combines all th existing reducers
 */
import * as loadingReducer from './loadingReducer';
import * as authReducer from './authReducer';
import * as googleMapReducer from './googleMapReducer';
import * as customersReducer from './customersReducer';
import * as servicesReducer from './servicesReducer';
import * as uiReducer from './uiReducer';
// import * as authReducer from './authReducer';
import * as queueReducer from './queueReducer';
import * as notificationReducer from './notificationReducer';
import * as teamMembersReducers from './teamMembersReducers';
import * as membershipReducer from './membershipReducer';
import * as rulesReducer from './rulesReducer';
import * as homeReducer from './homeReducer';
import * as paymentReducer from './paymentReducer';
import * as bookingReducer from './bookingReducer';
import * as userAccountReducer from './userAccountReducer';
import * as reportReducer from './reportReducer';




// import * as authReducer from './authReducer';
export default Object.assign(
    authReducer, 
    loadingReducer,
    googleMapReducer,
    uiReducer,
    customersReducer,
    queueReducer,
    servicesReducer,
    notificationReducer,
    teamMembersReducers,
    membershipReducer,
    rulesReducer,
    homeReducer,
    paymentReducer,
    bookingReducer,
    userAccountReducer,
    reportReducer

    );

/**
 * Loading reducer made seperate for easy blacklisting
 * Avoid data persist
 */
import createReducer from '../../app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {
    isLoginLoading: false,
    isSocialLoginLoading: false,
    isSignUpLoading: false,
    isLogoutLoading: false,
    forgotPasswordLoading: false,
    sendNewPasswordLoading: false,
    isGetAllCustomersLoading: false,
    isFilterIndustries: false,
    isGetCustomersVipLoading: false,
    isGetCustomersBlacklistLoading: false,
    isUpdateCustomersLoading: false,
    isAddNewCustomerLoading: false,
    isremoveCustomerFromBookLoading: false,
    isAddCustomersToblacklistLoading: false,
    isAddCustomersToVipLoading: false,
    isGetAllServices: false,
    isDeleteCustomerLoading: false,
    loadingChangeProflie: false,
    loadingChangePassword: false,
    loadingUpdateBusinessInfo: false,
    loadingUpdateBusinessLocation: false,
    loadingUpdateBusinessWorkingDayes: false

};

export const loadingReducer = createReducer(initialState, {
    [types.LOGIN_ENABLE_LOADER](state) {
        return { ...state, isLoginLoading: true };
    },
    [types.LOGIN_DISABLE_LOADER](state) {
        return { ...state, isLoginLoading: false };
    },
    [types.SIGNUP_REQUEST](state) {
        return { ...state, isSignUpLoading: true };
    },
    [types.SIGNUP_FAILED](state) {
        return { ...state, isSignUpLoading: false };
    },
    [types.SIGNUP_RESPONSE](state) {
        return { ...state, isSignUpLoading: false };
    },
    [types.LOGOUT_FAILED](state) {
        return { ...state, isLogoutLoading: false };
    },
    [types.LOGOUT_REQUEST](state) {
        return { ...state, isLogoutLoading: true };
    },
    [types.LOGOUT_RESPONSE](state) {
        return { ...state, isLogoutLoading: false };
    },


    [types.RESET_PASSWORD_RESPONSE_WITH_CODE](state) {
        return { ...state, sendNewPasswordLoading: false };
    },
    [types.RESET_PASSWORD_FAILED_WITH_CODE](state) {
        return { ...state, sendNewPasswordLoading: false };
    },
    [types.RESET_PASSWORD_REQUEST_WITH_CODE](state) {
        return { ...state, sendNewPasswordLoading: true };
    },
    [types.RESET_PASSWORD_RESPONSE](state) {
        return { ...state, forgotPasswordLoading: false };
    },
    [types.RESET_PASSWORD_FAILED](state) {
        return { ...state, forgotPasswordLoading: false };
    },
    [types.RESET_PASSWORD_REQUEST](state) {
        return { ...state, forgotPasswordLoading: true };
    },

    [types.GET_ALL_CUSTOMERS_REQUEST](state) {
        return { ...state, isGetAllCustomersLoading: true };
    },
    [types.GET_ALL_CUSTOMERS_FAILED](state) {
        return { ...state, isGetAllCustomersLoading: false };
    },
    [types.GET_ALL_CUSTOMERS_RESPONSE](state) {
        return { ...state, isGetAllCustomersLoading: false };
    },


    // isUpdateCustomersLoading ---------------------------
    [types.UPDATE_CUSTOMER_REQUEST](state) {
        return { ...state, isUpdateCustomersLoading: true };
    },
    [types.UPDATE_CUSTOMER_FAILED](state) {
        return { ...state, isUpdateCustomersLoading: false };
    },
    [types.UPDATE_CUSTOMER_RESPONSE](state) {
        return { ...state, isUpdateCustomersLoading: false };
    },



    [types.GET_ALL_CUSTOMERS_VIP_REQUEST](state) {
        return { ...state, isGetCustomersVipLoading: true };
    },
    [types.GET_ALL_CUSTOMERS_VIP_FAILED](state) {
        return { ...state, isGetCustomersVipLoading: false };
    },
    [types.GET_ALL_CUSTOMERS_VIP_RESPONSE](state) {
        return { ...state, isGetCustomersVipLoading: false };
    },



    [types.GET_ALL_CUSTOMERS_BLACKLIST_REQUEST](state) {
        return { ...state, isGetCustomersBlacklistLoading: true };
    },
    [types.GET_ALL_CUSTOMERS_BLACKLIST_FAILED](state) {
        return { ...state, isGetCustomersBlacklistLoading: false };
    },
    [types.GET_ALL_CUSTOMERS_BLACKLIST_RESPONSE](state) {
        return { ...state, isGetCustomersBlacklistLoading: false };
    },



    [types.SEARCH_INDUSTRIES_REQUEST](state) {
        return { ...state, isFilterIndustries: true };
    },
    [types.SEARCH_INDUSTRIES_FAILED](state) {
        return { ...state, isFilterIndustries: false };
    },
    [types.SEARCH_INDUSTRIES_RESPONSE](state) {
        return { ...state, isFilterIndustries: false };
    }
    ,
    [types.SOCIAL_LOGIN_FAILED](state) {
        return { ...state, isSocialLoginLoading: false };
    },
    [types.SOCIAL_LOGIN_REQUEST](state) {
        return { ...state, isSocialLoginLoading: true };
    },
    [types.SOCIAL_LOGIN_RESPONSE](state) {
        return { ...state, isSocialLoginLoading: false };
    },
    // add new customers
    [types.CREATE_CUSTOMER_REQUEST](state) {
        return { ...state, isAddNewCustomerLoading: true };
    },
    [types.CREATE_CUSTOMER_RESPONSE](state) {
        return { ...state, isAddNewCustomerLoading: false };
    },
    [types.CREATE_CUSTOMER_FAILED](state) {
        return { ...state, isAddNewCustomerLoading: false };
    },
    // isremoveCustomerFromBookLoading
    [types.REMOVE_CUSTOMER_FROM_BOOK_REQUEST](state, action) {
        return { ...state, [action.loader]: true };
    },
    [types.REMOVE_CUSTOMER_FROM_BOOK_FAILED](state, action) {
        return { ...state, [action.loader]: false };
    },
    [types.REMOVE_CUSTOMER_FROM_BOOK_RESPONSE](state, action) {
        return { ...state, [action.loader]: false };
    },
    // delete customer 
    [types.DELETE_CUSTOMER_REQUEST](state, action) {
        return { ...state, [action.loader]: true };
    },
    [types.DELETE_CUSTOMER_FAILED](state, action) {
        return { ...state, [action.loader]: false };
    },
    [types.DELETE_CUSTOMER_RESPONSE](state, action) {
        return { ...state, [action.loader]: false };
    },
    // add customer to books
    [types.ADD_CUSTOMER_TO_BOOKS_REQUEST](state, action) {
        return { ...state, [action.loader]: true };
    },
    [types.ADD_CUSTOMER_TO_BOOKS_FAILED](state, action) {
        return { ...state, [action.loader]: false };
    },
    [types.ADD_CUSTOMER_TO_BOOKS_RESPONSE](state, action) {
        return { ...state, [action.loader]: false };
    },

    [types.UPDATE_USER_ACCOUNT_REQUEST](state, action) {
        return {
            ...state,
            loadingChangeProflie: true
        };
    },
    [types.UPDATE_USER_ACCOUNT_FAILED](state, action) {
        return {
            ...state,
            loadingChangeProflie: false
        };
    },
    [types.UPDATE_USER_ACCOUNT_RESPONCE](state, action) {
        return {
            ...state, loadingChangeProflie: false
        };
    },


    [types.UPDATE_PASSWORD_REQUEST](state, action) {
        return {
            ...state,
            loadingChangePassword: true
        };
    },
    [types.UPDATE_PASSWORD_FAILED](state, action) {
        return {
            ...state,
            loadingChangePassword: false
        };
    },
    [types.UPDATE_PASSWORD_RESPONCE](state, action) {
        return {
            ...state, loadingChangePassword: false
        };
    },

    //  Update Business Info
    [types.UPDATE_BUSINESS_INFO_REQUEST](state, action) {
        return {
            ...state,
            loadingUpdateBusinessInfo: true
        };
    },
    [types.UPDATE_BUSINESS_INFO_FAILED](state, action) {
        return {
            ...state,
            loadingUpdateBusinessInfo: false
        };
    },
    [types.UPDATE_BUSINESS_INFO_RESPONCE](state, action) {
        return {
            ...state, loadingUpdateBusinessInfo: false
        };
    },


    //  Update Business Info
    [types.UPDATE_BUSINESS_WORKING_DAYES_REQUEST](state, action) {
        return {
            ...state,
            loadingUpdateBusinessWorkingDayes: true
        };
    },
    [types.UPDATE_BUSINESS_WORKING_DAYES_FAILED](state, action) {
        return {
            ...state,
            loadingUpdateBusinessWorkingDayes: false
        };
    },
    [types.UPDATE_BUSINESS_WORKING_DAYES_RESPONCE](state, action) {
        return {
            ...state, loadingUpdateBusinessWorkingDayes: false
        };
    },


    //  Update Business Info
    [types.UPDATE_BUSINESS_LOCATION_REQUEST](state, action) {
        return {
            ...state,
            loadingUpdateBusinessLocation: true
        };
    },
    [types.UPDATE_BUSINESS_LOCATION_FAILED](state, action) {
        return {
            ...state,
            loadingUpdateBusinessLocation: false
        };
    },
    [types.UPDATE_BUSINESS_LOCATION_RESPONCE](state, action) {
        return {
            ...state, loadingUpdateBusinessLocation: false
        };
    },

});

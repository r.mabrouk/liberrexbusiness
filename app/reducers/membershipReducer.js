

/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {
    loadingteamMembershipPlans: false,

    membershipPlans: [

    ],
    selectedPackagesIndex:0
};
export const membershipReducer = createReducer(initialState,
    {
        // -------- get member ship plans--------------
        [types.GET_MEMBERSHIP_PLAN_REQUEST](state, action) {
            return {
                ...state,
                loadingteamMembershipPlans: true
            };
        },
        [types.GET_MEMBERSHIP_PLAN_FAILED](state, action) {
            return {
                ...state,
                loadingteamMembershipPlans: false
            };
        },

        
        [types.SET_PLAN_SELECTED](state, action) {
            return {
                ...state,
                selectedPackagesIndex:action.selectedPackagesIndex
            };
        },
        [types.GET_MEMBERSHIP_PLAN_RESPONSE](state, action) {
            return {
                ...state,
                membershipPlans: action.response, 
                loadingteamMembershipPlans: false,
                selectedPackagesIndex:action.selectedPackagesIndex
            };
        },

    });

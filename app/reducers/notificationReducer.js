

/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {
    loadingNotificationsList: false,
    "notificationList": [
 
    ]
};

export const notificationReducer = createReducer(initialState,
    {
        // -------- get all notification--------------
        [types.GET_ALL_NOTIFICATION_REQUEST](state, action) {
            return {
                ...state,
                loadingNotificationsList: true
            };
        },
        [types.GET_ALL_NOTIFICATION_FAILED](state, action) {
            return {
                ...state,
                loadingNotificationsList: false
            };
        }
        ,
        [types.GET_ALL_NOTIFICATION_RESPONSE](state, action) {
            return {
                ...state,
                notificationList: action.response.data, loadingNotificationsList: false
            };
        }



    });

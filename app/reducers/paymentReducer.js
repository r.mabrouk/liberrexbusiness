/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {
    loadingAddPaymentCard: false,
    loadingGetPaymentCards: false,
    loadingDeletePaymentCard: false,
    cardsList: [
    ],
    loadingUpdateAccountMembership: false,
    membership: {

    },
    membershipDetails: {
        membershipPlan: "monthly",
        taxs: 20,
        plan: "plan",
        price:200,
        currency:"$",
        numberOfMonths:2
    }

};
export const paymentReducer = createReducer(initialState,
    {
        // -------- get queue list --------------
        [types.ADD_NEW_PAYMENT_CARD_REQUEST](state, action) {
            return {
                ...state,
                loadingAddPaymentCard: true,
            };
        },
        [types.ADD_NEW_PAYMENT_CARD_FAILED](state, action) {
            return {
                ...state,
                loadingAddPaymentCard: false,

            };
        },
        [types.ADD_NEW_PAYMENT_CARD_RESPONSE](state, action) {
            return {
                ...state,
                loadingAddPaymentCard: false
            };
        },



        // -------- get all payment cards--------------
        [types.GET_PAYEMENT_CARDS_REQUEST](state, action) {
            return {
                ...state,
                loadingGetPaymentCards: true,
            };
        },
        [types.GET_PAYEMENT_CARDS_FAILED](state, action) {
            return {
                ...state,
                loadingGetPaymentCards: false,

            };
        },
        [types.GET_PAYEMENT_CARDS_RESPONSE](state, action) {
            return {
                ...state,
                loadingGetPaymentCards: false,
                cardsList: action.response
            };
        },

        // -------- delete payment card --------------
        [types.DELETE_PAYEMENT_CARD_REQUEST](state, action) {
            return {
                ...state,
                loadingDeletePaymentCard: true,
            };
        },
        [types.DELETE_PAYEMENT_CARD_FAILED](state, action) {
            return {
                ...state,
                loadingDeletePaymentCard: false,
            };
        },
        [types.DELETE_PAYEMENT_CARD_RESPONSE](state, action) {
            let temp = state.cardsList
            temp.splice(action.index, 1)
            return {
                ...state,
                loadingDeletePaymentCard: false,
                cardsList: temp
            };
        },

        // -------- update payment card --------------
        [types.UPDATE_ACCOUNT_MEMBERSHIP_REQUEST](state, action) {
            return {
                ...state,
                loadingUpdateAccountMembership: true,
                membershipDetails: action.detailsForNextScreen
            };
        },
        [types.UPDATE_ACCOUNT_MEMBERSHIP_FAILED](state, action) {
            return {
                ...state,
                loadingUpdateAccountMembership: false,
            };
        },
        [types.UPDATE_ACCOUNT_MEMBERSHIP_RESPONSE](state, action) {
            return {
                ...state,
                loadingUpdateAccountMembership: false,
                membership: action.response
            };
        },

    });




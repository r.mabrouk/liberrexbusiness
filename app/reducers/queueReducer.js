/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {
    loadingQueueList: false,
    QueueList: [],
    QueueNextPage: 1,
    QueueNumberPages: 1,

    loadingAddQueue: false,

    selectedQueueID: -1,
    selectedQueue: {},
    // selectedQueueName: {},

    loadingQueueWaitingList: false,
    queueWaitingList: [],

    loadingQueuePendingList: false,
    queuePendingList: [],

    loadingApproveQueueRequest: false,
    loadingDeclineQueueRequest: false,

    loadingAddCutomerToQueue: false,

    selectedCustomer: {},
    loadingRedirectCustomer: false,

    loadingCancelCustomerInWaiting: false,

    loadingCallNextCustomer: false,

    loadingSwapCustomerRanks: false,



    queue:{

    },
    loadingGetQueue:false,
    loadingUpdateQueue:false,
    loadingDeleteQueue:false

};
export const queueReducer = createReducer(initialState,
    {
        // -------- get queue list --------------
        [types.GET_QUEUE_LIST](state, action) {
            return {
                ...state,
                loadingQueueList: true,
                QueueList: action.firstTime ? [] : state.QueueList,
                QueueNextPage: action.firstTime ? 1 : state.QueueNextPage,
            };
        },
        [types.GET_QUEUE_LIST_RESPONSE](state, action) {
            return {
                ...state,
                loadingQueueList: false,
                QueueList: action.response.current_page == 1 ? action.response.queues : state.QueueList.concat(action.response.queues),
                QueueNextPage: action.response.current_page + 1,
                QueueNumberPages: action.response.last_page
            };
        },
        [types.GET_QUEUE_LIST_FAILED](state, action) {
            return {
                ...state,
                loadingQueueList: false
            };
        },

        // -------- add queue --------------
        [types.ADD_QUEUE](state, action) {
            return {
                ...state,
                loadingAddQueue: true
            };
        },
        [types.ADD_QUEUE_RESPONSE](state, action) {
            return {
                ...state,
                loadingAddQueue: false,
            };
        },
        [types.ADD_QUEUE_FAILED](state, action) {
            return {
                ...state,
                loadingAddQueue: false
            };
        },

        // -------------- selected queue -----------
        [types.SELECTED_QUEUE](state, action) {
            return {
                ...state,
                selectedQueueID: action.id,
                // selectedQueueID: 19,
                queueWaitingList: [],
                queuePendingList: [],
                // selectedQueueName: action.name,
                selectedQueue: action.selectedQueue,
            };
        },


        // --------------- queue waiting list ------------
        [types.GET_QUEUE_WAITING_LIST](state, action) {
            return {
                ...state,
                loadingQueueWaitingList: true
            };
        },
        [types.GET_QUEUE_WAITING_LIST_RESPONSE](state, action) {
            return {
                ...state,
                loadingQueueWaitingList: false,
                queueWaitingList: action.response.waiting_list
            };
        },
        [types.GET_QUEUE_WAITING_LIST_FAILED](state, action) {
            return {
                ...state,
                loadingQueueWaitingList: false
            };
        },


        // --------------- queue penfing list ------------
        [types.GET_QUEUE_PENDING_LIST](state, action) {
            return {
                ...state,
                loadingQueuePendingList: true
            };
        },
        [types.GET_QUEUE_PENDING_LIST_RESPONSE](state, action) {
            return {
                ...state,
                loadingQueuePendingList: false,
                queuePendingList: action.response.requests
            };
        },
        [types.GET_QUEUE_PENDING_LIST_FAILED](state, action) {
            return {
                ...state,
                loadingQueuePendingList: false
            };
        },

        // --------------- approve queue request ------------
        [types.APPROVE_QUEUE_REQUEST](state, action) {
            return {
                ...state,
                loadingApproveQueueRequest: true
            };
        },
        [types.APPROVE_QUEUE_REQUEST_RESPONSE](state, action) {
            return {
                ...state,
                loadingApproveQueueRequest: false,
            };
        },
        [types.APPROVE_QUEUE_REQUEST_FAILED](state, action) {
            return {
                ...state,
                loadingApproveQueueRequest: false
            };
        },


        // --------------- decline queue request ------------
        [types.DECLINE_QUEUE_REQUEST](state, action) {
            return {
                ...state,
                loadingDeclineQueueRequest: true
            };
        },
        [types.DECLINE_QUEUE_REQUEST_RESPONSE](state, action) {
            return {
                ...state,
                loadingDeclineQueueRequest: false,
            };
        },
        [types.DECLINE_QUEUE_REQUEST_FAILED](state, action) {
            return {
                ...state,
                loadingDeclineQueueRequest: false
            };
        },

        // --------------- add customer to queue ------------
        [types.ADD_CUSTOMER_TO_QUEUE](state, action) {
            return {
                ...state,
                loadingAddCutomerToQueue: true
            };
        },
        [types.ADD_CUSTOMER_TO_QUEUE_RESPONSE](state, action) {
            return {
                ...state,
                loadingAddCutomerToQueue: false,
            };
        },
        [types.ADD_CUSTOMER_TO_QUEUE_FAILED](state, action) {
            return {
                ...state,
                loadingAddCutomerToQueue: false
            };
        },

        // ----------- selected customer ------------
        [types.SELECTED_CUSTOMER](state, action) {
            return {
                ...state,
                selectedCustomer: action.customer
            };
        },

        // --------------- redirect customer ------------
        [types.REDIRECT_CUSTOMER](state, action) {
            return {
                ...state,
                loadingRedirectCustomer: true
            };
        },
        [types.REDIRECT_CUSTOMER_RESPONSE](state, action) {
            return {
                ...state,
                loadingRedirectCustomer: false,
            };
        },
        [types.REDIRECT_CUSTOMER_FAILED](state, action) {
            return {
                ...state,
                loadingRedirectCustomer: false
            };
        },

        // --------------- cancel customer in waiting ------------
        [types.CANCEL_CUSTOMER_IN_WAITING](state, action) {
            return {
                ...state,
                loadingCancelCustomerInWaiting: true
            };
        },
        [types.CANCEL_CUSTOMER_IN_WAITING_RESPONSE](state, action) {
            return {
                ...state,
                loadingCancelCustomerInWaiting: false,
            };
        },
        [types.CANCEL_CUSTOMER_IN_WAITING_FAILED](state, action) {
            return {
                ...state,
                loadingCancelCustomerInWaiting: false
            };
        },


        // --------------- call next customer ------------
        [types.CALL_NEXT_CUSTOMER](state, action) {
            return {
                ...state,
                loadingCallNextCustomer: true
            };
        },
        [types.CALL_NEXT_CUSTOMER_RESPONSE](state, action) {
            return {
                ...state,
                loadingCallNextCustomer: false,
            };
        },
        [types.CALL_NEXT_CUSTOMER_FAILED](state, action) {
            return {
                ...state,
                loadingCallNextCustomer: false
            };
        },


        // --------------- Swap customer ranks ------------
        [types.SWAP_CUSTOMER_RANKS](state, action) {
            return {
                ...state,
                loadingSwapCustomerRanks: true
            };
        },
        [types.SWAP_CUSTOMER_RANKS_RESPONSE](state, action) {
            return {
                ...state,
                loadingSwapCustomerRanks: false,
            };
        },
        [types.SWAP_CUSTOMER_RANKS_FAILED](state, action) {
            return {
                ...state,
                loadingSwapCustomerRanks: false
            };
        },


        // --------------- delete a queue  ------------
        [types.DELETE_A_QUEUE_REQUEST](state, action) {
            return {
                ...state,
                loadingSwapCustomerRanks: true
            };
        },
        [types.DELETE_A_QUEUE_RESPONSE](state, action) {
            return {
                ...state,
                loadingSwapCustomerRanks: false,
            };
        },
        [types.DELETE_A_QUEUE_FAILED](state, action) {
            return {
                ...state,
                loadingSwapCustomerRanks: false
            };
        },





        // --------------- update a queue  ------------
        [types.UPDATE_A_QUEUE_REQUEST](state, action) {
            return {
                ...state,
                loadingUpdateQueue: true
            };
        },
        [types.UPDATE_A_QUEUE_RESPONSE](state, action) {
            return {
                ...state,
                loadingUpdateQueue: false,
            };
        },
        [types.UPDATE_A_QUEUE_FAILED](state, action) {
            return {
                ...state,
                loadingUpdateQueue: false
            };
        },



        // --------------- delete a queue  ------------
        [types.DELETE_A_QUEUE_REQUEST](state, action) {
            return {
                ...state,
                loadingDeleteQueue: true
            };
        },
        [types.DELETE_A_QUEUE_RESPONSE](state, action) {
            return {
                ...state,
                loadingDeleteQueue: false,
            };
        },
        [types.DELETE_A_QUEUE_FAILED](state, action) {
            return {
                ...state,
                loadingDeleteQueue: false
            };
        },


        // --------------- update a queue  ------------
        [types.GET_A_QUEUE_REQUEST](state, action) {
            return {
                ...state,
                loadingGetQueue: true
            };
        },
        [types.GET_A_QUEUE_RESPONSE](state, action) {
            return {
                ...state,
                loadingGetQueue: false,
                queue:action.response
            };
        },
        [types.GET_A_QUEUE_FAILED](state, action) {
            return {
                ...state,
                loadingGetQueue: false
            };
        },


    });

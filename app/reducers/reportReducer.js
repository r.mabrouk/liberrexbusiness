/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {
    loadingReportList: false,
    reports: {
        "waiting_time": {
            "type": "linechart",
            "labels": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June"
            ],
            "data": [
                "33",
                "44",
                "34",
                "29",
                "53",
                "59"
            ]
        },
        "service_time": {
            "type": "linechart",
            "labels": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June"
            ],
            "data": [
                "22",
                "21",
                "24",
                "33",
                "45",
                "22"
            ]
        },
        "services": {
            "type": "piechart",
            "labels": [
                "Haircut",
                "Shave",
                "Hair wash",
                "Facial"
            ],
            "data": [
                "1312",
                "232",
                "542",
                "533",
                "45"
            ]
        },
        "rating": {
            "type": "star",
            "labels": [
                "1",
                "2",
                "3",
                "4",
                "5"
            ],
            "data": [
                "230",
                "142",
                "33",
                "12",
                "70"
            ]
        }
    }
};
export const reportReducer = createReducer(initialState,
    {
        [types.GET_REPORTS_LIST_REQUEST](state) {
            return {
                ...state,
                loadingReportList: true
            };
        },
        [types.GET_REPORTS_LIST_FAILED](state) {
            return {
                ...state,
                loadingReportList: false
            };
        },
        [types.GET_REPORTS_LIST_RESPONSE](state, action) {
            return {
                ...state,
                loadingReportList: false, reports: action.response
            };
        },


        [types.FILTER_REPORT_REQUEST](state) {
            return {
                ...state,
                loadingReportList: true,
                reports:{

                }
            };
        },
        [types.FILTER_REPORT_RESPONSE](state,action) {
            return {
                ...state,
                loadingReportList: false, reports: action.response
            };
        },
        [types.FILTER_REPORT_FAILED](state, action) {
            return {
                ...state,
                loadingReportList: false
            };
        }
    });



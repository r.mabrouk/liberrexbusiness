

/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {
    loadingRulesList: false,
    loadingDeleteRule: false,
    loadingCreateAndUpdateRule: false,
    rulesList: [

    ]
};
export const rulesReducer = createReducer(initialState,
    {
        // -------- get rules list --------------
        [types.GET_RULES_LIST_REQUEST](state, action) {
            return {
                ...state,
                loadingRulesList: true
            };
        },
        [types.GET_RULES_LIST_FAILED](state, action) {
            return {
                ...state,
                loadingRulesList: false
            };
        },
        [types.GET_RULES_LIST_RESPONCE](state, action) {
            return {
                ...state,
                rulesList: action.response,
                loadingRulesList: false
            };
        },

        // -------- create rule --------------
        [types.CREATE_RULE_REQUEST](state, action) {
            return {
                ...state,
                loadingCreateAndUpdateRule: true
            };
        },
        [types.CREATE_RULE_FAILED](state, action) {
            return {
                ...state,
                loadingCreateAndUpdateRule: false
            };
        },
        [types.CREATE_RULE_RESPONCE](state, action) {
            return {
                ...state,
                rulesList: action.response,
                loadingCreateAndUpdateRule: false
            };
        },
        // -------- update rule --------------
        [types.UPDATE_RULE_REQUEST](state, action) {
            return {
                ...state,
                loadingCreateAndUpdateRule: true
            };
        },
        [types.UPDATE_RULE_FAILED](state, action) {
            return {
                ...state,
                loadingCreateAndUpdateRule: false
            };
        },
        [types.UPDATE_RULE_RESPONCE](state, action) {
            let temp = state.rulesList
            temp[action.index] = action.response
            return {
                ...state,
                rulesList: JSON.parse(JSON.stringify(temp)),
                loadingCreateAndUpdateRule: false
            };
        },

        // -------- delete rule --------------
        [types.DELETE_RULE_REQUEST](state, action) {
            return {
                ...state,
                loadingDeleteRule: true
            };
        },
        [types.DELETE_RULE_FAILED](state, action) {
            return {
                ...state,
                loadingDeleteRule: false
            };
        },
        [types.DELETE_RULE_RESPONCE](state, action) {   
            let temp = state.rulesList
            temp.splice(action.index, 1)
            if (state.rulesList.length===0)
            temp=[]
            return {
                ...state,
                rulesList: JSON.parse(JSON.stringify(temp)),
                loadingDeleteRule: false
            };
        },

    });

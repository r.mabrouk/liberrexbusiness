/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {
    loadingServicesList: false,
    loadingIndustryServices: false,
    loadingremoveService: false,
    loadingUpdateService: false,
    loadingCreateService: false,
    industryServices: [

    ],
    IndustryServices_pages: 1,
    IndustryServices_current_page:1,

    language: null,
    servicesList: [

    ],
    services_current_page: 1,
    services_last_page: 1,

    firstTime: true,
    selectedService: ''
};
export const servicesReducer = createReducer(initialState,
    {

        // get all services
        [types.GET_ALL_SERVICES_RESPONSE](state, action) {
            return {
                ...state,
                servicesList: state.servicesList.concat(action.response.data),
                loadingServicesList: false,
                services_current_page:action.response.current_page,
                services_last_page:action.response.last_page
            };
        },
        [types.GET_ALL_SERVICES_REQUEST](state, action) {
            if (action.resetServices)
                return {
                    ...state, servicesList: [],
                    loadingServicesList: true,
                    pages: 1
                };
            else return {
                ...state, loadingServicesList: true,
            }
        },
        [types.GET_ALL_SERVICES_FAILED](state, action) {
            return { ...state, loadingServicesList: false }
        },

        // remove service 
        [types.DELETE_SERVICE_REQUEST](state, action) {

            return { ...state, loadingremoveService: true };
        },
        [types.DELETE_SERVICE_RESPONSE](state, action) {
            let temp = state.servicesList
            temp.splice(action.index, 1)
            return { ...state, loadingremoveService: false, servicesList: temp };
        },
        [types.DELETE_SERVICE_FAILED](state, action) {
            return { ...state, loadingremoveService: false };
        },

        // update service 
        [types.UPDATE_SERVICE_REQUEST](state, action) {
            return { ...state, loadingUpdateService: true };
        },
        [types.UPDATE_SERVICE_RESPONSE](state, action) {

            //update serives
            let newServices = state.servicesList
            newServices[action.index] = action.response
            return { ...state, loadingUpdateService: false, servicesList: newServices };
        },
        [types.UPDATE_SERVICE_FAILED](state, action) {
            return { ...state, loadingUpdateService: false };
        },
        // create service 
        [types.CREATE_SERVICE_REQUEST](state, action) {
            return { ...state, loadingCreateService: true };
        },
        [types.CREATE_SERVICE_RESPONSE](state, action) {
            return { ...state, loadingCreateService: false };
        },
        [types.CREATE_SERVICE_FAILED](state, action) {
            return { ...state, loadingCreateService: false };
        },

        // create service 
        [types.GET_INDUSTRY_SERVICES_REQUEST](state, action) {
            if (action.reset) {
                return {...state,
                    industryServices: [],
                    IndustryServices_pages: 1,
                    IndustryServices_current_page: 1,
                    loadingIndustryServices: true 
                }
            } else
                return { ...state, loadingIndustryServices: true };
        },
        [types.GET_INDUSTRY_SERVICES_RESPONSE](state, action) {
            return {
                ...state, loadingIndustryServices: false,
                industryServices: action.response.data,
                IndustryServices_pages: action.response.last_page,
                IndustryServices_current_page: action.response.current_page,
            };
        },
        [types.GET_INDUSTRY_SERVICES_FAILED](state, action) {
            return { ...state, loadingIndustryServices: false };
        },

        // ---------------- get service ------------------
        [types.GET_SERVICE](state, action) {
            return {
                ...state,
                firstTime: action.firstTime,
                selectedServiceNames: action.firstTime ? '' : state.selectedServiceNames,
            };
        },
        [types.GET_SERVICE_RESPONSE](state, action) {
            return {
                ...state,
                selectedServiceNames: firstTime ?
                    action.response.title :
                    state.selectedServiceNames + ', ' + action.response.title
            };
        },
        [types.GET_SERVICE_FAILED](state, action) {
            return { ...state };
        },

    });

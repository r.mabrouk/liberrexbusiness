

/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {
    loadingteamMembersList: false,
    loadingDeleteTeamMember: false,
    loadingCreateAndEditTeamMember: false,
    teamMembers: [

    ]
};
export const teamMembersReducers = createReducer(initialState,
    {
        // -------- get all team Members List--------------
        [types.GET_ALL_TEAM_MEMBER_REQUEST](state, action) {
            return {
                ...state,
                loadingteamMembersList: true
            };
        },
        [types.GET_ALL_TEAM_MEMBER_FAILED](state, action) {
            return {
                ...state,
                loadingteamMembersList: false
            };
        },
        [types.GET_ALL_TEAM_MEMBER_RESPONSE](state, action) {
            return {
                ...state,
                teamMembers: action.response.data, loadingteamMembersList: false
            };
        },



       
        // -------- remove team Member --------------
        [types.REMOVE_TEAM_MEMBER_REQUEST](state, action) {
            return {
                ...state,
                loadingDeleteTeamMember: true
            };
        },
        [types.REMOVE_TEAM_MEMBER_FAILED](state, action) {
            return {
                ...state,
                loadingDeleteTeamMember: false
            };
        },
        [types.REMOVE_TEAM_MEMBER_RESPONSE](state, action) {

            let temp = state.teamMembers
            temp.splice(action.index,1)

            return {
                ...state,
                loadingDeleteTeamMember: false,
                teamMembers:JSON.parse(JSON.stringify(temp))

            };
        },
         // -------- update team Member --------------
         [types.UPDATE_TEAM_MEMBER_REQUEST](state, action) {
            return {
                ...state,
                loadingCreateAndEditTeamMember: true
            };
        },
        [types.UPDATE_TEAM_MEMBER_FAILED](state, action) {
            return {
                ...state,
                loadingCreateAndEditTeamMember: false
            };
        },
        [types.UPDATE_TEAM_MEMBER_RESPONSE](state, action) {
            let tamp=state.teamMembers
            tamp[action.index]=action.response
            return {
                ...state, loadingCreateAndEditTeamMember: false,
                teamMembers:JSON.parse(JSON.stringify(tamp))
            };
        },
        // -------- create team Member --------------
        [types.CREATE_TEAM_MEMBER_REQUEST](state, action) {
            return {
                ...state,
                loadingCreateAndEditTeamMember: true
            };
        },
        [types.CREATE_TEAM_MEMBER_FAILED](state, action) {
            return {
                ...state,
                loadingCreateAndEditTeamMember: false
            };
        },
        [types.CREATE_TEAM_MEMBER_RESPONSE](state, action) {
            return {
                ...state,
                loadingCreateAndEditTeamMember: false
            };
        }


    });

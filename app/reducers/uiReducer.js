/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {
    
    language: null,
    user_login_data: {
        email: '',
        password: '',
        remember:false,
       
    },
    menuState:false
};
export const uiReducer = createReducer(initialState,
    {
        [types.SET_APP_LANGUAGE](state, action) {
            return {
                ...state,
                language: action.lang
            };
        },
        [types.SET_USER_LOGIN_DATA](state, action) {
            return {
                ...state,
                user_login_data: action.login_data
            };
        },
        [types.SET_MENU_STATE](state, action) {
            return {
                ...state,
                menuState: action.menuState
            };
        }

    });

    

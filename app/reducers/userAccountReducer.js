
/* Login Reducer
 * handles login states in the app
 */
import createReducer from 'app/lib/createReducer';
import * as types from '../actions/types';

const initialState = {

    loadingUpdateAppSetting: false,
    appSettings:{
        language:"en",
        notification_push: {
            all_notification:1,
            notification_email: 0,
            notification_push: 0,
            notification_sms: 0
        }
    }
};
export const userAccountReducer = createReducer(initialState,
    {
        [types.UPDATE_APP_SETTING_REQUEST](state, action) {
            return {
                ...state,
                loadingUpdateAppSetting: true
            };
        },
        [types.UPDATE_APP_SETTING_FAILED](state, action) {
            return {
                ...state,
                loadingUpdateAppSetting: false
            };
        },
        [types.UPDATE_APP_SETTING_RESPONSE](state, action) {
            return {
                ...state,
                loadingUpdateAppSetting: false,
                appSettings:action.response.settings,
            };
        },

        [types.SET_APP_LANGUAGE](state, action) {
            let temp=state.appSettings
            temp.language=action.lang
            return {
                ...state,
                loadingUpdateAppSetting: false,
                appSettings:temp,
            };
        }


    });

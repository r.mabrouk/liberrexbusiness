
import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import {
    approveBookingRequest,
    createBooking,
    deleteBooking,
    deleteBookingRequest,
    filterBookingList,
    getAllBookingList,
    getBookingRequestList,
    updateBooking
} from '../api/methods/booking';
import * as bookingActions from '../actions/booking';
import { _string } from '../local';
import NavigationService from '../navigation/NavigationService';
import { _Toast } from '../components/toast/toast';

getToken = (state) => state.authReducer.user_profile.token;


//approve Booking Request Saga
export function* approveBookingRequestSaga(action) {
    const token = yield select(getToken);
    const response = yield call(approveBookingRequest, { token, id: action.id });
    console.log("approveBookingRequestSaga", response)
    if (response.status == 'success') {
        _Toast.open(_string("ApprovedRequestSuccessfuly"), "s")
        yield put(bookingActions.approveBookingRequestResponse(response, action.index));
        yield put(bookingActions.getBookingListRequest(true));
    } else {
        yield put(bookingActions.approveBookingRequestFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")

    }
}



//create Booking  Saga

export function* createBookingSaga(action) {
    const token = yield select(getToken);
    const response = yield call(createBooking, { token, data: action.data });
    console.log("createBookingSaga", response)
    if (response.status == 'success') {
        yield put(bookingActions.createBookingResponse(response));
        _Toast.open(_string("createBookingSuccessfuly"), "s")
        yield put(bookingActions.getBookingListRequest(true));
        NavigationService.goBack()

    } else {
        yield put(bookingActions.approveBookingRequestFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")

    }
}


// delete Booking Saga

export function* deleteBookingSaga(action) {
    const token = yield select(getToken);
    const response = yield call(deleteBooking, { token, id: action.id });
    console.log("deleteBookingSaga", response)
    if (response.status == 'success') {
        yield put(bookingActions.deleteBookingResponse(response, action.index));
        action.onCloseModal()
        _Toast.open(_string("DeleteBookingSuccessfuly"), "s")
    } else {
        yield put(bookingActions.deleteBookingFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")

    }
}


// delete Booking Request
export function* deleteBookingRequestSaga(action) {
    const token = yield select(getToken);
    const response = yield call(deleteBookingRequest, { token, id: action.id });
    console.log("deleteBookingRequestSaga", response)
    if (response.status == 'success') {
        yield put(bookingActions.deleteBookingRequestResponse(response, action.index));
        _Toast.open(_string("DeleteRequestSuccessfuly"), "s")
    } else {
        yield put(bookingActions.deleteBookingRequestFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}
// filter Booking 
export function* filterBookingListSaga(action) {
    const token = yield select(getToken);
    const response = yield call(filterBookingList, { token, filter: action.date, page: action.page || 1 });
    console.log("filterBookingListSaga", response)
    if (response.status == 'success') {
        yield put(bookingActions.filterBookingResponse(response));
    } else {
        yield put(bookingActions.filterBookingFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}
// get all Booking 
export function* getAllBookingListSaga(action) {
    const token = yield select(getToken);
    const response = yield call(getAllBookingList, { token, page: action.page, keyword: action.keyword });
    console.log("getAllBookingListSaga", response)
    if (response.status == 'success') {
        yield put(bookingActions.getBookingListResponse(response.bookings));
    } else {
        yield put(bookingActions.getBookingListFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}
// get Booking Request List
export function* getBookingRequestListSaga(action) {
    const token = yield select(getToken);
    const response = yield call(getBookingRequestList, { token, page: action.page || 1 });
    console.log("getAllBookingListSaga", response)
    if (response.status == 'success') {
        yield put(bookingActions.getBookingRequestListResponse(response.requests));
    } else {
        yield put(bookingActions.getBookingRequestListFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


export function* updateBookingSaga(action) {
    const token = yield select(getToken);
    const response = yield call(updateBooking, { token, data: action.data, id: action.id });
    console.log("updateBookingSaga", response)
    if (response.status == 'success') {
        yield put(bookingActions.updateBookingResponse(response.booking, action.index));
        
        _Toast.open(_string("UpdateBookingSuccessfuly"), "s")
        NavigationService.goBack()


    } else {
        yield put(bookingActions.updateBookingFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")

    }
}

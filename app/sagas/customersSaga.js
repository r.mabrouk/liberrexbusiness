/* Redux saga class
 * logins the user into the app
 * requires username and password.
 * un - username
 * pwd - password
 */
import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { Alert } from 'react-native';
import * as customerActions from '../actions/customers';
import * as navigationActions from '../actions/navigationActions';
import {
    createCustomer,
    deleteCustomer,
    getACustomers,
    getAllCustomers,
    updateCustomer,
    getAllCustomersSearch,
    getAllCustomerBooks,
    getAllCustomerBooksSearch,
    removeCustomerFromBook,
    addCustomerToBooks,
    getAllCustomerFeedback,
    filterCustomerFeedback
}
    from '../api/methods/customers';
import { _string } from '../local';
import NavigationService from '../navigation/NavigationService';
import { _Toast } from '../components/toast/toast';
getToken = (state) => state.authReducer.user_profile.token;


// Our worker Saga that logoutSaga
export function* createCustomerSaga(action) {
    //how to call api
    const token = yield select(getToken);
    const response = yield call(createCustomer, { customet_data: action.customer_data, token });
    console.log(response, "responseresponseresponsess")
    if (response.status == 'success') {
        yield put(customerActions.createCustomerResponse(response));

        if (action.onCloseModal)
            action.onCloseModal()
        else {
            yield put(customerActions.SelectCustomer(response.customer));
            NavigationService.goBack()
        }
        yield put(customerActions.getAllCustomersRequest(true, null, 1));

    } else {
        yield put(customerActions.createCustomerFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


// Our worker Saga that logoutSaga
export function* deleteCustomerSaga(action) {
    //how to call api
    const token = yield select(getToken);
    const response = yield call(deleteCustomer, { customer_id: action.customer_id, token });
    console.log(response, "responseresponseresponsess")
    if (response.status == 'success') {
        yield put(customerActions.deleteCustomerResponse(action.index, action.loader, action.keyOfReducer));
        if (action.keyOfReducer == 'vip_customers') {
            yield put(customerActions.getAllCustomersVipRequest(true, null, 1));
        }
        else
            yield put(customerActions.getAllCustomersRequest(true, null, 1));

        action.onCloseModal()
    } else {
        yield put(customerActions.deleteCustomerFailed(action.loader));
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


// Our worker Saga that logoutSaga
export function* getACustomersSaga(action) {
    //how to call api
    const response = yield call(getACustomers, { customer_id: action.customer_id });
    console.log(JSON.stringify(response) + "getACustomers")
    if (response.status == 'success') {
        yield put(customerActions.getAllCustomersResponse(response));
        // yield call(navigationActions.navigateToHome);
    } else {
        yield put(customerActions.getACustomersfdgFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}
// all customers 
export function* getAllCustomersVipSaga(action) {
    const token = yield select(getToken);
    response = yield call(getAllCustomerBooks, { token: token, page: 1, keyword: action.keyword, book_id: 1 });
    if (response.status == 'success') {
        yield put(customerActions.getAllCustomersVipResponse(response.customers));
        // yield call(navigationActions.navigateToHome);
    } else {
        yield put(customerActions.getAllCustomersVipFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}

// Our worker Saga that logoutSaga
export function* getAllCustomersBlacklistSaga(action) {
    const token = yield select(getToken);
    const response = yield call(getAllCustomerBooks, { token, page: action.page, keyword: action.keyword, book_id: 2 });
    if (response.status == 'success') {
        yield put(customerActions.getAllCustomersBlacklistResponse(response.customers));
    } else {
        yield put(customerActions.getAllCustomersBlacklistFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


// removeCustomerFromBookSaga


// get all customers 
export function* getAllCustomersSaga(action) {
    const token = yield select(getToken);
    const response = yield call(getAllCustomers, { token, page: action.page, keyword: action.keyword });
    if (response.status == 'success') {
        yield put(customerActions.getAllCustomersResponse(response.customers));
        // yield call(navigationActions.navigateToHome);
    } else {
        yield put(customerActions.getAllCustomersFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}

// Our worker Saga that logoutSaga
export function* updateCustomerSaga(action) {
    //how to call api
    const token = yield select(getToken);

    const response = yield call(updateCustomer, { customer_data: action.customer_data, token });
    console.log(JSON.stringify(response) + "responseresponseresponsess")
    if (response.status == 'success') {
        action.editCustomerModal()
        yield put(customerActions.updateCustomerResponse(response.customer, action.keyOfReducer));

        // yield call(navigationActions.navigateToHome);
    } else {
        yield put(customerActions.updateCustomerFailed());
        console.log(response, "updateCustomerFailed")

        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


// remove Customer From Book Saga
export function* removeCustomerFromBookSaga(action) {
    const token = yield select(getToken);
    const response = yield call(removeCustomerFromBook, { customer_id: action.id, book_id: action.books_id, token });
    if (response.status == 'success') {
        yield put(customerActions.removeCustomerFromBookResponse(response, action.loader, action.keyOfReducer));
        yield put(customerActions.getAllCustomersVipRequest(true, null, 1));

        action.onCloseModal()
    } else {
        yield put(customerActions.removeCustomerFromBookFailed(action.loader));
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


// add Customer To Books Saga
export function* addCustomerTobBooksSaga(action) {
    //how to call api
    const token = yield select(getToken);
    const response = yield call(addCustomerToBooks, { customer_id: action.id, book_id: action.books_id, token });
    console.log(response, "responseresponseresponsess", action.books_id, action.id, action.loader)
    if (response.status == 'success') {
        yield put(customerActions.addCustomerToBooksResponse(response, action.loader, action.books_id, action.keyOfReducer));
        action.onCloseModal()

        if (action.books_id == 2) {
            yield put(customerActions.getAllCustomersBlacklistRequest(true, null, 1));
        }
        if (action.books_id == 1) {
            yield put(customerActions.getAllCustomersVipRequest(true, null, 1));
        }
    } else {
        yield put(customerActions.addCustomerToBooksFailed(action.loader));
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}






// get all customer feedback
export function* getAllCustomerFeedbackSaga(action) {
    //how to call api
    const token = yield select(getToken);
    const response = yield call(getAllCustomerFeedback, { token });

    console.log("getAllCustomersBlacklistRequest", response)
    if (response.status == 'success') {
        yield put(customerActions.getAllCustomerFeedbackResponse(response));
    } else {
        yield put(customerActions.getAllCustomerFeedbackFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}

// filter  customer feedback
export function* filterCustomerFeedbackSaga(action) {
    //how to call api
    const token = yield select(getToken);
    const response = yield call(filterCustomerFeedback, { token, filter: action.filter_data });
    if (response.status == 'success') {
        yield put(customerActions.filterCustomerFeedbackResponse(response));
    } else {
        yield put(customerActions.filterCustomerFeedbackFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


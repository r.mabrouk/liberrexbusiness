
import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { GetPlaces } from "../api/methods/googleMap";

import * as googleMapActions from '../actions/googleMap';

export  function* getPlacesSaga(action) {
    const response = yield call(GetPlaces, { place_name: action.place_name });
    console.log("responseresponse",response)
if (response.status=='OK'||response.status=='ZERO_RESULTS')
yield put(googleMapActions.getPlacesResponse(response.results));

}
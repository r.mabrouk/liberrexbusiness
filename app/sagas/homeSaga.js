
import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import * as homeActions from '../actions/home';

import { getHomeStats, filterHomeReports } from '../api/methods/home';
import NavigationService from '../navigation/NavigationService';
getToken = (state) => state.authReducer.user_profile.token;


export function* getHomeStatsSaga(action) {
    const token = yield select(getToken);
    const response = yield call(getHomeStats, { token });
    console.log("getRulesgetRulesSagagetRulesSagaSaga", response)
    if (response.status == 'success') {
        yield put(homeActions.getHomeStatsResponse(response));
        NavigationService.replace("Tabs")
    } else {
        yield put(homeActions.getHomeStatsFailed());
    }

}

export function* filterHomeReportsSaga(action) {
    const token = yield select(getToken);
    const response = yield call(filterHomeReports, { token });
    console.log("getRulesgetRulesSagagetRulesSagaSaga", response)
    if (response.status == 'success') {
        yield put(homeActions.filterHomeReportsResponse(response));
    } else {
        yield put(homeActions.filterHomeReportsFailed());
    }

}


// FILTER_HOME_REPORTS_REQUEST
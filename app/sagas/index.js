/**
 *  Redux saga class init
 */
import { takeEvery, all, takeLatest, take } from 'redux-saga/effects';
import * as types from '../actions/types';
import { loginSaga, logoutSaga, resetPasswordSaga, signupSaga, getAllIndustriesSaga, sendNewPasswordSaga, searchIndustriesSaga, getCountriesSaga, socialLoginSaga, checkEmailSaga } from './outhSaga';
import {
    createCustomerSaga,
    deleteCustomerSaga,
    getACustomersSaga,
    getAllCustomersSaga,
    updateCustomerSaga,
    getAllCustomersVipSaga,
    getAllCustomersBlacklistSaga,
    removeCustomerFromBookSaga,
    addCustomerTobBooksSaga,
    getAllCustomerFeedbackSaga,
    filterCustomerFeedbackSaga
} from './customersSaga';
import { getPlacesSaga } from './googleMapSaga';
import { getAllCustomersBlacklistRequest } from '../actions/customers';
import { getAllServiesSaga, updateServiesSaga, removeServiesSaga, createServiesSaga, getIndustryServicesSaga, getServieSaga } from './servicesSaga';
import { getQueueList, addQueue, getQueueWaitingList, getQueuePendingList, approveQueueRequestSaga, declineQueueRequestSaga, addCustoemrToQueueSaga, redirectCustomerSaga, cancelCustomerInWaitingSaga, callNextCustomerSaga, swapCustomerRanksSaga, updateQueueSaga, getQueueSaga, deleteQueueSaga } from './queueSaga';
import { notificationSaga } from './notificationSaga';
import { getAllTeamMembersSaga, createTeamMembersSaga, updateTeamMembersSaga, deleteTeamMembersSaga } from './teamMembersSaga';
import { getMembershipSaga } from './membershipSaga';
import { getRulesSaga, createRuleSaga, deleteRuleSaga, updateRuleSaga } from './rulesSaga';
import { updateUserAccountSaga, updatePasswordSaga, updateBusinessWorkingDayesSaga, updateBusinessLocationSaga, updateBusinessInfoSaga, updateAppSettingSaga } from './userAccountSaga';
import { getHomeStatsSaga, filterHomeReportsSaga } from './homeSaga';
import { addNewPaymentCardSaga, getPaymentCardsSaga, deletePamentCardSaga, updateAccountMembershipSaga } from './paymentSaga';
import { getAllBookingListSaga, createBookingSaga, updateBookingSaga, deleteBookingSaga, filterBookingListSaga, approveBookingRequestSaga, deleteBookingRequestSaga, getBookingRequestListSaga } from './bookingSaga';
import { getReportListSaga, filterReportSaga } from './reportSaga';

export default function* watch() {
    yield all(
        [takeEvery(types.LOGIN_REQUEST, loginSaga),
        takeEvery(types.SOCIAL_LOGIN_REQUEST, socialLoginSaga),
        takeEvery(types.LOGOUT_REQUEST, logoutSaga),
        takeEvery(types.RESET_PASSWORD_REQUEST, resetPasswordSaga),
        takeEvery(types.RESET_PASSWORD_REQUEST_WITH_CODE, sendNewPasswordSaga),
        takeEvery(types.SIGNUP_REQUEST, signupSaga),
        takeEvery(types.GET_ALL_INDUSTRIES_REQUEST, getAllIndustriesSaga),
        takeLatest(types.SEARCH_INDUSTRIES_REQUEST, searchIndustriesSaga),
        takeEvery(types.GET_COUNTRIES_REQUEST, getCountriesSaga),
        // customers actions saga 
        takeEvery(types.CREATE_CUSTOMER_REQUEST, createCustomerSaga),
        takeEvery(types.DELETE_CUSTOMER_REQUEST, deleteCustomerSaga),
        takeEvery(types.UPDATE_CUSTOMER_REQUEST, updateCustomerSaga),
        takeLatest(types.GET_ALL_CUSTOMERS_REQUEST, getAllCustomersSaga),
        takeEvery(types.GET_A_CUSTOMERS_REQUEST, getACustomersSaga),
        takeEvery(types.GET_ALL_CUSTOMERS_VIP_REQUEST, getAllCustomersVipSaga),
        takeEvery(types.GET_ALL_CUSTOMERS_BLACKLIST_REQUEST, getAllCustomersBlacklistSaga),
        takeEvery(types.REMOVE_CUSTOMER_FROM_BOOK_REQUEST, removeCustomerFromBookSaga),
        takeEvery(types.GET_ALL_CUSTOMER_FEEDBACK_REQUEST, getAllCustomerFeedbackSaga),
        takeEvery(types.FILTER_CUSTOMER_FEEDBACK_REQUEST, filterCustomerFeedbackSaga),
        takeEvery(types.ADD_CUSTOMER_TO_BOOKS_REQUEST, addCustomerTobBooksSaga),
        // google map
        takeLatest(types.GET_PLACES_FROM_GOOGLE_API_REQUEST, getPlacesSaga),
        //    services
        takeLatest(types.GET_ALL_SERVICES_REQUEST, getAllServiesSaga),
        takeLatest(types.UPDATE_SERVICE_REQUEST, updateServiesSaga),
        takeLatest(types.DELETE_SERVICE_REQUEST, removeServiesSaga),
        takeLatest(types.CREATE_SERVICE_REQUEST, createServiesSaga),
        takeEvery(types.GET_SERVICE, getServieSaga),

        // queue
        takeLatest(types.GET_INDUSTRY_SERVICES_REQUEST, getIndustryServicesSaga),
        takeLatest(types.GET_QUEUE_LIST, getQueueList),
        takeLatest(types.ADD_QUEUE, addQueue),
        takeLatest(types.GET_QUEUE_WAITING_LIST, getQueueWaitingList),
        takeLatest(types.GET_QUEUE_PENDING_LIST, getQueuePendingList),
        takeLatest(types.APPROVE_QUEUE_REQUEST, approveQueueRequestSaga),
        takeLatest(types.DECLINE_QUEUE_REQUEST, declineQueueRequestSaga),
        takeLatest(types.ADD_CUSTOMER_TO_QUEUE, addCustoemrToQueueSaga),
        takeLatest(types.REDIRECT_CUSTOMER, redirectCustomerSaga),
        takeLatest(types.CANCEL_CUSTOMER_IN_WAITING, cancelCustomerInWaitingSaga),
        takeLatest(types.CALL_NEXT_CUSTOMER, callNextCustomerSaga),
        takeLatest(types.SWAP_CUSTOMER_RANKS, swapCustomerRanksSaga),

        takeLatest(types.DELETE_A_QUEUE_REQUEST, deleteQueueSaga),
        takeLatest(types.GET_A_QUEUE_REQUEST, getQueueSaga),
        takeLatest(types.UPDATE_A_QUEUE_REQUEST, updateQueueSaga),

        
        // notificationSaga
        takeLatest(types.GET_ALL_NOTIFICATION_REQUEST, notificationSaga),
        // teamMembersSaga
        takeLatest(types.GET_ALL_TEAM_MEMBER_REQUEST, getAllTeamMembersSaga),
        takeLatest(types.CREATE_TEAM_MEMBER_REQUEST, createTeamMembersSaga),
        takeLatest(types.UPDATE_TEAM_MEMBER_REQUEST, updateTeamMembersSaga),
        takeLatest(types.REMOVE_TEAM_MEMBER_REQUEST, deleteTeamMembersSaga),
        //member shaip plans 
        takeLatest(types.GET_MEMBERSHIP_PLAN_REQUEST, getMembershipSaga),
        // rules 
        takeLatest(types.GET_RULES_LIST_REQUEST, getRulesSaga),
        takeLatest(types.UPDATE_RULE_REQUEST, updateRuleSaga),
        takeLatest(types.DELETE_RULE_REQUEST, deleteRuleSaga),
        takeLatest(types.CREATE_RULE_REQUEST, createRuleSaga),
        // update user
        takeLatest(types.UPDATE_USER_ACCOUNT_REQUEST, updateUserAccountSaga),
        takeLatest(types.UPDATE_PASSWORD_REQUEST, updatePasswordSaga),
        // update business info
        takeLatest(types.UPDATE_BUSINESS_INFO_REQUEST, updateBusinessInfoSaga),
        takeLatest(types.UPDATE_BUSINESS_LOCATION_REQUEST, updateBusinessLocationSaga),
        takeLatest(types.UPDATE_BUSINESS_WORKING_DAYES_REQUEST, updateBusinessWorkingDayesSaga),
        // payment
        takeLatest(types.ADD_NEW_PAYMENT_CARD_REQUEST, addNewPaymentCardSaga),
        takeLatest(types.GET_PAYEMENT_CARDS_REQUEST, getPaymentCardsSaga),
        takeLatest(types.DELETE_PAYEMENT_CARD_REQUEST, deletePamentCardSaga),
        takeLatest(types.UPDATE_ACCOUNT_MEMBERSHIP_REQUEST, updateAccountMembershipSaga),



        // booking
        takeLatest(types.GET_BOOKING_LIST_REQUEST, getAllBookingListSaga),
        takeLatest(types.CREATE_BOOKING_REQUEST, createBookingSaga),
        takeLatest(types.UPDATE_BOOKING_REQUEST, updateBookingSaga),
        takeLatest(types.DELETE_BOOKING_REQUEST_REQUEST, deleteBookingRequestSaga),
        takeLatest(types.FILTER_BOOKING_REQUEST, filterBookingListSaga),
        takeLatest(types.APPROVE_BOOKING_REQUEST_REQUEST, approveBookingRequestSaga),
        takeLatest(types.DELETE_BOOKING_REQUEST, deleteBookingSaga),
        takeLatest(types.GET_HOME_STATS_REQUEST, getHomeStatsSaga),
        //home 
        takeLatest(types.GET_BOOKING_REQUEST_LIST_REQUEST, getBookingRequestListSaga),
        takeLatest(types.UPDATE_APP_SETTING_REQUEST, updateAppSettingSaga),
        takeLatest(types.FILTER_HOME_REPORTS_REQUEST, filterHomeReportsSaga),


        // report
        takeLatest(types.GET_REPORTS_LIST_REQUEST, getReportListSaga),
        takeLatest(types.FILTER_REPORT_REQUEST, filterReportSaga),

        takeLatest(types.CHECK_EMAIL_REQUEST, checkEmailSaga),

        ]
    )
        ;
}

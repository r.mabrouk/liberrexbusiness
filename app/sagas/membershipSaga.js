/* Redux saga class
 * logins the user into the app
 * requires username and password.
 * un - username
 * pwd - password
 */
import { put, call, select } from 'redux-saga/effects';
import * as membershipActions from '../actions/membership';
import { getMembershipPlans } from '../api/methods/membership';
import { _string } from '../local';
import { _Toast } from '../components/toast/toast';

getToken = (state) => state.authReducer.user_profile.token;

export function* getMembershipSaga(action) {
    const token = yield select(getToken);
    const response = yield call(getMembershipPlans, { token });

    console.log("notificationsnotifications", response)


    if (response.packages) {
        let selectedPackagesIndex=0
            for (let i=0;i<response.packages.length;i++)
            {
                if (response.packages[i].selected==1){
                    selectedPackagesIndex=i
                }
            }
        yield put(membershipActions.getMembershipPlansResponse(response.packages,selectedPackagesIndex));
    } else {
        yield put(membershipActions.getMembershipPlansFailed());
        
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


/* Redux saga class
 * logins the user into the app
 * requires username and password.
 * un - username
 * pwd - password
 */
import { put, call, select } from 'redux-saga/effects';
import * as notificationActions from '../actions/notification';
import { getNotificationList } from '../api/methods/notification';

getToken = (state) => state.authReducer.user_profile.token;


export function* notificationSaga(action) {
    const token = yield select(getToken);
    const response = yield call(getNotificationList, { token});

    console.log("notificationsnotifications",response)
    if (response.notifications) {
        yield put(notificationActions.getNotificationListResponse(response.notifications));
    } else {
        yield put(notificationActions.getNotificationListFailed());
        // _Toast.open(_string("messages_alert.there_are_errors_in_email_or_passowrd"), "e")
    }
}


/* Redux saga class
 * logins the user into the app
 * requires username and password.
 * un - username
 * pwd - password
 */
import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { Alert } from 'react-native';
import * as authActions from '../actions/auth';
import * as homeActions from '../actions/home';


import * as navigationActions from '../actions/navigationActions';
import { loginUser, socialLogin, signUp, logout, reset_password, getAllIndustries, sendNewPassword, searchIndustr, getCountries, checkEmail } from '../api/methods/outh';
import NavigationService from '../navigation/NavigationService';
import { Drower } from '../components/sideBar';
import { _string } from '../local';
import { _Toast } from '../components/toast/toast';

getToken = (state) => state.authReducer.user_profile.token;

export function* socialLoginSaga(action) {
    const response = yield call(socialLogin, { user_data: action.user_data });
    console.log(response,"socialLoginSaga")
    if (response.status == 'ok') {
        yield call(navigationActions.navigateToHome);
        yield put(authActions.socialLoginResponse(response));
    } else {
        NavigationService.navigate('SignUpBusiness', { socialLogin_data: action.user_data, user_data: new FormData() });
        yield put(authActions.socialLoginFailed());
        // _Toast.open(_string("messages_alert.there_are_errors_in_email_or_passowrd"), "e")
    }
}
// Our worker Saga that logins the user
export function* loginSaga(action) {
    yield put(authActions.enableLoader());
    //how to call api
    const response = yield call(loginUser, { user_data: action.user_data });
    console.log(JSON.stringify(response) + "responseresponseresponsess")
    if (response.status == 'ok') {
        yield put(authActions.onLoginResponse(response));
        yield put(homeActions.getHomeStatsRequest(response));
        yield put(authActions.disableLoader());
        NavigationService.navigate("Tabs")

    } else {
        yield put(authActions.loginFailed());
        yield put(authActions.disableLoader({}));
        _Toast.open(_string("messages_alert.there_are_errors_in_email_or_passowrd"), "e")
    }
}

// Our worker Saga that signup
export function* signupSaga(action) {
    //how to call api
    console.log(action.user_data)
    const response = yield call(signUp, { user_data: action.user_data, social: action.social });
    console.log(response, action.user_data, "sdsdsdjkfndlkjfglkjglkdjflkdjflkddff")
    if (response.status == 'ok') {
        yield put(authActions.onSignUpResponse(response));
        yield put(homeActions.getHomeStatsRequest(response));
        NavigationService.replace("Tabs")
    } else {
        yield put(authActions.SignUpFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}

// Our worker Saga that resetPasswordSaga
export function* resetPasswordSaga(action) {
    //how to call api
    const response = yield call(reset_password, { email: action.email });
    // console.log()
    if (response.status == 'success') {
        yield put(authActions.onResetPasswordResponse(response));
        NavigationService.navigate("NewPassword", response)
    } else {
        yield put(authActions.onResetPasswordFailed());
        _Toast.open(response.message, "e")
    }
}

// Our worker Saga that resetPasswordSaga
export function* sendNewPasswordSaga(action) {
    //how to call api
    const response = yield call(sendNewPassword, { data: action.data });
    console.log(response, "responseresponsddddsdse", action.data)
    if (response.status == 'success') {
        yield put(authActions.onResetPasswordWithCodeResponse(response));
        NavigationService.navigate("Login")
    } else {
        if (response.error)
            _Toast.open(response.error.message, "e")
        yield put(authActions.resetPasswordWithCodeFailed());
    }
}

// Our worker Saga that logoutSaga
export function* logoutSaga(action) {
    //how to call api
    const token = yield select(getToken);
    const response = yield call(logout, { token: token });
    console.log(JSON.stringify(response) + "responseresponseresponsess")
    if (response.status == 'ok') {
        yield put(authActions.LogoutResponse());
        Drower.onDrower()
        NavigationService.navigate('Login')
    } else {
        yield put(authActions.loginFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


export function* getAllIndustriesSaga(action) {
    //how to call api
    const response = yield call(getAllIndustries);
    if (response.industries) {
        yield put(authActions.getAllIndustryResponse(response.industries));
    } else {
        yield put(authActions.getAllIndustryFailed());
        _Toast.open(_string("messages_alert.cant_get_industries_please_try_again"), "e")
    }
}


export function* searchIndustriesSaga(action) {
    //how to call api
    let response = null
    if (action.keyword)
        response = yield call(searchIndustr, { keyword: action.keyword });
    else
        response = yield call(getAllIndustries);
    if (response.sub_industries || response.industries) {
        yield put(authActions.searchIndustryResponse(response.sub_industries ? response.sub_industries : response.industries));
    } else {
        yield put(authActions.searchIndustryFailed());
        _Toast.open(_string("messages_alert.cant_get_industries_please_try_again"), "e")
    }
}

export function* getCountriesSaga(action) {
    //how to call api
    const response = yield call(getCountries);
    if (response.status == 'success') {
        yield put(authActions.getCountriesResponse(response.countries));
    } else {
        yield put(authActions.getCountriesFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}

export function* checkEmailSaga(action) {
    //how to call api
    const response = yield call(checkEmail, { email: action.email });
    if (response.status == 'success') {
        yield put(authActions.checkEmailResponse(response.countries));
        if (response.exists)
            _Toast.open(_string("ThisEmailIsAlreadyInUse"), "e")
        else {
            NavigationService.navigate('SignUpBusiness', { user_data: action.data })
        }

    } else {
        yield put(authActions.checkEmailFailed());
        _Toast.open(_string("ThisEmailIsAlreadyInUse"), "e")
    }
}

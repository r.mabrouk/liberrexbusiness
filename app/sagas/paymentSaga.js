
import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { _string } from '../local';

import * as paymentActions from '../actions/payment';
import { AddPaymentCard, getPaymentCards, deletePaymentCard, updateAccountMembership } from '../api/methods/payment';
getToken = (state) => state.authReducer.user_profile.token;
import Stripe, { PaymentCardTextField } from 'tipsi-stripe'
import NavigationService from '../navigation/NavigationService';
import { _Toast } from '../components/toast/toast';

export function* addNewPaymentCardSaga(action) {
    const token = yield select(getToken);

    Stripe.setOptions({
        publishableKey: 'pk_test_Y3E54h1PXzc5ya3sFHH8peuU00ZtFiRPpI',
        androidPayMode: 'test', // Android only
    })
    const params = {
        // mandatory
        number: action.data.cardNamber,
        expMonth: parseInt(action.data.expMonth),
        expYear: parseInt(action.data.expYear),
        cvc: action.data.cvc,

    }
    console.log("tokentokentokentoken", action.data)
    try {
        const StripeToken = yield Stripe.createTokenWithCard(params)
        if (StripeToken.status != "fail") {
            let data = {
                payment_method_id: StripeToken.tokenId,
                default: 1
            }
            const response = yield call(AddPaymentCard, { token, data });


            if (response.status == 'success') {
                yield put(paymentActions.addnewPaymentCardResponse(response));
                yield put(paymentActions.getPaymentCardsRequest());
                _Toast.open(_string("AddPaymentCardSuccessfuly"), "s")
                action.CloseModal()

            } else {
                yield put(paymentActions.addnewPaymentCardFailed());
            }

            console.log("getRulesgetRulesSagagetRuldfdfesSagaSaga", response)

        } else {
            // _Toast.open(StripeToken.error, "e")

        }
    }
    catch (e) {
        console.log("paymentActions", e)
        _Toast.open(e.toString().replace("Error:", ""), "e")
        yield put(paymentActions.addnewPaymentCardFailed());

    }

}

export function* getPaymentCardsSaga(action) {
    const token = yield select(getToken);
    const response = yield call(getPaymentCards, { token });
    console.log("getRulesgetRulesSagagetRulesSagaSaga", response)
    if (response.status == 'success') {
        yield put(paymentActions.getPaymentCardsResponse(response.billing_methods.data));
    } else {
        yield put(paymentActions.getPaymentCardsFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")

    }

}
export function* deletePamentCardSaga(action) {
    const token = yield select(getToken);
    const response = yield call(deletePaymentCard, { token, billing_method_id: action.id });
    console.log("getRulesgetRulesSagagetRulesSagaSaga", response)
    if (response.status == 'success') {
        yield put(paymentActions.deletePaymentCardResponse(action.index));
        _Toast.open(_string("DeletePaymentCardSuccessfuly"), "s")

    } else {
        yield put(paymentActions.getPaymentCardsFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }

}

export function* updateAccountMembershipSaga(action) {
    const token = yield select(getToken);
    const response = yield call(updateAccountMembership, { token, data: action.data });
    console.log("updateAccountMembershipSaga", response)
    if (response.status == 'success') {
        yield put(paymentActions.updateAccountMembershipResponse(response));
        _Toast.open(_string("CheckoutSuccessfuly"), "s")
        NavigationService.navigate("MembershipCheckoutStatus", { checkoutStatus: response.membership })
    } else {
        yield put(paymentActions.updateAccountMembershipFailed());
        _Toast.open(_string("CheckoutFaild"), "e")
    }

}



/* Redux saga class
 * logins the user into the app
 * requires username and password.
 * un - username
 * pwd - password
 */
import { put, call, select } from 'redux-saga/effects';
import * as customerActions from '../actions/customers';
import * as navigationActions from '../actions/navigationActions';
import { _string } from '../local';
import { getQueueListMethod, addQueueMethod, getQueueWaitingListMethod, getQueuePendingListMethod, approveQueueRequestMethod, declineQueueRequestMethod, addCustomerToqueueMethod, redirectQueueMethod, cancelCustomerInWaitingMethod, callNextCustomerMethod, swapCustomerRanksMethod, getQueue, updateQueue, deleteQueue } from '../api/methods/queue';
import { ActionCreators } from '../actions';
import { _Toast, ModalToast } from '../components/toast/toast';
import NavigationService from '../navigation/NavigationService';

getToken = (state) => state.authReducer.user_profile.token;
getQueueListNextPage = (state) => state.queueReducer.cuurent_page;
getSelectedQueueID = (state) => state.queueReducer.selectedQueueID;
getSelectedCustomer = (state) => state.queueReducer.selectedCustomer;

// get queue list
export function* getQueueList(action) {
    const token = yield select(getToken);
    const QueueListNextPage = yield select(getQueueListNextPage);
    let response = null
    response = yield call(getQueueListMethod, { token: token, page: QueueListNextPage });
    console.log("queue Saga", 'getQueueList', 'response', response)
    if (response.status == 'success') {
        yield put(ActionCreators.getQueueListResponse(response));
    } else {
        yield put(ActionCreators.getQueueListFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}

export function* addQueue(action) {
    const token = yield select(getToken);
    response = yield call(addQueueMethod, { token: token, body: action.body });
    console.log("queue Saga", 'addQueue', 'response', response)
    if (response.status == 'success') {
        yield put(ActionCreators.addQueueResponse(response));
        yield call(navigationActions.goBack);
        yield put(ActionCreators.getQueueList(true));
    } else {
        yield put(ActionCreators.addQueueFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


export function* getQueueWaitingList(action) {
    const token = yield select(getToken);
    const selectedQueueID = yield select(getSelectedQueueID);
    response = yield call(getQueueWaitingListMethod, { token: token, QueueID: selectedQueueID });
    console.log("queue Saga", 'getQueueWaitingList', 'response', response)
    if (response.status == 'success') {
        yield put(ActionCreators.getQueueWaitingListResponse(response));
    } else {
        yield put(ActionCreators.getQueueWaitingListFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}

export function* getQueuePendingList(action) {
    const token = yield select(getToken);
    const selectedQueueID = yield select(getSelectedQueueID);
    response = yield call(getQueuePendingListMethod, { token: token, QueueID: selectedQueueID });
    console.log("queue Saga", 'getQueuePendingList', 'response', response)
    if (response.status == 'success') {
        yield put(ActionCreators.getQueuePendingListResponse(response));
    } else {
        yield put(ActionCreators.getQueuePendingListFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


export function* approveQueueRequestSaga(action) {
    const token = yield select(getToken);
    const selectedQueueID = yield select(getSelectedQueueID);
    response = yield call(approveQueueRequestMethod, { token: token, queue_id: selectedQueueID, queue_request_id: action.queue_request_id });
    console.log("queue Saga", 'approveQueueRequestSaga', 'response', response)
    if (response.status == 'success') {
        yield put(ActionCreators.approveQueueRequestResponse(response));
        yield put(ActionCreators.getQueuePendingList());
        if (action.onCloseModal) {
            action.onCloseModal()
        }
    } else {
        yield put(ActionCreators.approveQueueRequestFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}

export function* declineQueueRequestSaga(action) {
    const token = yield select(getToken);
    const selectedQueueID = yield select(getSelectedQueueID);
    response = yield call(declineQueueRequestMethod, { token: token, queue_id: selectedQueueID, queue_request_id: action.queue_request_id });
    console.log("queue Saga", 'declineQueueRequestSaga', 'response', response)
    if (response.status == 'success') {
        yield put(ActionCreators.declineQueueRequestResponse(response));
        yield put(ActionCreators.getQueuePendingList());
        if (action.onCloseModal) {
            action.onCloseModal()
        }
    } else {
        yield put(ActionCreators.declineQueueRequestFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


export function* addCustoemrToQueueSaga(action) {
    const token = yield select(getToken);
    const selectedQueueID = yield select(getSelectedQueueID);
  let response = yield call(addCustomerToqueueMethod, { token: token, queue_id: selectedQueueID, body: action.body })
    console.log("queue Saga", 'addCustoemrToQueueSaga', 'response', response)
    if (response.status == 'success') {
        action.onCloseModal(false)
        yield put(ActionCreators.addCustomerToQueueResponse(response));
       
        yield call(navigationActions.goBack);
    } else {
        action.onCloseModal(false)
        yield put(ActionCreators.addCustomerToQueueFailed());
        ModalToast.open(response.message, "e")
    }
}


export function* redirectCustomerSaga(action) {
    const token = yield select(getToken);
    const cureentQueueID = yield select(getSelectedQueueID);
    const selectedCustomer = yield select(getSelectedCustomer);

    response = yield call(redirectQueueMethod,
        {
            token: token,
            currentQueueID: cureentQueueID,
            customer_id: selectedCustomer.id,
            selectedQueueID: action.selectedQueueID
        })
    console.log("queue Saga", 'redirectCustomerSaga', 'response', response)
    if (response.status == 'success') {
        yield put(ActionCreators.redirectCustomerResponse(response));
        yield call(navigationActions.goBack);
    } else {
        yield put(ActionCreators.redirectCustomerFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}

export function* cancelCustomerInWaitingSaga(action) {
    const token = yield select(getToken);
    const cureentQueueID = yield select(getSelectedQueueID);

    response = yield call(cancelCustomerInWaitingMethod,
        {
            token: token,
            currentQueueID: cureentQueueID,
            customer_id: action.customer_id,
        })
    console.log("queue Saga", 'cancelCustomerInWaitingSaga', 'response', response)
    if (response.status == 'success') {
        yield put(ActionCreators.cancelCustomerInWaitingResponse(response));
        yield put(ActionCreators.getQueueWaitingList());
        if (action.onCloseModal) {
            action.onCloseModal()
        }
        // yield call(navigationActions.goBack);
    } else {
        yield put(ActionCreators.cancelCustomerInWaitingFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


export function* callNextCustomerSaga(action) {
    const token = yield select(getToken);
    // const cureentQueueID = yield select(getSelectedQueueID);

    response = yield call(callNextCustomerMethod,
        {
            token: token,
            queue_id: action.queue_id,
        })
    console.log("queue Saga", 'callNextCustomerSaga', 'response', response)
    if (response.status == 'success') {
        yield put(ActionCreators.callNextCustomerResponse(response));
        yield put(ActionCreators.getQueueWaitingList());
        if (action.onCloseModal) {
            action.onCloseModal()
        }
    } else {
        yield put(ActionCreators.callNextCustomerFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


export function* swapCustomerRanksSaga(action) {
    const token = yield select(getToken);
    const cureentQueueID = yield select(getSelectedQueueID);

    response = yield call(swapCustomerRanksMethod,
        {
            token: token,
            queue_id: cureentQueueID,
            customer_1: action.customer_1,
            customer_2: action.customer_2
        })
    console.log("queue Saga", 'swapCustomerRanksSaga', 'response', response)
    if (response.status == 'success') {
        yield put(ActionCreators.swapCustomerRanksResponse(response));
        yield put(ActionCreators.getQueueWaitingList());
    } else {
        yield put(ActionCreators.swapCustomerRanksFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


export function* getQueueSaga(action) {
    const token = yield select(getToken);
  let  response = yield call(getQueue,
        {
            token: token,
            queue_id: action.queue_id,
        })
        console.log("actionqueue_id",action.queue_id  ,"  ", response)
    if (response.status == 'success') {
        yield put(ActionCreators.getQueueResponse(response));
        NavigationService.navigate("QueueEdit",{queueData:response})
    } else {
        yield put(ActionCreators.getQueueFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


export function* updateQueueSaga(action) {
    const token = yield select(getToken);
    response = yield call(updateQueue,
        {
            token: token,
            queue_id: action.queue_id,
            newQueue:action.newQueue
        })
        console.log("actionqueue_id",action.queue_id  ,"  ", action.newQueue,"  ", response)

    if (response.status == 'success') {
        yield put(ActionCreators.updateQueueResponse(response));
        yield put(ActionCreators.getQueueList(true));
        NavigationService.goBack()
        
    } else {
        yield put(ActionCreators.updateQueueFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


export function* deleteQueueSaga(action) {
    const token = yield select(getToken);
    response = yield call(deleteQueue,
        {
            token: token,
            queue_id: action.queue_id
        })
        console.log("actionqueue_id1",action.queue_id  ,"  "+response)

    if (response.status == 'success') {
        yield put(ActionCreators.deleteQueueResponse(response));
        yield put(ActionCreators.getQueueList(true));
        NavigationService.goBack()
    } else {
        yield put(ActionCreators.deleteQueueFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}

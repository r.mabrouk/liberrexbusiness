
import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { getReportList, filterReport } from '../api/methods/report';
import * as reportActions from '../actions/report';



getToken = (state) => state.authReducer.user_profile.token;


// get All Servies Saga

export function* getReportListSaga(action) {

    const token = yield select(getToken);
    const response = yield call(getReportList, { token });
    console.log("getAllServicesResponse", response)
    if (response.waiting_time) {
        yield put(reportActions.getReportsResponse(response));
    } else {
        yield put(reportActions.getReportsFailed());
    }

}


// filter report Saga

export function* filterReportSaga(action) {

    const token = yield select(getToken);
    const response = yield call(filterReport, { token,keywords:action.keywords });
    console.log("getAllServicesResponse", response)
    if (response.waiting_time) {
        yield put(reportActions.filterReportResponse(response));
    } else {
        yield put(reportActions.filterReportFailed());
    }

}

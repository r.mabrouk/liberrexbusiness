
import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import * as rules from '../actions/rules';
import { getRulesList, createRule, deleteRule, updateRule } from '../api/methods/rules';
import { _string } from '../local';
import NavigationService from '../navigation/NavigationService';
import { _Toast } from '../components/toast/toast';


getToken = (state) => state.authReducer.user_profile.token;


// get All rules Saga

export function* getRulesSaga(action) {
    const token = yield select(getToken);
    const response = yield call(getRulesList, { token });
    console.log("getRulesSaga", response)
    if (response.status == 'success') {
        yield put(rules.getRulesListResponse(response.rules));

    } else {
        yield put(rules.getRulesListFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")

    }

}

// create  rule Saga
export function* createRuleSaga(action) {
    const token = yield select(getToken);
    const response = yield call(createRule, { token, rule_data: action.data });
    console.log("createRuleSaga", response)
    if (response.status == 'success') {
        yield put(rules.createRuleResponse(response));
        _Toast.open(_string("CreateRuleSuccessfuly"), "s")
        yield put(rules.getRulesListRequest([]));
        NavigationService.goBack()

    } else {
        yield put(rules.createRuleFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")

    }
}



// update  rule Saga
export function* updateRuleSaga(action) {
    const token = yield select(getToken);
    const response = yield call(updateRule, { token, rule_data: action.data, rule_id: action.id });
    console.log("updateRuleSaga", response)
    if (response.status == 'success') {
        yield put(rules.updateRuleResponse(response.rule, action.index));
        _Toast.open(_string("UpdateRuleSuccessfuly"), "s")
        NavigationService.goBack()

    } else {
        yield put(rules.updateRuleFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")

    }
}

// delete  rule Saga
export function* deleteRuleSaga(action) {
    const token = yield select(getToken);
    const response = yield call(deleteRule, { token, rule_id: action.id });
    console.log("updateRuleSaga", response)
    if (response.status == 'success') {
        _Toast.open(_string("DeleteRuleSuccessfuly"), "s")
        yield put(rules.deleteRuleResponse(response.rule, action.index));
        NavigationService.goBack()

    } else {
        yield put(rules.deleteRuleFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")

    }
}

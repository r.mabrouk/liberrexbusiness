
import { put, call, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { getAllServices, updateService, createService, deleteService, getIndustryServices, getServiceMethod } from '../api/methods/services';

import * as services from '../actions/services';


getToken = (state) => state.authReducer.user_profile.token;


// get All Servies Saga

export function* getAllServiesSaga(action) {
    const token = yield select(getToken);
    const response = yield call(getAllServices, { token, page: action.page, keyword:action.keyword });
    if (response.status == 'success') {
        yield put(services.getAllServicesResponse(response.services));
    } else {
        yield put(services.getAllServicesFailed());
    }

}

// update All Servies Saga

export function* updateServiesSaga(action) {
    const token = yield select(getToken);
    const response = yield call(updateService, { token, service_data: action.service_data, service_id: action.service_id });
    if (response.status == 'success') {
        yield put(services.updateServiceResponse(response.service,action.index));
        action.onCloseModal()
        // yield put(services.getAllServicesRequest([]));
    } else {
        yield put(services.updateServiceFailed());
    }

}


// create Servies Saga
export function* createServiesSaga(action) {
    const token = yield select(getToken);

    const response = yield call(createService, { token, service_data: action.service_data });
    console.log("createServiceResponse", response)
    if (response.status == 'success') {
        action.onCloseModal()
        yield put(services.createServiceResponse(response));
        yield put(services.getIndustryServicesRequest());
        yield put(services.getAllServicesRequest([],null,1));
    } else {
        yield put(services.createServiceFailed());
    }

}

// remove Servies Saga
export function* removeServiesSaga(action) {
    const token = yield select(getToken);
    const response = yield call(deleteService, { token, service_id: action.service_id });
    console.log("deleteServiceResponse", response)
    if (response.status == 'success') {
        yield put(services.deleteServiceResponse(response,action.index));
        action.onCloseModal()

        // yield put(services.getAllServicesRequest(true));
    } else {
        yield put(services.deleteServiceFailed());
    }

}


//get service
export function* getServieSaga(action) {
    const token = yield select(getToken);

    const response = yield call(getServiceMethod, { token, service_id: action.service_id });
    console.log("getServieSaga", response)
    if (response.status == 'success') {
        yield put(services.getServiceResponse(response.service));

    } else {
        yield put(services.getServiceFailed());
    }

}



// remove Servies Saga
export function* getIndustryServicesSaga(action) {
    const token = yield select(getToken);
    const response = yield call(getIndustryServices, { token,keyword:action.keyword, page:1});
    // console.log("deleteServiceResponse", response)
    if (response.status == 'success') {
        yield put(services.getIndustryServicesResponse(response.services));
    } else {
        yield put(services.getIndustryServicesFailed());

    }

}

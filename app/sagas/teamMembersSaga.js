/* Redux saga class
 * logins the user into the app
 * requires username and password.
 * un - username
 * pwd - password
 */
import { put, call, select } from 'redux-saga/effects';
import * as teamMembersActions from '../actions/teamMembers';
import { getAllTeamMembers, updateTeamMembers, deleteTeamMembers, createTeamMembers } from '../api/methods/teamMembers';
import { _string } from '../local';
import NavigationService from '../navigation/NavigationService';
import { _Toast } from '../components/toast/toast';

getToken = (state) => state.authReducer.user_profile.token;


export function* getAllTeamMembersSaga(action) {
    const token = yield select(getToken);
    const response = yield call(getAllTeamMembers, { token });
    console.log("getAllTeamMembersSaga", response)
    if (response.status == 'success') {
        yield put(teamMembersActions.getAllTeamMembersResponse(response.team));
    } else {
        yield put(teamMembersActions.getAllTeamMembersFailed());
        _Toast.open(_string("messages_alert.there_are_errors_in_email_or_passowrd"), "e")
    }
}

export function* updateTeamMembersSaga(action) {
    const token = yield select(getToken);
    const response = yield call(updateTeamMembers, { token, data: action.data, id: action.id });
    console.log("updateTeamMembersSaga", response)
    if (response.status == 'success') {
        yield put(teamMembersActions.updateMemberResponse(response.team_member,action.index));
        NavigationService.goBack()

    } else {
        yield put(teamMembersActions.updateMemberFailed());
        _Toast.open(_string("messages_alert.there_are_errors_in_email_or_passowrd"), "e")
    }
}

export function* deleteTeamMembersSaga(action) {
    const token = yield select(getToken);
    const response = yield call(deleteTeamMembers, { token, id: action.id });
    console.log("getAllTeamMembersSaga", response)
    if (response.status == 'success') {
        yield put(teamMembersActions.deleteMemberResponse(action.index));
        NavigationService.goBack()

    } else {
        yield put(teamMembersActions.deleteMemberFailed());
        _Toast.open(_string("messages_alert.there_are_errors_in_email_or_passowrd"), "e")
    }
}

export function* createTeamMembersSaga(action) {
    const token = yield select(getToken);
    const response = yield call(createTeamMembers, { token, data: action.data });
    console.log("getAllTeamMembersSaga", action.data)
    if (response.status == 'success') {
        yield put(teamMembersActions.createMemberResponse(response));
        yield put(teamMembersActions.getAllTeamMembersRequest(true));
        NavigationService.goBack()
    } else {
        yield put(teamMembersActions.createMemberFailed());
        _Toast.open(_string("messages_alert.there_are_errors_in_email_or_passowrd"), "e")
    }
}



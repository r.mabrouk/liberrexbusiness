
import { put, call, select } from 'redux-saga/effects';
import { _string, setlanguage } from '../local';
import { updateUserAccount, updatePassword, updateBusinessInfo, updateBusinessLocation, updateBusinessWorkingDayes, updateAppSettings } from '../api/methods/userAccount';
import * as userAccount from '../actions/userAccount';
import NavigationService from '../navigation/NavigationService';
import RNRestart from 'react-native-restart'
import { _Toast } from '../components/toast/toast';
import { setAppLanguage } from '../actions/uiActions';

getToken = (state) => state.authReducer.user_profile.token;



export function* updateAppSettingSaga(action) {
    console.log(action.data,"actionactionactionaction")

    const token = yield select(getToken);
    const response = yield call(updateAppSettings, { data: action.data, token });
    if (response.status == 'success') {
        yield put(userAccount.updateAppSettingsResponse(response));
        _Toast.open(_string("UpdateAppSettingSuccessfully"), "s")
        yield put( setAppLanguage(action.data.language));
        NavigationService.navigate("Home");

    } else {
        yield put(userAccount.updateAppSettingsFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}


export function* updateUserAccountSaga(action) {
    console.log("updateUserAccountSaga", action.data)
    const token = yield select(getToken);
    const response = yield call(updateUserAccount, { data: action.data, token });
    if (response.status == 'success') {
        yield put(userAccount.updateUserAccountResponse(response.account));
        _Toast.open(_string("EditedSuccessfully"), "s")
        NavigationService.goBack();
    } else {
        yield put(userAccount.updateUserAccountFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
    }
}



export function* updatePasswordSaga(action) {
    console.log("updatePasswordSaga", action.data)
    const token = yield select(getToken);
    const response = yield call(updatePassword, { data: action.data, token });
    if (response.status == 'success') {
        yield put(userAccount.updatePasswordResponse(response));
        _Toast.open(_string("UpdatedPasswordSuccessfully"), "s")
        NavigationService.goBack();
    } else {
        yield put(userAccount.updatePasswordFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
        // NavigationService.goBack();

    }
}



export function* updateBusinessInfoSaga(action) {
    console.log("updateBusinessInfoSaga22", action.data)

    const token = yield select(getToken);
    const response = yield call(updateBusinessInfo, { data: action.data, token });
    console.log("updateBusinessInfoSaga", response)

    if (response.status == 'success') {
        yield put(userAccount.updateBusinessInfoResponse(response.business));
        _Toast.open(_string("UpdateBusinessInfoSuccessfully"), "s")
        NavigationService.goBack();
    } else {
        yield put(userAccount.updateBusinessInfoFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
        // NavigationService.goBack();

    }
}


export function* updateBusinessLocationSaga(action) {
    console.log("updateBusinessLocation", action.data)
    const token = yield select(getToken);
    const response = yield call(updateBusinessLocation, { data: action.data, token });
    if (response.status == 'success') {
        yield put(userAccount.updateBusinessLocationResponse(response.business));
        _Toast.open(_string("UpdateBusinessLocationSuccessfully"), "s")
        NavigationService.goBack();
    } else {
        yield put(userAccount.updateBusinessLocationFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
        // NavigationService.goBack();

    }
}



export function* updateBusinessWorkingDayesSaga(action) {
    const token = yield select(getToken);
    console.log("updateBusinessLocation2", action.data)

    const response = yield call(updateBusinessWorkingDayes, { data: action.data, token });
    console.log("updateBusinessLocation", response)

    if (response.status == 'success') {
        yield put(userAccount.updateBusinessWorkingDayesResponse(response.working_days));
        _Toast.open(_string("UpdateBusinessWorkingDayesSuccessfully"), "s")
        NavigationService.goBack();
    } else {
        yield put(userAccount.updateBusinessWorkingDayesFailed());
        _Toast.open(_string("messages_alert.network_error"), "e")
        // NavigationService.goBack();

    }
}

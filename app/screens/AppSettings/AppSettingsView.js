import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles'
import { Header } from '../../components/header/header';
import { HeaderSection } from '../../components/headerSection/headerSection';

import { Card } from '../../components/card/card';
import { _Text } from '../../components/text/text';
import { Icons } from '../../config';
import { Button } from '../../components/button/button';
import { getWidth, getHeight, Width } from '../../components/utils/dimensions';
import { _string } from '../../local';
import NotificationModal from './notificationModal';
import LanguageModal from './LanguageModal';
import ButtonWithHeaderSection from '../../components/buttonWithHeaderSection/buttonWithHeaderSection';



export default class AppSettingsView extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        console.log(this.props.appSettings,"thispropsappSettings")
        let {
            notification_push,
            onChangeNoification,
            saveSelectedNotification,
            selectedNotificationText,
            notificationList,
            language,
            onSelectedLanguage,
            lanIndex,
            loadingUpdateAppSetting,
            onUpdateAppSettings }
            = this.props
        return (
            <View style={styles.container}>
                <Header style={{ width: '100%', zIndex: 16 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />
                <HeaderSection style={{
                    shadow: 6,
                    zIndex: 15,
                    width: Width,
                    height: getHeight(67),
                    marginTop: -getHeight(30),
                    paddingTop: getHeight(15),
                    flexDirection: 'row',
                    alignItems: 'center',
                    backgroundColor: 'white',
                    justifyContent: 'space-between',
                    paddingHorizontal: getWidth(15)
                }}
                    text={_string("app_settings.app_settings").toUpperCase()}
                    iconName='appsettings'

                />




                <ButtonWithHeaderSection
                    marginTop={getHeight(20)}
                    headerIcon='lang'
                    headerText={_string("app_settings.app_language")}
                    onPress={() => {
                        this.languageModal.onAnimated(true)
                    }}
                    numberOfLines={1}
                    value={this.props.langs[lanIndex].value}
                />

                <ButtonWithHeaderSection
                    marginTop={getHeight(15)}
                    headerIcon='notification'
                    headerText={_string("app_settings.push_notifications")}
                    onPress={() => {
                        this.notificationModal.onAnimated(true)
                    }}
                    numberOfLines={1}
                    value={selectedNotificationText.toString()}
                />

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Button
                        isLoading={loadingUpdateAppSetting}
                        onPress={onUpdateAppSettings}
                        iconName="done"
                        text={_string("app_settings.save")}
                        style={{
                            colors: 'blue',
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: getWidth(345),
                            height: getHeight(44),
                            borderRadius: 3,
                            textColor: 'white',
                            fontSize: 'medium15Px',
                            marginTop: getHeight(15),
                            fontFamily:'bold',
                        
                        }}
                        iconStyle={{ width: getWidth(18),height:getHeight(18) }}

                    />
                </View>
                <LanguageModal
                selected={this.props.lanIndex}
                    onSelectedLanguage={onSelectedLanguage}
                    data={this.props.langs}
                    language={language}
                    ref={(LanguageModal) => this.languageModal = LanguageModal}
                />
                <NotificationModal
                    data={notificationList}
                    saveSelectedNotification={saveSelectedNotification}
                    onChangeNoification={onChangeNoification}
                    keys={notification_push} ref={(notification) => this.notificationModal = notification} />
            </View>
        );
    }
}


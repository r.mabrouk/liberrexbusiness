





import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import { Card } from '../../components/card/card';
import { _Text } from '../../components/text/text';
import { Icons } from '../../config';
import { Button } from '../../components/button/button';
import { getWidth, getHeight, Width } from '../../components/utils/dimensions';
import { _string, setlanguage } from '../../local';
import { connect } from 'react-redux'
import AppSettingsView from './AppSettingsView';
import { updateAppSettingsRequest } from '../../actions/userAccount';




class AppSettings extends Component {
    constructor(props) {
        super(props);
        let lang=this.props.appSettings.language
        this.state = {
            language:props.appSettings.language?props.appSettings.language:"en",
            notification_push: {
                all_notification:props.appSettings.all_notification?props.appSettings.all_notification:0,
                notification_email: props.appSettings.notification_email,
                notification_push: props.appSettings.notification_push,
                notification_sms: props.appSettings.notification_sms
            },
            notificationList: [_string("AllNotification"),_string("Email"), _string("PushNotification"), _string("SMS") ],
            selectedNotificationText: [_string("AllNotification")],
            lanIndex:lang=='fr'?1:lang=='ru'?2:0,
         langs : [
                { key: "en", value: _string("language.english") },
                { key: "fr", value: _string("language.french") },
                { key: "ru", value: _string("language.russian") }
            ]
            
        };
    }
    notificationAll = (value) => {
        return {
            all_notification: value,
            notification_email: value,
            notification_push: value,
            notification_sms: value
        }
    }
    componentDidMount=()=>{
        this.saveSelectedNotification()
    }
    onSaveLanguage=(lang)=>{
        this.setState({language:lang})
    }
    onSelectedLanguage=(lang,index)=>{
    this.setState({language:lang,lanIndex:index})
}

    onChangeNoification = (key) => {
        let { notification_push } = this.state
        let notif = notification_push
        if (key == "all_notification" && (notif[key] == 0)) notif = this.notificationAll(1)
        else if (key == "all_notification" && notif[key] == 1) notif = this.notificationAll(0)
        else if (notif[key] == 1) notif[key] = 0
        else notif[key] = 1
        this.setState({ notification_push: notif })
    }
    saveSelectedNotification = () => {
        let { selectedNotificationText, notification_push, notificationList } = this.state
        let selected = []
        Object.keys(notification_push).filter((element, index) => {
            if (notification_push[element] == 1)
                selected.push(notificationList[index])
        })
        if (notification_push.all_notification == 1)
            selected = [_string("AllNotification")]
        this.setState({ selectedNotificationText: selected.length>=1?selected: [_string("AllNotification")] })
    }
    onUpdateAppSettings = () => {
        let {notification_push,language}=this.state
        let data = {
            language: language,
           ...notification_push
        }
        this.props._updateAppSettings(data)


    }
    render() {
        return (
            <AppSettingsView {...this} {...this.state} {...this.props} />
        );
    }
}






function mapStateToProps(state) {
    return {
        
        appSettings:state.userAccountReducer.appSettings,
        loadingUpdateAppSetting:state.userAccountReducer.loadingUpdateAppSetting
    }
}
function mapDispatchToProps(Dispatch) {
    return {
        _updateAppSettings: (data) => Dispatch(updateAppSettingsRequest(data))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(AppSettings)

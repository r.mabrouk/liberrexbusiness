import React, { Component } from 'react';
import {
    View
} from 'react-native';
import styles, { stylesComponant } from './styles';
import { Button } from '../../components/button/button';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { Icons } from '../../config';

import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { getHeight, getWidth } from '../../components/utils/dimensions';
import { PopupModal } from '../../components/popupModal/popupModal';
import ServiceCard from '../../components/ServiceCard/ServiceCard';
import { _string, getlanguage } from '../../local';
import { CheckBox } from '../../components/checkBox/checkBox';
import { ColorsList } from '../../components/PropsStyles';
import { InputSearch } from '../../components/inputSearch/inputSearch';
import { FlatList } from 'react-native-gesture-handler';
import { LoaderLists } from '../../components/LoaderList/loaderLists';

class NotificationModal extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    onAnimated = (state) => {
        this.popupAddModal.onAnimated(state)
    }





    render() {
        let { data, onChangeNoification, saveSelectedNotification, keys } = this.props
        return (
            <PopupModal
                height={getHeight(330)}
                duration={400}
                zIndex={30}
                ref={_popupAddModal => this.popupAddModal = _popupAddModal}>
                <Card style={stylesComponant.popupContainer}>
                    <View style={styles.popupHeader}>
                        <Icons name='gridLinear' width={getWidth(28)} height={getWidth(28)} />
                        <_Text
                            style={styles.popupHeaderText}>{_string("your_service.add_a_new_service").toLocaleUpperCase()}</_Text>
                    </View>
                    <View style={{ width: '100%' }}>

                        {Object.keys(keys).map((key, index) => {
                            return (
                                <ServiceItem
                                    service_name={data[index]}
                                    onChecked={() => {
                                        onChangeNoification(key)
                                    }}
                                    checked={keys[key] == 1} />
                            )
                        })}



                    </View>
                    <Button
                        isLoading={false}
                        iconName="done"
                        text={_string("app_settings.save")}
                        style={stylesComponant.popupDonebtn}
                        iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                        onPress={() => {
                            saveSelectedNotification()
                            this.onAnimated()
                        }}
                    />
                </Card>
            </PopupModal>
        );
    }
}
const ServiceItem = ({ checked, onChecked, service_name }) => {
    return (
        <Button
            onPress={onChecked}
            style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: getHeight(15),
                paddingHorizontal: getWidth(15),
                borderBottomWidth: 1,
                borderColor: ColorsList.rgbaBlack(0.04)
            }}>
            <CheckBox
                text={service_name}
                onChecked={onChecked}
                checked={checked} />
        </Button>
    )
}

export default NotificationModal
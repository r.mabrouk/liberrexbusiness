import { Width, Height, getWidth, getHeight } from "../../components/utils/dimensions";
import { ColorsList } from "../../components/PropsStyles";



const styles = {
    container: {
        width: Width,
        height: Height,
        backgroundColor: ColorsList.white,
    },
    optionCardContainer: {
        width: Width,
        backgroundColor: ColorsList.rgbaOrange,
        paddingHorizontal: getWidth(15),
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: getHeight(20),
    },
    optionHeader: {
        width: getWidth(345),
        height: getHeight(18),
        marginBottom: getHeight(10),
        //backgroundColor:'red',
        flexDirection: 'row',
    },
    headerTitle: {
        fontFamily: 'bold',
        fontSize: 'medium15Px',
        color: 'black',
        marginLeft: getWidth(5),
    },
    optionCard: {
        width: getWidth(345),
        height: getHeight(41),
        shadow: 4,
        backgroundColor: ColorsList.white,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: getWidth(10),
    },
    optionbtn: {
        width: getWidth(345),
        height: getHeight(41),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: getWidth(10)
    },
    popupHeader: {
        flexDirection: 'row', width: getWidth(345),
        justifyContent: 'center', alignItems: 'center',
        marginTop: getHeight(20)
    },
    popupHeaderText: {

        marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily: 'bold',
    },
    popupHeaderText: {

        marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily: 'bold',
    },


}
export const stylesComponant = {
    card: {
        flexDirection: 'row', justifyContent: 'flex-start',
        backgroundColor: 'white',
        shadow: 5,
        height: getHeight(700),
        width: getWidth(150),
        alignItems: 'center',
        paddingLeft: getWidth(20),
        borderRadius: 2
    },
    HeaderContainer: {
        alignItems: 'center', width: Width, paddingBottom: getHeight(12)
    },
    Input: {

        width: '85%',
        height: getHeight(41),
        shadow: 2, backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: -getHeight(10),
        marginLeft: getWidth(15),
    },
    Service: {
        flexDirection: 'row',
        backgroundColor: 'white',
        width: '95%',
        height: getHeight(50),
        marginTop: 5,
        justifyContent: 'space-around'
    },
    workingSection: {

        flexDirection: 'row',
        width: getWidth(345),
        shadow: 6, height: getHeight(41), backgroundColor: 'white',
        justifyContent: 'space-around', alignItems: 'center',
        // marginTop:5,
        marginBottom: 4,


    },
    ServiceCard: {
        flexDirection: 'row', justifyContent: 'flex-start',
        backgroundColor: 'white', shadow: 6,
        height: getHeight(41),
        width: getWidth(167.5),
        alignItems: 'center',
        paddingLeft: 20,
        borderRadius: 3,
        marginVertical: 7
    },
    popupContainer: {

        width: '100%',
        alignItems: 'center',
        height: getHeight(300)


    },
    serviePopupHeaderText: {
        marginVertical: getHeight(10),
        // marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily: 'bold',
    },
    popupDonebtn: {
        colors: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(345),
        height: getHeight(44),
        borderRadius: 3,
        textColor: 'white',
        // shadow: 6,
        marginTop: getHeight(15),
        fontFamily: 'bold',
        color: 'white',
        fontSize: 'size15',
        // overflow:'visible'
    },
}


export default styles




import React, { Component } from 'react';
import {
    View,
    ImageBackground,
} from 'react-native';
import styles from './styles';
import { Button } from '../../components/button/button';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { images, metrics, Icons } from '../../config';
import PenddingandApprove from '../../components/PenndeingandApprove/PenddingandApprove'
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import Calendar from '../../components/calendar/src/calendar';
import CalendarStrip from '../../components/calendar/weekly/CalendarStrip';
import CustomerPendingInfo from '../../components/CustomerPending/CustomerPending'
import CustomerApproveInfo from '../../components/CustomerPending/CustomerApproveInfo'
import CalendarModeModal from './calendarModeModal';
import SwiperTabButtons from '../../components/swiperTabButtons/swiperTabButtons';
import PendingModal from '../QueueRequests/pendingModal';
import AppointmentsApprovedModal from './appointmentsApprovedModal';
import { _string } from '../../local';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import EmptyData from '../../components/emptyData/emptyData';
import { ColorsList } from '../../components/PropsStyles';
import moment from 'moment';

// import {} from 'react-native-ca'
class AppointmentsApprovedView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            approve: true,
            pending: false,
            
            selectedIndex: -1
        };
    }
    _approve = () => {
        this.setState({
            approve: true,
            pending: false

        })
    }

    _pendding = () => {
        this.setState({
            approve: false,
            pending: true

        })
    }

    componentWillMount() {
    }

    render() {
        let { selectedIndex } = this.state
        let { bookingList, loadingBookingList, loadingBookingRequestsList,
            bookingRequestList,
            onGetBookingListFilter,
            selectedDate,
            onRemoveBooking,
            loadingDeleteBooking,
            onEditCustomerApproved,
            onApprovedBookingRequest,
            onDeclineBookingRequest,
            loadingApproveBookingRequest,
            loadingDeleteBookingRequest,calendarMode,
            onChangeCalnderMode

        } = this.props
        let { requestsList, CustomerList, CustomerPendingList } = require("../../config/LocalDatabase")
        return (
            <View
                style={styles.container}>

                <Header style={{ width: '100%', zIndex: 16 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.navigate("Home") }}
                />

                <HeaderSection

                    onPress={() => { this.props.navigation.navigate("NewAppointment") }}

                    iconSize={getWidth(28)}
                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}

                    withButton
                    textButton={_string("appointments_approved.add_new")}
                    text={_string("appointments_approved.appointments")}
                    iconName='appointmentsgray'
                    iconButton='addwhite'
                />






                {/* <PenddingandApprove /> */}

                <View
                    style={styles.container}>

                    <View style={{
                        alignItems: 'center',
                        width: Width,
                        // height: '100%'
                    }}>
                        <SwiperTabButtons tabButons={[
                            { iconsName: 'Check', label: _string("Approved") },
                            { iconsName: 'Pending', label: _string("Pending") }]} >

                            {/* {this.state.approve && */}

                            <View style={{height:getHeight(450),paddingBottom:getHeight(30)}}>

                                <FlatList
                                ListHeaderComponentStyle={{zIndex:100,marginBottom:getHeight(15)}}
                                    ListHeaderComponent={() => {
                                        return (
                                            <Card style={styles.calendarContainer}>
                                                {calendarMode == _string("modals.report_by.monthly") ? <CalendarStrip
                                                startingDate={new Date()}
                                                    selectedDate={selectedDate}
                                                    onDateSelected={(date) => {
                                                        onGetBookingListFilter(moment(date).format("YYYY-MM-DD"))
                                                    }}
                                                    calendarType={calendarMode}
                                                    onChangeCalnderMode={onChangeCalnderMode}
                                                    daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: 'white' }}
                                                    showDate
                                                    style={{ width: Width, height: getHeight(131), paddingTop: getHeight(15) }}

                                                /> :
                                                    <Calendar
                                                    firstDay={1}

                                                        calendarType={calendarMode}
                                                        onDayPress={(date, state) => {
                                                            if (state != "disabled")
                                                                onGetBookingListFilter(date.dateString)
                                                        }}
                                                        onChangeCalnderMode={onChangeCalnderMode}
                                                        headerStyles={{ width: getWidth(190) }}
                                                        ref={(refd) => this.CalendarRef == refd}
                                                        theme={{

                                                        }}
                                                        markedDates={{
                                                            [selectedDate]: { selected: true, selectedColor: 'white' },
                                                        }}
                                                        disableMonthChange={true}
                                                        style={{
                                                            width: '100%',
                                                        }}
                                                    />}
                                                <EmptyData
                                                    height={getHeight(260)}
                                                    show={(bookingList.length < 1) && !loadingBookingList} />
                                            </Card>
                                        )
                                    }}
                                    data={bookingList}
                                    contentContainerStyle={{}} renderItem={({ item, index }) => {
                                        return (
                                            <CustomerApproveInfo
                                                onPress={() => this.appointmentsApprovedModal.onAnimated(true, item, index)}
                                                name={`${item.fname} ${item.lname}`}
                                                time={item.start_datetime}
                                                service={item.service_names}
                                            />
                                        )
                                    }}
                                    ListFooterComponent={() => {
                                        return (<LoaderLists isLoading={loadingBookingList} />)
                                    }}
                                />

                            </View>

                            <View >
                                <FlatList
                                    ListHeaderComponent={() => {
                                        return (
                                            <Card style={styles.calendarContainer}>
                                                {/* {calendarMode == _string("modals.report_by.monthly") ? <CalendarStrip
                                                    selectedDate={selectedDate}
                                                    onDateSelected={(date) => {
                                                        onGetBookingListFilter(moment(date).format("YYYY-MM-DD"))
                                                    }}
                                                    calendarType={calendarMode}
                                                    onChangeCalnderMode={onChangeCalnderMode}
                                                    daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: 'white' }}
                                                    showDate
                                                    style={{ width: Width, height: getHeight(131), paddingTop: getHeight(15) }}

                                                /> :
                                                    <Calendar
                                                        calendarType={calendarMode}
                                                        onDayPress={(date, state) => {
                                                            if (state != "disabled")
                                                                onGetBookingListFilter(date.dateString)
                                                        }}
                                                        onChangeCalnderMode={onChangeCalnderMode}
                                                        headerStyles={{ width: getWidth(190) }}
                                                        ref={(refd) => this.CalendarRef == refd}
                                                        theme={{

                                                        }}
                                                        markedDates={{
                                                            [selectedDate]: { selected: true, selectedColor: 'white' },
                                                        }}
                                                        disableMonthChange={true}
                                                        style={{
                                                            width: '100%',
                                                        }}
                                                    />} */}
                                                <EmptyData
                                                    height={getHeight(400)}
                                                    show={(bookingRequestList.length < 1) && !loadingBookingRequestsList} />
                                            </Card>
                                        )
                                    }}

                                    contentContainerStyle={{}}
                                    data={bookingRequestList} renderItem={({ item, index }) => {
                                        return (
                                            <CustomerPendingInfo
                                                loadingApproveBookingRequest={loadingApproveBookingRequest && (selectedIndex == index)}
                                                loadingDeleteBookingRequest={loadingDeleteBookingRequest && (selectedIndex == index)}
                                                onApprovedBookingRequest={() => {
                                                    this.setState({ selectedIndex: index }, () => {
                                                        onApprovedBookingRequest(item.id, index)
                                                    })
                                                }}
                                                onDeclineBookingRequest={() => {
                                                    this.setState({ selectedIndex: index }, () => {
                                                        onDeclineBookingRequest(item.id, index)
                                                    })
                                                }}
                                                onPress={() => this.pendingModal.onAnimated(true, item, index)}
                                                name={`${item.fname} ${item.lname}`}
                                                time={item.start_datetime}
                                                service={item.service_names}
                                            />
                                        )
                                    }}
                                    ListFooterComponent={() => {
                                        return (<LoaderLists isLoading={loadingBookingRequestsList} />)
                                    }}
                                />

                            </View>


                        </SwiperTabButtons>
                    </View>
                </View>



                <PendingModal
                    loadingApproveBookingRequest={loadingApproveBookingRequest}
                    loadingDeleteBookingRequest={loadingDeleteBookingRequest}

                    onApprove={() => {
                        this.pendingModal.onAnimated()

                    }}
                    onCancel={() => {
                        this.pendingModal.onAnimated()
                    }}
                    onEdit={() => {
                        this.pendingModal.onAnimated()
                        this.props.navigation.navigate("NewAppointment")
                    }}

                    duration={500}
                    ref={(_pendingModal) => this.pendingModal = _pendingModal} />

                <AppointmentsApprovedModal
                    loadingDeleteBooking={loadingDeleteBooking}
                    onRemoveBooking={onRemoveBooking}
                    onEditCustomerApproved={onEditCustomerApproved}
                    duration={500} ref={(_appointmentsApprovedModal) => this.appointmentsApprovedModal = _appointmentsApprovedModal} />
                <CalendarModeModal onChangeMode={(mode) => {
                    this.setState({ calendarMode: mode })
                }} ref={(_CalendarModeModal) => this.calendarModeModal = _CalendarModeModal} />
            </View>
        );
    }
}


export default AppointmentsApprovedView;

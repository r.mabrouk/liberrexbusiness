import React, { Component } from 'react';
import {
    View,
    ScrollView
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { images, metrics, Icons } from '../../config';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { ReportRequestItem } from '../../components/reportRequestItem/reportRequestItem';
import { CustomerFeedbackItem } from '../../components/customerFeedbackItem/customerFeedbackItem';
import { Width, Height, getWidth, getHeight } from '../../components/utils/dimensions';
import { Card } from '../../components/card/card';
import { Rating } from '../../components/rating/rating';
import { Button } from '../../components/button/button';
import { PopupModal } from '../../components/popupModal/popupModal';
import { _string } from '../../local';
import moment from 'moment'

type Props = {
    duration: number
}
class AppointmentsApprovedModal extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            customer: {

            },
            index: -1
        }
    }
    async componentDidMount() {
    }
    onAnimated = (state, item, index) => {
        if (item)
            this.setState({ customer: item, index })

        this.PendingAppointModal.onAnimated(state)
    }
    render() {

        let { onRemoveBooking, onEditCustomerApproved, loadingDeleteBooking } = this.props
        let { customer, index } = this.state
        return (
            <PopupModal duration={500} ref={_popupModal => this.PendingAppointModal = _popupModal}
                height={getHeight(260)}  >
                <Card
                    style={styles.modalContainer}>
                    <View style={styles.customerInfo}>
                        <Icons
                            name='user'
                            width={getWidth(28)}
                            height={getWidth(28)}
                        />
                        <_Text
                            style={styles.custometName} >
                            {`${customer.fname} ${customer.lname}`.toLocaleUpperCase()}
                        </_Text>
                    </View>
                    <View style={styles.textAndIconItemView}>
                        <TextAndIconItem
                            fontSize='size12'
                            style={styles.textAndIconItemStyle()}
                            iconName='phone'
                            text={customer.phone_number ? customer.phone_number :_string("appointments_approved.noPhoneNumber")}
                        />
                        <TextAndIconItem
                            fontSize='size12'
                            style={styles.textAndIconItemStyle(getWidth())}
                            iconName='email'
                            text={customer.email}
                        />
                    </View>

                    <TextWithIcon
                        marginTop={getHeight(15)}
                        iconName='calendar'
                        text={this.getFormatDate(customer.start_datetime)}
                        border={1}
                    />
                    <TextWithIcon
                        iconName='grid'
                        text={customer.service_names ? customer.service_names : _string("appointments_approved.noServices")}
                        border={0}
                    />
                    <View style={styles.buttonViewContainer}>
                        <Button
                            onPress={() => {
                                this.onAnimated()
                                onEditCustomerApproved(customer, index)
                            }

                            }
                            style={styles.buttonsStyle}
                            iconName='editpen'
                            text={_string("appointments_approved.edit")}
                            iconStyle={{ width: getWidth(18),height:getHeight(18) }}

                        />
                        <Button
                            isLoading={loadingDeleteBooking}
                            loaderColor={'darkGray1'}
                            onPress={() => onRemoveBooking(customer.id, index, this.onAnimated)}
                            style={styles.buttonsStyle}
                            iconName='cancel'
                            text={_string("appointments_approved.cancel_it")}
         
                            iconStyle={{ width: getWidth(18),height:getHeight(18), color: 'red' }}

                        />
                    </View>

                </Card>

            </PopupModal>
        );
    }
    getFormatDate = (date) => {
        return moment(date).format("DD MMM,YYYY HH:mm")
    }
}
const TextAndIconItem = ({ iconName, text, style, fontSize }) => {
    return (
        <Card style={style}>
            <Icons
                name={iconName}
                width={getWidth(18)} height={getWidth(18)}
            />
            <_Text
                style={styles.textItem(fontSize)} >
                {text}
            </_Text>
        </Card>
    )
}

const TextWithIcon = ({ iconName, text, border, marginTop }) => {
    return (
        <Card style={styles.cardContainer(border, marginTop)}>
            <Icons
                name={iconName}
                width={getWidth(18)} height={getWidth(18)}
            />
            <_Text
                style={styles.textStyle} >
                {text}
            </_Text>
        </Card>
    )
}


export default AppointmentsApprovedModal;
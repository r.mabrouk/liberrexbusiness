import React, { Component } from 'react';
import {
    View, FlatList, TouchableOpacity
} from 'react-native';
// import { PopupModal } from '../../components/popupModal/popupModal';
import { ColorsList } from '../../components/PropsStyles';
import { getHeight, Width } from '../../components/utils/dimensions';
import { _Text } from '../../components/text/text';
import styles from './styles';
import { CheckBox } from '../../components/checkBox/checkBox';
import { PopupModal } from '../../components/popupModal/popupModal';
import { _string } from '../../local';

type CalendarModeModalProps = {
    duration: number,
    currentModal: ''

}
class CalendarModeModal extends Component<CalendarModeModalProps> {
    constructor(props) {
        super(props);
        this.state = {

            currentSelected: ""
        }
    }
    async componentDidMount() {
    }
    componentWillReceiveProps = (props) => {
        this.setState({ currentModal: props.currentModal })
    }
    onAnimated = (state) => {
        this.reportByModal.onAnimated(state)
    }
    render() {


        let data = [
            { date:_string("modals.report_by.weekly") },
            { date:_string("modals.report_by.monthly") }
        ]
        let {currentSelected}=this.state
        let {onChangeMode}=this.props

        return (
            <PopupModal
                backgroundColor={ColorsList.rgbaWhite(.97)}
                duration={700}
                ref={(_reportByModal) => this.reportByModal = _reportByModal}
                height={getHeight(140)} >

                <FlatList
                    data={data}
                    style={{ width: Width }}
                    contentContainerStyle={styles.contentContainerStyleList}
                    renderItem={({ item, index }) => {    
                        return (
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ currentSelected: index })
                                    onChangeMode(item.date)
                                    this.reportByModal.onAnimated(false)
                                }}
                                style={styles.contentContainerModal}>
                                <_Text
                                    style={styles.modaleText(this.state.currentSelected, index)} >
                                    {item.date}
                                </_Text>
                                {this.state.currentSelected== index &&
                                    <CheckBox />}
                            </TouchableOpacity>

                        )
                    }} />
            </PopupModal>
        );
    }
}


export default CalendarModeModal;
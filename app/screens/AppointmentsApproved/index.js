import React, { Component } from 'react';
import { View, Text } from 'react-native';
import AppointmentsApprovedView from './AppointmentsApprovedView';
import { getBookingListRequest, getBookingRequestListRequest, deleteBookingRequest, deleteBookingRequestRequest, approveBookingRequest } from '../../actions/booking';
import { connect } from 'react-redux'
import moment from 'moment';
import { SelectCustomer } from '../../actions/customers';
import {NavigationEvents} from 'react-navigation'
import { LocaleConfig } from '../../components/calendar/src';
import { _string } from '../../local';

class Booking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDate: moment().format("YYYY-MM-DD"),
      calendarMode: _string("modals.report_by.monthly"),
    };
  }

  componentDidMount = () => {
    this.onGetBookingList()
    this.setCalendarLang()
  }

  onDeclineBookingRequest = (id, index, onCloseModal) => {
    this.props._DeclineBookingRequest(id, index, onCloseModal)
  }
  onApprovedBookingRequest = (id, index, onCloseModal) => {
    this.props._ApprovedBookingRequest(id, index, onCloseModal)

  }
  onEditCustomerApproved = (customerData, index) => {
    this.props.navigation.navigate("NewAppointment", { customerData, index, isUpdate: true })
    this.props.SelectCustomer(customerData)
  }
  onRemoveBooking = (id, index, onCloseModal) => {
    this.props._RemoveBooking(id, index, onCloseModal)
  }
  setCalendarLang=()=>{
    LocaleConfig.locales['en'] = {
        monthNames: _string("calendar.month_names"),
        monthNamesShort: _string("calendar.month_names_short"),
        dayNames: _string("calendar.day_names"),
        dayNamesShort: _string("calendar.day_names_short"),
        today: 'Aujourd\'hui'
    };
    LocaleConfig.defaultLocale = 'en';
}
  onGetBookingListFilter = (date) => {
    let filter_date = moment(date).format("YYYY-MM-DD")
    this.setState({ selectedDate: date })
    this.props._getBookingList(true, filter_date, 1)
  }
  onGetBookingList = () => {
    this.props._getBookingList(true, null, 1)
    this.props._getBookingRequestList(true, null, 1)
  }
  onChangeCalnderMode = () => {
    if (this.state.calendarMode == _string("modals.report_by.weekly"))
        this.setState({ calendarMode: _string("modals.report_by.monthly") })
    else
        this.setState({ calendarMode: _string("modals.report_by.weekly") })

}
  render() {
    return (
      <View>
        <NavigationEvents onDidFocus={(payload) => {
          this.setCalendarLang()
          this.setState({ refresh: true,calendarMode:_string("modals.report_by.monthly") })

        }} />
        <AppointmentsApprovedView {...this.state} {...this} {...this.props} />

      </View>
    );
  }
}


function mapStateToProps(state) {
  return {
    bookingList: state.bookingReducer.bookingList,
    bookingRequestList: state.bookingReducer.bookingRequestList,
    loadingBookingList: state.bookingReducer.loadingBookingList,
    loadingBookingRequestsList: state.bookingReducer.loadingBookingRequestsList,
    loadingDeleteBooking: state.bookingReducer.loadingDeleteBooking,
    loadingDeleteBookingRequest: state.bookingReducer.loadingDeleteBookingRequest,
    loadingApproveBookingRequest: state.bookingReducer.loadingApproveBookingRequest,
  }
}
function mapDispatchToProps(Dispatch) {
  return {
    _getBookingList: (reset, keyword, page) => Dispatch(getBookingListRequest(reset, keyword, page)),
    _getBookingRequestList: (reset, keyword, page) => Dispatch(getBookingRequestListRequest(reset, keyword, page)),
    _RemoveBooking: (id, index, onCloseModal) => Dispatch(deleteBookingRequest(id, index, onCloseModal)),
    _DeclineBookingRequest: (id, index, onCloseModal) => Dispatch(deleteBookingRequestRequest(id, index, onCloseModal)),
    _ApprovedBookingRequest: (id, index, onCloseModal) => Dispatch(approveBookingRequest(id, index, onCloseModal)),
    SelectCustomer: (customer) => Dispatch(SelectCustomer(customer)),

  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Booking)

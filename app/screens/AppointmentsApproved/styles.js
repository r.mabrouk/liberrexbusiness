import { StyleSheet } from 'react-native';
import { metrics } from '../../config';
import { getHeight, Width, Height, getWidth } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        width: Width,
        height: Height,
        backgroundColor: ColorsList.white
    },
    logo: {
        width: metrics.screenWidth * .8,
        height: metrics.screenHeight * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: '100%',
        height: '100%',
        alignItems: 'center'
    },
    headerSectionContainer: {

        backgroundColor: 'white', shadow: 6,
        width: '100%', alignItems: 'center',
        justifyContent: 'center', marginTop: -getHeight(13)

    },
    calendarContainer:
    {
        shadow: 6, alignItems: 'center', justifyContent: 'center',
        width: Width, backgroundColor: 'white',zIndex:100
    },
    contentContainerModal: {
        width: getWidth(375),
        height: getHeight(50),
        // backgroundColor: ColorsList.white,
        borderBottomWidth: 1,
        borderColor: ColorsList.rgbaBlack(.05),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: getWidth(15)
    },
    modaleText: (currentSelected, index) => ({
        fontFamily: 'black',
        color: currentSelected == index ? 'black' : 'gray0',
        fontSize: currentSelected == index ? 'medium15Px' : 'small13Px'
    }),
    contentContainerStyleList: { alignItems: 'center' },
    cardContainer: (border, marginTop) => ({
        width: getWidth(375),
        height: getHeight(50),
        paddingHorizontal: getWidth(15),
        flexDirection: 'row',
        alignItems: 'center',
        borderTopWidth: border,
        borderBottomWidth: 1,
        borderColor: ColorsList.rgbaBlack(.04),
        marginTop: marginTop
    }),
    iconView: {
        width: getWidth(25),
        height: getHeight(25),
        justifyContent: 'center',
        alignItems: 'center'
    },
    textStyle: {
        fontSize: 'size15',
        fontFamily: 'bold',
        color: 'darkGray1',
        paddingHorizontal: getHeight(5)
    },
    textItem: (fontSize) => ({
        fontSize: fontSize ? fontSize : 'medium15Px',
        fontFamily: 'medium',
        color: 'lightGray1', paddingHorizontal: getWidth(5)
    }),
    smallTextItem: {
        fontSize: 'small1',
        fontFamily: 'regular',
        color: 'lightGray1'
    },
    custometName: {
        fontSize: 'size15',
        fontFamily: 'bold',
        color: 'darkGray1',
        paddingHorizontal: getWidth(10)
    },
    ratingView: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalContainer: {
        width: getWidth(375),
        height: getHeight(291),
        alignItems: 'center'
    },
    modalHeader: {
        width: getWidth(375),
        height: getHeight(85),
        marginTop: getHeight(25),
        marginBottom: getHeight(8),
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    textAndIconItemStyle: () => ({
        flexDirection: 'row',
        alignItems: 'center',
        alignItems: 'center',
    }),
    textAndIconItemView: {
        width: getWidth(275),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: getHeight(10)
    },
    buttonViewContainer: {
        width: getWidth(345),
        marginVertical: getHeight(15),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    buttonsStyle: {
        height: getHeight(44),
        width: getWidth(165),
        shadow: 6,
        borderRadius: 3,
        justifyContent: 'center',
        backgroundColor: 'white',
        alignItems: 'center',
        textColor: 'darkGray1',
        fontSize: 'size15',
        fontFamily: 'bold'
    },
    approveButton: {
        height: getHeight(44),
        width: getWidth(345),
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        textColor: 'white',
        colors: 'blue',
        fontSize: 'medium15Px',
        fontFamily: 'bold'
    },
    customerInfo: {
        width: getWidth(345),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: getHeight(20)
    },
    headerIconView: {
        height: '100%',
        width: getWidth(30),
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerNameView: {
        height: '100%',
        width: getWidth(103),
        justifyContent: 'center',
        alignItems: 'center'
    },
    paragraphTextView: {
        width: getWidth(345),
        height: getHeight(45),
        justifyContent: 'center'
    },
    paragraphText: {
        fontSize: 'small13Px',
        fontFamily: 'bold',
        color: 'lightGray1',
        textAlign: 'center'
    },

};

export default styles;

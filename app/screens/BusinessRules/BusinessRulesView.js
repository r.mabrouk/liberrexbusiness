import React, { Component } from 'react';
import {
    View,
    TouchableOpacity
} from 'react-native';
import styles from './styles';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { images, metrics, Icons } from '../../config';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import { getHeight, Width, Height, getWidth } from '../../components/utils/dimensions';
import { _string } from '../../local';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import { Button } from '../../components/button/button';
import EmptyData from '../../components/emptyData/emptyData';



export default class BusinessRulesView extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        let { loadingRulesList, rulesList, onUpdateRule, onCreateRule } = this.props
        let { Rules } = require("../../config/LocalDatabase")
        return (
            <View
                style={styles.container}>


                <Header style={{ width: '100%', zIndex: 16 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />
                <HeaderSection
                    onPress={onCreateRule}
                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15),
                        
                    }}
                    text={_string("business_rules.business_rules")} iconName='rules'
                    withButton
                    iconButton={'addwhite'}
                    textButton={_string("business_rules.add_new")}
                    iconStyle={{ width: getWidth(15),height:getHeight(15) }}

                />

                <FlatList data={rulesList} renderItem={({ item, index }) => {
                    return (
                        <Card style={styles.serviceContainer}>

                            <Card style={styles.cardInfo}>
                                
                                
                                <Card style={{ flexDirection: 'row', width: metrics.screenWidth * .7, }}>
                                    <Card style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Icons name='rulesBlue' width={getWidth(28)} height={getWidth(28)} />
                                    </Card>
                                    <Card style={styles.textContainer}>
                                        <_Text style={styles.serviceName}>{item.name}</_Text>
                                        <_Text style={styles.duration}>{item.type}</_Text>
                                    </Card>
                                </Card>



                                <Button
                                    onPress={() => onUpdateRule(item, index)}
                                    style={styles.leftButton}
                                    iconName='more'
                                    iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                                />

                            </Card>
                        </Card>
                    )
                }}
                    ListFooterComponent={() => {
                        return <LoaderLists isLoading={loadingRulesList} />
                    }}

                />
                <EmptyData
                    show={(rulesList.length < 1) && !loadingRulesList} />

            </View >

        );
    }
}

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import BusinessRulesView from './BusinessRulesView';
import { getRulesListRequest } from '../../actions/rules';
import { connect } from 'react-redux'
class AppointmentsApproved extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  componentDidMount = () => {
    this.props.getRulesList()
  }
  onUpdateRule = (item, index) => {
    this.props.navigation.navigate("BusinessRulesCustom", { item, index,isUpdate:true })
  }

  onCreateRule = () => {
    this.props.navigation.navigate("BusinessRulesCustom")
  }
  render() {
    return (
      <BusinessRulesView {...this} {...this.state} {...this.props} />
    );
  }
}



function mapStateToProps(state) {
  return {
    loadingRulesList: state.rulesReducer.loadingRulesList,
    rulesList: state.rulesReducer.rulesList,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getRulesList: () => dispatch(getRulesListRequest())
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppointmentsApproved);




import { StyleSheet } from 'react-native';
import { metrics } from '../../config';
import { getHeight, Width, Height, getWidth } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        width:Width,
        height:Height,
        backgroundColor:ColorsList.white
    },
    logo: {
        width: Width * .8,
        height: Height * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: '100%',
        height: '100%',
    },
    headerSection: {
        backgroundColor: 'white', height: getHeight(67),
        width: metrics.screenWidth,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: Width * .05,
        shadow: 5, top: getHeight(80),
        position: 'absolute',
        paddingTop: getHeight(13)
    },
    notificationCard: {


        width: metrics.screenWidth,
        backgroundColor: 'white',
        shadow: 4,
        height: getHeight(86),
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: getHeight(67),
        flexDirection: 'row'


    },
    Icon: {
        //width: metrics.screenWidth * .2,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: 'red'
    },
    title: {
        fontSize: 'small13Px',
        fontWeight: 'bold',
        color: 'black'
    },
    subtitle: {

        fontSize: 'small1',
        fontWeight: '500',
        color: 'black',
        marginTop: getHeight(2)

    },
    time: {
        fontSize: 'small1',
        fontWeight: '500',
        marginTop: getHeight(2)
    },
    notificationContainer: {
        width: Width,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: getHeight(30),
    },
    cardInfo: {
        width: Width,
        height: getHeight(58),
        backgroundColor: 'white',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal:getWidth(15)
    },
    serviceContainer: {

        width: '100%', justifyContent: 'center', alignItems: 'center',
        shadow: 3, marginTop:getHeight(15), height: getHeight(58), backgroundColor: 'white'

    },
    infoContainer:
    {
        flexDirection: 'row',
        width: Width* .95,
        // shadow: 5,
        alignSelf: 'center',
        // justifyContent: 'space-between',
        //backgroundColor:'green'
    },
    textContainer: {
        flexDirection: 'row', marginLeft: getHeight (10), marginTop: 4,alignItems:'center'
    },
    serviceName: {
        fontSize: 'size15',
        fontFamily: 'bold', color: 'DarkGray1'
    },
    duration: {
        fontSize: 'size11',
        fontFamily: 'medium', marginLeft:getWidth (5),
        color:ColorsList.lightGray1
    },
    leftButton: { justifyContent: 'center', alignItems: 'flex-end',width:getWidth(18),height:getHeight(18) },
    input: {
        borderRadius: 3,
        width: getWidth(325),
        height: getHeight(41),
        shadow: 3, 
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: getHeight(35)
    },


};

export default styles;

import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import { Button } from '../../components/button/button';
import { _Text } from '../../components/text/text';
import { Card } from '../../components/card/card';
import { Icons } from '../../config';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import { PopupModal } from '../../components/popupModal/popupModal';
import DatePicker from 'react-native-date-picker';
import { _string } from '../../local';
type Props = {
    duration: number
}
class BusinessRulesCustomerModel extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            start_end_time: ""
        };
    }

    onAnimated = (state, start_end_time,date) => {
        this.setState({ start_end_time,date })
        this.AddTime.onAnimated(state)
    }
    render() {
        let { start_end_time } = this.state
        let { set_StartEndTime,onSetDate } = this.props
        return (
            <PopupModal duration={500} ref={(Add) => this.AddTime = Add} height={getHeight(266)}>
                <Card style={{
                    height: '100%', width: '100%',
                    alignItems: 'center', padingBottom: getHeight(10),

                }}>

                    <Card style={{
                        flexDirection: 'row', width: getWidth(375), justifyContent: 'center',
                        alignItems: 'center', marginTop: getHeight(20),
                    }}>
                        <Icons name='appointmentsgray' width={getWidth(18)} height={getWidth(18)} />
                        <_Text style={{
                            fontSize: 'size15', fontFamily: 'bold',
                            color: 'darkGray1', marginLeft: getWidth(5),
                        }}>{_string("business_rules_add.time_farms_tart")}</_Text>
                    </Card>

                    <DatePicker
                    
                        locale={'en'}
                        style={{ marginTop: getHeight(10), width: Width, height: getHeight(140) }}
                        date={this.state.date}
                        onDateChange={(date) => {
                            this.setState({date})
                            set_StartEndTime(date, start_end_time)

                        }}
                    // mode={'datetime'}
                    />

                    <Button
                        iconName="done"
                        text='Done'
                        style={{
                            colors: 'blue',
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: getWidth(345),
                            height: getHeight(44),
                            borderRadius: 3,
                            textColor: 'white',
                            fontSize: 'size15',
                            marginTop:getHeight(10),
                            fontFamily:'bold'
                        }}
                        onPress={() => { 
                            onSetDate()
                            this.AddTime.onAnimated() }}
                            iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                    />


                </Card>

            </PopupModal>

        );
    }
}


export default BusinessRulesCustomerModel






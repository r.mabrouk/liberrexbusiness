import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import styles from './styles';
import { Button } from '../../components/button/button';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { images, metrics, Icons } from '../../config';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import ServiceCard from '../../components/ServiceCard/ServiceCard';
import TimeCard from '../../components/TimeCard/TimeCard'
import { PopupModal } from '../../components/popupModal/popupModal';
import DatePicker from 'react-native-date-picker';
import BusinessRulesCustomModel from './BusinessRulesCustomModel';
import { ColorsList } from '../../components/PropsStyles';
import TimeModal from '../NewAppointment/TimeModal';
import moment from 'moment';
import { _string } from '../../local';
export default class BusinessRulesCustomView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date()
        };
    }


    render() {
        let { rule_name, onChangeRuleNmae, flow_target, type_apply, action_trigger, onChecked, time_frame, start_date, end_date, set_StartEndTime, onCreateRule, loadingCreateAndUpdateRule, onSetDate,
            start_date_,
            end_date_,
            onRemoveRule,
            onUpdateRule,
            isUpdate,
            loadingDeleteRule
        } = this.props



        return (
            <View
                style={styles.container}>

                <Header style={{ width: '100%', zIndex: 16 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />
                <HeaderSection
                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}
                    text={_string("NewBusinessRules")} iconName='rules'

       
                    iconStyle={{ width: getWidth(18),height:getHeight(18) , color: 'gray'}}
                />
                <ScrollView contentContainerStyle={{
                    alignItems: 'center'
                }}>
                    <CustomTextInput
                        marginTop={getHeight(20)}
                        onChangeText={onChangeRuleNmae}
                        value={rule_name} headerIcon='rules' headerText={_string("RuleName")}
                        leftIcon='editpen' placeholder={_string("pleace_holder")} />


                    <Card style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Card style={{ flexDirection: 'row', width: getWidth(345) }}>
                            <Icons name={'send'} width={getWidth(18)} height={getWidth(18)} />
                            <_Text style={styles.headerTitle}>{_string("FlowTarget")}</_Text>
                        </Card>
                        <Card style={styles.choocies(getHeight(10), getHeight(15))}>
                            <ServiceCard zIndex={100} checked={flow_target == 1} onChecked={() => onChecked('flow_target', 1)} text= {_string("Appointments")}  />
                            <ServiceCard zIndex={111} checked={flow_target == 2} onChecked={() => onChecked('flow_target', 2)} text={_string("Queues")} />
                        </Card>
                    </Card>


                    <Card style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Card style={{ flexDirection: 'row', width: getWidth(345) }}>
                            <Icons name={'reports'} width={getWidth(18)} height={getWidth(18)} />
                            <_Text style={styles.headerTitle}>{_string("ApplyFor")} { 'Apply for:'}</_Text>
                        </Card>
                        <Card style={styles.choocies(getHeight(15), getHeight(5))}>
                            <ServiceCard zIndex={5} checked={type_apply == 1} onChecked={() => onChecked('type_apply', 1)} text={_string("Allcustomers")} />
                            <ServiceCard zIndex={6} checked={type_apply == 2} onChecked={() => onChecked('type_apply', 2)} text= {_string("VIPCustomers")} />
                        </Card>
                        <Card style={styles.choocies(getHeight(5), getHeight(15))}>
                            <ServiceCard zIndex={7} checked={type_apply == 3} onChecked={() => onChecked('type_apply', 3)} text={_string("Newcustomers")} />
                            <ServiceCard zIndex={8} checked={type_apply == 4} onChecked={() => onChecked('type_apply', 4)} text={_string("Blacklist")} />
                        </Card>
                    </Card>

                    <Card style={{ justifyContent: 'center', alignItems: 'center', marginTop: getHeight(10) }}>
                        <Card style={{ flexDirection: 'row', width: getWidth(345) }}>
                            <Icons name={'redirect'} width={getWidth(18)} height={getWidth(18)} />
                            <_Text style={styles.headerTitle}>{_string("ActionToTrigger")}</_Text>
                        </Card>
                        <Card style={styles.choocies(getHeight(10), getHeight(15))}>
                            <ServiceCard checked={action_trigger == 1} onChecked={() => onChecked('action_trigger', 1)} text={_string("Approve")}  />
                            <ServiceCard checked={action_trigger == 2} onChecked={() => onChecked('action_trigger', 2)} text={_string("Decline")} />
                        </Card>
                    </Card>


                    <Card style={{ justifyContent: 'center', alignItems: 'center', marginTop: getHeight(10) }}>
                        <Card style={{ flexDirection: 'row', width: getWidth(345) }}>
                            <Icons name={'appointmentsgray'} width={getWidth(18)} height={getWidth(18)} />
                            <_Text style={style = styles.headerTitle}>{_string("TimeFrame")}</_Text>
                        </Card>
                        <Card style={styles.choocies(getHeight(10), getHeight(15))}>
                            <ServiceCard checked={time_frame == 1} onChecked={() => onChecked('time_frame', 1)} text= {_string("Forever")} />
                            <ServiceCard checked={time_frame == 2} onChecked={() => onChecked('time_frame', 2)} text={_string("Custom")} />
                        </Card>


                        {time_frame == 2 && <View>
                            <Button style={styles.timeCard(0, 0)}
                                onPress={() => this.add_custome_date.onAnimated(true, 'start_date_', start_date)}
                            >
                                <_Text style={{ width: '90%', fontSize: 'size13', fontFamily: 'medium', color: "darkGray1" }}>
                                    <_Text style={styles.subText}>{_string("Start")}</_Text>
                                    {` ${moment(start_date).format('DD MMM YYYY h:mm:ss')}`}
                                </_Text>
                                <Icons width={getWidth(12)} height={getWidth(12)} name={'arrowDown'} /></Button>
                            <Button style={styles.timeCard(getHeight(10), getHeight(15))}
                                onPress={() => this.add_custome_date.onAnimated(true, 'end_date_', end_date)}
                            ><_Text style={{ width: '90%', fontSize: 'size13', fontFamily: 'medium', color: "darkGray1" }}><_Text style={styles.subText}>{_string("End")}</_Text>{` ${moment(end_date).format('DD MMM YYYY h:mm:ss')}`}</_Text>
                                <Icons width={getWidth(12)} height={getWidth(12)} name={'arrowDown'} />
                            </Button>
                        </View>}


                    </Card>

                    <Card style={{ flexDirection: 'row', width: getWidth(345), marginTop: getHeight(15) }}>
                        <Icons name={'calendar'} width={getWidth(18)} height={getWidth(18)} />
                        <_Text style={styles.headerTitle}>{_string("ApplyOnThisDays")}</_Text>
                    </Card>

                    {this.props.selectedTimes.map((item, index) => {
                        console.log("selectedTimes",item.active)
                        return (
                            <View key={`option${index}`} >
                                <TimeCard
                                isSelected={item.active}

                                    name={item.day}
                                    onDayPress={(status) => { this.props.onDayPress(index, status) }}
                                    onPress={() => {
                                        this.AddTime.onAnimated(true)
                                        this.props.onSetTime(index)
                                        this.setState({ selectedDay: index })
                                    }}
                                    from={this.props.formatedTime(this.props.selectedTimes[index].start_time)}
                                    to={this.props.formatedTime(this.props.selectedTimes[index].end_time)}
                                />
                            </View>
                        )
                    })}




                    <View style={{ width:Width, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: getWidth(15),paddingBottom:getHeight(15) }}>
                        <Button
                            isLoading={loadingCreateAndUpdateRule}

                            onPress={() => {
                                if (isUpdate)
                                    onUpdateRule()
                                else
                                    onCreateRule()
                            }}
                            backgroundColor={'blue'}
                            iconStyle={{ width: getWidth(18),height:getHeight(18) , color: '#ffffff'}}
                            text={_string("app_settings.save")}
                            iconName='done'
                            style={styles.donebtn(getWidth(isUpdate ? 165 : 345))} />
                        {isUpdate && <Button
                            loaderColor={'darkGray1'}
                            isLoading={loadingDeleteRule}
                            onPress={() => onRemoveRule()}
                            backgroundColor={'white'}
                            iconStyle={{ width: getWidth(18),height:getHeight(18) , color: '#ffffff'}}
                            text={_string("your_service.remove")}
                            iconName='delete'
                            style={styles.removeButton} />}
                    </View>
                </ScrollView>


                <BusinessRulesCustomModel
                    onSetDate={onSetDate}
                    set_StartEndTime={set_StartEndTime}
                    duration={500} ref={(add_custome_date) => this.add_custome_date = add_custome_date} />
                <TimeModal
                title={_string("BusinessRulesTime")}
                         startDate={this.props.startDate}
                         endDate={this.props.endDate}
                    ref={(time) => { this.AddTime = time }}
                    onDonePress={(newTimes) => {
                        this.props.onTimeChange(this.state.selectedDay, newTimes)
                    }}
                />
            </View >

        );
    }
}



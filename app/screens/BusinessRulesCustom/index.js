import React, { Component } from 'react';
import { View, Text } from 'react-native';
import BusinessRulesCustomView from './BusinessRulesCustomView';
import moment from 'moment';
import { createRuleRequest, updateRuleRequest, deleteRuleRequest } from '../../actions/rules';
import { connect } from 'react-redux'
import { deleteCustomerRequest, updateCustomerRequest } from '../../actions/customers';
import { _string } from '../../local';
import { _Toast } from '../../components/toast/toast';
class BusinessRulesCustom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rule_name: "",
      flow_target: 1,
      action_trigger: 1,
      type_apply: 1,
      time_frame: 1,
      start_date_: new Date(),
      end_date_: new Date(),
      start_date: new Date(),
      end_date: new Date(),
      isUpdate: props.navigation.getParam("isUpdate"),
      selectedTimes: [
        { "day": _string("dayes.Monday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Tuesday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Wednesday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Thursday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Friday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
          { "day": _string("dayes.Saturday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Sunday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
      ],
      startDate: new Date(moment().format("YYYY-MM-DDT09:00:00")),
      endDate: new Date(moment().format("YYYY-MM-DDT09:00:00"))
    };
  }

  setRulesDataWhenUpdateAndDelete = () => {
    let item = this.props.navigation.getParam("item")
    let index = this.props.navigation.getParam("index")
    if (item) {
      this.setState({
        rule_name: item.name,
        flow_target: item.type == "booking" ? 1 : 2,
        action_trigger: item.action == "approve" ? 1 : 2,
        time_frame: item.time_frame == "forever" ? 1 : 2,
        start_date: item.start_date ? item.start_date : new Date(),
        end_date: item.start_date ? item.end_date : new Date(),
        start_date_: item.start_date ? item.start_date : new Date(),
        end_date_: item.start_date ? item.end_date : new Date(),
        selectedTimes:item.working_days.length>=1?item.working_days:this.props.working_days
      })
      console.log("working_days",item.working_days)
    }
  }
  componentDidMount = () => {
    this.setRulesDataWhenUpdateAndDelete()
  }
  // ------------ change the time of a specific dat -----------------
  onTimeChange = (dayOfWeek, newTimes) => {
    console.log('dayOfWeek', dayOfWeek, ' newTimes', newTimes)
    let start_time = moment(newTimes.from).format('HH:mm:00')
    let end_time = moment(newTimes.to).format('HH:mm:00')
    let selectedTimesCopy = [...this.state.selectedTimes];
    // selectedTimesCopy[dayOfWeek].status = "enabled"
    selectedTimesCopy[dayOfWeek].start_time = start_time
    selectedTimesCopy[dayOfWeek].end_time = end_time
    this.setState({ selectedTimes: selectedTimesCopy })
    console.log('selectedTimesCopy',start_time,'selectedTimesCopy', end_time)
  }

  // ----------- update the selected days -------------

  onSetTime=(dayOfWeek)=>{
    let selectedTimesCopy = [...this.state.selectedTimes];
    let startDate = new Date(moment().format("YYYY-MM-DDT" + selectedTimesCopy[dayOfWeek].start_time))
    let endDate = new Date(moment().format("YYYY-MM-DDT" + selectedTimesCopy[dayOfWeek].end_time))
    this.setState({ selectedTimes: selectedTimesCopy, startDate, endDate })
  }
  onDayPress = (dayOfWeek, status) => {
    let selectedTimesCopy = [...this.state.selectedTimes];
    selectedTimesCopy[dayOfWeek].active = status ? 1 : 0

    let startDate = new Date(moment().format("YYYY-MM-DDT" + selectedTimesCopy[dayOfWeek].start_time))
    let endDate = new Date(moment().format("YYYY-MM-DDT" + selectedTimesCopy[dayOfWeek].end_time))
    console.log(selectedTimesCopy[dayOfWeek].start_time,"dfkdngkdjghdkjghfdgdfgfdgdfgfdg")
    this.setState({ selectedTimes: selectedTimesCopy, startDate, endDate })
  }

  // ------------- format the time ------------
  formatedTime = (time) => {
    return moment(time, ["HH:mm:00"]).format('HH:mm').toString()
  }



  validationForm = () => {
    let { rule_name } = this.state
    if (!rule_name) {
      _Toast.open(_string("PleaseTypeTheRuleName"), "w")
      return false
    }
    else
      return true
  }


  ruleData = () => {
    let { rule_name, selectedTimes, action_trigger, end_date, flow_target, start_date, time_frame, type_apply } = this.state
    let start = moment(start_date).format("YYYYY-MM-DD h:mm:ss");
    let end = moment(end_date).format("YYYYY-MM-DD h:mm:ss");

    return {
      name: rule_name,
      type: flow_target == 1 ? "booking" : "queues",
      action: action_trigger == 1 ? "approve" : "decline",
      time_frame: time_frame == 1 ? "forever" : "custom",
      start_date: start,
      end_date: end,
      customer_book_id: type_apply,
      working_days: selectedTimes
    }
  }
  onCreateRule = () => {


    if (this.validationForm()) {
      this.props._createRule(this.ruleData())


    }

  }
  onRemoveRule = () => {
    let item = this.props.navigation.getParam("item")
    let index = this.props.navigation.getParam("index")
    this.props._removeRule(item.id, index)
  }
  onUpdateRule = () => {
    let item = this.props.navigation.getParam("item")
    let index = this.props.navigation.getParam("index")
    console.log(this.ruleData(), "onUpdateRule")

    this.props._updateRule(this.ruleData(), item.id, index)
  }


  onSetDate = () => {
    let { end_date_, start_date_ } = this.state
    this.setState({ end_date: end_date_, start_date: start_date_ })
  }
  set_StartEndTime = (date, key) => {
    this.setState({ [key]: date })
  }
  onChecked = (key, value) => {
    this.setState({ [key]: value })
  }
  onChangeRuleNmae = (text) => {
    this.setState({ rule_name: text })
  }
  render() {
    return (
      <BusinessRulesCustomView {...this} {...this.state} {...this.props} />
    );
  }
}

function mapStateToProps(state) {
  return {
    working_days: state.authReducer.user_profile.working_days,

    loadingCreateAndUpdateRule: state.rulesReducer.loadingCreateAndUpdateRule,
    loadingDeleteRule: state.rulesReducer.loadingDeleteRule,
  };
}
function mapDispatchToProps(Dispatch) {
  return {
    _createRule: (data) => Dispatch(createRuleRequest(data)),
    _removeRule: (id, index) => Dispatch(deleteRuleRequest(id, index)),
    _updateRule: (data, id, index) => Dispatch(updateRuleRequest(data, id, index)),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BusinessRulesCustom);


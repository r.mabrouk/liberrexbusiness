import { StyleSheet } from 'react-native';
import { getHeight, Width, getWidth, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles/colors';

const styles = {

    subText: {
        color:
            ColorsList.rgbaGray(.6),
        fontSize: 'size13', fontFamily: 'medium'
    },

    container: {
        width: Width,
        height: Height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: ColorsList.white
    },
    logo: {
        width: Width * .8,
        height: Height * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: '100%',
        height: '100%',
    },


    Icon: {

        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',

    },
    title: {
        fontSize: 'small13Px',
        fontWeight: 'bold',
        color: 'black'
    },
    subtitle: {

        fontSize: 'small1',
        fontWeight: '500',
        color: 'black',
        marginTop: 2

    },
    time: {
        fontSize: 'small1',
        fontWeight: '500',
        marginTop: getHeight(2)
    },

    cardInfo: {
        width: Width,
        height: getHeight(58),
        backgroundColor: 'white',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    serviceContainer: {

        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        shadow: 3, marginTop: getHeight(15),
        height: getHeight(58),
        backgroundColor: 'white'

    },
    infoContainer:
    {
        flexDirection: 'row',
        width: getWidth(345),
        // shadow: 5,
        alignSelf: 'center',
        // justifyContent: 'space-between',
        //backgroundColor:'green'
    },
    textContainer: {
        flexDirection: 'row', marginLeft: getWidth(10), marginTop: getHeight(4)
    },
    serviceName: {
        fontSize: 'medium15Px',
        fontWeight: 'bold', color: 'black'
    },
    duration: {
        fontSize: 'small1', alignItems: 'center', justifyContent: 'center',
        fontWeight: '500', marginLeft: 5,
    },
    leftButton: { justifyContent: 'center', alignItems: 'flex-end' },
    input: {
        borderRadius: 3,
        width: getWidth(325),
        height: getHeight(41),
        shadow: 3,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: getHeight(35)
    },
    donebtn: (width)=>({
            width: width,
            marginTop: getHeight(15),
            textColor: '#fff',
            shadow: 6,
            height: getHeight(44),
            colors: 'blue',
            borderRadius: 3,
            fontFamily:'bold',
            fontSize:'size15'
    }),
    removeButton: {
        width: getWidth(165),
        marginTop: getHeight(15),
        textColor:'darkGray1',
        shadow: 6,
        height: getHeight(44),
      backgroundColor:'white',

        borderRadius: 3,
        fontFamily:'bold',
        fontSize:'size15'
    },
    timeCardContainer: {
        flexDirection: 'row',
        justifyContent: 'center'
        , alignItems: 'center',
        width: getWidth(345),
        height: getHeight(41),
        backgroundColor: 'white',
        shadow: 4,
        marginTop: getHeight(10),
        marginBottom: getHeight(5),
    },
    timeCard: (marginTop, marginBottom) => ({
        flexDirection: 'row',
        justifyContent: 'center'
        , alignItems: 'center',
        width: getWidth(345),
        height: getHeight(41),
        backgroundColor: 'white',
        marginBottom: marginBottom,
        shadow: 6,
        marginTop: marginTop,

    }),
    headerTitle: {
        fontSize: 'size15', marginLeft: getWidth(5), color: 'black',
        fontFamily: 'bold'
    },
    choocies: (paddinTop, paddingBottom) => ({
        width: Width,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: getWidth(15),
        paddingBottom: paddingBottom,
        paddingTop: paddinTop,
        overflow:'visible'

    })
};

export default styles;

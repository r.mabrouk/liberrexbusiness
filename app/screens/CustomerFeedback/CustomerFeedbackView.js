import React, { Component } from 'react';
import {
    View,
    ScrollView, FlatList
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { images, metrics, Icons } from '../../config';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { ReportRequestItem } from '../../components/reportRequestItem/reportRequestItem';
import { CustomerFeedbackItem } from '../../components/customerFeedbackItem/customerFeedbackItem';
import { Width, getWidth, getHeight } from '../../components/utils/dimensions';

import ReportByModal from '../Home/reportByModal';
import CustomerFeedbackModal from './customerFeedbackModal';
import { _string } from '../../local';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import EmptyData from '../../components/emptyData/emptyData';

class CustomerFeedbackView extends Component {
    constructor(props) {
        super(props);
        this.state = {

            openCard: false,
            currentIndex: -1
        }
    }
    async componentDidMount() {
        this.props.nextScreen()
    }

    feedbackItemClicked = (item) => {
        this.customerFeedbackModal.onAnimated(true, item)
    }
    render() {

        let { customer_feedback, customer_feedback_statistics ,loadingCustomerFeedback,onFilter,
            onGetAllCustomerFeedback,
            isRefreshing,
            onRefresh
        } = this.props
        return (
            <View
                style={styles.container}>
                <Header style={{ width: '100%', zIndex: 16 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />
                <View style={styles.contentContainer}>


                    <HeaderSection
                        onPress={() => {
                            this.reportByModal.onAnimated(true)
                        }}
                        iconwidth={getWidth(28)} height={getWidth(28)}
                        style={{
                            shadow: 6,
                            zIndex: 15,
                            width: Width,
                            height: getHeight(67),
                            marginTop: -getHeight(30),
                            paddingTop: getHeight(15),
                            flexDirection: 'row',
                            alignItems: 'center',
                            backgroundColor: 'white',
                            justifyContent: 'space-between',
                            paddingHorizontal: getWidth(15)
                        }}
                        withButton
                        textButton={_string("modals.report_by.weekly")}
                        text={_string("customer_feedback.customer_feedback")}
                        iconName='reportsEdit'
                        iconButton='arrowDown'
                        textStyle={{fontFamily:'bold',fontSize:'size17',color:'darkGray1',paddingHorizontal:getWidth(10)}}
                        buttonStyle={styles.buttonStyle}

                    />


                    <View style={styles.customerFeedbackItem}>
                        <FlatList
                        extraData={this.props}
                        keyExtractor={item => item.id}
                        refreshing={isRefreshing}
                        onRefresh={onRefresh}
                        onEndReached={onGetAllCustomerFeedback}
                        onEndReachedThreshold={0.1}


                            data={customer_feedback}
                            ListHeaderComponent={() => {
                                return (
                                    !loadingCustomerFeedback&&customer_feedback.length>=1?<View style={styles.reportItem}>
                                        {Object.keys(customer_feedback_statistics).map((item) => (
                                            <ReportRequestItem
                                                usedValue={customer_feedback_statistics[item]}
                                                totalValue={100}
                                                status={item}
                                            />
                                        ))}
                                    </View>:null
                                )
                            }}
                            renderItem={({ item, index }) => {
                                return (
                                    <CustomerFeedbackItem
                                        fullname={item.fname + " " + item.lname}
                                        icon={item.anonymous == 1 ? "anonymous" : "user"}
                                        rating={item.rating}
                                        feedback={item.feedback}
                                        onPress={() => this.feedbackItemClicked(item)}
                                    />
                                )
                            }}

                            ListFooterComponent={()=>{
                                return(
                                    <LoaderLists  isLoading={loadingCustomerFeedback} />
                                )
                            }}
                        />
                    </View>

                    <EmptyData
                    height={getHeight(270)}
                    show={(customer_feedback.length < 1) && !loadingCustomerFeedback} />
                </View>

                <ReportByModal
                onFilter={onFilter}
                ref={(_reportByModal) => this.reportByModal = _reportByModal} />

                <CustomerFeedbackModal ref={(_popupModal) => this.customerFeedbackModal = _popupModal} />
              
            </View>
        );
    }
}


CustomerFeedbackView.propTypes = {
    nextScreen: PropTypes.func
};

export default CustomerFeedbackView;


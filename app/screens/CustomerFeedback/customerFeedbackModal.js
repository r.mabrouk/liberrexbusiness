import React, { Component } from 'react';
import {
    View,
    ScrollView
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Icons } from '../../config';
import { _Text } from '../../components/text/text';
import { PopupModal } from '../../components/popupModal/popupModal';
import { getWidth, getHeight } from '../../components/utils/dimensions';
import { Rating } from '../../components/rating/rating';
import { Card } from '../../components/card/card';


type Props = {
    duration: number
}
class CustomerFeedbackModal extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            rating: 0,
            feedback: "",
            date: "",
            fullname: "",
            services: ""
        }
    }
    async componentDidMount() {
    }
    onAnimated = (state, feedback_info) => {
        this.setState({
            rating: feedback_info.rating,
            feedback: feedback_info.feedback,
            fullname: feedback_info.fname + " " + feedback_info.lname,
            date: "25 June,2019 4:00PM",
            services: "Hair Cut, Hair coloring, Shave"
        })
        this.customerFeedbackModal.onAnimated(state)
    }
    render() {
        let { fullname, date, feedback, rating, services } = this.state
        return (
            <PopupModal ref={_popupModal => this.customerFeedbackModal = _popupModal}
                height={getHeight(236)}  >

                <View style={styles.customerInfo}>
                    <View
                        style={styles.headerIconView}>
                        <Icons
                            name='user'
                         width={getWidth(30)}
                         height={getWidth(30)}
                        />
                    </View>
                    <_Text
                        style={styles.queueText} >
                        {fullname.toLocaleUpperCase()}
                    </_Text>
                </View>

                <_Text
                    style={styles.paragraphText}>
                    {feedback}
                </_Text>

                <Rating rating={rating} style={styles.ratingView} />

                <InputItem
                    iconName='calendar'
                    text={date}
                    border
                />
                <InputItem
                    iconName='grid'
                    text={services}
                />

            </PopupModal>
        );
    }
}


const InputItem = ({ iconName, text, border }) => {
    return (
        <Card style={styles.cardContainer(border)}>
            <View style={styles.iconView}>
                <Icons
                    name={iconName}
                    width={getWidth(18)} height={getWidth(18)}
                />
            </View>
            <_Text
                style={styles.textStyle} >
                {text}
            </_Text>
        </Card>
    )
}

CustomerFeedbackModal.propTypes = {
    nextScreen: PropTypes.func
};

export default CustomerFeedbackModal;
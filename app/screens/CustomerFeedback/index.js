import React, { Component } from 'react';
import { connect } from 'react-redux';
import CustomerFeedbackView from './CustomerFeedbackView';
import { getAllCustomerFeedbackRequest, filterCustomerFeedbackRequest } from '../../actions/customers';
import { _string } from '../../local';

class CustomerFeedback extends Component {
  constructor(props) {
    super(props);
    this.state={
      isRefreshing:false
    }
  }

  nextScreen = () => {

    setTimeout(() => {
    }, 2000);
  }
  onFilter = (filter) => {
    let filter_by=""
    if (_string("modals.report_by.daily"))
      filter_by = "daily"
    else if (filter == _string("modals.report_by.weekly"))
      filter_by = "weekly"
    else if (filter == _string("modals.report_by.monthly"))
      filter_by = "monthly"
    else if (filter == _string("modals.report_by.yearly"))
      filter_by = "yearly"

    let filterData = {
      service_id: 1,
      date_start: "2019-05-20",
      date_end: "2019-08-01",
      anonymous: 0
    }
    this.props.onFilterCustomerFeedback(filterData)
  }
  componentDidMount = () => {
    this.props.getAllCustomerFeedback()
  }

  onGetAllCustomerFeedback=()=>{
    //  this.props.getAllCustomerFeedback()
  }
 
  onRefresh=()=>{
    this.props.getAllCustomerFeedback()
// this.setState({isRefreshing:true})
  }


  render() {
    let width = this.props.width
    return (
      <CustomerFeedbackView {...this}
      {...this.state}
        {...this.props}
        nextScreen={this.nextScreen}
      />
    );
  }
}


function mapStateToProps(state) {
  return {
    loadingCustomerFeedback: state.customersReducer.loadingCustomerFeedback,
    customer_feedback: state.customersReducer.customer_feedback,
    customer_feedback_statistics: state.customersReducer.customer_feedback_statistics
  };
}
function mapDispatchToProps(Dispatch) {
  return {

    getAllCustomerFeedback: () => Dispatch(getAllCustomerFeedbackRequest()),
    onFilterCustomerFeedback: () => Dispatch(filterCustomerFeedbackRequest())

  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerFeedback);



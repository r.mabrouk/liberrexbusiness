import { getHeight, Width, getWidth, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        width: Width,
        height: Height,
        alignItems: 'center',
        backgroundColor: ColorsList.white
    },
    
    contentContainer: {
        alignItems: 'center',
        width: '95%',
        height: '100%'
    },
    headerView: {
        backgroundColor: 'white',
        height: 67,
        width: Width,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: Width * .05,
        shadow: 5
    },
    textStyle: {
        color: 'darkGray1',
        fontFamily: 'bold',
        fontSize: 'regular17Px'
    },
    buttonStyle: {
        backgroundColor: 'white',
        shadow: 6,
        width: getWidth(75),
        height: getHeight(31),
        borderRadius: 3,
        fontSize: 'size11',
        flexDirection: 'row-reverse',
        fontFamily:'medium',
        color:'darkGray1'
    },
    reportItem: {
        width: getWidth(375),
        paddingHorizontal:getWidth(15),
        height: getHeight(140),
        marginTop: getHeight(3),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        // backgroundColor: 'white'
    },
    customerFeedbackItem: {
        width: getWidth(375),
        paddingBottom: Height * 0.1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: getHeight(5)
    },
    queueText: {
        fontSize: 'size15',
        fontFamily: 'bold',
        color: 'darkGray1',
        paddingHorizontal:getWidth(10)
    },
    ratingView: {
        justifyContent: 'center',
        alignItems: 'center',marginTop:getHeight(10)
    },
    customerInfo: {
        width: getWidth(345),
        height: getHeight(28),
        marginTop: getHeight(20),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerIconView: {
        height: '100%',
        width: getWidth(30),
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerNameView: {
        height: '100%',
        width: getWidth(103),
        justifyContent: 'center',
        alignItems: 'center'
    },
    paragraphTextView: {
        width: getWidth(345),
        height: getHeight(45),
        justifyContent: 'center',
        alignItems: 'center'
    },
    paragraphText: {
        height:getHeight(32),
        fontSize: 'size13',
        fontFamily: 'medium',
        color: 'lightGray1',
        textAlign:'center',
        marginTop:getHeight(10)
    },
    cardContainer : (border) => ({
        width: getWidth(375),
        height: getHeight(48),
        paddingHorizontal:getWidth(15),
        flexDirection: 'row',
        alignItems: 'center',
        borderTopWidth:border?getHeight(1):0,
        borderBottomWidth:border?getHeight(1):0,
        borderColor:ColorsList.rgbaBlack(.04)
    }),
    iconView: {
        width: getWidth(25),
        height: getHeight(25),
        justifyContent: 'center',
        alignItems: 'center'
    },
    textStyle: {
        fontSize: 'medium15Px',
        fontFamily: 'bold',
        color: 'darkGray1'
    }
};
export default styles;
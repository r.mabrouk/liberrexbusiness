import React from 'react'
import { Card } from "../../components/card/card";

import { Icons } from "../../config";
import { _Text } from "../../components/text/text";
import styles from "./styles";
import { TouchableOpacity, View } from 'react-native'
import { Width, getWidth, getHeight } from "../../components/utils/dimensions";
import { Button } from '../../components/button/button';

type CustomerItemProps = {
    onPress: Function,
    fullName: String,
    email: String

}
export const CustomerItem = (props: CustomerItemProps) => {
    return (
        <Card style={styles.memberCard}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icons name='user' width={getWidth(28)} height={getWidth(28)} />
                <Card style={styles.TeamMemberInfo}>
                    <_Text style={styles.memberName}>{props.fullName}</_Text>
                  {props.email!==""&&  <_Text numberOfLines={1} style={styles.memberPosition}>( {props.email} )</_Text>}

                </Card>
            </View>
            <Button
                style={{
                    width: getWidth(45),
                    padddignHorizontal: getWidth(15)
                }}
                onPress={props.onPress}
                iconName='more'
                iconStyle={{ width: getWidth(18),height:getHeight(18) }}

             />


        </Card>
    )
}
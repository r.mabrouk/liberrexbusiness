import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text, ActivityIndicator
} from 'react-native';
import styles from './styles';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { metrics, Icons, AppStyles } from '../../config';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import { getHeight, Width, getWidth } from '../../components/utils/dimensions';

import { Input } from '../../components/input/input';
import SwiperTabButtons from '../../components/swiperTabButtons/swiperTabButtons';
import AddCustomerModel from '../QueueAddCustomer/AddCustomerModel'
import CustomersEditModal from './CustomersEditModal';
import CustomersAddModal from './CustomersAddModal';
import { connect } from 'react-redux'
import { _string } from '../../local';
import { getAllCustomersRequest, resetCustomersList, getAllCustomersBlacklistRequest, resetCustomersBlackList, getAllCustomersVipRequest, resetCustomersVipList } from '../../actions/customers';
import { ColorsList } from '../../components/PropsStyles';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import { CustomerItem } from './CustomerItem';
import UnBlockCustomerModal from '../QueueRequests/unBlockCustomerModal';
import EmptyData from '../../components/emptyData/emptyData';
class CustomerListView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isRefreshing: false,
            all_customers_keyword: null,
            vip_customers_keyword: null,
            blacklist_customers_keyword: null,
            isRefreshing_customer_vip: false,
            isRefreshing_customer_blacklist: false
        };
    }
    // onRefreshServiceList = () => {
    //     this.setState({ refreshing_list: true })
    //   }

    //   onEndReachedServiceList = () => {
    //     let {servicesListkeyword}=this.state
    //     let { services_current_page, services_last_page,loadingServicesList } = this.props
    //     if (services_current_page < services_last_page){
    //       this.props._getAllServices(null,servicesListkeyword?servicesListkeyword:null,services_current_page+1)
    //     }
    //   }
    //   onServicesSearch = (keyword) => {
    //     this.props._getAllServices(true, keyword, 1)
    //     this.setState({servicesListkeyword:keyword})
    //   }
    //   onAddService = (service_data, onCloseModal) => {
    //     this.props.onCreateService(service_data, onCloseModal)
    //   }
    //   onSearchIndustryServices = (keyword) => {
    //     this.props._getIndustryServices(true, keyword, 1)
    //   }
    // get_blacklist_customers -------------------------------------------

    onCustomeBlacklistSearch = (keyword) => {
        this.setState({ blacklist_customers_keyword: keyword }, () => {
            this.props.getAllCustomersBlacklist(true, keyword, 1)
        })
    }

    get_blacklist_customers = () => {
        let { blacklist_customers_keyword } = this.state
        let { blacklist_cuurent_page, blacklist_pages, isGetCustomersBlacklistLoading } = this.props
        if (!isGetCustomersBlacklistLoading) {
            if (blacklist_cuurent_page < blacklist_pages) {
                this.props.getAllCustomersBlacklist(false, blacklist_customers_keyword ? blacklist_customers_keyword : null, blacklist_cuurent_page + 1)
            }
            this.setState({ isRefreshing_customer_blacklist: false })
        }
    }
    // get_vip_customers -------------------------------------------
    onCustomeVipSearch = (keyword) => {
        this.setState({ vip_customers_keyword: keyword }, () => {
            this.props.getAllCustomersVip(true, keyword, 1)
        })
    }

    get_vip_customers = () => {
        let { vip_customers_keyword } = this.state
        let { vip_cuurent_page, vip_pages, isGetCustomersVipLoading } = this.props
        if (!isGetCustomersVipLoading) {
            if (vip_cuurent_page < vip_pages) {
                this.props.getAllCustomersVip(false, vip_customers_keyword ? vip_customers_keyword : null, vip_cuurent_page + 1)
            }
            this.setState({ isRefreshing_customer_vip: false })
        }

    }


    onCustomeSearch = (keyword) => {
        this.setState({ all_customers_keyword: keyword }, () => {
            this.props.getAllCustomers(true, keyword, 1)
        })
    }
    get_customers = () => {
        let { all_customers_keyword } = this.state
        let { cuurent_page, pages, isGetAllCustomersLoading } = this.props
        console.log("cuurent_pagecuurent_page", cuurent_page, pages)

        if (!isGetAllCustomersLoading) {
            if (cuurent_page < pages) {
                this.props.getAllCustomers(false, all_customers_keyword ? all_customers_keyword : null, cuurent_page + 1)
            }
            this.setState({ isRefreshing: false })
        }

    }

componentDidMount=()=>{
     this.props.getAllCustomers(true,false,1)
     this.props.getAllCustomersBlacklist(true,false,1)
    this.props.getAllCustomersVip(true,false,1)
}
    render() {
        let { isRefreshing, all_customers_keyword, isRefreshing_customer_blacklist, isRefreshing_customer_vip } = this.state
        let { all_customers,
            vip_customers,
            blacklist_customers,
            isGetCustomersBlacklistLoading,
            isGetCustomersVipLoading,
            onUpdate,
            onAddCustomer,
            onUnblock_customer,
            isremoveCustomerFromBookLoading,
            onAddCustomerToBooks,
            onRemoveCustomerFromVip,
            onDeleteCustomer,
            isDeleteCustomerLoading

        } = this.props

        return (
            <View
                style={styles.container}>

                <Header style={{ width: '100%', zIndex: 16 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />
                <HeaderSection

                    style={AppStyles.headerSection.container}
                    text={_string("customer_list.customer_list").toLocaleUpperCase()}
                    iconName='reports'
                    textStyle={{ fontFamily: 'bold', fontSize: 'size17', color: 'darkGray1', paddingHorizontal: getWidth(10) }}
                    iconStyle={{ width: getWidth(15), height: getHeight(15), color: 'gray' }}

                    withButton
                    buttonStyle={{
                        width: getWidth(110),
                        height: getHeight(35), justifyContent: 'center',
                        alignItems: 'center', colors: 'blue',
                        borderRadius: 3,
                        textColor: 'white',
                        fontFamily: 'bold',
                        fontSize: 'size13',
                        shadow: 6
                    }}
                    iconwidth={getWidth(28)} height={getWidth(28)}
                    textButton={_string("customer_list.add_new")}
                    iconButton='addwhite'
                    onPress={() => { this.addNew.onAnimated(true) }}
                />

                <View style={{
                    alignItems: 'center',
                    width: Width.width,
                }}>
                    <SwiperTabButtons
                        buttonWidth={getWidth(150)}
                        tabButons={[
                            { iconsName: 'reports', label: _string("customer_list.my_customers") },
                            { iconsName: 'star', label: _string("customer_list.vip_customers") },
                            { iconsName: 'cancel', label: _string("customer_list.blacklist") }]}>


                        {/* ----------------------------------- all customers start ----------------------------------- */}

                        <View style={{ width: Width, alignItems: 'center',height:getHeight(430) }}>
                            <ScrollView  scrollEnabled={true} contentContainerStyle={{width:Width,alignItems:'center',height:getHeight(71)}} style={{width:Width}}>
                            <Input
                                onChangeText={this.onCustomeSearch}
                                style={styles.search}
                                iconName={'search'}
                                placeholder={_string("customer_list.search_by_name_email_phone")}
                                iconStyle={{ width: getWidth(15), height: getHeight(15) }}
                                verticalLine={false}
                            />
                            </ScrollView>
                       
                            <EmptyData
                                height={getHeight(370)}
                                onPress={() => {
                                    this.addNew.onAnimated(true)
                                }}
                                show={(all_customers.length < 1) && !this.props.isGetAllCustomersLoading} />
                            <FlatList
                                extraData={this.state}
                                keyExtractor={item => item.id}
                                refreshing={isRefreshing}
                                onRefresh={() => this.setState({ isRefreshing: true })}
                                onEndReached={() => this.get_customers()}
                                onEndReachedThreshold={0.1}
                                style={{ height: getHeight(400) }}
                                data={all_customers} renderItem={({ item, index }) => {
                                    return (
                                        <CustomerItem

                                            fullName={item.fname + " " + item.lname}
                                            email={item.email} onPress={() => {
                                                this.editCustomer.onAnimated(true, item, index, "all_customers")
                                            }}

                                        />
                                    )
                                }}
                                ListFooterComponent={() => (
                                    <LoaderLists isLoading={this.props.isGetAllCustomersLoading} />
                                )}
                            />
                        </View>
                        {/* ----------------------------------- all customers end ----------------------------------- */}


                        {/* ----------------------------------- vip customers start ----------------------------------- */}

                        <View style={{ width: Width, alignItems: 'center' }}>
                        <ScrollView  scrollEnabled={true} contentContainerStyle={{width:Width,alignItems:'center',height:getHeight(71)}} style={{width:Width}}>

                            <Input
                                onChangeText={this.onCustomeVipSearch}
                                style={styles.search}
                                iconName={'search'}
                                placeholder={_string("customer_list.search_by_name_email_phone")}
                                iconStyle={{ width: getWidth(18), height: getHeight(18) }}

                                verticalLine={false}


                            />
                            </ScrollView>
                            <EmptyData
                                height={getHeight(370)}
                                onPress={() => {
                                    this.addNew.onAnimated(true)
                                }}
                                show={(vip_customers.length < 1) && !isGetCustomersVipLoading} />
                            <FlatList
                                extraData={this.state}
                                keyExtractor={item => item.id}
                                refreshing={isRefreshing_customer_vip}
                                onRefresh={() => this.setState({ isRefreshing_customer_vip: true })}
                                onEndReached={() => this.get_vip_customers()}
                                onEndReachedThreshold={0.1}
                                style={{ height: getHeight(400) }}
                                data={vip_customers} renderItem={({ item, index }) => {
                                    return (
                                        <CustomerItem
                                            fullName={item.fname + " " + item.lname}
                                            email={item.email} onPress={() => {
                                                this.editVipCustomer.onAnimated(true, item, index, "vip_customers")
                                            }}
                                        />
                                    )
                                }}
                                ListFooterComponent={() => (
                                    <LoaderLists isLoading={isGetCustomersVipLoading} />
                                )}
                            />
                        </View>
                        {/* ----------------------------------- vip customers end ----------------------------------- */}

                        {/* ----------------------------------- blacklist customers start ----------------------------------- */}


                        <View style={{ width: Width, alignItems: 'center' }}>
                            <Input
                                onChangeText={this.onCustomeBlacklistSearch}
                                style={styles.search}
                                iconName={'search'}
                                placeholder={_string("customer_list.search_by_name_email_phone")}
                                iconStyle={{ width: getWidth(18), height: getHeight(18) }}

                                verticalLine={false}


                            />
                            <EmptyData
                                height={getHeight(370)}
                                onPress={() => {
                                    this.addNew.onAnimated(true)
                                }}
                                show={(blacklist_customers.length < 1) && !isGetCustomersBlacklistLoading} />
                            <FlatList
                                extraData={this.state}
                                keyExtractor={item => item.id}
                                refreshing={isRefreshing_customer_blacklist}
                                onRefresh={() => this.setState({ isRefreshing_customer_blacklist: true })}
                                onEndReached={() => this.get_blacklist_customers()}
                                onEndReachedThreshold={0.1}
                                style={{ height: getHeight(400) }}
                                data={blacklist_customers} renderItem={({ item, index }) => {
                                    return (
                                        <CustomerItem
                                            fullName={item.fname + " " + item.lname}
                                            email={item.email} onPress={() => {
                                                this.unBlockCustomer.onAnimated(true, item, index)
                                            }}
                                        />
                                    )
                                }}
                                ListFooterComponent={() => (
                                    <LoaderLists isLoading={isGetCustomersBlacklistLoading} />
                                )}
                            />
                        </View>
                        {/* ----------------------------------- blacklist customers end ----------------------------------- */}

                    </SwiperTabButtons>
                </View>
                <CustomersAddModal

                    onAddCustomer={onAddCustomer}
                    ref={(addNew) => { this.addNew = addNew }} />
                <CustomersEditModal

                    isDeleteCustomerLoading={isDeleteCustomerLoading}
                    onDeleteCustomer={onDeleteCustomer}

                    onUpdate={onUpdate}
                    onAddCustomerToBooks={onAddCustomerToBooks}
                    zIndex={200}
                    ref={(edit) => { this.editCustomer = edit }} />
                <CustomersEditModal
                    isDeleteCustomerLoading={isDeleteCustomerLoading}
                    isVip
                    onDeleteCustomer={onDeleteCustomer}
                    onRemoveCustomerFromVip={onUnblock_customer}
                    onUpdate={onUpdate}
                    onAddCustomerToBooks={onAddCustomerToBooks}
                    zIndex={200}
                    ref={(edit) => { this.editVipCustomer = edit }} />

                <UnBlockCustomerModal
                    isremoveCustomerFromBookLoading={isremoveCustomerFromBookLoading}
                    onUnblock_customer={onUnblock_customer}
                    ref={(unBlockCustomer) => { this.unBlockCustomer = unBlockCustomer }} />
            </View >
        );
    }
}

function mapStateToProps(state) {

    return {
        all_customers: state.customersReducer.all_customers,
        blacklist_customers: state.customersReducer.blacklist_customers,
        vip_customers: state.customersReducer.vip_customers,

        isGetAllCustomersLoading: state.loadingReducer.isGetAllCustomersLoading,
        isGetCustomersVipLoading: state.loadingReducer.isGetCustomersVipLoading,
        isGetCustomersBlacklistLoading: state.loadingReducer.isGetCustomersBlacklistLoading,

        cuurent_page: state.customersReducer.cuurent_page,
        pages: state.customersReducer.pages,

        vip_cuurent_page: state.customersReducer.vip_cuurent_page,
        vip_pages: state.customersReducer.vip_pages,
        blacklist_cuurent_page: state.customersReducer.blacklist_cuurent_page,
        blacklist_pages: state.customersReducer.blacklist_pages,
        isremoveCustomerFromBookLoading: state.loadingReducer.isremoveCustomerFromBookLoading,
    }
}

function mapDispatchToProps(Dispatch) {

    return {
        getAllCustomers: (reset, keyword, page) => Dispatch(getAllCustomersRequest(reset, keyword, page)),
        getAllCustomersBlacklist: (reset, keyword, page) => Dispatch(getAllCustomersBlacklistRequest(reset, keyword, page)),
        getAllCustomersVip: (reset, keyword, page) => Dispatch(getAllCustomersVipRequest(reset, keyword, page)),



    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerListView)


import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import styles from './styles';
import { Button } from '../../components/button/button';
import { _Text } from '../../components/text/text';
import { Card } from '../../components/card/card';
import { Icons } from '../../config';
import { getHeight, getWidth } from '../../components/utils/dimensions';
import { PopupModal } from '../../components/popupModal/popupModal';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { connect } from 'react-redux'
import { createCustomerRequest } from '../../actions/customers';
import { validateEmail } from '../../utils/stringUtils';
import { _string } from '../../local';

type Props = {
    duration: number
}
class CustomersAddModal extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            fullname: "",
            email: "",
            phoneNumber: ""
        }
    }


    onAnimated = (state) => {
        this.Add.onAnimated(state)
    }
    render() {

        let { onAddCustomer, isAddNewCustomerLoading } = this.props
        let { fullname, email, phoneNumber } = this.state
        return (
            <PopupModal
                inputHeight={getHeight(150)}
                inputDuration={600}
                duration={400} ref={(Add) => this.Add = Add} height={getHeight(399)}>
                <Card style={styles.popupContainer}>
                    <View style={styles.popupHeader}>
                        <Icons name='user' width={getWidth(28)} height={getWidth(28)} />
                        <_Text style={styles.popupHeaderText}>{_string("AddANewCustomer").toLocaleUpperCase()}</_Text>
                    </View>
                    <CustomTextInput
                        returnKeyType='next'
                        onSubmitEditing={() => {
                            this.input2.focus()
                        }}
                        marginTop={getHeight(20)}
                        onChangeText={(text) => this.setState({ fullname: text })}
                        value={fullname} headerIcon={'userGray'} headerText={_string("appointment.full_name")}
                        placeholder={_string("pleace_holder")} leftIcon='editpen' />

                    <CustomTextInput
                        onSubmitEditing={() => {
                            this.input3.focus()
                        }}
                        ref={(input) => this.input2 = input}

                        returnKeyType='next'

                        onChangeText={(text) => this.setState({ email: text })}
                        value={email} headerIcon={'email'} headerText={_string("appointment.email")}
                        placeholder={_string("pleace_holder")} leftIcon='editpen' />

                    <CustomTextInput
                        ref={(input) => this.input3 = input}

                        onSubmitEditing={() => {
                            onAddCustomer(this.state, this.onAnimated)
                        }}
                        returnKeyType='done'
                        KeyboardType='numeric'
                        onChangeText={(text) => this.setState({ phoneNumber: text })}
                        value={phoneNumber} headerIcon={'phone'} headerText={_string("appointment.phone_number")}
                        placeholder={_string("pleace_holder")} leftIcon='editpen' />

                    <Button
                        isLoading={isAddNewCustomerLoading}
                        iconName="done"
                        text={_string('AddNewCustomer')}
                        style={styles.popupDonebtnAdd}
                        iconStyle={{ width: getWidth(18), height: getHeight(18) }}

                        onPress={() => onAddCustomer(this.state, this.onAnimated)}
                    />


                </Card>




            </PopupModal>


        );
    }
}
function mapStateToProps(state) {

    return {
        isAddNewCustomerLoading: state.loadingReducer.isAddNewCustomerLoading,
    }
}

function mapDispatchToProps(Dispatch) {

    return {
    }
}



export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(CustomersAddModal)



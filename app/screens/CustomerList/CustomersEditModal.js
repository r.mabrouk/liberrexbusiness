import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import styles from './styles';
import { Button } from '../../components/button/button';
import { _Text } from '../../components/text/text';
import { Card } from '../../components/card/card';
import { images, Icons } from '../../config';
import { getHeight, Width, getWidth } from '../../components/utils/dimensions';
import { PopupModal } from '../../components/popupModal/popupModal';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { connect } from 'react-redux'
import { _string } from '../../local';


type Props = {
    duration: number,
    onAddToVip: Function,
    onAddToBlacklist: Function,
    onUpdate: Function,

}
class CustomersEditModal extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            phone_number: "",
            lbrx_id: -1,
            id: -1,
            index: -1,
            keyOfReducer: ""
        }
    }

    onAnimated = (state, customerData, index, keyOfReducer) => {
        if (customerData) {
            this.
                setState({
                    name: customerData.fname + " " + customerData.lname,
                    phone_number: customerData.phone_number,
                    email: customerData.email,
                    lbrx_id: customerData.lbrx_id,
                    id: customerData.id,
                    index, keyOfReducer
                }, () => {
                })
        }
        this.AddCustomer.onAnimated(state)

    }
    render() {
        let { name, email, phone_number, id, index, keyOfReducer } = this.state
        let { isUpdateCustomersLoading, isAddCustomersToVipLoading, isAddCustomersToblacklistLoading, onAddCustomerToBooks, onRemoveCustomerFromVip, isDeleteCustomerLoading, onDeleteCustomer,isVip } = this.props
        return (
            <PopupModal
            inputDuration={400}
            inputHeight={getHeight(50)}
            duration={400} zIndex={200} ref={(_AddCustomer) => this.AddCustomer = _AddCustomer} height={getHeight(520)}>
                <Card style={styles.popupContainer}>
                    <View style={styles.popupHeader}>
                        <Icons name='user' width={getWidth(30)} height={getWidth(30)} />
                        <_Text style={styles.popupHeaderText}>{_string("EditCustomer").toLocaleUpperCase()}</_Text>
                    </View>
                    <CustomTextInput
                    onSubmitEditing={()=>{
                        this.input2.focus()
                    }}
                    returnKeyType='next'
                        marginTop={getHeight(20)}
                        onChangeText={(text) => this.setState({ name: text })}
                        value={name}
                        headerIcon={'userGray'} headerText={_string('appointment.full_name')}
                        placeholder={_string("pleace_holder")} leftIcon='editpen' />

                    <CustomTextInput
                         onSubmitEditing={()=>{
                            this.input3.focus()
                        }}
                    ref={(input)=>this.input2=input}
                                        returnKeyType='next'

                        onChangeText={(text) => this.setState({ email: text })}

                        value={email}
                        headerIcon={'email'} headerText={_string("appointment.email")}
                        placeholder={_string("pleace_holder")} leftIcon='editpen' />

                    <CustomTextInput  
                         onSubmitEditing={()=>{
                            if (onRemoveCustomerFromVip) {
                                onAddCustomerToBooks(id, index, "isAddCustomersToblacklistLoading", 2, this.onAnimated, "vip_customers")
                            } else
                                onAddCustomerToBooks(id, index, "isAddCustomersToblacklistLoading", 2, this.onAnimated, "all_customers")
                        }}
                    ref={(input)=>this.input3=input}
                    returnKeyType='done'

                        value={phone_number}
                        onChangeText={(text) => this.setState({ phone_number: text })}

                        headerIcon={'phone'} headerText={_string("appointment.phone_number")}
                        placeholder={_string("pleace_holder")} leftIcon='editpen' />
                    <View style={{ flexDirection: 'row', width: Width, paddingHorizontal: getWidth(15), justifyContent: 'space-between' }}>
                        <Button
                            isLoading={isAddCustomersToblacklistLoading}
                            loaderColor={'darkGray1'}

                            iconName="cancel"
                            iconStyle={{ width: getWidth(18),height:getHeight(18) , color: 'red'}}
                            text={_string('Block')}
                            style={styles.popupaddbtnRemoveAndblock}
                            onPress={() => {
                                if (onRemoveCustomerFromVip) {
                                    onAddCustomerToBooks(id, index, "isAddCustomersToblacklistLoading", 2, this.onAnimated, "vip_customers")
                                } else
                                    onAddCustomerToBooks(id, index, "isAddCustomersToblacklistLoading", 2, this.onAnimated, "all_customers")
                                // this.AddCustomer.onAnimated()
                            }}
                        />


                        <Button
                            isLoading={isDeleteCustomerLoading}
                            loaderColor={'darkGray1'}
                            iconName="delete"
                            iconStyle={{ width: getWidth(18),height:getHeight(18)}}

                            text={_string('Delete')}
                            style={styles.popupaddbtnRemoveAndblock}
                            onPress={() => {
                                if (isVip) {
                                    onDeleteCustomer(id, index, "isDeleteCustomerLoading", this.onAnimated, "vip_customers")
                                } else
                                    onDeleteCustomer(id, index, "isDeleteCustomerLoading", this.onAnimated, "all_customers")
                                // this.AddCustomer.onAnimated()
                            }}
                        />
                    </View>


                    <Button
                        loaderColor={'darkGray1'}
                        isLoading={isAddCustomersToVipLoading}
                        iconName="star"
                        iconStyle={{ width: getWidth(18),height:getHeight(18) , color: 'orange'}}
                        text={onRemoveCustomerFromVip ? _string("RemoveCustomerFromVip") : _string('AddToVIP')}
                        style={styles.popupaddbtn}
                        onPress={() => {
                            if (onRemoveCustomerFromVip) {
                                onRemoveCustomerFromVip(id, index, "isAddCustomersToVipLoading", 1, this.onAnimated, "vip_customers")
                            }
                            else
                                onAddCustomerToBooks(id, index, "isAddCustomersToVipLoading", 1, this.onAnimated, "vip_customers")
                            // this.AddCustomer.onAnimated()
                        }}
                    />

                    <Button
                    iconStyle={{width:getWidth(18),height:getWidth(18)}}
                        isLoading={isUpdateCustomersLoading}
                        iconName="done"
                        text={_string('appointment.done')}
                        style={styles.popupDonebtn}
                        onPress={() => {
                            let { phone_number, lbrx_id, name, id, index } = this.state
                            let newCustomer = {
                                fname: name.split(" ")[0],
                                lname: name.split(" ")[1] ? name.split(" ")[1] : "",
                                phone_number,
                                lbrx_id,
                                id
                            }
                            this.props.onUpdate(newCustomer, this.onAnimated, index, keyOfReducer)
                            // this.AddCustomer.onAnimated()
                        }}
                    />


                </Card>




            </PopupModal>


        );
    }
}

function mapStateToProps(state) {
    return {
        isUpdateCustomersLoading: state.loadingReducer.isUpdateCustomersLoading,
        isAddCustomersToVipLoading: state.loadingReducer.isAddCustomersToVipLoading,
        isAddCustomersToblacklistLoading: state.loadingReducer.isAddCustomersToblacklistLoading

    }
}

export default connect(mapStateToProps, null, null, { forwardRef: true })(CustomersEditModal)


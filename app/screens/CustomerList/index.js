import React, { Component } from 'react';
import { View, Text } from 'react-native';
import CustomerListView from './CustomerListView';
import { connect } from 'react-redux'
import { updateCustomerRequest, createCustomerRequest, removeCustomerFromBookRequest, addCustomerToBooksRequest, deleteCustomerRequest } from '../../actions/customers';
import { validateEmail } from '../../utils/stringUtils';
import { _Toast, ModalToast } from '../../components/toast/toast';
import { _string } from '../../local';
class CustomerList extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  onUpdate = (new_customer, editCustomerModal, index, keyOfReducer) => {

    this.props.updateCustomer(new_customer, editCustomerModal, index, keyOfReducer)
  }

  onAddCustomerToBooks = (id, index, loader, books_id, onCloseModal, keyOfReducer) => {
    this.props.addCustomerToBooks(id, index, loader, books_id, onCloseModal, keyOfReducer)
  }
  onUnblock_customer = (id, index, loader, books_id, onCloseModal, keyOfReducer) => {
    this.props.onUnblock_and_remove_from_vip_customer(id, index, onCloseModal, books_id, loader, keyOfReducer)
  }
  onDeleteCustomer = (id, index, loader, onCloseModal, keyOfReducer) => {
    this.props._onDeleteCustomer(id, index, loader, onCloseModal, keyOfReducer)
  }


  validationForm = (fullname, email, phoneNumber) => {
    if (!fullname) {
      ModalToast.open(_string("PleaseEnterACustomerName"), "w")
      return false
    }
    else if (!email) {
      ModalToast.open(_string("PleaseEnterTheEmailAddress"), "w")
      return false
    }
    else if (!validateEmail(email)) {
      ModalToast.open(_string("PleaseEnterAValidEmail"), "w")
      return false
    }
    else if (!phoneNumber) {
      ModalToast.open(_string("PleaseEnterAPhoneNumber"), "w")
      return false
    }
    else
      return true
  }

  onAddCustomer = (customer_data, onCloseModal) => {

    if (this.validationForm(customer_data.fullname, customer_data.email, customer_data.phoneNumber)) {
      let data = {
        fname: customer_data.fullname.split(" ")[0],
        lname: customer_data.fullname.split(" ")[1] ? customer_data.fullname.split(" ")[1] : "",
        email: customer_data.email,
        lbrx_i: 1,
        phone_number: customer_data.phoneNumber

      }
      this.props.createNewCustomer(data, onCloseModal)
    }

  }
  render() {
    return (
      <CustomerListView {...this} {...this.state} {...this.props} />
    );
  }
}

function mapStateToProps(state) {
  return {
    isDeleteCustomerLoading: state.loadingReducer.isDeleteCustomerLoading,
    isUpdateCustomersLoading: state.loadingReducer.isUpdateCustomersLoading
  }
}
function mapDispatchToProps(Dispatch) {
  return {
    updateCustomer: (customer_data, editCustomerModal, index, keyOfReducer) => Dispatch(updateCustomerRequest(customer_data, editCustomerModal, index, keyOfReducer)),
    createNewCustomer: (customer_data, onCloseModal) => Dispatch(createCustomerRequest(customer_data, onCloseModal)),
    onUnblock_and_remove_from_vip_customer: (id, index, onCloseModal, books_id, loader, keyOfReducer) => Dispatch(removeCustomerFromBookRequest(id, index, onCloseModal, books_id, loader, keyOfReducer)),
    addCustomerToBooks: (id, index, loader, books_id, onCloseModal, keyOfReducer) => Dispatch(addCustomerToBooksRequest(id, index, loader, books_id, onCloseModal, keyOfReducer)),
    _onDeleteCustomer: (id, index, loader, onCloseModal, keyOfReducer) => Dispatch(deleteCustomerRequest(id, index, loader, onCloseModal, keyOfReducer))


  }
}


export default connect(mapStateToProps, mapDispatchToProps)(CustomerList)

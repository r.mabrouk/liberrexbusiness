import { getHeight, Width, Height, getWidth } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        width: Width,
        height: Height,

        backgroundColor: ColorsList.white
    },
    logo: {
        width: Width * .8,
        height: Height * .2,
        resizeMode: 'contain'
    },

    selectedbtn: {
        width: Width,
        height: getHeight(42),
        backgroundColor: 'black',
        marginTop: getHeight(50),
    },

    headerSection: {
        backgroundColor: 'white', height: getHeight(67),
        width: Width,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: Width * .05,
        shadow: 5,
        paddingTop: getHeight(13)
    },

    search: {
        width: getWidth(345),
        height: getHeight(41),
        shadow: 6, backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: getHeight(15),
        borderRadius: 3
    },
    memberCard: {
        marginTop: getHeight(15),
        width: Width,
        flexDirection: 'row',
        height: getHeight(58),
        shadow: 6,
        backgroundColor: 'white',
        justifyContent: 'space-between',
        paddingLeft: getWidth(15)
    },
    TeamMemberInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: getWidth(10)
    },
    memberName: {
        fontSize: 'medium15Px',
        fontFamily: 'bold',
        color: 'darkGray1'
    },
    memberPosition: {
        fontSize: 'size12',
        marginLeft: getWidth(5),
        color: 'lightGray1',
        fontFamily: 'medium',
        width:getWidth(130)
    },
    popupHeader: {
        flexDirection: 'row', width: getWidth(345),
        justifyContent: 'center', alignItems: 'center',
        marginTop:getHeight(20)
    },
    popupDonebtnAdd:{
        colors: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(345),
        height: getHeight(44),
        borderRadius: 3,
        textColor: 'white',
        fontSize: 'medium15Px',
        marginTop: getHeight(5),
        fontFamily:'bold'
    },
    popupDonebtn: {
     
       colors: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(345),
        height: getHeight(44),
        borderRadius: 3,
        textColor: 'white',
        fontSize: 'size15',
        marginTop: getHeight(15),
        fontFamily:'bold'


    },
    popupContainer: {

        height: '100%', width: '100%',
        alignItems: 'center'


    },
    popupHeaderText: {

        marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily:'bold',
    },

    popupaddbtnRemoveAndblock:{
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(165),
        height: getHeight(44),
        borderRadius: 3,
        textColor: 'black',
        fontSize: 'medium15Px',
        marginTop: getHeight(15),
        shadow: 3,
        fontFamily:'bold'
    },
    popupaddbtn: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(345),
        height: getHeight(44),
        borderRadius: 3,
        textColor: 'black',
        fontSize: 'medium15Px',
        marginTop: getHeight(15),
        shadow: 3,
        fontFamily:'bold'


    }




}

export default styles
import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text, FlatList
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Button } from '../../components/button/button';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { images, metrics, Icons } from '../../config';
import MapView, { Marker, Callout } from 'react-native-maps';

import ImagePicker from 'react-native-image-picker';
import HeaderButtons from '../../components/HeaderButtons/HeaderButtons'
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { ScrollView } from 'react-native-gesture-handler';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import { Input } from '../../components/input/input';
import SwiperTabButtons from '../../components/swiperTabButtons/swiperTabButtons';
import SignUpModal from '../SignUpBusiness/signUpModal';
import { _string } from '../../local';
import ButtonWithHeaderSection from '../../components/buttonWithHeaderSection/buttonWithHeaderSection';
import { ColorsList } from '../../components/PropsStyles';

import IndustrtyModal from '../SignUpBusiness/industrtyModal';
import MapMarkerWithAnimation from '../../components/mapMarkerWithAnimation/mapMarkerWithAnimation';
import TimeCard from '../../components/TimeCard/TimeCard';
import TimeModal from '../NewAppointment/TimeModal';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { _Toast } from '../../components/toast/toast';

const options = {
    title: 'Select Image',
    storageOptions: {
        skipBackup: true,
        path: 'images',
        country: "country",
        city: "city",
        state: "state",
        countrySelectedIndex: -1,
        citySelectedIndex: -1,
        postal_codeSelectedIndex: -1,
        currentModal: "",

        location: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        }

    },
};



export default class EditBusinessView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            info: false,
            location: true,
            currentSelected: 0,
            countrySelectedIndex: -1,
            citySelectedIndex: -1,
            postal_codeSelectedIndex: -1,
            currentModal: "",
        };
    }

    render() {
        let {
            profileImage,
            address,
            city,
            country,
            postal_code,
            industry, onUpdateBusinessInfo,
            onChangeText,
            legalName, onSelectCountryAndCity,
            handleImagePickedProfile, currentModal,
            onSelectIndustry, industryIdSelected, industryd_data,
            onFilter,
            filter,
            countries,
            onSelectLocation,
            onNext,
            places,
            onChangeTextLocation,
            isSearchLocation,
            places_name, location,
            onMarkerChange,
            loadingUpdateBusinessInfo,
            loadingUpdateBusinessLocation,
            loadingUpdateBusinessWorkingDayes,
            onUpdateBusinessLocation,
            onUpdateBusinessWorkingDayes,
            onSetMarker,
            mapLocation

        } = this.props

        let { } = this.props

        return (
            <View
                style={styles.container}>

                <Header style={{ width: '100%', zIndex: 15 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />

                <HeaderSection
                    style={{
                        shadow: 6,
                        zIndex: 14,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}

                    text={_string("edit_business.edit_your_business")}
                    iconName='userGray'
                    iconStyle={{ width: getWidth(18), height: getHeight(18) }}
                />


                <View style={{
                    alignItems: 'center',
                    width: Width.width
                }}>
                    <SwiperTabButtons
                        buttonWidth={getWidth(160)}
                        tabButons={[
                            { iconsName: 'info', label: _string("edit_business.business_info") },
                            { iconsName: 'location', label: _string("edit_business.working_days") },
                            { iconsName: 'location', label: _string("edit_business.business_location") }]}>

                        {/* {this.state.currentSelected === 0 &&  */}
                        {/* } */}
                        <KeyboardAwareScrollView style={{
                            height: getHeight(435)
                        }} contentContainerStyle={{
                            alignItems: 'center', width: metrics.screenWidth, paddingBottom: getHeight(20)
                        }} >


                            <Card
                                style={styles.profileImgContainer} >
                                <TouchableOpacity
                                    onPress={handleImagePickedProfile}
                                >
                                    <Card style={styles.imgProfileContainer}>
                                        <Image source={!profileImage ? images.company_profile : { uri: profileImage }}
                                            style={{ width: getHeight(64), height: getHeight(64) }}
                                            resizeMode="cover" />
                                    </Card>
                                </TouchableOpacity>
                                <_Text style={{
                                    fontSize: 'size13',
                                    marginTop: getHeight(10),
                                    fontFamily: 'medium',
                                    color: ColorsList.rgbaBlack(.54)
                                }}>
                                    {_string("sign_up.upload_your_business_logo_or_photo")}
                                </_Text>
                            </Card>

                            <CustomTextInput
                                onChangeText={(text) => onChangeText(text, "legalName")}
                                value={legalName} marginTop={getHeight(15)} placeholder='Oncloud'
                                leftIcon={'editpen'}
                                headerText={_string("edit_business.legal_name")}
                                headerIcon={'info'} />

                            <ButtonWithHeaderSection
                                onPress={() => {
                                    this.setState({ currentModal: 'postal_code' }, () => {
                                        this.industrtyModal.onAnimated(true)
                                    })
                                }}
                                value={industry}
                                headerText={_string("edit_business.industry")}
                            />
                            {/* <CustomTextInput placeholder={_string("pleace_holder")} leftIcon={'editpen'}
                                headerText={_string("edit_business.industry")} headerIcon={'grid'} /> */}


                            <CustomTextInput
                                onChangeText={(text) => onChangeText(text, "address")}
                                value={address}
                                placeholder='Address line 1' leftIcon={'editpen'}
                                headerText={_string("edit_business.business_address")} headerIcon={'location'} />
                            <CustomTextInput
                                onChangeText={(text) => onChangeText(text, "postal_code")}
                                value={postal_code} placeholder={_string("sign_up.postal_code")} leftIcon={'editpen'}
                                headerText={_string("sign_up.postal_code")} headerIcon={'location'} />

                            {/* DropDawn */}
                            <View
                                style={styles.ButtonsContainer}>
                                <Button
                                    onPress={() => {

                                        this.setState({ currentModal: 'country' }, () => {
                                            this.signUpModel.onAnimated(true)
                                        })

                                    }}
                                    iconStyle={{ width: getWidth(15), height: getHeight(15), color: '#000' }}

                                    iconName='arrowDown'
                                    text={country}
                                    style={styles.socailMediaButtons}
                                />
                                <Button
                                    onPress={() => {
                                        this.setState({ currentModal: 'city' }, () => {
                                            if (country == "Country")
                                                return _Toast.open(_string("PleaceSelectCountryThenSelectCity"), 'w')
                                            this.signUpModel.onAnimated(true)
                                        })
                                    }}
                                    iconStyle={{ width: getWidth(15), height: getHeight(15), color: '#000' }}
                                    iconName='arrowDown'
                                    text={city}
                                    style={styles.socailMediaButtons}
                                />
                            </View>
                            <Button
                                isLoading={loadingUpdateBusinessInfo}
                                onPress={onUpdateBusinessInfo}
                                iconStyle={{ width: getWidth(18), height: getHeight(18), color: '#ffffff', marginBottom: getHeight(5) }}
                                text={_string("edit_business.save")}
                                iconName='done'
                                style={styles.saveInfoButton}
                            />

                        </KeyboardAwareScrollView>
                        {/* } */}

                        <View style={{ width: Width, alignItems: 'center', marginTop: getHeight(10) }} >

                            {this.props.selectedTimes.map((item, index) => {
                                return (
                                    <View key={`option${index}`} >
                                        <TimeCard
                                            name={item.day}
                                            onDayPress={(status) => { this.props.onDayPress(index, status) }}
                                            onPress={() => {
                                                this.AddTime.onAnimated(true)
                                                this.props.onSetTime(index)
                                                this.setState({ selectedDay: index })
                                            }}
                                            isSelected={item.active}
                                            from={this.props.formatedTime(this.props.selectedTimes[index].start_time)}
                                            to={this.props.formatedTime(this.props.selectedTimes[index].end_time)}
                                        />
                                    </View>
                                )
                            })}

                            <Button
                                isLoading={loadingUpdateBusinessWorkingDayes}
                                onPress={onUpdateBusinessWorkingDayes}
                                iconStyle={{ color: '#ffffff', size: 25, marginTop: getHeight(10), }}
                                text={_string("edit_business.save")}
                                iconName='done'
                                style={styles.nextButton}
                            />
                        </View>


                        {/* {this.state.currentSelected === 1 &&  */}


                        <View style={{ width: Width }}>


                            <View style={{
                                zIndex: 30, position: 'absolute', left: getWidth(15), marginTop: getHeight(10)
                            }}>

                                <Input
                                    value={places_name}
                                    onChangeText={onChangeTextLocation}
                                    style={styles.input}
                                    iconName={'search'}
                                    placeholder={_string("select_location.search_for_nearby_places")}
                                    iconStyle={{ width: getWidth(15), height: getHeight(15) }}

                                    verticalLine={false}


                                />
                                {isSearchLocation && <Card style={{ width: getWidth(345), maxHeight: getHeight(200), backgroundColor: ColorsList.rgbaWhite(.9), position: 'absolute', zIndex: 3, shadow: 6, borderRadius: 4, top: getHeight(45) }}>
                                    <FlatList extraData={this.props} data={places} renderItem={({ item, index }) => {
                                        return (
                                            <Button onPress={() => {
                                                let newPlace = {
                                                    latitude: item.geometry.location.lat,
                                                    longitude: item.geometry.location.lng,
                                                    latitudeDelta: 0.00122,
                                                    longitudeDelta: 0.00421
                                                }
                                                onSelectLocation(item.formatted_address, newPlace)
                                                this.setState({ location: newPlace })
                                            }}
                                                style={{
                                                    width: '100%',
                                                    height: getHeight(40),
                                                    borderBottomWidth: 1,
                                                    borderColor: ColorsList.rgbaGray(.04),
                                                    justifyContent: 'flex-start',
                                                    paddingHorizontal: getWidth(10)
                                                }}>
                                                <Icons name='location' width={getWidth(15)} height={getWidth(15)} />
                                                <_Text style={{ fontFamily: 'regular', fontSize: 'small', paddingHorizontal: getWidth(5) }}>{item.formatted_address}</_Text>
                                            </Button>
                                        )
                                    }} />

                                </Card>}
                            </View>

                            <Card style={styles.mapContainer} >
                                {/* <Image source={require("../../assets/img/map.jpg")}
                                    style={styles.mapimg} /> */}
                                <MapView
                                    // onCalloutPress={()=>{
                                    // }}
                                    onPress={(e) => {
                                        onSetMarker(
                                            e.nativeEvent.coordinate.latitude,
                                            e.nativeEvent.coordinate.longitude)
                                    }}

                                    style={{ width: '100%', height: '100%', backgroundColor: 'red' }}
                                    initialRegion={mapLocation}
                                    region={location}
                                >
                                    <Marker
                                        coordinate={mapLocation}  >
                                        <MapMarkerWithAnimation />
                                    </Marker>


                                    <Marker
                                        draggable
                                        onDragEnd={
                                            onMarkerChange
                                        }
                                        coordinate={location}  >
                                    </Marker>
                                </MapView>
                            </Card>

                            <Button
                                isLoading={loadingUpdateBusinessLocation}
                                iconStyle={{ width: getWidth(18), height: getHeight(18), color: '#ffffff' }}
                                text={_string("edit_business.save")}
                                iconName='done'
                                style={styles.nextButton}
                                onPress={onUpdateBusinessLocation} />


                        </View>

                    </SwiperTabButtons>
                </View>

                <IndustrtyModal
                    onFilter={onFilter}
                    filter={filter}
                    onSelectIndustry={onSelectIndustry}
                    selectItem={industryIdSelected}
                    data={industryd_data}
                    ref={(industrty) => this.industrtyModal = industrty} />
                <TimeModal
                          title={_string("WorkingDaysTime")}
                          startDate={this.props.startDate}
                          endDate={this.props.endDate}
                    ref={(time) => { this.AddTime = time }}
                    onDonePress={(newTimes) => {
                        this.props.onTimeChange(this.state.selectedDay, newTimes)
                    }}
                />
                <SignUpModal

                    selected_country={country}
                    onSelect={onSelectCountryAndCity}
                    data={countries}
                    currentModal={this.state.currentModal}
                    ref={(signUpModel) => this.signUpModel = signUpModel} />
            </View>
        );
    }
}


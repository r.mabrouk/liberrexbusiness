import React, { Component } from 'react';
import { View, Text } from 'react-native';
import EditBusinessView from './EditBusinessView'
import { searchIndustryRequest } from '../../actions/auth';
import ImagePicker from 'react-native-image-picker';
import { compressImages } from '../../utils/compressImages';
import Geolocation from '@react-native-community/geolocation';

import { connect } from 'react-redux'
import { _string } from '../../local';
import ApiConstants from '../../api/ApiConstants';
import { getPlacesRequest } from '../../actions/googleMap';
import { updateUserAccountRequest, updateBusinessInfoRequest, updateBusinessLocationRequest, updateBusinessWorkingDayesRequest } from '../../actions/userAccount';
import moment from 'moment';
import { _Toast } from '../../components/toast/toast';
import { images } from '../../config';
class EditBusiness extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileImage:props.businessInfo.photo? (ApiConstants.IMAGE_BASE_URL + props.businessInfo.photo):null,
      legalName: props.businessInfo.name,
      industry: props.businessInfo.industry,
      address: props.businessInfo.address,
      country: props.businessInfo.country,
      city: props.businessInfo.city,
      image_file: compressImages(ApiConstants.IMAGE_BASE_URL + props.businessInfo.photo),
      postal_code: props.businessInfo.postal_code,
      countrySelectedIndex: -1,
      citySelectedIndex: -1,
      currentModal: "",
      industryIdSelected: 1,
      places_name: '',
      location: {
        latitude: props.businessInfo.lat,
        longitude: props.businessInfo.lng,
        latitudeDelta: 0.00322,
        longitudeDelta: 0.00721,
      },
      mapLocation: {
        latitude: props.businessInfo.lat,
        longitude: props.businessInfo.lng,
        latitudeDelta: 0.00322,
        longitudeDelta: 0.00721,
      },
      isSearchLocation: false,

      selectedTimes: props.working_days.length >= 1 ? props.working_days : [
        { "day":_string("dayes.Saturday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Sunday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Monday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Tuesday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day":_string("dayes.Wednesday") , "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Thursday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day":_string("dayes.Friday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 }
      ],
      startDate:new Date(moment().format("YYYY-MM-DDT09:00:00")),
      endDate:new Date(moment().format("YYYY-MM-DDT09:00:00")),
    };
  }


  onUpdateBusinessLocation = () => {
    let { location } = this.state
    let data = {
      lat: location.latitude,
      lng: location.longitude
    }
    this.props.updateBusinessLocation(data)
  }

  onUpdateBusinessInfo = () => {
    let {
      image_file,
      legalName,
      industry,
      address,
      country, city,
      postal_code
    } = this.state


    if (this.validationForm()) {

      let user_data = new FormData()
      user_data.append("photo", image_file)
      user_data.append("name", legalName)
      user_data.append("address", address)
      user_data.append("postal_code", postal_code)
      user_data.append("country", country)
      user_data.append("city", city)
      user_data.append("industry", industry)

      this.props.updateBusinessInfo(user_data)

    }


  }
  formatedTime = (time) => {
    return moment(time, ["HH:mm:ss"]).format('HH:mm').toString()
  }

  // ----------- update the selected days -------------
  onSetTime=(dayOfWeek)=>{
    let selectedTimesCopy = [...this.state.selectedTimes];
    let startDate = new Date(moment().format("YYYY-MM-DDT" + selectedTimesCopy[dayOfWeek].start_time))
    let endDate = new Date(moment().format("YYYY-MM-DDT" + selectedTimesCopy[dayOfWeek].end_time))
    this.setState({ selectedTimes: selectedTimesCopy, startDate, endDate })
  }
  onDayPress = (dayOfWeek, status) => {
    let selectedTimesCopy = [...this.state.selectedTimes];
    selectedTimesCopy[dayOfWeek].active = status ? 1 : 0
    let startDate = new Date(moment().format("YYYY-MM-DDT"+ selectedTimesCopy[dayOfWeek].start_time))
    let endDate = new Date(moment().format("YYYY-MM-DDT" + selectedTimesCopy[dayOfWeek].end_time))
    console.log(selectedTimesCopy[dayOfWeek].start_time,"dfkdngkdjghdkjghfdgdfgfdgdfgfdg")
    this.setState({ selectedTimes: selectedTimesCopy, startDate, endDate })
  }
  // ------------ change the time of a specific dat -----------------
  onTimeChange = (dayOfWeek, newTimes) => {
    console.log('dayOfWeek', dayOfWeek, ' newTimes', newTimes)
    let start_time = moment(newTimes.from).format('HH:mm:00')
    let end_time = moment(newTimes.to).format('HH:mm:00')
    let selectedTimesCopy = [...this.state.selectedTimes];
    // selectedTimesCopy[dayOfWeek].status = "enabled"
    selectedTimesCopy[dayOfWeek].start_time = start_time
    selectedTimesCopy[dayOfWeek].end_time = end_time
    this.setState({ selectedTimes: selectedTimesCopy })
    console.log('selectedTimesCopy', selectedTimesCopy)
  }

  componentWillMount() {
    this.getMyLocation()
  }
  onChangeText = (text, fieldName) => {
    this.setState({ [fieldName]: text })
  }


  getMyLocation=()=>{
    Geolocation.getCurrentPosition((location)=>{

      let _location={
        latitude:location.coords.latitude,
        longitude: location.coords.longitude,
        latitudeDelta: 0.00322,
        longitudeDelta: 0.00721,
      }
        this.setState({
            location:_location,
            mapLocation:_location
        })
console.log(location,"locationlocation")
    },
     error => {

      console.log(error,"locationlocation")

    }, { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 });

}


  validationForm = () => {
    let {
      address,
      postal_code,
      city,
      country,
      image_file,
      industry,
      legalName,
    } = this.state

    if (!legalName) {
      _Toast.open(_string("messages_alert.please_enter_the_legal_name"), 'w')
      return false
    }
    else if (!image_file) {
      _Toast.open(_string("messages_alert.please_choose_your_image_profile"), 'w')
      return false
    }
    else if (industry == 'Industry') {
      _Toast.open(_string("messages_alert.please_choose_industry"), 'w')
      return false
    }
    else if (!address) {
      _Toast.open(_string("messages_alert.please_enter_the_address"), 'w')
      return false
    }
    else if (!postal_code) {
      _Toast.open(_string("messages_alert.please_enter_the_postal_code"), 'w')
      return false
    }
    else if (country == "Country") {
      _Toast.open(_string("messages_alert.please_chose_your_country"), 'w')
      return false
    }
    else if (city == "City") {
      _Toast.open(_string("messages_alert.please_chose_your_city"), 'w')
      return false
    }
    else return true

  }
  handleImagePickedProfile = () => {

    ImagePicker.showImagePicker({}, async (response) => {
      //   console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let image = await compressImages(response.uri)
        this.setState({ profileImage: image.uri, image_file: image });
      }
    });
  }
  onSelectCountryAndCity = (keyState, value) => {
    if (keyState == 'country')
      this.setState({ city: 'City', citySelectedIndex: -1 })

    this.setState({ [keyState]: value })
  }
  onSelectIndustry = (id, name) => {
    if (this.state.industryIdSelected == id)
      this.setState({ industryIdSelected: -1 })
    else this.setState({ industryIdSelected: id, industry: name })

  }
  onSaveSelectedIndustry = () => {

  }
  onFilter = (keyword) => {
    this.props.onFilter(keyword)
  }





  onUpdateBusinessWorkingDayes = () => {
    let data = { working_days: JSON.stringify(this.state.selectedTimes) }
    this.props.updateBusinessWorkingDayes(data)
  }



  onChangeTextLocation = (places_name) => {
    this.setState({ isSearchLocation: true, places_name: places_name })
    this.props.getPlaces(places_name)


  }
  onSelectLocation = (places_name, location) => {
    this.setState({ places_name, location }, () => {
      this.setState({ isSearchLocation: false })
    })
  }

  onMarkerChange = (e) => {
    console.log(e.nativeEvent.coordinate)
    this.setState({ location: e.nativeEvent.coordinate })
  }
  getlocation = () => {
    navigator.geolocation.getCurrentPosition((position) => {
      let location = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
        latitudeDelta: 0.00322,
        longitudeDelta: 0.00721,
      }

      this.setState({ location })
    }, error => {
    }, { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 });
  }
  onSetMarker=(lat,lon)=>{
    this.setState({location:{
      latitude:lat,
      longitude: lon,
      latitudeDelta: 0.00322,
      longitudeDelta: 0.00721,
    }})
    }


  render() {
    return (
      <EditBusinessView {...this.state} {...this} {...this.props} />
    );
  }
}


function mapStateToProps(state) {

  return {
    industryies: state.authReducer.industryies,
    countries: state.authReducer.countries,
    businessInfo: state.authReducer.user_profile.business,
    working_days: state.authReducer.user_profile.working_days,
    places: state.googleMapReducer.places,
    loadingUpdateBusinessInfo: state.loadingReducer.loadingUpdateBusinessInfo,
    loadingUpdateBusinessLocation: state.loadingReducer.loadingUpdateBusinessLocation,
    loadingUpdateBusinessWorkingDayes: state.loadingReducer.loadingUpdateBusinessWorkingDayes,
  }
}

function mapDispatchToProps(Dispatch) {

  return {
    onFilter: (keyword) => Dispatch(searchIndustryRequest(keyword)),
    getPlaces: (place_name) => Dispatch(getPlacesRequest(place_name)),
    updateBusinessInfo: (data) => Dispatch(updateBusinessInfoRequest(data)),
    updateBusinessLocation: (data) => Dispatch(updateBusinessLocationRequest(data)),
    updateBusinessWorkingDayes: (data) => Dispatch(updateBusinessWorkingDayesRequest(data)),


  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditBusiness)


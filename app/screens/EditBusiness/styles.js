import { StyleSheet } from 'react-native';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        width: Width,
        height: Height,
        backgroundColor: ColorsList.white
    },
    logo: {
        width: Width * .8,
        height: Height * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: Width,
        height: '100%',
        alignItems: 'center'
    },
    headerSectionContainer: {

        backgroundColor: 'white',
        shadow: 6,
        width: Width,
        paddingLeft: getWidth(20),
        justifyContent: 'center',
        marginTop: -getHeight(10),

    },

    ButtonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: getHeight(10),
        width: getWidth(345),

    },
    socailMediaButtons: {
        borderRadius: 3,
        width: getWidth(165),
        textColor: ColorsList.rgbaBlack(.54),
        shadow: 6,
        justifyContent: 'space-between',
        paddingHorizontal: getWidth(15),
        backgroundColor: '#fff',
        flexDirection: 'row-reverse',
        height: getHeight(44),
        fontSize: 'size13',
        fontFamily: 'medium'


    },

    imgProfileContainer: {
        width: getWidth(64),
        height: getWidth(64),
        borderRadius: getWidth(32),
        backgroundColor: 'white',
        shadow: 6,
        justifyContent: 'center',
        alignItems: 'center'
    },
    saveInfoButton: {

        width: getWidth(345),
        marginTop: getHeight(15),
        textColor: '#fff',
        shadow: 6,
        height: getHeight(44),
        colors: 'blue',
        borderRadius: 3,
        fontFamily:'bold',
        fontSize:'size15',

    },
    nextButton: {

        width: getWidth(345),
        marginTop: getHeight(15),
        textColor: '#fff',
        shadow: 6,
        height: getHeight(44),
        colors: 'blue',
        borderRadius: 3,
        fontFamily:'bold',
        fontSize:'size15',
        position:'absolute',top:getHeight(350),
        left:getWidth(15)

    },
    input: {
        borderRadius: 3,
        width: getWidth(345),
        height: getHeight(41),
        shadow: 6,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: getHeight(6),
        fontSize:'size13',
        fontFamily:'bold'
    },
    profileImgContainer: {
        width: getWidth(345),
        height: getHeight(116),
        backgroundColor: 'white',
        shadow: 5,
        borderRadius: 3,
        marginTop: getHeight(10),
        alignItems: 'center',
        justifyContent: 'center'
    },
    mapimg: {
        borderRadius: 3,
        width: '100%',
        height: getHeight(315),

    },
    mapContainer:
    {

        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        shadow: 6,
        borderRadius: 3,
        height: '100%'

    },
    search: {

        width: Width * .92,
        height: getHeight(41),
        shadow: 5, backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: getHeight(20),
        borderRadius: 3

    }

};
export default styles;

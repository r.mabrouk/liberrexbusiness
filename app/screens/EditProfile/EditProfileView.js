import React, { Component } from 'react';
import {
    View,
    Image, TouchableOpacity
} from 'react-native';
import styles from './styles';
import { Button } from '../../components/button/button';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { metrics, images } from '../../config';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { ScrollView } from 'react-native-gesture-handler';
import { getHeight, getWidth, Width } from '../../components/utils/dimensions';
import SwiperTabButtons from '../../components/swiperTabButtons/swiperTabButtons';
import { _string } from '../../local';
import { ColorsList } from '../../components/PropsStyles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


export default class EditProfileView extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }




    render() {

        let { role_business,
            phone,
            email,
            fname,
            lname,
            profileImage,
            onChangeText,
            onChangeImageProfile,
            oldPassowrd,
            newPassword,
            retypePassword,
            onChangePassword,
            onChangeProfileInfo,
            loadingChangeProflie,
            loadingChangePassword
        } = this.props
        return (
            <View
                style={styles.container}>

                <Header style={{ width: Width, zIndex: 15 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />


                <HeaderSection

                    style={{
                        shadow: 6,
                        zIndex: 14,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}

                    text={_string("edit_profile.edit_your_profile")}
                    iconName='userGray'
                // iconStyle={{ width: getWidth(20),height:getHeight(20)}}

                />


                <View style={{
                    alignItems: 'center',
                    width: Width.width
                }}>
                    <SwiperTabButtons tabButons={[
                        { iconsName: 'info', label: _string("edit_profile.personal_info") },
                        { iconsName: 'password', label: _string("edit_profile.password") }]}>

                        <KeyboardAwareScrollView style={{ height: getHeight(420) }} contentContainerStyle={{
                            alignItems: 'center', width: metrics.screenWidth
                        }}>

                            <View style={{ width: Width, justifyContent: 'center', alignItems: 'center' }}>
                                <Card
                                    style={styles.profileImg} >
                                    <TouchableOpacity
                                        onPress={onChangeImageProfile}
                                    >
                                        <Card style={styles.imgProfileContainer}>
                                            <Image source={!profileImage ? images.user_profile : { uri: profileImage }} style={{ width: '100%', height: '100%' }} resizeMode="cover" />
                                        </Card>
                                    </TouchableOpacity>

                                    <_Text style={{ fontSize: 'size13', marginTop: getHeight(10), fontFamily: 'medium', color: ColorsList.rgbaBlack(.54) }}>{_string("edit_profile.change_your_profile_photo")}</_Text>
                                </Card>

                                <CustomTextInput marginTop={getHeight(20)} placeholder={_string("pleace_holder")} onChangeText={(text) => onChangeText(text, 'fname')} value={fname} leftIcon={'editpen'}
                                    headerText={_string("sign_up.first_name")} headerIcon={'userGray'} />
                                <CustomTextInput marginTop={getHeight(5)} placeholder={_string("pleace_holder")} value={lname} onChangeText={(text) => onChangeText(text, 'lname')} leftIcon={'editpen'}
                                    headerText={_string("sign_up.last_name")} headerIcon={'userGray'} />

                                <CustomTextInput marginTop={getHeight(5)} placeholder={_string("pleace_holder")} value={email} onChangeText={(text) => onChangeText(text, 'email')} leftIcon={'editpen'}
                                    headerText={_string("edit_profile.email")} headerIcon={'email'} />

                                <CustomTextInput
                                    showonly

                                    marginTop={getHeight(5)} placeholder={_string("pleace_holder")} value={phone} onChangeText={(text) => onChangeText(text, 'phone')} leftIcon={'editpen'}
                                    headerText={_string("edit_profile.phone_number")} headerIcon={'phone'} />

                                <CustomTextInput marginTop={getHeight(5)} placeholder={_string("pleace_holder")} value={role_business} onChangeText={(text) => onChangeText(text, 'role_business')} leftIcon={'editpen'}
                                    headerText={_string("edit_profile.role_in_your_business")} headerIcon={'case'} />

                                <Button
                                    isLoading={loadingChangeProflie}
                                    onPress={onChangeProfileInfo}
                                    iconStyle={{ width: getWidth(20), height: getHeight(20), color: '#ffffff' }}
                                    text={_string("edit_profile.save")}
                                    iconName='done'
                                    style={styles.nextButton} />
                            </View>
                        </KeyboardAwareScrollView>



                        <KeyboardAwareScrollView contentContainerStyle={{
                            alignItems: 'center', width: metrics.screenWidth, height: getHeight(400)
                        }}>

                            <View style={{ width: Width, justifyContent: 'center', alignItems: 'center' }}>

                                <CustomTextInput 
                                
                                marginTop={getHeight(20)} password value={oldPassowrd} onChangeText={(text) => onChangeText(text, "oldPassowrd")} placeholder={_string("pleace_holder")} leftIcon={'eye'}
                                    headerText={_string("edit_profile.old_password")} headerIcon={'password'} />

                                <CustomTextInput marginTop={getHeight(5)} password value={newPassword} onChangeText={(text) => onChangeText(text, "newPassword")} placeholder={_string("pleace_holder")} leftIcon={'eye'}
                                    headerText={_string("edit_profile.new_password")} headerIcon={'password'} />

                                <CustomTextInput marginTop={getHeight(5)} password value={retypePassword} onChangeText={(text) => onChangeText(text, "retypePassword")} placeholder={_string("pleace_holder")} leftIcon={'eye'}
                                    headerText={_string("edit_profile.retype_new_password")} headerIcon={'password'} />

                                <Button
                                    isLoading={loadingChangePassword}
                                    onPress={onChangePassword}
                                    iconStyle={{ width: getWidth(18), height: getHeight(18), color: '#ffffff' }}
                                    text={_string("edit_profile.save")}
                                    iconName='done'
                                    style={styles.nextButton} />
                            </View>
                        </KeyboardAwareScrollView>


                    </SwiperTabButtons>
                </View>

            </View>
        );
    }
}


import React, { Component } from 'react';
import { View, Text } from 'react-native';
import EditProfileView from './EditProfileView'
import { connect } from 'react-redux'
import ApiConstants from '../../api/ApiConstants';
import { compressImages } from '../../utils/compressImages';
import ImagePicker from 'react-native-image-picker';
import { _string } from '../../local';
import { updateUserAccountRequest, updatePasswordRequest } from '../../actions/userAccount';
import { validateEmail } from '../../utils/stringUtils';
import { _Toast } from '../../components/toast/toast';

class EditProfile extends Component {
  constructor(props) {
    super(props);
    let { user_profile } = props
    this.state = {
      profileImage:user_profile.user.photo?( ApiConstants.IMAGE_BASE_URL + user_profile.user.photo):null,
      firstname: "",
      lastname: "",
      phoneNumber: '',
      roleInBusiness: "",
      image_file: "",
      role_business: user_profile.user.position,
      phone: user_profile.user.telephone,
      email: user_profile.user.email,
      fname: user_profile.user.fname,
      lname: user_profile.user.lname,
      oldPassowrd: "",
      newPassword: "",
      retypePassword: "",
    };
  }

  getImageFile = async () => {
    let { user_profile } = this.props
    let image_file = await compressImages(ApiConstants.IMAGE_BASE_URL + user_profile.user.photo)
    this.setState({ image_file })
  }
  componentDidMount = async () => {
    this.getImageFile()
  }



  onChangeImageProfile = () => {
    ImagePicker.showImagePicker({}, async (response) => {
      //   console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let image = await compressImages(response.uri)
        this.setState({ profileImage: image.uri, image_file: image });
      }
    });

  }
  passwordValidationForm = () => {
    let { newPassword, oldPassowrd, retypePassword } = this.state
    if (!oldPassowrd) {
      _Toast.open(_string("messages_alert.please_enter_your_old_password"), "w")
      return false
    }
    else if (!newPassword) {
      _Toast.open(_string("messages_alert.please_enter_your_new_password"), "w")
      return false
    }
    else if (!retypePassword) {
      _Toast.open(_string("messages_alert.please_enter_reType_password"), "w")
      return false
    }
    else if (newPassword != retypePassword) {
      _Toast.open(_string("messages_alert.retype_password_not_match_with_password"), "w")
      return false
    }
    else return true

  }

  validationForm = () => {
    let {
      profileImage,
      fname, lname,
      phone,
      role_business, email
    } = this.state
    if (!fname) {
      _Toast.open(_string("messages_alert.Please_enter_the_first_name"), 'w')
      return false
    }
    if (!lname) {
      _Toast.open(_string("messages_alert.Please_enter_the_last_name"), 'w')
      return false
    }
    else if (!validateEmail(email)) {
      _Toast.open(_string("messages_alert.please_enter_valid_email"), "w")
      return false
    }
    else if (!profileImage) {
      _Toast.open(_string("messages_alert.please_choose_your_image_profile"), 'w')
      return false
    }
    else if (!phone) {
      _Toast.open(_string("messages_alert.please_enter_phone_number"), 'w')
      return false
    }
    else if (!role_business) {
      _Toast.open(_string("messages_alert.please_enter_the_role_in_your_business"), 'w')
      return false
    }
    else return true

  }


  onChangeText = (text, key) => {
    this.setState({ [key]: text })
  }

  onChangePassword = () => {
    let {
      oldPassowrd, newPassword, retypePassword
    } = this.state

    let data = {
      oldPassowrd: oldPassowrd,
      newPassword: newPassword,
      retypePassword: retypePassword
    }
    if (this.passwordValidationForm()) {
      this.props.onUpdatePasword(data)
    }
  }
  onChangeProfileInfo = () => {
    if (this.validationForm()) {
      let {
        fname, lname,
        phone,
        role_business,
        email,
        image_file
      } = this.state

      let newProfile = new FormData()
      newProfile.append("fname", fname)
      newProfile.append("lname", lname)
      newProfile.append("email", email)
      newProfile.append("photo", image_file)
      newProfile.append("telephone", phone)
      newProfile.append("position", role_business)
      // newProfile.append("_method", "PUT")
      console.log("newProfilenewProfilenewProfilenewProfile", newProfile)
      this.props.onUpdateProfile(newProfile)

    }

  }

  render() {
    return (
      <EditProfileView {...this.state} {...this} {...this.props} />
    );
  }
}

function mapStateToProps(state) {
  return {
    user_profile: state.authReducer.user_profile,
    loadingChangeProflie: state.loadingReducer.loadingChangeProflie,
    loadingChangePassword: state.loadingReducer.loadingChangePassword
  };
}
function mapDispatchToProps(Dispatch) {
  return {
    onUpdateProfile: (data) => Dispatch(updateUserAccountRequest(data)),
    onUpdatePasword: (data) => Dispatch(updatePasswordRequest(data))


  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProfile);

import { StyleSheet } from 'react-native';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        width: Width,
        height: Height,
        backgroundColor: ColorsList.white
    },
    logo: {
        width: Width * .8,
        height: Height * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: Width,
        height: '100%',
        alignItems: 'center'
    },
    headerSectionContainer: {

        backgroundColor: 'white',
        shadow: 4,
        width: Width,
        paddingLeft: getWidth(20),
        justifyContent: 'center',
        marginTop: -getHeight(13)

    },
    profileImg:
    {
        width: getWidth(345),
        height: getHeight(116),
        backgroundColor: 'white',
        shadow: 6,
        borderRadius: 3,
        marginTop: getHeight(20),
        alignItems: 'center',
        justifyContent: 'center'
    },

    imgProfileContainer: {
        width: getWidth(64),
        height: getWidth(64),
        borderRadius: getWidth(32),
        backgroundColor: 'white',
        shadow: 6,
        justifyContent: 'center',
        alignItems: 'center'
    },
    nextButton: {

        width: getWidth(345),
        marginTop: getHeight(5),
        textColor: '#fff',
        shadow: 6,
        height: getHeight(44),
        colors: 'blue',
        borderRadius: 3,
        fontFamily:'bold',
        fontSize:'size15'

    },

};
export default styles;

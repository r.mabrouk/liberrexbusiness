import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text, ScrollView
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { images } from '../../config';
import { Input } from '../../components/input/input';
import { Button } from '../../components/button/button';
import { Card } from '../../components/card/card'
import { _Text } from '../../components/text/text';
import { getWidth, getHeight } from '../../components/utils/dimensions';
import { validateEmail } from '../../utils/stringUtils';
import { _string } from '../../local';
export default class ForgetPasswordView extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        let { forgotPasswordLoading, email, onChangeText, onforgotPassword } = this.props
        return (

            <View
                style={styles.container}>
                <Card style={{ width: '100%', height: '100%', }}>
                    <ImageBackground
                        resizeMode='cover'
                        source={images.loginbackground}
                        style={styles.imageBackground}>
                        <ScrollView scrollEnabled={false} contentContainerStyle={{ width: getWidth(375), alignItems: 'center',paddingBottom:getHeight(15) }}>

                            <Image
                                source={images.WhiteLogo}
                                style={styles.logo} />

                            <View style={{ alignItems: 'center', width: '100%' }}>

                                <Input
                                    autoFocus
                                    onSubmitEditing={onforgotPassword}
                                    returnKeyType='go'
                                    keyboardType='email-address'
                                    value={email}
                                    onChangeText={onChangeText}
                                    style={styles.input}
                                    iconName={'email'}
                                    placeholder={_string("forgot_password.your_email")}
                                    iconStyle={{ width: getWidth(15), height: getHeight(15) }}
                                    verticalLine={false}


                                />

                                <Button
                                    isLoading={forgotPasswordLoading}
                                    iconStyle={{ color: '#ffffff', width: getWidth(18), height: getHeight(18), marginBottom: getHeight(5), }}
                                    text={_string("forgot_password.send_reset_code")}
                                    iconName='send'
                                    style={styles.resetButton}
                                    onPress={onforgotPassword}

                                />

                                {/* <Text style={styles.text}>{"if you remember your password"}</Text> */}
                                <_Text style={styles.textStyle}>{_string("forgot_password.If_you_remember_your_password").toLocaleUpperCase()}</_Text>
                                <Button
                                    iconStyle={{ color: 'gray1', width: getWidth(18), height: getHeight(18), marginBottom: getHeight(5), }}
                                    text={_string("forgot_password.login")}
                                    iconName='login'
                                    style={styles.loginButton}
                                    onPress={() => { this.props.navigation.navigate('Login') }} />
                            </View>


                        </ScrollView>
                    </ImageBackground>

                </Card>
            </View>

        );
    }
}

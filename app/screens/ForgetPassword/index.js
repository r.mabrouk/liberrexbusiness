import React, { Component } from 'react';
import { View, Text ,Keyboard} from 'react-native';
import ForgetPasswordView from './ForgetPasswordView'
import { connect } from 'react-redux'
import { requestResetPassword } from '../../actions/auth';
import { validateEmail } from '../../utils/stringUtils';
import { _string } from '../../local';
import { _Toast } from '../../components/toast/toast';
class ForgetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: ""
    };
  }
  onChangeText = (text) => {
    this.setState({ email: text })
  }

  validationForm = () => {
    let { email } = this.state
    if (!email) {
      _Toast.open(_string("messages_alert.please_enter_your_email"), "w")
      return false
    }
    else if (!validateEmail(email)) {
      _Toast.open(_string("messages_alert.please_enter_valid_email"), "w")
      return false
    }
    else return true

  }
  onforgotPassword = () => {
    Keyboard.dismiss()
    let { email } = this.state
    if (this.validationForm()) {
      this.props.onForgotPassword({ email: email })
    }
    // this.props.navigation.navigate('NewPassword');
  }
  render() {
    return (
      <ForgetPasswordView {...this} {...this.state} {...this.props} />
    );
  }
}
function mapStateToProps(state) {
  return { forgotPasswordLoading: state.loadingReducer.forgotPasswordLoading }
}
function mapDispatchToProps(Dispatch) {
  return { onForgotPassword:(email)=>Dispatch(requestResetPassword(email)) }
}
export default connect(mapStateToProps,mapDispatchToProps)(ForgetPassword)

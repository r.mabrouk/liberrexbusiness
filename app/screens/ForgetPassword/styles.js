import { StyleSheet } from 'react-native';
import { getWidth, getHeight, Width, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: ColorsList.white
    },
    logo: {
        width: getWidth(250),
        height: getHeight(52),
        resizeMode: 'contain',
        marginTop: getHeight(100)
    },
    imageBackground: {
        width: getWidth(375),
        height: '100%',
        alignItems: 'center',


    },
    contentContainer: {
        marginTop: Height * .1,
        alignItems: 'center',

    },
    text: {
        textTransform: "uppercase",
        marginTop: getHeight(30),
        fontSize: 15

    },
    resetbtn: {
        width: '90%',
        marginTop: getHeight(25),
    },
    resetButton: {
        width: getWidth(345),
        marginTop: getHeight(15),
        textColor: '#fff',
        shadow: 6,
        height: getHeight(44),
        colors: 'blue',
        fontSize:'size15',
        borderRadius: 3,
        fontFamily: 'bold',
    },
    loginButton: {
        width: getWidth(345),
        marginTop: getHeight(10),
        textColor: ColorsList.rgbaGray(.7),
        fontFamily:'bold',
        shadow: 6,
        fontSize:'size15',
        height: getHeight(44),
        borderRadius: 3,
        backgroundColor: 'white',

    },
    input: {

        borderRadius: 3,
        width: getWidth(345),
        height: getHeight(41),
        shadow: 6, backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: getHeight(56),
        fontFamily:'medium',
        fontSize:'size13',

    },
    textStyle:
    {
        fontFamily: 'regular',
        fontSize: 'small1',
        marginTop: getHeight(30),
        fontFamily: 'regular',
        color: ColorsList.rgbaGray(.7)
    }

};

export default styles;

import React, { Component } from 'react';
import {
    View,
    StatusBar, Modal

} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { images, metrics, Icons } from '../../config';
import { RequestsStatusItem } from '../../components/requestsStatusItem/requestsStatusItem';
import { Header } from '../../components/header/header';
import { Card } from '../../components/card/card';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { getWidth, getHeight, Width } from '../../components/utils/dimensions';
import { ReportRequestItem } from '../../components/reportRequestItem/reportRequestItem';
import Swiper from 'react-native-swiper'
import HomeHeader from '../../components/HomeHeader/HomeHeader';
import { ScrollView } from 'react-native-gesture-handler';
import { isIOs } from '../../components/utils/functions';
import { ColorsList } from '../../components/PropsStyles';
import ReportByModal from './reportByModal';
import { Drower } from '../../components/sideBar';
import { connect } from 'react-redux'
import { _string } from '../../local';
import {
    LineChart,
    BarChart
} from 'react-native-chart-kit'
import Charts from '../../components/Charts/charts';
import { RFValue } from 'react-native-responsive-fontsize'
class LoginView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isopenDrower:!props.menuState
        };
    }
    async componentDidMount() {
        StatusBar.setHidden(true)
    }
componentWillReceiveProps=(props)=>{
    this.setState({isopenDrower:!props.menuState})
}

    render() {
        let { requestsList } = require("../../config/LocalDatabase")
        let { name, user_profile, homeStats, onFilter, filterDate, reports } = this.props
        let { user, business } = user_profile
        let { isopenDrower } = this.state
        return (
            <View
                style={styles.container}>
                <Header
                    onBack={() => {
                        this.setState({ isopenDrower: !isopenDrower }, () => {
                            Drower.onDrower(true)
                            this.props.navigation.navigate('Home')
                        })

                    }}
                    onNotification={() => {
                        this.props.navigation.navigate('Notification')
                    }}
                    leftIconStyle={{ width: getWidth(22), height: getHeight(22) }}
                    iconName={this.props.menuState ? 'closeIcon' : 'menuIcon'}
                    style={{ width: Width, zIndex: 3 }} />
                <HomeHeader
                    name={user.fname + " " + user.lname}
                    image={user.photo} />

                <View >

                    <Card style={{
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        shadow: 6,
                        backgroundColor: ColorsList.white
                    }}>

                        <Card style={{ width: '100%' }}>
                            <HeaderSection
                                onPress={() => {
                                    this.reportByModal.onAnimated(true)
                                }}
                                style={{
                                    width: Width,
                                    paddingHorizontal: getHeight(15),
                                    height: getHeight(70),
                                    justifyContent: 'space-between',
                                    flexDirection: 'row',
                                    alignItems: 'center'
                                }}
                                iconStyle={{ width: getWidth(12), height: getHeight(12), color: 'gray0' }}
                                buttonStyle={styles.btnHeader}
                                withButton
                                textButton={filterDate}
                                text={_string("home.requests_report")}

                                iconName='report'
                                iconButton='arrowDown'
                            />
                        </Card>
                        {/* Report Request */}
                        <View
                            style={{
                                width: '100%',
                                height: getHeight(195),
                                shadowColor: 'gray'
                            }}>
                            <Swiper
                                dotStyle={{ width: getWidth(10), height: getWidth(10), top: getHeight(15), borderRadius: getWidth(5) }}
                                activeDotStyle={{ width: getWidth(10), height: getWidth(10), top: getHeight(15), borderRadius: getWidth(5) }}
                                activeDotColor={'#A0A5AA'}
                                dotColor={'#E4E6E8'}
                                style={{
                                    width: !isIOs() ?
                                        metrics.screenWidth : undefined,
                                    alignItems: 'center'
                                }} >
                                {Object.keys(reports).map((key, index) => {

                                    if (reports[key].type == "linechart") {
                                        return (<Charts
                                            labels={reports[key].labels}
                                            data={reports[key].data}
                                            // title={key.replace("_", " ")}
                                            zIndex={Object.keys(reports).length - index}
                                            type='linechart' />)
                                    }
                                    else if (reports[key].type == "piechart") {
                                        return (<Charts
                                            labels={reports[key].labels}
                                            data={reports[key].data}
                                            // title={key.replace("_", " ")}
                                            zIndex={Object.keys(reports).length - index}
                                            type='piechart' />)
                                    }
                                    else if (reports[key].type == "star") {
                                        return (<Charts
                                            labels={reports[key].labels}
                                            data={reports[key].data}
                                            // title={key.replace("_", " ")}
                                            zIndex={Object.keys(reports).length - index}
                                            type='star' />)
                                    }
                                })}
                            </Swiper>
                        </View>
                    </Card>
                    <Card style={{
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <HeaderSection

                            style={{
                                width: getWidth(345),
                                height: getHeight(62),
                                flexDirection: 'row'
                            }}
                            iconSize={getWidth(28)}
                            iconStyle={{ width: getWidth(15), height: getHeight(15), color: 'gray0' }}
                            text={_string("home.your_requests").toLocaleUpperCase()}
                            iconName='requests'
                        />
                        <View style={{
                            width: Width,
                            alignItems: 'center',
                            paddingHorizontal: getWidth(15),
                            justifyContent: 'space-between',
                            flexDirection: 'row',
                            marginBottom: getHeight(20)
                        }}>
                            <RequestsStatusItem
                                titles={[_string("Pending"), _string("WatingList")]}
                                Colors={[ColorsList.orange, ColorsList.green]}
                                onPress={() => {
                                    this.props.navigation.navigate('QueueList')
                                }}
                                watingValue={homeStats.waiting}
                                pendingValue={homeStats.queue_requests}
                                iconName='queueIcon2'
                                status='Pending'
                                requestType={_string("home.queues")}
                                style={styles.RequestsStatusItem} />
                            <RequestsStatusItem
                                titles={[_string("Pending"), _string("Confirmed")]}

                                Colors={[ColorsList.orange, ColorsList.green]}
                                watingValue={homeStats.bookings}
                                pendingValue={homeStats.booking_requests}
                                onPress={() => {
                                    this.props.navigation.navigate('AppointmentsApproved')
                                }}

                                iconName='appointmentsIcon'
                                status='Pending'
                                requestType={_string("home.appointments")}
                                style={styles.RequestsStatusItem} />
                        </View>
                    </Card>
                </View>
                <ReportByModal
                    onFilter={onFilter}
                    ref={(_reportByModal) => this.reportByModal = _reportByModal} />

            </View>
        );
    }
}


LoginView.propTypes = {
    nextScreen: PropTypes.func
};
















function mapStateToProps(state) {

    return {
        isLoginLoading: state.loadingReducer.isLoginLoading,
        user_profile: state.authReducer.user_profile
    }
}

export default connect(mapStateToProps)(LoginView)
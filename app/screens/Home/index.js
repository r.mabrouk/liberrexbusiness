import React, { Component } from 'react';
import { View, } from 'react-native'
import HomeView from './HomeView'
import { connect } from 'react-redux'
import { NavigationEvents } from 'react-navigation'
import { filterHomeReportsRequest } from '../../actions/home';
import { _string } from '../../local';
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterDate:_string("modals.report_by.daily")
    };
  }
  onFilter = (keyword) => {
    this.setState({ filterDate: keyword.date })
    let data = {
      date: keyword
    }
    this.props._filterHomeReports(data)
  }
  render() {
    return (
      <View>
        <NavigationEvents onDidFocus={(payload) => {
          this.setState({filterDate:_string("modals.report_by.daily")})
        }}
        />
        <HomeView {...this} {...this.state} {...this.props} />

      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    isLoginLoading: state.loadingReducer.isLoginLoading,
    homeStats: state.homeReducer.homeStats,
    reports: state.reportReducer.reports,
    menuState: state.uiReducer.menuState


  }
}

function mapDispatchToProps(Dispatch) {
  return {
    _filterHomeReports: (keyword) => (Dispatch(filterHomeReportsRequest(keyword)))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Home)
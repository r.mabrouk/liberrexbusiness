import { StyleSheet } from 'react-native';
import { getWidth, getHeight, Width, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';
const styles = {
    container: {
        height: Height,
        width: Width,
        backgroundColor: ColorsList.white
    },
    logo: {
        width: Width * .8,
        height: Height * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: Width,
        backgroundColor: ColorsList.white
    },
    btnHeader:
    {
        backgroundColor: 'white', shadow: 6,
        width: getWidth(75), height: getHeight(31),
        borderRadius: 3, fontSize: 'size11',
        flexDirection: 'row-reverse',fontFamily:'medium',textColor:'darkGray1'
    },
    ReportsContainer: {
        width: '92%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    RequestsStatusItem: {
        width: getWidth(165),
        height: getHeight(110),
        shadow: 5,
        borderRadius: 3,
        marginTop:getHeight(3),
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    contentContainerModal: {
        width: getWidth(375),
        height: getHeight(50),
        // backgroundColor: ColorsList.white,
        borderBottomWidth: 1,
        borderColor: ColorsList.rgbaBlack(.05),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: getWidth(15)
    },
    modaleText: (currentSelected, index) => ({
        fontFamily: 'bold',
        color: currentSelected == index ? 'black' : 'gray0',
        fontSize: currentSelected == index ? 'size15' : 'size13'
    }),
    contentContainerStyleList: { alignItems: 'center' }


};

export default styles;

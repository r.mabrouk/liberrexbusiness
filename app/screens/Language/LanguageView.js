import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import styles from './styles';
// import PropTypes from 'prop-types';
import { images } from '../../config';
import { Button } from '../../components/button/button';
import { _string } from '../../local';
import { _Text } from '../../components/text/text';
import { ColorsList } from '../../components/PropsStyles';
import { getHeight, getWidth } from '../../components/utils/dimensions';


export default class LanguageView extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {

        let { onSelectLanguage } = this.props
        return (
            <View
                style={styles.container}>
                <ImageBackground
                    resizeMode='cover'
                    source={images.splash}
                    style={styles.imageBackground}>
                    <Image
                        source={images.logo}
                        style={styles.logo} />



                    <View style={styles.textContainer}>
                        <_Text style={styles.titleText}>{_string("language.select_your_lan").toLocaleUpperCase()}</_Text>
                        <_Text style={{color:ColorsList.rgbaGray(.7),fontFamily:'regular',fontSize:'size11'}}>{_string("language.massege").toLocaleUpperCase()}</_Text>
                    </View>


                    <View
                        style={styles.ButtonsContainer}>

                        <Button
                        iconStyle={{ width: getWidth(18),height:getHeight(18), color: '#000'}}

                            text={_string("language.english")}
                            iconName='ukFlag'
                            style={styles.Button}
                            onPress={() => onSelectLanguage('en')}
                        />

                        <Button
                            iconStyle={{ color: '#000',width: getWidth(18),height:getHeight(18), }}
                            text={_string("language.french")}
                            iconName='frinshFlag'
                            style={styles.Button}
                            onPress={() => onSelectLanguage('fr')}

                        />
                        <Button
                            iconStyle={{ color: '#000', width: getWidth(18),height:getHeight(18), }}
                            text={_string("language.russian")}
                            iconName='russainFlag'
                            style={styles.Button}
                            onPress={() => onSelectLanguage('ru')}


                        />
                    </View>

                </ImageBackground>
            </View>
        );
    }
}

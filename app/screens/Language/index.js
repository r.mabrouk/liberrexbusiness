import React, { Component } from 'react';
import { View, Text } from 'react-native';
import LanguageView from './LanguageView'
import { connect } from 'react-redux';
import { setAppLanguage } from '../../actions/uiActions';
import NavigationService from '../../navigation/NavigationService';
class Language extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  onSelectLanguage = (lang) => {
    this.props.setAppLanguage(lang)
    
    NavigationService.navigate("Login")
  }
  render() {
    return (
      <LanguageView
        {...this.state}
        {...this}
        {...this.props} />
    );
  }
}
function mapStateToProps(state) {
  return {
    user_profile: state.authReducer.user_profile
  }
}
function mapDispatchToProps(Dispatch) {
  return {
    setAppLanguage: (lang) => Dispatch(setAppLanguage(lang))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Language)

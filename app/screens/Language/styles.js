import { StyleSheet } from 'react-native';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: ColorsList.white
    },
    logo: {
        width:getWidth(300),
        height: getHeight(62),
        resizeMode: 'contain',
        marginTop:getHeight(233)
    },
    imageBackground: {
        width: '100%',
        height: '100%',
        alignItems: 'center'
    },
    textContainer: {
        alignItems: 'center',
        justifyContent: 'center',marginTop:getHeight(20)
    },
    titleText: {
        fontSize: 'size14',
        fontFamily: 'bold',
        color: 'darkGray1'
    },
    subTitleText: {
        textTransform: 'uppercase',
        fontSize: 12
    },
    ButtonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '90%',
        marginTop: getHeight(20)
    },
    Button: {
        width: getWidth(106),
        textColor: 'darkGray1',
        shadow: 6,
        backgroundColor: '#fff',
        borderRadius: 3,
        height: getHeight(38),
        fontSize: 'size15',
        fontFamily: 'bold'
    }
};

export default styles;

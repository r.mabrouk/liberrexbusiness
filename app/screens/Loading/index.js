import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoadingView from './loadingView';

class Loading extends Component {
  constructor(props) {
    super(props);
  }



  render() {
    return (
      <LoadingView
        {...this.props}
      />
    );
  }
}
function mapStateToProps(state) {
  return {

  };
}
function mapDispatchToProps(dispatch) {
  return {

  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Loading);

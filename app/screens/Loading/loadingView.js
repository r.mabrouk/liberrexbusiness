import React, { Component } from 'react';
import {
  View,
  ImageBackground,
  StatusBar,
  Image,
  Modal, ActivityIndicator, Animated
} from 'react-native';
import styles from './styles';
import { images } from '../../config';
import { getWidth, getHeight, Width, Height } from '../../components/utils/dimensions';
class LoadingView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      leftAnimate: new Animated.Value(-40),
      toValue: -40
    };
  }
  async componentDidMount() {
    StatusBar.setHidden(true)
  }
  animation = () => {
    let { toValue } = this.state
    Animated.timing(this.state.leftAnimate, {
      toValue: toValue,
      duration: 1500
    }).start(() => {
      let _toValue = -getWidth(40)
      if (toValue == getWidth(150))
        _toValue = -getWidth(40)
      else
        _toValue = getWidth(150)

      this.setState({ toValue: _toValue }, () => {
        this.animation()
      })
    })
  }
  componentDidMount = () => {
    this.animation()
  }
  render() {
    let { toValue, leftAnimate } = this.state
    return (
      <Modal
        visible={this.props.isLoading}
        animationType='fade'
        transparent
        style={styles.container}>
        {/* <ImageBackground
          resizeMode='cover'
          source={images.splash}
          style={styles.imageBackground}>
          <View style={{ width: getWidth(150), height: getHeight(57), alignItems: 'center' }}>
            <Image resizeMode='contain' source={images.loaderLogo} style={{ width: getWidth(88), height: getHeight(57) }} />

            <View style={{ width: 150, height: '100%', position: 'absolute' }}>
              <Animated.View style={{ height: '100%', width: leftAnimate, flexDirection: 'row' }}>
                <Animated.View style={{ width: leftAnimate, height: '100%', backgroundColor: 'white' }}></Animated.View>
                <Image source={images.fadeLogo} style={{ width: 40, height: getHeight(57)}} />
              </Animated.View>
            </View>
          </View>
        </ImageBackground> */}
        <View style={{ width: Width, height: Height, alignItems: 'center', justifyContent: 'center' }}>

          <ActivityIndicator color={'gray'} size={'large'} />

        </View>

      </Modal>
    );
  }
}

export default LoadingView;

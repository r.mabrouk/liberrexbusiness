import { StyleSheet } from 'react-native';
import { metrics } from '../../config';
import { ColorsList } from '../../components/PropsStyles';
import { Width, Height } from '../../components/utils/dimensions';

const styles = StyleSheet.create({
    container: {
        width: Width,
        height: Height,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: ColorsList.white,
        // position: 'absolute'
    },
    logo: {
        width: metrics.screenWidth * .8,
        height: metrics.screenHeight * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: '100%',
        height: '100%',
        alignItems: 'center', justifyContent: 'center'
    }
});

export default styles;

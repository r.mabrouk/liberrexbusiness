import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text, ScrollView
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { images, metrics, Icons } from '../../config';
import { Input } from '../../components/input/input';
import { Button } from '../../components/button/button';
import { Card } from '../../components/card/card'
import { _Text } from '../../components/text/text';
import { Width, Height, getWidth, getHeight } from '../../components/utils/dimensions';
import MapView, { Marker, Callout } from 'react-native-maps';
import { FlatList } from 'react-native-gesture-handler';
import MapMarkerWithAnimation from '../../components/mapMarkerWithAnimation/mapMarkerWithAnimation';
import { _string } from '../../local';
import { ColorsList } from '../../components/PropsStyles';


export default class LocationView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            location: {
                latitude: 15.785834,
                longitude: -111.406417,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            }
        };
    }

    render() {
        // let { location } = this.state
        let { onSelectLocation, onNext, places, onChangeTextLocation, isSearchLocation, places_name,location,onMarkerChange,mapLocation,onSetMarker } = this.props
        return (

            <View
                style={styles.container}>
                <Card style={{ width: '100%', height: '100%', }}>
                    <ImageBackground
                        resizeMode='cover'
                        source={images.loginbackground}
                        style={styles.imageBackground}>

                        <Button
                            onPress={() => this.props.navigation.goBack()}
                            iconStyle={{ width: getWidth(22),height:getHeight(22), color: "white"}}
                            style={{ position: 'absolute', left: getWidth(0), top: getHeight(25), padding: getWidth(15),zIndex:30 }}
                            iconName='arrowBack'
                        />

                        <ScrollView contentContainerStyle={{ width: Width, paddingHorizontal: getWidth(15), alignItems: 'center' }} style={{ width: Width }}>


                            <Image
                                source={images.WhiteLogo}
                                style={styles.logo} />
                            <_Text style={{ fontFamily: 'bold', fontSize: 'size17', color: 'white', marginTop: getHeight(5) }} >

                                {_string("select_location.select_your_location_on_map").toLocaleUpperCase()}</_Text>
                            <View style={{
                                zIndex: 30, marginTop: getHeight(30),
                            }}>

                                <Input
                                    value={places_name}
                                    onChangeText={onChangeTextLocation}
                                    style={styles.input}
                                    iconName={'search'}
                                    placeholder={_string("select_location.search_for_nearby_places")}
                                    iconStyle={{ width: getWidth(15),height:getHeight(15)}}

                                    verticalLine={false}


                                />
                                {isSearchLocation && <Card style={{ width: getWidth(345), maxHeight: getHeight(200), backgroundColor: ColorsList.rgbaWhite(.9), position: 'absolute', zIndex: 3, shadow: 6, borderRadius: 4, top: getHeight(45) }}>
                                    <FlatList extraData={this.props} data={places} renderItem={({ item, index }) => {
                                        return (
                                            <Button onPress={() => {
                                                let newPlace = {
                                                    latitude: item.geometry.location.lat,
                                                    longitude: item.geometry.location.lng,
                                                    latitudeDelta: 0.0922,
                                                    longitudeDelta: 0.0421
                                                }
                                                onSelectLocation(item.formatted_address, newPlace)
                                                this.setState({ location: newPlace })
                                            }}
                                                style={{
                                                    width: '100%',
                                                    height: getHeight(40),
                                                    borderBottomWidth: 1,
                                                    borderColor: ColorsList.rgbaGray(.04),
                                                    justifyContent: 'flex-start',
                                                    paddingHorizontal: getWidth(10)
                                                }}>
                                                <Icons name='location' width={getWidth(13)} height={getWidth(13)} />
                                                <_Text style={{ fontFamily: 'regular', fontSize: 'small', paddingHorizontal: getWidth(5) }}>{item.formatted_address}</_Text>
                                            </Button>
                                        )
                                    }} />

                                </Card>}
                            </View>

                            <Card style={styles.mapContainer} >
                                {/* <Image source={require("../../assets/img/map.jpg")}
                                    style={styles.mapimg} /> */}
                                <MapView
                                    // onCalloutPress={()=>{
                                    // }}
                                    onPress={(e)=>{
                                        onSetMarker(
                                            e.nativeEvent.coordinate.latitude,
                                            e.nativeEvent.coordinate.longitude)
                                    }}

                                    style={{ width: '100%', height: '100%', backgroundColor: 'red' }}
                                    initialRegion={mapLocation}
                                    region={location}
                                >
                                    <Marker
                                        coordinate={mapLocation}  >
                                        <MapMarkerWithAnimation />
                                    </Marker>

                                    
                                    <Marker
                                        draggable
                                        onDragEnd={
                                          onMarkerChange
                                        }
                                        coordinate={location}  >
                                    </Marker>
                                </MapView>
                            </Card>

                            <Button
                                iconStyle={{ width: getWidth(18),height:getHeight(18), color: '#ffffff',}}
                                text={_string("select_location.next")}
                                iconName='next'
                                style={styles.nextButton}
                                onPress={onNext} />
                        </ScrollView>

                    </ImageBackground>

                </Card>
            </View>

        );
    }
}

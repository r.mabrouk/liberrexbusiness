import React, { Component } from 'react';
import { View, Text } from 'react-native';
import LocationView from './Location'
import { connect } from 'react-redux'
import { getPlacesRequest } from '../../actions/googleMap';
import { _string } from '../../local';
import Geolocation from '@react-native-community/geolocation';
import { _Toast } from '../../components/toast/toast';

class Location extends Component {
  constructor(props) {
    super(props);
    this.state = {
      places_name: '',
      location: {
        latitude: 37.785834,
        longitude:-122.406417,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
      mapLocation: {
        latitude: 37.785834,
        longitude: -122.406417,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
      isSearchLocation: false
    };
  }

  onChangeTextLocation = (places_name) => {
    this.setState({ isSearchLocation: true, places_name: places_name })
    this.props.getPlaces(places_name)


  }
  onSelectLocation = (places_name, location) => {
    this.setState({  places_name, location },()=>{
      this.setState({isSearchLocation: false})
    })
  }
  onNext = () => {
    let { location } = this.state
    if (!location)
      return _Toast.open(_string("messages_alert.plase_select_your_location"), "w")
    let userInfoWithBusinessInfo = this.props.navigation.getParam('userInfoWithBusinessInfo')
    // let data = Object.assign({}, userInfoWithBusinessInfo, businessLocation)
    userInfoWithBusinessInfo.append("lat", location.latitude)
    userInfoWithBusinessInfo.append("lng", location.longitude)
    let socialLogin_data = this.props.navigation.getParam('socialLogin_data')

    this.props.navigation.navigate('SignUpPerson', { userAndBusinessInfo: userInfoWithBusinessInfo,socialLogin_data })
  }

  componentDidMount() {
  // this.getlocation()
  }
  onMarkerChange = (e) => {
    console.log(e.nativeEvent.coordinate)
    this.setState({ location: e.nativeEvent.coordinate })
  }
  componentWillMount=()=>{
    this.getMyLocation()  
}
getMyLocation=()=>{
    Geolocation.getCurrentPosition((location)=>{

      let _location={
        latitude:location.coords.latitude,
        longitude: location.coords.longitude,
        latitudeDelta: 0.00322,
        longitudeDelta: 0.00721,
      }
        this.setState({
            location:_location,
            mapLocation:_location
        })
console.log(location,"locationlocation")
    },
     error => {

      console.log(error,"locationlocation")

    }, { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 });

}
onSetMarker=(lat,lon)=>{
this.setState({location:{
  latitude:lat,
  longitude: lon,
  latitudeDelta: 0.00322,
  longitudeDelta: 0.00721,
}})
}
  render() {
    return (
      <LocationView
        {...this}
        {...this.props}
        {...this.state}
      />
    );
  }
}
function mapStateToProps(state) {
  return {
    places: state.googleMapReducer.places
  }
}
function mapDispatchToProps(dispatch) {
  return {
    getPlaces: (place_name) => dispatch(getPlacesRequest(place_name))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Location)

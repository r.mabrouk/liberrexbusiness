import { StyleSheet } from 'react-native';
import { getHeight, getWidth,Width,Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: ColorsList.white
    },
    logo: {
        width: getWidth(250),
        height:getHeight(52),
       marginTop:getHeight(100),
        resizeMode: 'contain',

    },
    imageBackground: {
        width: '100%',
        height: '100%',
        alignItems: 'center',


    },
    mapContainer:
    {
        width: '90%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: getHeight(10),

    },
    mapimg: {
        borderRadius: 3,
        width: '100%',
        height: getHeight(315),

    },
    card: {
        width: '90%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: getHeight(10),

        borderRadius: 10
    },
    input: {
        borderRadius: 3,
        width: getWidth(345),
        height: getHeight(41),
        shadow: 6, backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        zIndex:3
    },
  
    mapContainer:
    {
        width: getWidth(345),
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: getHeight(10),
        shadow: 7,
        borderRadius: 3,
        height:getHeight(319)
    },
    nextButton:
    {
        width: getWidth(345),
        marginTop: getHeight(15),
        textColor: '#fff',
        shadow: 6,
        height:  getHeight(44),
        colors: 'blue',
        borderRadius: 3,
        fontFamily:'bold',
        fontSize:'size15'
    }


};

export default styles;

import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text, ScrollView, ActivityIndicator
} from 'react-native';
import { images } from '../../config';
import { Input } from '../../components/input/input';
import { Button } from '../../components/button/button';
import styles from './styles'
import { Card } from '../../components/card/card'

import LinearGradient from 'react-native-linear-gradient'
import { Colors, GradientColors, GradientColorsList, ColorsList } from '../../components/PropsStyles/colors'

import { _Text } from '../../components/text/text';
import { getWidth, Width, Height, getHeight } from '../../components/utils/dimensions';
import { validateEmail } from '../../utils/stringUtils';
import { PopupModal } from '../../components/popupModal/popupModal';
import { CheckBox } from '../../components/checkBox/checkBox';
import { _string } from '../../local';


type LoginViewProps = {
    flag: Boolean,
    email: String,
    password: String,
    onChangeText: Function,
    _selected: Boolean
}
export default class LoginView extends Component<LoginViewProps> {
    constructor(props) {
        super(props);
        this.state = {
            flag: true,
            passwordSecure: true
        }
    }

    render() {
        let { passwordSecure } = this.state
        let { email, password, _selected, onSignIn, onChangeText, onRemember, remember, isLoginLoading, isSocialLoginLoading, socailType } = this.props
        return (
            <View
                style={styles.container}>
                <View>
                    {/* <FBLogin /> */}

                </View>
                <Card style={{ width: Width, height: '100%', alignItems: 'center' }}>
                    <ImageBackground
                        resizeMode='stretch'
                        source={images.loginbackground}
                        style={styles.imageBackground}>
                        <ScrollView scrollEnabled={false} contentContainerStyle={{ alignItems: 'center',paddingBottom:getHeight(15) }}
                            style={{ width: '100%' }}>


                        <Image
                            source={images.WhiteLogo}
                            style={styles.logo} />

                        <View style={{ alignItems: 'center', width: '100%', marginTop: getHeight(46) }}>

                            <Input
                                onSubmitEditing={() => {
                                    this.input2.focus()
                                }}
                                ref={(ref) => this.input1 = ref}
                                returnKeyType='next'
                                autoFocus
                                onChangeText={(text) => onChangeText(text, 'email')}
                                value={email}
                                style={styles.input}
                                iconName={'email'}
                                placeholder={_string("login.your_email")}
                                iconStyle={{ width: getWidth(15), height: getHeight(15) }}
                            />

                            <Input
                                ref={(ref) => this.input2 = ref}
                                onSubmitEditing={onSignIn}


                                returnKeyType='go'

                                iconStyle={{ width: getWidth(15), height: getHeight(15) }}

                                onRightPress={() => {
                                    this.setState({ passwordSecure: !passwordSecure })
                                }}
                                secureTextEntry={passwordSecure}
                                onChangeText={(text) => onChangeText(text, 'password')}
                                value={password}
                                style={styles.input}
                                iconName={'password'}
                                placeholder={_string("login.your_password")}
                            />


                            <Button
                                isLoading=
                                {isLoginLoading}
                                onPress={onSignIn}
                                iconStyle={{ color: '#fff', width: getWidth(18), height: getHeight(18), marginBottom: 5, }}
                                text={_string("login.login")}
                                iconName='login'
                                style={styles.loginButton} />
                            <View style={styles.forgetContainer}>


                                <CheckBox textColor={ColorsList.rgbaLightGray(.7)} checked={remember} fontSize='size13' text={_string("login.remember_me")} onChecked={onRemember} />



                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('ForgetPassword') }} >
                                    <_Text style={{ fontFamily: 'medium', fontSize: 'size13', color: ColorsList.rgbaLightGray(.7) }}>{_string("login.forget_password")}</_Text>
                                </TouchableOpacity>
                            </View>
                            <_Text style={styles.haveAccount}>{_string("DontHaveAnAccount").toLocaleUpperCase()}</_Text>


                            <Button


                                iconStyle={{ width: getWidth(18), height: getHeight(18), color: ColorsList.rgbaLightGray(.3) }}
                                text={_string("login.signup")}
                                iconName='signupgray'
                                style={styles.signupButton}
                                onPress={() => { this.props.navigation.navigate('SingUp') }}
                            />


                            <View style={styles.socailTextContainer}>
                                <_Text style={styles.socailtext}>{_string("login.or_login_with_socail_media").toLocaleUpperCase()}</_Text>
                                <_Text style={styles.socailsubtext}>{_string("login.we_dont_have_any_acceses_to_your_accounts").toLocaleUpperCase()}</_Text>

                            </View>
                            {/* socail icon buttons */}
                            <View
                                style={styles.ButtonsContainer}>
                                <Button
                                    isLoading={isSocialLoginLoading && socailType == 'facebook'}
                                    loaderColor={'darkGray1'}
                                    onPress={this.props.onfacebookLogin}

                                    iconStyle={{ width: getWidth(18), height: getHeight(18) }}
                                    iconName='facebook'
                                    style={styles.socialMediaButtons}
                                />
                                <Button
                                    isLoading={isSocialLoginLoading && socailType == 'twitter'}

                                    onPress={this.props.onTwitterLogin}

                                    iconStyle={{ width: getWidth(18), height: getHeight(18) }}
                                    iconName='twitter'
                                    style={styles.socialMediaButtons}
                                />
                                <Button
                                    isLoading={isSocialLoginLoading && socailType == 'google'}

                                    onPress={this.props.onGoogleLogin}
                                    iconStyle={{ width: getWidth(18), height: getHeight(18) }}

                                    iconName='googlepluse'
                                    style={styles.socialMediaButtons}
                                />
                            </View>

                        </View>
                        </ScrollView>
                    </ImageBackground>

                </Card>
                <PopupModal duration={1000} height={300} ref={_BottomMenu => this.BottomMenu = _BottomMenu}  >
                    < View style={{
                        width: '100%', height: '100%'
                    }}></View>
                </PopupModal>
            </View>

        );
    }
}

import React, { Component } from 'react';
import { View, Text, NativeModules, Keyboard } from 'react-native';
import LoginView from './LoginView';
import { validateEmail } from '../../utils/stringUtils';
import { connect } from 'react-redux'
import { requestLogin, socialLoginRequest } from '../../actions/auth';
import { set_UserLoginData } from '../../actions/uiActions';
import { _string } from '../../local';
import { LoginManager, AccessToken } from "react-native-fbsdk";
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import { compressImages } from '../../utils/compressImages';
import firebase from 'react-native-firebase'
import { _Toast } from '../../components/toast/toast';
const { RNTwitterSignIn } = NativeModules

// var FBLoginMock = require('./facebook/FBLoginMock.js');

const Constants = {
  //Dev Parse keys
  TWITTER_COMSUMER_KEY: "O0UzoFkUY4AvlpECEjAvibBiY",
  TWITTER_CONSUMER_SECRET: "j81HqnFffZXQKnAsqvbhrpeMDMF5KKQCFvDwpzF12PVC1vyocr",
}
GoogleSignin.configure({
  scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
  iosClientId: '1056402514225-efbfut40d2ld7agtbti33f69qa0lumjn.apps.googleusercontent.com', // only for iOS
  // hostedDomain: '', // specifies a hosted domain restriction
  // forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login
  // accountName: '', // [Android] specifies an account name on the device that should be used
});
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      remember: this.props.user_login_data.remember,
      email: this.props.user_login_data.email,
      password: this.props.user_login_data.password,
      socialType: 'facebook'
    };
  }
  onSignIn = async() => {


    Keyboard.dismiss()
    let { email, password, remember } = this.state
    let data = {
      email,
      password,
      remember
    }
    if (this.validationForm()) {
      if (remember)
        this.props.set_UserLoginData(data)
      else {
        this.props.set_UserLoginData({})
      }
      this.props.login(data)

    }
    // this.props.navigation.navigate('Home');
  }
  signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ user: null }); // Remember to remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };
  _selected = () => {
    if (this.state.flag) {
      this.setState({
        flag: false
      })
    }
    else if (!this.state.flag) {
      this.setState({
        flag: true
      })
    }
  }

  validationForm = () => {
    let { email, password } = this.state
    if (!email) {
      _Toast.open(_string("messages_alert.please_enter_your_email"), 'w')
      return false
    }
    else if (!validateEmail(email)) {
      _Toast.open(_string("messages_alert.please_enter_valid_email"), 'w')
      return false
    }
    else if (!password) {
      _Toast.open(_string("messages_alert.please_enter_your_password"), 'w')
      return false
    }
    else return true

  }
  onChangeText = (text, fieldName) => {
    this.setState({ [fieldName]: text })
  }

  onTwitterLogin = async () => {
    RNTwitterSignIn.init(Constants.TWITTER_COMSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET)
    RNTwitterSignIn.logIn()
      .then(async twitter_data => {
        console.log(twitter_data, "twitter_data")

        let image = await compressImages("https://tinyjpg.com/images/social/website.jpg")

        let twitter_user_data = {
          provider: "twitter",
          provider_id: twitter_data.userID,
          email: twitter_data.email,
          token: twitter_data.authToken,
          first_name: twitter_data.first_name,
          last_name: twitter_data.last_name,
          user_photo: image,
          uri: image.uri,
          authTokenSecret: twitter_data.authTokenSecret
        }
        this.setState({ socialLogin: 'twitter' })
        this.props.socialLogin(twitter_user_data)
        console.log(loginData)
        const { authToken, authTokenSecret } = loginData
        if (authToken && authTokenSecret) {
          this.setState({
            isLoggedIn: true
          })
        }
      })
      .catch(error => {
        console.log(error)
      }
      )
  }


  onGoogleLogin = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const google_data = await GoogleSignin.signIn();
      let image = await compressImages(google_data.user.photo)

      let facebook_user_data = {
        provider: "google",
        provider_id: google_data.user.id,
        email: google_data.user.email,
        token: google_data.idToken,
        first_name: google_data.user.givenName,
        last_name: google_data.user.familyName,
        user_photo: image,
        uri: image.uri

      }
      console.log("TAG", "ercvncgmnfgnfgnmfngn,fror", image)

      this.setState({ socialLogin: "google" })
      this.props.socialLogin(facebook_user_data)
    } catch (error) {
      console.log("TAG", "error", error)
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      } else if (error.code === statusCodes.IN_PROGRESS) {
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      } else {
      }
    }
    this.signOut()
  }

  onfacebookLogin = async () => {
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      async (result) => {
        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {
          AccessToken.getCurrentAccessToken().then(
            async (data) => {
              var url = `https://graph.facebook.com/v3.2/${
                data.userID
                }?fields=id,first_name,last_name,email,picture.width(2000).height(2000)&access_token=${data.accessToken}`;
              let facebook_data = await fetch(url).then((res) => res.json())
              console.log(facebook_data, "facebook_datafacebook_datafacebook_datafacebook_data")
              let image = await compressImages(facebook_data.picture.data.url)
              let facebook_user_data = {
                provider: "facebook",
                provider_id: data.userID,
                email: facebook_data.email,
                token: data.accessToken,
                first_name: facebook_data.first_name,
                last_name: facebook_data.last_name,
                user_photo: image,
                uri: image.uri
              }
              this.setState({ socialLogin: "facebook" })
              this.props.socialLogin(facebook_user_data)
            }
          )
        }
        LoginManager.logOut()
      },
      function (error) {
        console.log("Login fail with error: " + error);
      }
    );
  }

  onRemember = () => {
    this.setState({ remember: !this.state.remember })
  }
  render() {
    // let {}
    return (
      <LoginView
        {...this.state}
        {...this}
        {...this.props} />
    );
  }
}
function mapStateToProps(state) {
  return {
    isLoginLoading: state.loadingReducer.isLoginLoading,
    user_login_data: state.uiReducer.user_login_data,
    isSocialLoginLoading: state.loadingReducer.isSocialLoginLoading,


  }
}
function mapDispatchToProps(dispatch) {
  return {
    login: (user_data) => dispatch(requestLogin(user_data)),
    set_UserLoginData: (user_data) => dispatch(set_UserLoginData(user_data)),
    socialLogin: (user_data) => dispatch(socialLoginRequest(user_data))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login)
import { StyleSheet } from 'react-native';
import { getWidth, getHeight, Height, Width } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(375),
        backgroundColor: ColorsList.white,
        height: Height
    },
    logo: {
        width: getWidth(250),
        height: getHeight(52),
        resizeMode: 'contain',
        marginTop: getHeight(100)
    },
    imageBackground: {
        width: '100%',
        height: '100%',
        alignItems: 'center',


    },
    inputContainer: {
        marginBottom: getHeight(10),
        width: '100%'
    },
    haveAccount: {
        fontSize: 'size11',
        color:ColorsList.rgbaLightGray(.7),
        fontFamily: 'regular',
        marginTop: getHeight(31)
    },
    text: {
        textTransform: "uppercase",
        marginTop: getHeight(15),
        fontSize: 15

    },
    resetbtn: {
        width: getWidth(345),
        marginTop: getHeight(25),
    },
    arrivaltime: {
        fontWeight: 'bold',
        fontSize: 15
    },
    ButtonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: getHeight(11),
        width: getWidth(345),
        height: getHeight(38)

    },
    socailtext: {

        fontFamily:'bold',
        fontSize: 'size14',
        color: "darkGray1"
    },
    socailsubtext:{
        fontFamily:'regular',
        fontSize: 'size11',
        color: ColorsList.rgbaGray(.7)
    },
    socailTextContainer: {
        marginTop: getHeight(30),
        alignItems: 'center',
    },
    forgetContainer: {
        marginTop: getHeight(15),
        flexDirection: "row",
        width: getWidth(345),
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    signupButton: {
        width: getWidth(345),
        marginTop: getHeight(10),
        textColor: 'darkGray1',
        shadow: 6,
        fontFamily: 'bold',
        fontSize:'size15',
        height: getHeight(44),
        backgroundColor: 'white',
        borderRadius: 3,


    },
    loginButton: {
        width: getWidth(345),
        marginTop: getHeight(15),
        textColor: 'white',
        shadow: 6,
        fontFamily: 'bold',
        fontSize: 'size15',
        height: getHeight(44),
        colors: 'blue',
        borderRadius: 3,

    },
    circle: {
        width: getWidth(15), height: getWidth(15), borderRadius: getWidth(7.5),
        alignItems: 'center', justifyContent: 'center'
    },
    input: {
        fontFamily: 'medium',
        borderRadius: 3,
        width: getWidth(345),
        fontSize:'size13',
        height: getHeight(44),
        shadow: 6, backgroundColor: 'white',
        // alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: getHeight(10),


    },
    socialMediaButtons: {
        width: getWidth(106),
        textColor: '#000',
        shadow: 6,
        backgroundColor: '#fff',
        borderRadius: 3,
        height: getHeight(38)
    }
};

export default styles;

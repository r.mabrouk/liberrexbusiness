import React, { Component } from 'react';
import { connect } from 'react-redux';
import MembershipCheckoutView from './membershipCheckoutView';
import { addnewPaymentCardRequest, getPaymentCardsRequest, updateAccountMembershipRequest } from '../../../actions/payment';
// import Stripe from 'react-native-stripe-api';
import Stripe, { PaymentCardTextField } from 'tipsi-stripe'
import { ModalToast } from '../../../components/toast/toast';
import { _string } from '../../../local';

class MembershipCheckout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      plans: props.navigation.getParam("plan", {}),
      plan: {

      },
      planIndex: -1,
      cardIndex: -1,
      card: {
      },
      numberOfMonths: 1
    }
  }

  nextScreen = () => {

    setTimeout(() => {
    }, 2000);
  }


  validationPaymentForm = (cardNamber, expiry_date, cvv) => {
    if (!cardNamber) {
      ModalToast.open(_string("PleaseTypeYourCardNumber"), "w")
      return false
    }
    else if (cardNamber.length<16) {
      ModalToast.open(_string("PleaseEnterAValidCardNumber"), "w")
      return false
    }
    else if (!expiry_date) {
      ModalToast.open(_string("PleaseTypeExpiryDate"), "w")
      return false
    }
    else if (!cvv) {
      ModalToast.open(_string("PleaseTypeYourCvv"), "w")
      return false
    }
    else
      return true
  }
  onAddNewPaymentCard = async (cardNamber, expiry_date, cvv, savePaymentMethods, CloseModal) => {
    if (this.validationPaymentForm(cardNamber, expiry_date, cvv, CloseModal)) {
      let data = {
        cardNamber,
        expMonth: expiry_date.split("/")[0],
        expYear: expiry_date.split("/")[1],
        cvv
      }
      this.props.AddNewPaymentCard(data, savePaymentMethods, CloseModal)
    }
  }
  componentWillMount = () => {
    this.setMonthlyPlan()
    this.getPaymentCards()
  }
  setMonthlyPlan = () => {
    let plans = this.props.navigation.getParam("plan", [])
    plans.plans.filter((element, index) => {
      console.log("elementelementelement",element)
      if (element.name ==_string("modals.report_by.monthly"))
        this.setState({ plan: element, planIndex: index })
    })
  }

  onSelectPlan = (plan, index) => {
    let numberOfMonths = 1
    if (plan.name == _string("modals.report_by.yearly"))
      numberOfMonths = 12
    else
      numberOfMonths = 1
    this.setState({ planIndex: index, plan, numberOfMonths })
  }
  onSelectCard = (card, index) => {
    if (card.card)
    this.setState({ cardIndex: index, card: card.card })
  }

  onCheckout = () => {
    let {plan,plans}=this.state
    let data={
      package_id:plan.package_id,
      product:plans.name,
      plan:plan.stripe_id
    }
    let detailsForNextScreen={
      membershipPlan:plans.name,
      taxs:plans.vat,
      plan:plan.name,
      price:plans.price,
      currency:plans.currency,
      numberOfMonths: this.state.numberOfMonths
    }
    if (this.state.cardIndex==-1&&this.props.cardsList.length<1){
      return _Toast.open(_string("PleaseSelectOrAddAPaymentMethod"),"w")
    }
    this.props._checkout(data,detailsForNextScreen)
  }
  getPaymentCards = () => {
    this.props._getPaymentCards()
  }
  
  render() {
    return (
      <MembershipCheckoutView
        {...this.state}
        {...this}
        {...this.props}
        nextScreen={this.nextScreen}
      />
    );
  }
}


function mapStateToProps(state) {
  return {
    loadingAddPaymentCard: state.paymentReducer.loadingAddPaymentCard,
    cardsList: state.paymentReducer.cardsList,
    loadingAddPaymentCard: state.paymentReducer.loadingAddPaymentCard,
    loadingGetPaymentCards: state.paymentReducer.loadingGetPaymentCards,
    loadingDeletePaymentCard: state.paymentReducer.loadingDeletePaymentCard,
    loadingUpdateAccountMembership: state.paymentReducer.loadingUpdateAccountMembership,
  };
}
function mapDispatchToProps(Dispatch) {
  return {
    _checkout:(data,detailsForNextScreen) => Dispatch(updateAccountMembershipRequest(data,detailsForNextScreen)),
    _getPaymentCards: () => Dispatch(getPaymentCardsRequest()),
    AddNewPaymentCard: (data, saved, CloseModal) => Dispatch(addnewPaymentCardRequest(data, saved, CloseModal))
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MembershipCheckout);

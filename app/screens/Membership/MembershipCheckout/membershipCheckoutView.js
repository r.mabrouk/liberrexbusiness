import React, { Component } from 'react';
import {
    View, ScrollView
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Header } from '../../../components/header/header';
import { _Text } from '../../../components/text/text';
import { HeaderSection } from '../../../components/headerSection/headerSection';
import { Card } from '../../../components/card/card';
import { Button } from '../../../components/button/button';
import { GradientColorsList } from '../../../components/PropsStyles';
import LinearGradient from 'react-native-linear-gradient';
import { ButtonSectionItem } from '../../../components/buttonSection/buttonSection';
import { getHeight, getWidth, Width } from '../../../components/utils/dimensions';
import PaymentsModel from '../../Payments/PaymentsModel';
import { _string } from '../../../local';
import ButtonWithHeaderSection from '../../../components/buttonWithHeaderSection/buttonWithHeaderSection';
import CardsModal from '../cardsModal/cardsModal';
import PlansModal from '../plansModal/plansModal';

class MembershipCheckoutView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            numberOfMonths: 1
        }
    }
    async componentDidMount() {


    }
    render() {
        let { loadingAddPaymentCard, onAddNewPaymentCard, plans, onSelectCard, onSelectPlan, plan, planIndex, numberOfMonths, cardsList, loadingGetPaymentCards, card, cardIndex,onCheckout,loadingUpdateAccountMembership } = this.props
        return (
            <View
                style={styles.container}>
                <Header
                    onNotification={() => {
                        this.props.navigation.navigate("Notification")
                    }}
                    onBack={() => {
                        this.props.navigation.goBack()
                    }}
                    style={{ width: '100%', zIndex: 16 }} />

                <HeaderSection
                    onPress={() => {
                        this.props.navigation.navigate('QueueEdit')
                    }}
                    iconSize={getWidth(28)}
                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}
                    text={_string("member_ship.membership_plans").toLocaleUpperCase()}
                    iconName='membership'
                />
                <ScrollView >

                    <View style={styles.contentStyle}>

                        <ButtonWithHeaderSection

                            headerIcon='list'
                            onPress={() => {
                                if (plan.name)
                                this.plansModal.onAnimated(true)

                            }}
                            value={plan.name?plan.name:_string("Free")}
                            headerText={_string("member_ship.order_details")}
                        />


                        <Card style={styles.invoiceCard}>
                            <InvoiceItem
                                text1={_string("member_ship.order_details")}
                                text2={plans.name}
                                Top Bottom Left Right
                            />
                            <InvoiceItem
                                text1={_string("member_ship.price_before_taxs")}
                                text2={`${plans.currency} ${Math.floor( ((plans.price) * numberOfMonths)*10)/10} / ${plan.name}`}
                                Bottom Left Right
                            />
                            <InvoiceItem
                                text1={_string("member_ship.taxs")}
                                text2={`${plans.vat}%`}
                                Left Right
                            />
                            <InvoiceItem
                                text1={_string("member_ship.total_price")}
                                text2={`${plans.currency} ${ Math.floor( (((plans.price) * numberOfMonths) + ((plans.price) * (numberOfMonths) * plans.vat) / 100)*10)/10 } / ${plan.name}`}
                                color
                            />

                        </Card>

                        <ButtonWithHeaderSection
                        marginTop={getHeight(10)}
                            headerIcon='paymentCardGray'
                            onPress={() => {
                                this.cardsModal.onAnimated(true)


                            }}
                            value={card.brand?`${card.brand} ${_string("EndingIn")} ${card.last4}`:_string("SelectPaymentMethod")}
                            headerText={_string("member_ship.payment_method")}
                        />

                        {/* 
                        <ButtonSectionItem
                            iconName1='paymentCardGray'
                            iconName2='arrowDown'
                            text1={_string("member_ship.payment_method")}
                            text2='Visa Ending In 4242'
                            leftIcon='paymentCardBlue' /> */}

                        <Button
                            onPress={() => {
                                this.addPayment.onAnimated(true)
                            }}
                            iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                            text={_string("member_ship.add_new_card")}
                            iconName='paymentCardBlue'
                            style={styles.normalButton(true)} />
                        <Button
                        isLoading={loadingUpdateAccountMembership}
                            onPress={onCheckout}
                            iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                            text={_string("member_ship.checkout_now")}
                            iconName='done'
                            style={styles.blueButton(true)} />

                    </View>
                </ScrollView>
                <PaymentsModel
                    loadingAddPaymentCard={loadingAddPaymentCard}
                    onAddNewPaymentCard={onAddNewPaymentCard}
                    ref={(add) => this.addPayment = add} />
                <CardsModal
                    loadingGetPaymentCards={loadingGetPaymentCards}
                    cardIndex={cardIndex}
                    ref={cardsModal => this.cardsModal = cardsModal}
                    onSelectCard={onSelectCard}
                    data={cardsList}
                />
                <PlansModal
                    planIndex={planIndex}
                    ref={plansModal => this.plansModal = plansModal}
                    onSelectPlan={onSelectPlan}
                    data={plans.plans}
                />

            </View>
        );
    }
}
const InvoiceItem = ({ text1, text2, Top, Bottom, Left, Right, color }) => {
    return (
        <View style={styles.invoiceContainer(Top, Bottom, Left, Right, color)}>
            {color && <LinearGradient
                style={{ width: '100%', height: '100%', position: 'absolute' }}
                locations={[0, 1]}
                start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }}
                colors={GradientColorsList.blue}>
            </LinearGradient>
            }
            <View style={styles.labelView(color)}>
                <_Text
                    style={styles.textStyle(color, 'lightGray1')} >
                    {text1}
                </_Text>
            </View>
            <View style={styles.resultView}>
                <_Text
                    style={styles.textStyle(color, 'darkGray1')} >
                    {text2}
                </_Text>
            </View>

        </View>
    )
}


export default MembershipCheckoutView;
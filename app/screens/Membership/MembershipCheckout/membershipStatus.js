import React, { Component } from 'react';
import {
    View
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Header } from '../../../components/header/header';
import { _Text } from '../../../components/text/text';
import { HeaderSection } from '../../../components/headerSection/headerSection';
import { Card } from '../../../components/card/card';
import { Button } from '../../../components/button/button';
import { GradientColorsList } from '../../../components/PropsStyles';
import LinearGradient from 'react-native-linear-gradient';
import { ButtonSectionItem } from '../../../components/buttonSection/buttonSection';
import { getHeight, getWidth } from '../../../components/utils/dimensions';
import { Icons } from '../../../config';
import { _string } from '../../../local';

class MembershipStatus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: 'success'
        }
    }
    async componentDidMount() {
        this.props.nextScreen()
    }

    render() {

        let { status } = this.state

        return (
            <View
                style={styles.container}>
                <Header style={{ width: '100%' }} />
                <Card style={styles.successCard}>
                    <View style={styles.iconView}>
                        <Icons name={status == 'success' ? 'doneGreen' : 'cancel'}      width={getWidth(42)}
                         height={getWidth(42)} />
                    </View>

                    <_Text style={styles.statusText1}>
                        {status == 'success' ? _string("member_ship.thank_you_for_your_purchase").toLocaleUpperCase()
                            : _string("member_ship.sorry_we_didnt_received_money").toLocaleUpperCase()}

                    </_Text>
                    <View style={{
                        height: getHeight(5)
                    }} />
                    <_Text style={styles.statusText2()}>
                        {status == 'success' ? 'Your are now on Basic Plan for'+_string("member_ship.your_are_now_on_basic_plan_for") +' 30' +_string("member_ship.days")
                            : _string("member_ship.try_again_or_try_another_payment_method")}
                    </_Text>

                </Card>

                <Button
                    iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                    text={status == 'success' ? _string("member_ship.back_to_home") : _string("member_ship.try_again")}
                    iconName={status == 'success' ? 'done' : 'redirect'}
                    style={status == 'success' ? styles.blueButton() : styles.normalButton()} />

                <View style={styles.contentStyle}>

                    <Card style={styles.statusLabel}>
                        <View style={{ width: getWidth(20) }}>
                            <Icons name='list' width={getWidth(18)} height={getWidth(18)} />
                        </View>
                        <_Text style={styles.statusText2('bold')}>
                            {_string("member_ship.order_details")}
                            </_Text>

                    </Card>

                    <Card style={styles.invoiceCard}>
                        <InvoiceItem
                            text1= {_string("member_ship.membership_plan")}
                            text2='Basic Plan'
                            Top Bottom Left Right
                        />
                        <InvoiceItem
                            text1={_string("member_ship.price_before_taxs")}
                            text2='200 USD/month'
                            Bottom Left Right
                        />
                        <InvoiceItem
                            text1={_string("member_ship.taxs")}
                            text2='14%'
                            Left Right
                        />
                        <InvoiceItem
                            text1={_string("member_ship.total_price")}
                            text2='220 USD/month'
                            color
                        />

                    </Card>

                </View>

            </View>
        );
    }
}
const InvoiceItem = ({ text1, text2, Top, Bottom, Left, Right, color }) => {
    return (
        <View style={styles.invoiceContainer(Top, Bottom, Left, Right, color)}>
            {color && <LinearGradient
                style={{ width: '100%', height: '100%', position: 'absolute' }}
                locations={[0, 1]}
                start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }}
                colors={GradientColorsList.blue}>
            </LinearGradient>
            }
            <View style={styles.labelView(color)}>
                <_Text
                    style={styles.textStyle(color, 'lightGray1')} >
                    {text1}
                </_Text>
            </View>
            <View style={styles.resultView}>
                <_Text
                    style={styles.textStyle(color, 'darkGray1')} >
                    {text2}
                </_Text>
            </View>
        </View>
    )
}

MembershipStatus.propTypes = {
    nextScreen: PropTypes.func
};

export default MembershipStatus;
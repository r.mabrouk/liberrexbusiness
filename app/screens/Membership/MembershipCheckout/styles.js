import { getHeight, Width, getWidth, Height } from '../../../components/utils/dimensions';
import { ColorsList } from '../../../components/PropsStyles';

const styles = {
    container: {
        width: Width,
        height: Height,
        alignItems: 'center',
        backgroundColor: ColorsList.white
    },
    headerStyle: {
        height: getHeight(67),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: Width, shadow: 3,
        marginTop: -getHeight(30),
        paddingHorizontal: getWidth(15),
        // backgroundColor: 'white'
    },
    contentStyle: {
        width: getWidth(375),
        marginTop: getHeight(20),
        // justifyContent: 'center',
        alignItems: 'center'
    },
    invoiceCard: {
        width: getWidth(345),
        height: getHeight(186),
        paddingHorizontal: getWidth(15),
        marginTop:getHeight(1),
        marginBottom:getHeight(10),
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        shadow: 3
    },
    normalButton: (top) => ({
        height: getHeight(44),
        width: getWidth(345),
        shadow: 3,
        backgroundColor: 'white',
        borderRadius: 3,
        marginTop: 0,
        justifyContent: 'center',
        alignItems: 'center',
        textColor: ColorsList.darkGray1,
        fontSize: 'medium15Px',
        fontFamily: 'bold'
    }),
    blueButton: (top) => ({
        height: getHeight(44),
        width: getWidth(345),
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        textColor: 'white',
        marginTop: top ? getHeight(20) : 0,
        colors: 'blue',
        fontSize: 'medium15Px',
        fontFamily: 'bold'
    }),
    invoiceContainer: (Top, Bottom, Left, Right, color) => ({
        height: getHeight(46),
        width: getWidth(345),
        flexDirection: 'row',
        borderTopWidth: Top ? getHeight(1) : 0,
        borderBottomWidth: Bottom ? getHeight(1) : 0,
        borderLeftWidth: Left ? getHeight(1) : 0,
        borderRightWidth: Right ? getHeight(1) : 0,
        borderColor: color ? ColorsList.white : ColorsList.rgbaBlack(.04),
        justifyContent: 'center',
        backgroundColor: ColorsList.white
    }),
    labelView: (color) => ({
        height: '100%',
        width: getWidth(130),
        borderRightWidth: getHeight(1),
        borderColor: color ? ColorsList.white : ColorsList.rgbaBlack(.04),
        paddingHorizontal: getWidth(10),
        justifyContent: 'center'
    }),
    textStyle: (color, clr) => ({
        fontSize: 'small13Px',
        fontFamily: 'bold',
        color: color ? 'white' : clr
    }),
    resultView: {
        height: '100%',
        width: getWidth(215),
        paddingHorizontal: getWidth(10),
        justifyContent: 'center'
    },

    //success

    successCard: {
        height: getHeight(152),
        width: getWidth(375),
        marginBottom: getHeight(20),
        shadow: 3,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconView: {
        width: getWidth(42),
        marginBottom: getHeight(10)
    },
    statusText1: {
        fontSize: 'regular',
        fontFamily: 'bold',
        color: 'darkGray1'
    },
    statusText2: (bold) => ({
        fontSize: 'medium15Px',
        fontFamily: bold ? 'bold' : 'regular',
        color: 'darkGray1'
    }),
    statusLabel: {
        flexDirection: 'row',
        width: getWidth(345)
    },

    //plans

    profileHeaderView: {
        width: getWidth(375),
        height: getHeight(101),
        alignItems: 'flex-end'
    },
    profileHeaderContent: {
        width: getWidth(375),
        height: getHeight(86),
        // paddingHorizontal:getWidth(10),
        flexDirection: 'row',
        alignItems: 'center',
        colors: 'blue',
        shadow:6,
        zIndex:12
    },
    imageContainer: {
        width: getWidth(46),
        height: getWidth(46),
        marginHorizontal: getWidth(10),
        borderRadius: 23,
        shadow: 7, backgroundColor: 'white'

    },
    changeButton: {
        width: getWidth(143),
        height: getHeight(31),
        shadow: 6,
        borderRadius: 5,
        right: getWidth(15),
        top: getHeight(-16.2),
        backgroundColor: 'white',
        textColor: ColorsList.rgbaBlack(.7),
        fontSize: 'small',
        fontFamily: 'bold',
        zIndex:14
    },
    membershipCard: {
        width: getWidth(375),
        height: getHeight(347),
        marginTop: getHeight(15),
        shadow: 5,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    buttonsView: {
        width: getWidth(375),
        height: getHeight(79),
        flexDirection: 'row'
    },
    planBlueButton: {
        width: getWidth(125),
        height: getHeight(80),
        justifyContent: 'center',
        alignItems: 'center',
        colors: 'blue',
    },
    planNormalButton: {
        width: getWidth(125),
        height: getHeight(79),
        shadow: 3,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    planTextView: {
        width: getWidth(375),
        height: getHeight(194),
        marginVertical: getHeight(10),
        justifyContent: 'center',
        alignItems: 'center'
    },
    textView: (index, length) => ({
        width: getWidth(375),
        height: getHeight(38.8),
        borderBottomWidth: index == length ? 0 : getHeight(1),
        borderColor: ColorsList.rgbaBlack(.04),
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    }),
    planeText: {
        fontSize: 'small13Px',
        fontFamily: 'bold',
        color: ColorsList.gray
    },
    headerText:(size)=>({
        fontSize: size,
        fontFamily: 'bold',
        color: 'white',
        lineHeight:getHeight(22)
    }),
    planButtonsView:{
        flexDirection: 'column',
        justifyContent:'center',
        alignItems:'center'
    },
    planText1: (selected)=>({
        color: selected ? 'white' : ColorsList.darkGray1,
        fontSize: 'medium15Px',
        fontFamily: 'bold',
        textAlign: 'center',
        marginBottom: getHeight(5)
    }),
    planText2: (selected)=>({
        color: selected ? 'white' : ColorsList.darkGray1,
        fontSize: 'small13Px',
        fontFamily: 'regular',
        textAlign: 'center'
    })

};
export default styles;
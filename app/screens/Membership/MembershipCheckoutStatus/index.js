import React, { Component } from 'react';
import { connect } from 'react-redux';
import MembershipCheckoutStatusView from './membershipCheckoutStatusView';
class MembershipCheckoutStatus extends Component {
  constructor(props) {
    super(props);
  }

  nextScreen = () => {

  }

  render() {
    return (
      <MembershipCheckoutStatusView
      {...this}
      {...this.state}
        {...this.props}
        nextScreen={this.nextScreen}
      />
    );
  }
}
function mapStateToProps(state) {
  return {
    membershipDetails:state.paymentReducer.membershipDetails
  };
}
function mapDispatchToProps(dispatch) {
  return {
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MembershipCheckoutStatus);

import React, { Component } from 'react';
import {
    View
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Header } from '../../../components/header/header';
import { _Text } from '../../../components/text/text';
import { HeaderSection } from '../../../components/headerSection/headerSection';
import { Card } from '../../../components/card/card';
import { Button } from '../../../components/button/button';
import { GradientColorsList, ColorsList } from '../../../components/PropsStyles';
import LinearGradient from 'react-native-linear-gradient';
import { ButtonSectionItem } from '../../../components/buttonSection/buttonSection';
import { getHeight, getWidth, Width } from '../../../components/utils/dimensions';
import { Icons } from '../../../config';
import { _string } from '../../../local';

class MembershipCheckoutStatusView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: 'success'
        }
    }
    async componentDidMount() {
        this.props.nextScreen()
    }

    render() {
        let { membershipDetails } = this.props
        let { status } = this.state
        // let detailsForNextScreen={
        //     membershipPlan:"monthly",
        //     taxs:plans.vat,
        //     plan:"plan"
        //   }
        return (
            <View
                style={styles.container}>
                <Header
                    onNotification={() => {
                        this.props.navigation.navigate("Notification")
                    }}
                    onBack={() => {
                        this.props.navigation.goBack()
                    }} style={{ width: '100%', zIndex: 16 }} />
                <Card style={styles.successCard}>
                    <View style={{ marginTop: getHeight(35) }}>
                        <Icons name={status == 'success' ? 'doneGreen' : 'cancel'} width={getWidth(42)} height={getWidth(42)}  />

                    </View>
                    <_Text style={styles.statusText1}>
                        {status == 'success' ? _string("thankYouForYourPurchase")
                            :_string("SorryWeDidntReceived")}
                    </_Text>
                    <_Text style={styles.statusText2()}>
                        {status == 'success' ? `${_string("YourAreNowOn")} ${membershipDetails.membershipPlan}`
                            : _string("TryAgainOrTryAnotherPaymentMethod")}
                    </_Text>

                </Card>
                <Button
                    onPress={() => {
                        this.props.navigation.navigate("Home")
                    }}
                    iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                    text={status == 'success' ? _string("BackToHome") : _string("TryAgain")}
                    iconName={status == 'success' ? 'next' : 'redirect'}
                    style={status == 'success' ? styles.blueButton() : styles.normalButton()} />



                <View style={styles.contentStyle}>
                    <HeaderSection
                    iconSize={getWidth(18)}
                        textStyle={{
                            fontFamily: 'bold',
                            fontSize: 'size15',
                            color: 'darkGray1',
                            paddingHorizontal: getWidth(5)
                        }}
                        style={
                            {
                                width: Width,
                                height: getHeight(20),
                                flexDirection: 'row',
                                alignItems: 'center',
                                backgroundColor: 'white',
                                justifyContent: 'space-between',
                                paddingHorizontal: getWidth(15)
                            }
                        }
                        text={_string("OrderDetails")} iconName='list'
                    />
                    <Card style={styles.invoiceCard}>
                        <InvoiceItem
                            text1={_string("MembershipPlan")}
                            text2={membershipDetails.membershipPlan}
                            Top Bottom Left Right
                        />
                        <InvoiceItem
                            text1={_string('PriceBeforeTaxs')}
                            text2=  {`${membershipDetails.currency} ${Math.floor((membershipDetails.price*membershipDetails.numberOfMonths)*10)/10} / ${membershipDetails.plan}`}
                            Bottom Left Right
                        />
                        <InvoiceItem
                            text1={_string('Taxs')}
                            text2={`${membershipDetails.taxs}%`}
                            Left Right
                        />
                        <InvoiceItem
                            text1={_string('totalPrice')}
                            text2={`${membershipDetails.currency} ${Math.floor((membershipDetails.price*membershipDetails.numberOfMonths+((membershipDetails.price) * (membershipDetails.numberOfMonths) * membershipDetails.taxs) / 100)*10 )/10} / ${membershipDetails.plan}`}
                            colors='blue'
                        />
                        {/* {`${plans.currency} ${((plans.price) * numberOfMonths) + ((plans.price) * (numberOfMonths) * plans.vat) / 100} / ${plan.name}`} */}
                    </Card>

                </View>

            </View>
        );
    }
}
const InvoiceItem = ({ text1, text2, Top, Bottom, Left, Right, colors }) => {
    return (
        <Card style={styles.invoiceContainer(Top, Bottom, Left, Right, colors)}>
            <View style={styles.labelView(colors)}>
                <_Text
                    style={styles.textStyle(colors, ColorsList.rgbaGray(.7))} >
                    {text1}
                </_Text>
            </View>
            <View style={styles.resultView}>
                <_Text
                    style={styles.textStyle(colors, 'darkGray1')} >
                    {text2}
                </_Text>
            </View>
        </Card>
    )
}

MembershipCheckoutStatusView.propTypes = {
    nextScreen: PropTypes.func
};

export default MembershipCheckoutStatusView;
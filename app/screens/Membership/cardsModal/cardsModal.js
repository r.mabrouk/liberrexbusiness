import React, { Component } from 'react';
import {
    View, FlatList
} from 'react-native';
import styles from './styles';
import { Button } from '../../../components/button/button';
import { _Text } from '../../../components/text/text';
import { Card } from '../../../components/card/card';
import { Icons } from '../../../config';
import { getHeight, getWidth, Width } from '../../../components/utils/dimensions';
import { PopupModal } from '../../../components/popupModal/popupModal';
import { _string, getlanguage } from '../../../local';
import { CheckBox } from '../../../components/checkBox/checkBox';
import { ColorsList } from '../../../components/PropsStyles';
import { LoaderLists } from '../../../components/LoaderList/loaderLists';
class CardsModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: props.language,
            index: props.planIndex,
            card: {

            }
        };
    }
    onAnimated = (state) => {
        this.cardsModal.onAnimated(state)
    }
    render() {
        let { onSelectCard,
            data,loadingGetPaymentCards
        } = this.props
        return (
            <PopupModal
                height={getHeight(330)}
                duration={500}
                zIndex={30}
                ref={_cardsModal => this.cardsModal = _cardsModal}>
                <Card style={styles.popupContainer}>
                    <View style={styles.popupHeader}>
                        <Icons name='paymentCardBlue' width={getWidth(28)} height={getWidth(28)} />
                        <_Text
                            style={styles.popupHeaderText}>{_string("payments.payment_methods").toLocaleUpperCase()}</_Text>
                    </View>
                    <FlatList
                        extraData={this.props}
                        style={{ width: Width }}
                        contentContainerStyle={{ alignItems: 'center', width: '100%' }}
                        data={data}
                        renderItem={({ item, index }) => {
                    
                            return (
                                <CardItem
                                    // brand={"Ddd"}
                                    brand={`${item.card.brand} ${_string("EndingIn")} ${item.card.last4}`}
                                    onChecked={() => {
                                        this.setState({ card: item, index }, () => {
                                        })
                                    }}
                                    checked={index == this.state.index} />
                            )
                        }} 
                        
                        ListFooterComponent={()=>{
                            return(
                                <LoaderLists isLoading={loadingGetPaymentCards} />
                            )
                        }}
                        />
                    <Button
                        isLoading={false}
                        iconName="done"
                        text={_string("app_settings.save")}
                        style={styles.popupDonebtn}
                        iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                        onPress={() => {
                            onSelectCard(this.state.card, this.state.index)
                            this.onAnimated()
                        }}
                    />
                </Card>
            </PopupModal>
        );
    }
}


const CardItem = ({ checked, onChecked, brand }) => {
    return (
        <Button
            onPress={onChecked}
            style={{
                width: Width,
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: getHeight(15),
                paddingHorizontal: getWidth(15),
                borderBottomWidth: 1,
                borderColor: ColorsList.rgbaBlack(0.04)
            }}>
            <View style={{flexDirection:'row',alignItems:'center'}}>
            <Icons name={"paymentCardBlue"} color='gray' width={getWidth(18)} height={getWidth(18)} />

                <_Text style={{ fontSize: 'size13', fontFamily: 'bold', color: 'darkGray1', paddingHorizontal: getWidth(10) }}>{brand}</_Text>
            </View>
            <CheckBox
                onChecked={onChecked}
                checked={checked} />
        </Button>
    )
}

export default CardsModal




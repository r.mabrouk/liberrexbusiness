import { getHeight, getWidth } from "../../../components/utils/dimensions";

export default Styles = {
    popupContainer: {

        width: '100%',
        alignItems: 'center',
        height: getHeight(300)
    },
    popupHeader: {
        flexDirection: 'row', width: getWidth(345),
        justifyContent: 'center', alignItems: 'center',
        marginTop: getHeight(20)
    },
    popupHeaderText: {

        marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily: 'bold',
    },
    popupHeaderText: {
        marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily: 'bold',
    },
    popupDonebtn: {
        colors: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(345),
        height: getHeight(44),
        borderRadius: 3,
        textColor: 'white',
        shadow: 6,
        marginTop: getHeight(15),
        fontFamily: 'bold',
        color: 'white',
        fontSize: 'size15',  position: 'absolute',
        top:getHeight(240),
        left:getWidth(15)
    },
}
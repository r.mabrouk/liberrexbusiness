import React, { Component } from 'react';
import { connect } from 'react-redux';
import MembershipView from './membershipView';
import { getMembershipPlansRequest, setPlanSelectedAction } from '../../../actions/membership';

class Membership extends Component {
  constructor(props) {
    super(props);
  }


  componentWillMount = () => {
    this.props._getMembershipPlans()
  }
  onSubscribe = (plan) => {
    this.props.navigation.navigate("MembershipCheckout",{plan})
  }
  setPlanSelected=(index)=>{
    this.props._setPlanSelected(index)
  }
  render() {
    let width = this.props.width
    return (
      <MembershipView
        {...this}
        {...this.state}
        {...this.props}
        nextScreen={this.nextScreen}
      />
    );
  }
}
function mapStateToProps(state) {
  return {
    membershipPlans: state.membershipReducer.membershipPlans,
    loadingteamMembershipPlans: state.membershipReducer.loadingteamMembershipPlans,
    user_profile: state.authReducer.user_profile,
    selectedPackagesIndex:state.membershipReducer.selectedPackagesIndex

  };
}
function mapDispatchToProps(Dispatch) {
  return {
    _getMembershipPlans: () => Dispatch(getMembershipPlansRequest()),
    _setPlanSelected: (index) => Dispatch(setPlanSelectedAction(index)),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Membership);

import React, { Component } from 'react';
import {
    View,
    ScrollView,
    Image, Animated
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Header } from '../../../components/header/header';
import { _Text } from '../../../components/text/text';
import { HeaderSection } from '../../../components/headerSection/headerSection';
import { Width, getWidth, getHeight } from '../../../components/utils/dimensions';
import { Card } from '../../../components/card/card';
import { Button } from '../../../components/button/button';
import { Icons, images } from '../../../config';
import { _string } from '../../../local';
import ApiConstants from '../../../api/ApiConstants';
import { LoaderLists } from '../../../components/LoaderList/loaderLists';

let { planText } = require("../../../config/LocalDatabase")

class MembershipView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: 0
        }
    }
    async componentDidMount() {
        this.props.nextScreen()
    }
    componentWillMount = () => {

    }


    render() {
        let { selected } = this.state
        let { membershipPlans, loadingteamMembershipPlans, user_profile, onSubscribe, selectedPackagesIndex } = this.props
        let { user, business } = user_profile

        console.log("membershipPlans", membershipPlans)
        return (
            <View
                style={styles.container}>
                <Header onBack={() => {
                    this.props.navigation.goBack()
                }} style={{ width: '100%', zIndex: 16 }} />

                <HeaderSection
                    onPress={() => {
                        this.props.navigation.navigate('QueueEdit')
                    }}
                    iconSize={getWidth(28)}
                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}
                    text={_string("member_ship.membership_plans")}
                    iconName='membership'
                />

                <View style={styles.profileHeaderView}>
                    <Card style={styles.profileHeaderContent}>
                        <Card style={styles.imageContainer}>
                            <Image
                                source={!user.photo ? images.user_profile : { uri: ApiConstants.IMAGE_BASE_URL + user.photo }}
                                style={{
                                    width: '100%',
                                    height: '100%',
                                    borderRadius: getWidth(43 / 2)
                                }}
                                resizeMode="cover" />
                        </Card>
                        <View style={{
                            width: getWidth(240),
                            height: getHeight(46),
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: getHeight(15)
                        }}>
                            <_Text
                                style={styles.headerText('size13', 'medium')} >
                                {_string("Hi")} {user.fname + " " + user.lname}, {_string("NowYouAreOnThe")}
                                    <_Text style={styles.headerText('size15', 'bold')}>
                                    {`“${_string("FreePlan")}”`}
                                    </_Text>
                                {25} {_string("DaysRemaining")}
                                </_Text>
                        </View>
                 
                    </Card>
                    <Button
                        onPress={() => onSubscribe(membershipPlans[selectedPackagesIndex])}
                        style={styles.changeButton}
                        iconStyle={{ width: getWidth(12), height: getHeight(12) }}

                        text={_string("member_ship.change_your_plan")}
                        iconName='editpen'
                    />
                </View>

                {!loadingteamMembershipPlans && membershipPlans.length >= 2 && <Card style={styles.membershipCard}>

                    <View style={styles.buttonsView}>

                        {membershipPlans.map((item, index) => {

                            return (
                                <PlanButton
                                    onPress={() => this.selectPlan(index)}
                                    text1={item.name}
                                    text2={item.price_display + '/'+_string("month")}
                                    index={index}
                                    selected={selectedPackagesIndex}
                                />
                            )
                        })}

                    </View>

                    <View style={styles.planTextView}>
                        {Array(5).fill().map((item, index) => {
                            let option = membershipPlans[selectedPackagesIndex]["option" + (index + 1)]
                            return (
                                option != "" ? <TextItem
                                    text={option}
                                    index={index} /> : null
                            )
                        })
                        }
                    </View>



                    <Button
                        onPress={() => onSubscribe(membershipPlans[selectedPackagesIndex])}
                        iconStyle={{ width: getWidth(18), height: getHeight(18) }}
                        text={_string("member_ship.subscribe") +
                            ' (' + membershipPlans[selectedPackagesIndex].price_display + '/'+ _string("month")+')'}
                        iconName='done'
                        style={styles.blueButton()} />

                </Card>}
                <LoaderLists isLoading={loadingteamMembershipPlans} />


            </View>
        );
    }
    selectPlan = (index) => {
        this.props.setPlanSelected(index)
        this.setState({ selected: index })
    }
}

const PlanButton = ({ index, text1, text2, selected, onPress }) => {
    return (
        <Button style={selected == index ? styles.planBlueButton : styles.planNormalButton}
            onPress={onPress}>
            <View style={styles.planButtonsView}>
                <_Text
                    style={styles.planText1(selected, index)} >
                    {text1}
                </_Text>
                <_Text
                    style={styles.planText2(selected, index)} >
                    {text2}
                </_Text>
            </View>
        </Button>
    )
}

const TextItem = ({ text, index }) => {
    return (
        <View style={styles.textView(index, planText.length - 1)}>
            <_Text
                style={styles.planeText} >
                {text}
            </_Text>

        </View>
    )
}

MembershipView.propTypes = {
    nextScreen: PropTypes.func
};

export default MembershipView;
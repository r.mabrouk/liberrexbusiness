import React, { Component } from 'react';
import {
    View
} from 'react-native';
import styles from './styles';
import { Button } from '../../../components/button/button';
import { _Text } from '../../../components/text/text';
import { Card } from '../../../components/card/card';
import { Icons } from '../../../config';
import { getHeight, getWidth } from '../../../components/utils/dimensions';
import { PopupModal } from '../../../components/popupModal/popupModal';
import { _string, getlanguage } from '../../../local';
import { CheckBox } from '../../../components/checkBox/checkBox';
import { ColorsList } from '../../../components/PropsStyles';
class PlansModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: props.language,
            index: props.planIndex,
            card:{
                
            }
        };
    }
    onAnimated = (state) => {
        this.cardsModal.onAnimated(state)
    }
    render() {
        let { onSelectPlan,
            data
        } = this.props
        return (
            <PopupModal
                height={getHeight(250)}
                duration={500}
                zIndex={30}
                ref={_cardsModal => this.cardsModal = _cardsModal}>
                <Card style={styles.popupContainer}>
                    <View style={styles.popupHeader}>
                        <Icons name='gridLinear' width={getWidth(28)} height={getWidth(28)} />
                        <_Text
                            style={styles.popupHeaderText}>{_string("your_service.add_a_new_service").toLocaleUpperCase()}</_Text>
                    </View>
                    <View style={{ width: '100%' }}>

                        {data.map((item, index) => {
                            return (
                                <PlanItem
                                    brand={item.name}
                                    onChecked={() => {
                                        this.setState({ card: item, index }, () => {
                                        })
                                    }}
                                    checked={index == this.state.index} />
                            )
                        })}



                    </View>
                    <Button
                        isLoading={false}
                        iconName="done"
                        text={_string("app_settings.save")}
                        style={styles.popupDonebtn}
                        iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                        onPress={() => {
                            onSelectPlan(this.state.card,this.state.index)
                            this.onAnimated()
                        }}
                    />
                </Card>
            </PopupModal>
        );
    }
}


const PlanItem = ({ checked, onChecked, brand }) => {
    return (
        <Button
            onPress={onChecked}
            style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: getHeight(15),
                paddingHorizontal: getWidth(15),
                borderBottomWidth: 1,
                borderColor: ColorsList.rgbaBlack(0.04)
            }}>
            <CheckBox
                text={brand}
                onChecked={onChecked}
                checked={checked} />
        </Button>
    )
}

export default PlansModal
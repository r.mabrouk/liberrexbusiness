import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text, ScrollView,
    TextInput
} from 'react-native';
import styles from './styles';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { images, metrics, Icons } from '../../config';
import { getHeight, getWidth, Width } from '../../components/utils/dimensions';
import ServiceCard from '../../components/ServiceCard/ServiceCard';
import { Button } from '../../components/button/button';
import DatePicker from 'react-native-date-picker';
import { PopupModal } from '../../components/popupModal/popupModal';
import TimeModal from './TimeModal';
import { Calendar } from '../../components/calendar/src';
import CalendarStrip from '../../components/calendar/weekly/CalendarStrip';
import CalendarModeModal from '../AppointmentsApproved/calendarModeModal';
import { _string } from '../../local';
import ServiceModal from '../QueueEdit/ServiceModal';
import ButtonWithHeaderSection from '../../components/buttonWithHeaderSection/buttonWithHeaderSection';
import moment from 'moment';
export default class NewAppointmentView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            start: '9:00 am',
            end: '9:00 pm',
            startdate: new Date(),
            date: new Date(),
            calendarMode: _string("modals.report_by.monthly"),
        }
    }
    onChangeCalnderMode = () => {
        if (this.state.calendarMode == _string("modals.report_by.weekly"))
            this.setState({ calendarMode: _string("modals.report_by.monthly") })
        else
            this.setState({ calendarMode: _string("modals.report_by.weekly") })
    }


    render() {
        let { calendarMode } = this.state
        let {
            selectedDate,
            startTime,
            endTime,
            _startTime,
            _endTime
            , displayStartTime,
            displayEndTime,
            selectedCustomer,
            onDateSelected,
            selectedDay,
            onAddBooking,
            onTimeChange,
            loadingCreateBooking,
            isUpdate,
            onUpdateBooking
        } = this.props

        let customerName = _string("SelectedYourCustomer")
        if (selectedCustomer.fname)
            customerName = `${selectedCustomer.fname} ${selectedCustomer.lname}`

        return (
            <View
                style={styles.container}>



                <Header onNotification={() => this.props.navigation.navigate("Notification")} onBack={() => this.props.navigation.goBack()} style={{ width: '100%', zIndex: 16 }} />

                <HeaderSection
                    onPress={() => {

                        if (isUpdate)
                            onUpdateBooking()
                        else
                            onAddBooking()


                    }}
                    iconSize={getWidth(28)}
                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}
                    isLoading={loadingCreateBooking}
                    withButton
                    textButton={_string(isUpdate ? "appointments_approved.edit" : "appointment.add")}
                    text={_string("appointment.new_appointments")}
                    iconName='appointmentsgray'
                    iconButton='done'
                />
                <ScrollView>

                    <Card style={styles.calendarContainer}>
                        {calendarMode == _string("modals.report_by.monthly") ? <CalendarStrip
                            selectedDate={selectedDay}
                            onDateSelected={(date) => {
                                onDateSelected(moment(date).format("YYYY-MM-DD"))
                            }}
                            calendarType={calendarMode}
                            onChangeCalnderMode={this.onChangeCalnderMode}
                            daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: 'white' }}
                            showDate
                            style={{ width: Width, height: getHeight(131), paddingTop: getHeight(15) }}

                        /> :
                            <Calendar
                                calendarType={calendarMode}
                                onDayPress={(date, state) => {
                                    if (state != "disabled")
                                        onDateSelected(date.dateString)
                                }}
                                onChangeCalnderMode={this.onChangeCalnderMode}
                                headerStyles={{ width: getWidth(190) }}
                                ref={(refd) => this.CalendarRef == refd}
                                theme={{

                                }}
                                markedDates={{
                                    [selectedDay]: { selected: true, selectedColor: 'white' },
                                }}
                                disableMonthChange={true}
                                style={{
                                    width: '100%',
                                }}
                            />}
                        {/* {calendarMode == 'Weekly' ?
                            <CalendarStrip
                            showMonth={true}
                                onDateSelected={onDateSelected}
                                calendarType={calendarMode}
                                onChangeCalnderMode={this.onChangeCalnderMode}
                                daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: 'white' }}

                                // {...selectedDate}
                                showDate
                                style={{ width: Width, height: getHeight(131), paddingTop: getHeight(15) }}
                            //  customDatesStyles={{width:Width}}

                            /> :
                            <Calendar
                            
                                calendarType={calendarMode}

                                onChangeCalnderMode={this.onChangeCalnderMode}
                                headerStyles={{ width: getWidth(190) }}
                                ref={(refd) => this.CalendarRef == refd}
                                theme={{

                                }}

                                disableMonthChange={true}
                                dayComponent={({ date, state }) => {
                                    console.log("statestsdfsdfsdatestate", date.dateString)
                                    return (<Button
                                        onPress={() => {
                                            if (state != "disabled")
                                                onDateSelected(date.dateString)
                                        }}
                                        text={date.day}
                                        style={{ width: 30, height: 30, borderRadius: 15, colors: date.dateString == selectedDay ? 'blue' : undefined, textColor: date.dateString == selectedDay ? 'white' : state == 'disabled' ? '#E7E7E7' : 'black' }}
                                    >
                                    </Button>)
                                }}

                                style={{
                                    width: '100%',
                                }}
                            />} */}
                    </Card>

                    <ButtonWithHeaderSection
                        marginTop={getHeight(15)}
                        headerIcon='clockTime'
                        headerText={_string("appointment.appointment_time")}
                        onPress={() => {
                            this.AddTime.onAnimated(true)
                        }}
                        numberOfLines={1}
                        value={`${_string("From")} ${displayStartTime} : ${displayEndTime}`}
                    />
                    <ButtonWithHeaderSection
                        headerIcon='userGray'
                        headerText={_string("appointment.appointment_for")}
                        onPress={() => {
                            this.props.navigation.navigate("NewAppointmentCustomer", { selectCustomer: true })
                        }}
                        numberOfLines={1}
                        value={customerName}
                    />

                    <ButtonWithHeaderSection
                        headerIcon='grid'
                        headerText={_string("AppointmentService")}
                        onPress={() => {
                            this.selectService.onAnimated(true)
                        }}
                        numberOfLines={1}
                        value={this.props.selectedServicesNames.length > 0 ? this.props.selectedServicesNames.toString() : _string("SelectService")}
                    />


                </ScrollView>
                <TimeModal
                startDate={this.props.startDate}
                endDate={this.props.endDate}
                    ref={(time) => { this.AddTime = time }}
                    onDonePress={this.props.onTimeChange}
                />
                <ServiceModal
                    servicesList={this.props.servicesList}
                    selectedServices={this.props.selectedServices}
                    selectedServicesNames={this.props.selectedServicesNames}
                    loadingServicesList={this.props.loadingServicesList}
                    onServicesSearch={this.props.onServicesSearch}
                    onEndReachedServiceList={this.props.onEndReachedServiceList}
                    onServiceAdd={(selectedServicesID, selectedServicesNames) => {
                        this.props.servicesSelected(selectedServicesID, selectedServicesNames)
                        this.selectService.onAnimated()
                    }}
                    ref={(selectService) => this.selectService = selectService} />
                <CalendarModeModal onChangeMode={(mode) => {
                    this.setState({ calendarMode: mode })
                }} ref={(_CalendarModeModal) => this.calendarModeModal = _CalendarModeModal} />
            </View>
        );
    }
}


import React, { Component } from 'react';
import {
    View,

} from 'react-native';
import styles from './styles';
import { _Text } from '../../components/text/text';
import { Card } from '../../components/card/card';
import { getHeight, getWidth } from '../../components/utils/dimensions';
import { Button } from '../../components/button/button';
import DatePicker from 'react-native-date-picker';
import { PopupModal } from '../../components/popupModal/popupModal';
import { Icons } from '../../config';
import { _string } from '../../local';
import moment from 'moment'
import { _Toast, ModalToast } from '../../components/toast/toast';
export default class TimeModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startdate: props.startDate,
            date: props.endDate,
        };
    }
    
    onAnimated = (state) => {
        this.AddTime.onAnimated(state)
    }
    componentWillReceiveProps = (props) => {
        let from = props.startDate
        let to = props.endDate

        if (new Date().getTimezoneOffset() == -60) {
            from = new Date(new Date(props.startDate).getTime() - 3600000)
            to = new Date(new Date(props.endDate).getTime() - 3600000)
        }
        this.setState({ startdate: from, date: to })
    }
    render() {

        return (

            <PopupModal

                inputDuration={0}

                inputHeight={getHeight(0)}
                duration={500} ref={(Add) => this.AddTime = Add} height={getHeight(264)}>
                <Card style={{
                    height: '100%', width: '100%',
                    alignItems: 'center', padingBottom: getHeight(10),

                }}>
                    <Card style={{
                        flexDirection: 'row', width: getWidth(375), justifyContent: 'center',
                        alignItems: 'center', marginTop: getHeight(20),
                    }}>
                        <Icons name='clockTime' width={getWidth(18)} height={getWidth(18)} />
                        <_Text style={{
                            fontSize: 'size15', fontFamily: 'bold',
                            color: 'darkGray1', marginLeft: getWidth(5),
                        }}>{this.props.title ? this.props.title : _string("appointment.appointment_time")}</_Text>
                    </Card>


                    <View style={{
                        flexDirection: 'row', justifyContent: 'center',
                        alignItems: 'center', marginTop: getHeight(10)
                    }}>
                        <View style={{ width: getWidth(187.5), justifyContent: 'center', alignItems: 'center' }}>
                            <DatePicker
                                minuteInterval={15}
                                locale='fr'
                                style={{ width: getWidth(187.5), height: getHeight(140) }}
                                date={this.state.startdate}
                                onDateChange={startdate => {

                                    console.log(new Date().getTimezoneOffset() * 60 * 1000, "dsdsdmnsfndsmfnmdf")

                                    this.setState({ startdate, date: new Date((new Date(startdate).getTime() + 900000)) })
                                }}
                                mode={'time'}
                            />
                        </View>
                        <_Text style={{
                            color: 'lightBlue', fontSize: 'size15', fontWeight: 'bold',
                            justifyContent: 'center',
                            alignItems: 'center', position: 'absolute',
                            width: getWidth(20)
                        }}>{_string("appointment.to")}</_Text>
                        <View style={{ width: getWidth(187.5), justifyContent: 'center', alignItems: 'center' }}>
                            <DatePicker

                                minuteInterval={15}
                                locale='fr'
                                style={{ width: getWidth(187.5), height: getHeight(140) }}
                                date={this.state.date}
                                onDateChange={date => {

                                    let startTime = moment(this.state.startdate).diff(date)
                                    if (startTime >= 0)
                                        this.setState({ startdate: date, date: new Date((new Date(date).getTime() + 900000)) })
                                    else
                                        this.setState({ date })
                                    console.log(new Date().getTimezoneOffset(), "dsdsdmnsfndsmfnmdf")
                                    // let endTime=new Date(date).getTime()
                                    // if (startTime<endTime)
                                    // this.setState({startdate:date,date})
                                    // else
                                    // this.setState({ date })

                                }}
                                mode={'time'}
                            />
                        </View>
                    </View>
                    <Button
                        iconStyle={{ width: getWidth(18), height: getHeight(18) }}
                        iconName="done"
                        text={_string("appointment.done")}
                        style={styles.donebtn}
                        onPress={() => {
                            this.AddTime.onAnimated()
                            if (this.props.onDonePress) {
                                let from = this.state.startdate
                                let to = this.state.date
                                if (new Date().getTimezoneOffset() == -5000) {
                                    from = new Date(new Date(this.state.startdate).getTime() + 3600000)
                                    to = new Date(new Date(this.state.date).getTime() + 3600000)
                                }
                                this.props.onDonePress({ from: from, to: to })
                            }
                        }}
                    />


                </Card>

            </PopupModal>
        );
    }
}

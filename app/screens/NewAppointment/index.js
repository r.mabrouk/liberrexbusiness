import React, { Component } from 'react';
import { View, Text } from 'react-native';
import NewAppointmentView from './NewAppointmentView';
import { connect } from 'react-redux'
import moment from 'moment'
import { getAllServicesRequest } from '../../actions/services';
import { createBookingRequest, updateBookingRequest } from '../../actions/booking';
import { _Toast } from '../../components/toast/toast';
import { _string } from '../../local';
class NewAppointment extends Component {
  constructor(props) {
    super(props);


    this.state = {
      DaysOfTheWeek: "time",
      selectedServices: [],
      selectedServicesNames: [],
      servicesListkeyword: "",
      selectedDate: new Date(),
      displayStartTime: "00:00:00",
      displayEndTime: "00:00:00",
      startTime: "",
      endTime: "",
      _startTime: "00",
      _endTime: "00",
      customerName: _string("SelectYourCustomer"),
      selectedDay: moment().format("YYYY-MM-DD"),
      isUpdate:props.navigation.getParam("isUpdate"),
      startDate:new Date(moment().format("YYYY-MM-DDTHH:00:00")) ,
      endDate: new Date(moment().format("YYYY-MM-DDTHH:00:00")),
    };
  }

  
  componentDidMount =async () => {
    this.setBookingDate()
  }
  selectServices=async(services)=>{
    console.log("componentDidMountcomponentDidMountcomponentDidMount",services)

    let selectedServices=[]
    await services.filter((element)=>{
      selectedServices.push(element.id)
    })
    this.setState({selectedServices:selectedServices})

    console.log("servicesservices",selectedServices)

  }
  setBookingDate = () => {
    let customer = this.props.navigation.getParam("customerData")
    let isUpdate = this.props.navigation.getParam("isUpdate")
    if (isUpdate) {
      this.setState({
        displayStartTime: moment(customer.start_datetime).format("HH:mm"),
        displayEndTime: moment(customer.end_datetime).format("HH:mm"),
        startTime: moment(customer.start_datetime).format("HH:mm"),
        endTime: moment(customer.end_datetime).format("HH:mm"),
        selectedDay: moment(customer.start_datetime).format("YYYY-MM-DD"),
        selectedServicesNames:customer.service_names?customer.service_names:"",
        startDate:new Date(moment().format("YYYY-MM-DDT"+customer.start_datetime.split(" ")[1])),
        endDate:new Date(moment().format("YYYY-MM-DDT"+customer.end_datetime.split(" ")[1])),
      })
      this.selectServices(customer.services_array)
    }
  }

  validateNewBookingForm = () => {
    let { selectedCustomer } = this.props
    if (!this.state.startTime) {
      _Toast.open(_string("PleaseSelectTheBookingTime"), "w")
      return false
    }
    else if (!selectedCustomer.fname) {
      _Toast.open(_string("PleaseSelectTheCustomer"), "w")
      return false
    }

    else if (this.state.selectedServices.length < 1) {
      _Toast.open(_string("SelectTheServices"), "w")
      return false
    }
    else {
      return true
    }
  }

  servicesSelected = (selectedServicesID, selectedServicesNames) => {
    console.log('QueueEdit', 'servicesSelected', 'selectedServicesID', selectedServicesID, 'selectedServicesNames', selectedServicesNames)
    this.setState({ selectedServices: selectedServicesID })
    this.setState({ selectedServicesNames })
  }
  onDateSelected = (date) => {
    this.setState({selectedDay:date})
  }

  onTimeChange = (time) => {
    

    this.setState({
      displayStartTime: moment(time.from).format("HH:mm"),
      displayEndTime: moment(time.to).format("HH:mm"),
      startTime: moment(time.from).format("HH:mm"),
      endTime: moment(time.to).format("HH:mm"),
      startDate:new Date(moment().format("YYYY-MM-DDT" + time.from.toString().split(" ")[4])),
      endDate:new Date(moment().format("YYYY-MM-DDT" + time.to.toString().split(" ")[4]))
    })

    console.log(moment().format("YYYY-MM-DD")+"timetimetimetimetimetimetime")

  }
  render() {
    return (
      <NewAppointmentView {...this} {...this.state} {...this.props} />
    );
  }

  returnBookingData = () => {

    let { selectedCustomer } = this.props
    let { startTime, endTime, selectedDay, selectedServices } = this.state
    let start = `${selectedDay} ${startTime.split(" ")[0]}:00`
    let end = `${selectedDay} ${endTime.split(" ")[0]}:00`
    return {
      customer_id: selectedCustomer.id,
      start_datetime: start,
      end_datetime: end,
      services: selectedServices.toString()
    }
  }
  onUpdateBooking = () => {
    console.log(this.returnBookingData(),"returnBookingData")

    let index=this.props.navigation.getParam("index")
    let customer=this.props.navigation.getParam("customerData")
    if (this.validateNewBookingForm()) {
      this.props._updateBooking(this.returnBookingData(),customer.id,index)
    }
  }
  onAddBooking = () => {

    if (this.validateNewBookingForm()) {
      this.props.createBooking(this.returnBookingData())
    }
  }
  componentWillMount = () => {
    this.props._getAllServices(true, null, 1)
  }

  onRefreshServiceList = () => {
    this.setState({ refreshing_list: true })
  }

  onEndReachedServiceList = () => {
    let { servicesListkeyword } = this.state
    let { services_current_page, services_last_page, loadingServicesList } = this.props
    if (services_current_page < services_last_page) {
      this.props._getAllServices(null, servicesListkeyword ? servicesListkeyword : null, services_current_page + 1)
    }
  }
  onServicesSearch = (keyword) => {
    this.props._getAllServices(true, keyword, 1)
    this.setState({ servicesListkeyword: keyword })
  }


}


function mapStateToProps(state) {
  return {
    services_current_page: state.servicesReducer.services_current_page,
    services_last_page: state.servicesReducer.services_last_page,
    servicesList: state.servicesReducer.servicesList,
    loadingServicesList: state.servicesReducer.loadingServicesList,
    selectedCustomer: state.customersReducer.selectedCustomer,
    loadingCreateBooking: state.bookingReducer.loadingCreateBooking,
  }
}
function mapDispatchToProps(Dispatch) {
  return {
    _getAllServices: (reset, keyword, page) => Dispatch(getAllServicesRequest(reset, keyword, page)),
    createBooking: (data) => Dispatch(createBookingRequest(data)),
    _updateBooking: (data, id, index) => Dispatch(updateBookingRequest(data, id, index)),

  }
}


export default connect(mapStateToProps, mapDispatchToProps)(NewAppointment)


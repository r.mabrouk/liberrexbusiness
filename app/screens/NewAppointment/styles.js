import { StyleSheet } from 'react-native';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        width: Width, height: Height,
        // alignItems: 'center',
        backgroundColor: ColorsList.white
    },
    logo: {
        width: Width * .8,
        height: Height * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: '100%',
        height: '100%',

    },

    headerSection: {
        backgroundColor: 'white', height: getHeight(67),
        width: getWidth(375),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: Width * .05,
        shadow: 5, top: getHeight(80),
        position: 'absolute',
        paddingTop: getHeight(13)
    },
    calendarContainer:
    {
        width: '100%',
        backgroundColor: ColorsList.white,
        shadow: 5,
    },


    Service:(paddingTop)=>( {
        flexDirection: 'row',
        width: Width,
        paddingHorizontal:getWidth(15),
        justifyContent: 'space-between',
        paddingTop:paddingTop,
       paddingBottom:getHeight(10)
    }),
    options: {
        width: getWidth(350),
        marginTop: getHeight(10),
        height: getHeight(75),
        justifyContent: 'center',
        alignItems: 'center',




    },
    time: {
        fontSize: 'small1',
        fontWeight: '500',
        color: 'darkGray',
        width: '95%'
    },
    optionHeader: {
        flexDirection: 'row', width: getWidth(345)
    },
    optionTitle: {
        fontSize: 'medium15Px',
        fontWeight: 'bold',
        marginLeft: 5,
        color: 'darkGray'
    },
    selectCustomerOption:
    {
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(345),
        height: getHeight(41),
        backgroundColor: 'white',
        shadow: 4,
        borderRadius: 3,
        marginTop: getHeight(10)
    },
    touchableButton:
    {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '90%',
        alignItems: 'center'
    },
    appointmentService:
    {
        marginTop: getHeight(10),
        width: '100%',
        justifyContent: 'center'
        , alignItems: 'center',
    },
    donebtn: {
        colors: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(345),
        height: getHeight(44),
        borderRadius: 3,
        textColor: 'white',
        fontSize: 'size15',
        marginTop:getHeight(10),
        fontFamily:'bold'
    }

};
export default styles;

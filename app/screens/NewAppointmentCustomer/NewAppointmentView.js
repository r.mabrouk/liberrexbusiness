import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text, ScrollView, FlatList,
    TextInput
} from 'react-native';
import styles from './styles';
import { Button } from '../../components/button/button';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { images } from '../../config';
import { Input } from '../../components/input/input';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput'
import HeaderButtons from '../../components/HeaderButtons/HeaderButtons';
import { getHeight, Width, getWidth, Height } from '../../components/utils/dimensions';
import CustomerCardWithGreenButton from '../../components/CustomerCardWithGreenButton/CustomerCardWithGreenButton';
import SwiperTabButtons from '../../components/swiperTabButtons/swiperTabButtons';
import { _string } from '../../local';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import EmptyData from '../../components/emptyData/emptyData';
class NewAppointmentCustomerView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            approve: true,
            pending: false,
            Popup: false,
            name: '',
            currentSelected: 0
        };
    }

    _Popup = () => {
        this.setState(
            {
                Popup: true,
                name: 'ACHRAF AMMAR'
            }
        )

    }

    render() {
        let { all_customers, onSelectCustomer, fname, lname, email, phone, onChangeText, onAddCustomer, isAddNewCustomerLoading,onCustomerSearch } = this.props
        return (
            <View
                style={styles.container}>
                <Header
                    onBack={() => this.props.navigation.goBack()}
                    style={{ width: '100%', zIndex: 16 }} />


                <HeaderSection
                    onPress={() => {
                        this.props.navigation.navigate('QueueEdit')
                    }}
                    iconSize={getWidth(28)}
                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}
                    // withButton
                    // textButton={'Queue1'}
                    text={_string("appointment.new_appointments")}
                    iconName='appointmentsgray'
                // iconButton={'arrowDown'}
                />

                <View style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: Width,
                }}>
                    <SwiperTabButtons tabButons={[
                        { iconsName: 'list', label: _string("appointment.customers_list") },
                        { iconsName: 'add', label: _string("appointment.new_customer") }]} >




                        <Card style={{ width: Width, alignItems: 'center' }}>


                            <ScrollView contentContainerStyle={{ height: getHeight(70), width: Width, alignItems: 'center' }}>
                                <Input
                                    onChangeText={onCustomerSearch}
                                    style={styles.searchInput}
                                    iconName={'search'}
                                    placeholder={_string("queue.search_by_name_email_phone")}
                                    iconStyle={{ width: getWidth(15), height: getHeight(15) }}

                                    verticalLine={false}


                                />
                            </ScrollView>


                            <ScrollView contentContainerStyle={{ height: Height, alignItems: 'center' }}>
                                <Card style={{
                                    alignItems: 'center', justifyContent: 'center',

                                    width: '100%',
                                }}>

                                    <EmptyData
                                        height={getHeight(260)}
                                        show={(all_customers.length < 1) && !this.props.isGetAllCustomersLoading} />
                                    <FlatList
                                        extraData={this.props}
                                        keyExtractor={item => item.id}
                                        // refreshing={isRefreshing}
                                        // onRefresh={() => this.setState({ isRefreshing: true })}
                                        onEndReached={() => this.props.get_customers()}
                                        onEndReachedThreshold={0.1}
                                        style={{ height: getHeight(400) }}
                                        data={all_customers}
                                        renderItem={({ item, index }) => {
                                            return (
                                                <CustomerCardWithGreenButton
                                                    onPress={() => onSelectCustomer(item)}
                                                    name={item.fname + " " + item.lname}
                                                    email={item.email}
                                                    onEditCustomer={() => this.AddCustomer.onAnimated(true)}
                                                />
                                            )
                                        }}
                                        ListFooterComponent={() => (
                                            <LoaderLists isLoading={this.props.isGetAllCustomersLoading} />
                                        )}
                                    />
                                </Card>

                            </ScrollView>
                        </Card>
                        {/* } */}

                        {/* {
                            this.state.currentSelected == 1 && */}
                        <ScrollView contentContainerStyle={{ marginTop: getHeight(15), alignItems: 'center' }}>

                            <CustomTextInput onChangeText={(text) => onChangeText(text, "fname")} value={fname} placeholder={_string("pleace_holder")} headerIcon='userGray' headerText={_string("appointment.full_name")} leftIcon='editpen' />
                            <CustomTextInput onChangeText={(text) => onChangeText(text, "lname")} value={lname} placeholder={_string("pleace_holder")} headerIcon='userGray' headerText={_string("sign_up.last_name")} leftIcon='editpen' />

                            <CustomTextInput onChangeText={(text) => onChangeText(text, "email")} value={email} placeholder={_string("pleace_holder")} headerIcon='email' headerText={_string("appointment.email")} leftIcon='editpen' />
                            <CustomTextInput onChangeText={(text) => onChangeText(text, "phone")} value={phone} KeyboardType='numeric' placeholder={_string("pleace_holder")} headerIcon='phone' headerText={_string("appointment.phone_number")} leftIcon='editpen' />
                            <Button
                                isLoading={isAddNewCustomerLoading}
                                onPress={onAddCustomer}
                                iconStyle={{ width: getWidth(18), height: getHeight(18), color: '#ffffff', marginBottom: 5 }}
                                text={_string("appointment.add_and_select_customer")}
                                iconName='done'
                                style={styles.addButton} />
                        </ScrollView>
                    </SwiperTabButtons>
                </View>


                {/* <PopupModal ref={(Add) => this.AddModel = Add} height={getHeight(290)}>
                    <Card style={{height:'100%',width:'100%',justifyContent:'center',
                    alignItems:'center',padingBottom:getHeight(10),
                    backgroundColor:'red'

                    }}>
                        <Card style={{ flexDirection: 'row', width: getWidth(375), alignItems: 'center', justifyContent: 'center' }}>
                            <Card style={{ width: getWidth(48) }}>
                                <Icons name='user' width={getWidth(28)} height={getWidth(28)} />
                            </Card>
                            <_Text style={styles.HeaderTitle}>ADD</_Text>
                            <_Text style={styles.name}>{" " + "Remon" + " "}</_Text>
                            <_Text style={styles.HeaderTitle}>TO QUEUE 1</_Text>
                        </Card>

                        <Card style={styles.appointmentService}>
                            <Card style={{ flexDirection: 'row', width: getWidth(345), marginTop: getHeight(5), marginBottom: getHeight(7), }}>
                                <Icons name='grid' width={getWidth(18)} height={getWidth(18)} />
                                <_Text style={styles.optionTitle}>
                                    Queue Service:</_Text>

                            </Card>
                            <Card style={styles.Service}>
                                <ServiceCard name={'Hair Cut'} style={styles.ServiceCard} />
                                <ServiceCard name={'Hair Cut'} style={styles.ServiceCard} />
                            </Card>
                            <Card style={styles.Service}>
                                <ServiceCard name={'Hair Cut'} style={styles.ServiceCard} />
                                <ServiceCard name={'Hair Cut'} style={styles.ServiceCard} />
                            </Card>
                        </Card>

                        <Button
                            iconStyle={{ color: '#fff', size: 25, marginBottom: 5, }}
                            text='Add Customer'
                            iconName='done'
                            style={styles.loginButton}
                            onPress={() => { this.AddModel.onAnimated() }}
                        />
                    </Card>
                </PopupModal> */}
            </View>
        );
    }
}



export default NewAppointmentCustomerView;

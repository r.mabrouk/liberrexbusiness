import React, { Component } from 'react';
import { View, Text } from 'react-native';
import NewAppointmentCustomerView from './NewAppointmentView'
import { connect } from 'react-redux'
import { getAllCustomersRequest, resetCustomersList, SelectCustomer, createCustomerRequest } from '../../actions/customers';
class NewAppointmentCustomer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fname: "",
      lname: "",
      email: "",
      phone: ""
    };
  }

  render() {
    return (
      <NewAppointmentCustomerView {...this} {...this.state} {...this.props} />
    );
  }
  onChangeText = (text, key) => {
    this.setState({ [key]: text })
  }
  onAddCustomer = () => {
    let { fname, lname, email, phone } = this.state
    let data = {
      fname: fname,
      lname: lname,
      email: email,
      lbrx_i: 1,
      phone_number: phone
    }
    this.props.createNewCustomer(data)
  }
  onSelectCustomer = (customer) => {
    this.props.SelectCustomer(customer)
    this.props.navigation.goBack()
  }
componentDidMount=()=>{
  this.props.getAllCustomers(true,false,1)
}
onCustomerSearch = (keyword) => {
  this.setState({ all_customers_keyword: keyword }, () => {
      this.props.getAllCustomers(true, keyword, 1)
  })
}

  get_customers = () => {
    let { all_customers_keyword } = this.state
    let { cuurent_page, pages, isGetAllCustomersLoading } = this.props
    console.log("cuurent_pagecuurent_page", cuurent_page, pages)
    if (!isGetAllCustomersLoading) {
        if (cuurent_page < pages) {
            this.props.getAllCustomers(false, all_customers_keyword ? all_customers_keyword : null, cuurent_page + 1)
        }
        this.setState({ isRefreshing: false })
    }

}


}


function mapStateToProps(state) {
  return {
    isAddNewCustomerLoading: state.loadingReducer.isAddNewCustomerLoading,
    all_customers: state.customersReducer.all_customers,
    isGetAllCustomersLoading: state.loadingReducer.isGetAllCustomersLoading,
    cuurent_page: state.customersReducer.cuurent_page,
    pages: state.customersReducer.pages,
    selectedCustomer: state.customersReducer.selectedCustomer,
  };
}
function mapDispatchToProps(Dispatch) {
  return {
    getAllCustomers: (reset, keyword,page) => Dispatch(getAllCustomersRequest(reset, keyword,page)),
    SelectCustomer: (customer) => Dispatch(SelectCustomer(customer)),
    createNewCustomer: (customer_data) => Dispatch(createCustomerRequest(customer_data)),

  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewAppointmentCustomer);













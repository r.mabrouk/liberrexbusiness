import { StyleSheet } from 'react-native';
import { metrics } from '../../config';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        width: Width,
        height: Height,
        backgroundColor: ColorsList.white
    },
    logo: {
        width: Width * .8,
        height: Height * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: '100%',
        height: '100%',
    },
    headerbtn: {
        backgroundColor: 'white',
        flexDirection: 'row-reverse',
        borderRadius: 4,
        shadow: 6,
        width: getWidth(98),
        height: getHeight(31),
        textColor: '#444',
        fontWeight: '500',
        fontSize: 'small13px',

    },


    headerSection: {
        backgroundColor: 'white',
        height: getHeight(67),
        width: Width,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: Width * .05,
        shadow: 6,
        backgroundColor: 'white',
        marginTop: -getHeight(30),
        paddingTop: getHeight(17),

        zIndex: 31
    },
    input: {


        width: '92%',
        height: getHeight(41),
        shadow: 2, backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: getHeight(-15),
        marginLeft: 15,

    },
    HeaderSectionContainer: {
        backgroundColor: 'white', shadow: 4,
        width: '100%', alignItems: 'center',
        justifyContent: 'center', marginTop: -getHeight(13)
    },

    searchInput: {
        borderRadius: 4,
        width: getWidth(345),
        height: getHeight(41),
        shadow: 6,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        //marginBottom: getHeight(10),
        marginTop: getHeight(15),
        zIndex: 30

    },
    selectedbtnContainer: {
        width: '100%',
        height: getHeight(44),
        marginBottom: getHeight(5),
        backgroundColor: 'white',
        // shadow: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },

    addButton: {
        width: getWidth(345),
        marginTop: getHeight(15),
        textColor: '#fff',
        shadow: 6,
        height: getHeight(44),
        colors: 'blue',
        borderRadius: 3,


    },
    name: {

        color: 'lightBlue',
        fontSize: 'medium15Px',
        fontWeight: 'bold',
    },
    HeaderTitle: {
        color: 'black',
        fontSize: 'medium15Px',
        fontWeight: 'bold',
    },
    HeaderIconContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: getWidth(7),
    },

    Service: (paddignTop) => ({
        flexDirection: 'row',
        // backgroundColor: 'white',
        width: Width,
        // height:100,
        paddingHorizontal: getWidth(15),

        paddingBottom: getHeight(10),
        paddingTop: paddignTop,
        justifyContent: 'space-between',
        //    backgroundColor:'green',
        alignItems: 'center',
    }),
    appointmentService: {
        // marginTop: getHeight(10),
        width: getWidth(375),
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'red'
    },
    optionTitle: {
        fontSize: 'medium15Px',
        fontWeight: 'bold',
        marginLeft: getWidth(7),
        color: 'darkGray',

    },
    loginButton: {
        width: getWidth(345),
        marginTop: getHeight(5),
        textColor: '#fff',
        shadow: 6,
        height: getHeight(44),
        colors: 'blue',
        borderRadius: 5,



    },
    choocies: {
        width: metrics.screenWidth,
        flexDirection: 'row', height: getHeight(44),
        justifyContent: 'space-around',
        alignItems: 'center',
        marginTop: getHeight(5)
    },
    popupHeader: {
        flexDirection: 'row', width: getWidth(345),
        height: getHeight(44),
        justifyContent: 'center', alignItems: 'center'
    },
    popupDonebtn: {
        colors: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(345),
        height: getHeight(44),
        borderRadius: 3,
        textColor: 'white',
        fontSize: 'medium15Px',
        marginTop: getHeight(18),
        marginBottom: getHeight(10)

    },
    popupContainer: {

        height: '100%', width: '100%',
        alignItems: 'center'


    },
    serviePopupHeaderText: {
        marginVertical: getHeight(10),
        // marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily:'bold',
    },

    popupHeaderText: {
        fontSize: 'medium15Px',
        color: 'black',
        marginLeft: 3,
        fontWeight: '500',
        textTransform: 'uppercase'
    },
    popupaddbtn: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(345),
        height: getHeight(44),
        borderRadius: 3,
        textColor: 'black',
        fontSize: 'medium15Px',
        marginTop: getHeight(18),
        shadow: 3,
        fontWeight: 'bold'



    },


    





};

export default styles;

import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text, ScrollView
} from 'react-native';
import { images } from '../../config';
import { Input } from '../../components/input/input';
import { Button } from '../../components/button/button';
import styles from './styles'
import { Card } from '../../components/card/card'
import { _string } from '../../local';
import { getHeight, Width, getWidth } from '../../components/utils/dimensions';
import { _Text } from '../../components/text/text';
export default class NewPasswordView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            secureTextEntryPassword: true,
            secureTextEntryRetype_password: true
        };
    }





    showPassword = (key) => {
        this.setState({ [key]: !this.state[key] })
    }

    render() {
        let { secureTextEntryPassword, secureTextEntryRetype_password } = this.state
        let { password, reTypePassword, code, onChangeText, getTimes, onResetPassword, onLogin, sendNewPasswordLoading, forgotPasswordLoading, countTimeDown } = this.props
        return (

            <View
                style={styles.container}>
                <Card style={{ width: '100%', height: '100%', }}>

                    <ImageBackground
                        resizeMode='cover'
                        source={images.loginbackground}
                        style={styles.imageBackground}>

                        <ScrollView scrollEnabled={false} contentContainerStyle={{ width: '100%', alignItems: 'center' }}>

                            <Image
                                source={images.WhiteLogo}
                                style={styles.logo} />

                            <View style={{ alignItems: 'center', width: Width, marginTop: getHeight(42) }}>
                                <Input
                                    maxLength={6}
                                    onSubmitEditing={() => {
                                        this.input2.focus()
                                    }}
                                    ref={(ref) => this.input1 = ref}
                                    returnKeyType='next'
                                    autoFocus
                                    value={code}
                                    onChangeText={(text) => onChangeText(text, "code")}

                                    style={styles.input}
                                    iconName={'info'}
                                    placeholder={_string("forgot_password.your_reset_code")}
                                    iconStyle={{ width: getWidth(15), height: getHeight(15) }}
                                    verticalLine={false}
                                />

                                <Input
                                    maxLength={40}

                                    onSubmitEditing={() => {
                                        this.input3.focus()
                                    }}
                                    ref={(ref) => this.input2 = ref}
                                    returnKeyType='next'
                                    onRightPress={() => this.showPassword("secureTextEntryPassword")}
                                    secureTextEntry={secureTextEntryPassword}

                                    value={password}
                                    onChangeText={(text) => onChangeText(text, "password")}
                                    style={styles.input}

                                    iconName={'password'}
                                    placeholder={_string("forgot_password.your_password")}
                                    iconStyle={{ width: getWidth(15), height: getHeight(15) }}
                                    verticalLine={false}
                                />

                                <Input
                                    maxLength={40}

                                    onSubmitEditing={onLogin}
                                    ref={(ref) => this.input3 = ref}
                                    returnKeyType='go'
                                    onRightPress={() => this.showPassword("secureTextEntryRetype_password")}
                                    secureTextEntry={secureTextEntryRetype_password}
                                    value={reTypePassword}
                                    onChangeText={(text) => onChangeText(text, "reTypePassword")}
                                    style={styles.input}

                                    iconName={'password'}
                                    placeholder={_string("forgot_password.retype_password")}
                                    iconStyle={{ width: getWidth(15), height: getHeight(15) }}
                                    verticalLine={false}


                                />

                                <Button
                                    isLoading={sendNewPasswordLoading}
                                    onPress={onLogin}
                                    iconStyle={{ color: '#fff', width: getWidth(18), height: getHeight(18) }}
                                    text={_string("forgot_password.login")}
                                    iconName='login'
                                    style={styles.loginButton}
                                //onPress={()=>{this.props.navigation.navigate('Home')}}
                                />

                                <_Text style={styles.text}>{_string("forgot_password.If_your_code_dosent_arrive_in").toLocaleUpperCase()}</_Text>
                                <_Text style={styles.arrivaltime}>{getTimes() + " MIN"}</_Text>

                                <Button
                                    activeOpacity={countTimeDown == 0 ? .8 : 1}
                                    loaderColor={'darkGray1'}
                                    isLoading={forgotPasswordLoading}
                                    onPress={onResetPassword}
                                    iconStyle={{ color: 'lightGray', width: getWidth(18), height: getHeight(18) }}
                                    text={_string("forgot_password.resend_reset_code")}
                                    iconName='send'
                                    style={styles.resetButton(countTimeDown)} />

                            </View>

                        </ScrollView>
                    </ImageBackground>


                </Card>
            </View>

        );
    }
}

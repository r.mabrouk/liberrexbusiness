import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux'
import NewPasswordView from './NewPasswordView'
import { requestResetPasswordWithCode, requestResetPassword } from '../../actions/auth';
import { _string } from '../../local';
import { _Toast } from '../../components/toast/toast';
class NewPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countTimeDown: 180,
      time: 0,
      code: "",
      password: "",
      reTypePassword: ""
    };
  }
  onChangeText = (text, fieldName) => {
    this.setState({ [fieldName]: text })
  }
  validationForm = () => {
    let { code, password, reTypePassword } = this.state
    if (!code) {
      _Toast.open(_string("messages_alert.please_enter_the_code"), 'w')
      return false
    }
    else if (!password) {
      _Toast.open(_string("messages_alert.please_enter_the_new_password"), 'w')
      return false
    }
    else if (!reTypePassword) {
      _Toast.open(_string("messages_alert.please_enter_reType_password"), 'w')
      return false
    }
    else if (reTypePassword != password) {

      _Toast.open(_string("messages_alert.retype_password_not_match_with_password"), 'w')
      return false
    }
    else return true

  }
  componentWillMount = () => {
    this.updateTimer()

  }
  getTimes = () => {
    let { countTimeDown } = this.state
    let sucand = (countTimeDown % 60) < 10 ? ("0" + (countTimeDown % 60)) : (countTimeDown % 60)
    minuit = Math.floor(countTimeDown / 60) < 10 ? ("0" + Math.floor(countTimeDown / 60)) : Math.floor(countTimeDown / 60)
    return minuit + ":" + sucand
  }
  onResetPassword = () => {
    let email = this.props.navigation.getParam("email")
    if (this.state.countTimeDown==0){
      this.props.onForgotPassword({ email: email })
      this.setState({ countTimeDown: 180 }, () => this.updateTimer())
    }

  }


  onLogin = () => {
    let email = this.props.navigation.getParam("email")
    let { password, code } = this.state
    let data = {
      password,
      token: code,
      email
    }
    if (this.validationForm()) {
      this.props.sendNewPassword(data)

    }

    // this.props.navigation.navigate('Home');
  }



  updateTimer = () => {
    let timer = setInterval(() => {
      let { countTimeDown } = this.state
      if (countTimeDown > 0) {
        let { countTimeDown, time } = this.state
        this.setState({ countTimeDown: countTimeDown - 1, time: time + 1 })
      }
      else
        clearInterval(timer)
    }, 1000)
  }
  render() {
    return (
      <NewPasswordView {...this} {...this.state} {...this.props} />
    );
  }
}


function mapStateToProps(state) {
  return {
    forgotPasswordLoading: state.loadingReducer.forgotPasswordLoading,
    sendNewPasswordLoading: state.loadingReducer.sendNewPasswordLoading
  }
}
function mapDispatchToProps(Dispatch) {
  return {
    sendNewPassword: (data) => Dispatch(requestResetPasswordWithCode(data)),
    onForgotPassword: (email) => Dispatch(requestResetPassword(email))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(NewPassword)


import { StyleSheet } from 'react-native';
import { getWidth, getHeight, Height, Width } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(375),
        backgroundColor: ColorsList.white
    },
    logo: {
        width: getWidth(250),
        height: getHeight(52),
        resizeMode: 'contain',
        marginTop:getHeight(100)
    },
    imageBackground: {
        width: getWidth(375),
        height: '100%',
        alignItems: 'center',


    },
    contentContainer: {
        marginTop: Height * .1,
        alignItems: 'center',

    },
    text: {
      fontFamily: 'regular',
        fontSize: 'small',
        marginTop: getHeight(28),
        color: ColorsList.rgbaGray(.7)

    },
    resetbtn: {
        width: getWidth(345),
        marginTop: 25,
    },
    arrivaltime: {
        fontFamily: 'bold',
        fontSize: 'size15',
        marginTop: getHeight(10),
        color:  'darkGray1'
    },
    resetButton:(countTimeDown)=>( {
        width: getWidth(345),
        marginTop: getHeight(11),
        textColor:countTimeDown==0?'darkGray1':"lightGray",
        shadow: 6,
        fontSize:'size15',
        height: getHeight(44),
        backgroundColor: 'white',
        borderRadius: 3,
        fontFamily:'bold'
    }),
    loginButton: {
        width: getWidth(345),
        marginTop: getHeight(15),
        textColor: 'white',
        shadow: 6,
        fontSize:'size15',
        height: getHeight(44),
        colors: 'blue',
        borderRadius: 3,
                fontFamily: 'bold',



    },
    input:
    {
        fontFamily: 'medium',
        borderRadius: 3,
        width: getWidth(345),
        height: getHeight(44),
        shadow: 5, backgroundColor: 'white',
        // alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: getHeight(10),
    }

};

export default styles;

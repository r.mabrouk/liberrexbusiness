import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Button } from '../../components/button/button';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { images, metrics, Icons } from '../../config';

import ImagePicker from 'react-native-image-picker';
import HeaderButtons from '../../components/HeaderButtons/HeaderButtons'
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import { Input } from '../../components/input/input';
import Notification from '../../components/Notification/Notification';
import BoxShadow from '../../components/shadow/BoxShadow';
import { getHeight, getWidth, Width } from '../../components/utils/dimensions';
import { _string } from '../../local';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import EmptyData from '../../components/emptyData/emptyData';
import { _Toast } from '../../components/toast/toast';


export default class NotificationView extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        let { notificationList, loadingNotificationsList } = this.props
        let { requestsList, CustomerList, notification } = require("../../config/LocalDatabase")
        return (
            <View
                style={styles.container}>

                <Header onBack={() => this.props.navigation.goBack()} style={{ width: '100%', zIndex: 100 }} />
                <HeaderSection
                    onPress={() => {

                        _Toast.open(_string("messages_alert.successfully_updated_profile"), "s")
                    }}
                    iconSize={getWidth(28)}
                    style={{
                        zIndex: 20,
                        shadow: 6,
                        width: Width,
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(17),

                        height: getHeight(67),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}

                    textButton='Weekly'
                    text={_string("notifications.notifications")}
                    iconName='notification'

                />

                <FlatList data={notificationList} renderItem={({ item, index }) => {
                    return (
                        <Notification title={item.title} subtitle={item.text} time={item.created_at} />
                    )
                }}
                    ListFooterComponent={() => {
                        return (<LoaderLists isLoading={loadingNotificationsList} />)
                    }}
                />
                <EmptyData
                    show={(notificationList.length < 1) && !loadingNotificationsList} />
            </View >

        );
    }
}


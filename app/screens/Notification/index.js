import React, { Component } from 'react';
import { View, Text } from 'react-native';
import NotificationView from './NotificationView'
import {connect} from 'react-redux'
import { getNotificationListRequest } from '../../actions/notification';
class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
componentDidMount=()=>{
  this.props.getNotificationList()
}
  render() {
    return (
      <NotificationView {...this} {...this.state} {...this.props} />
    );
  }
}

function mapStateToProps(state) {
  return {
    loadingNotificationsList: state.notificationReducer.loadingNotificationsList,
    notificationList: state.notificationReducer.notificationList
  }
}
function mapDispatchToProps(Dispatch) {
  return {
getNotificationList:()=>Dispatch(getNotificationListRequest())
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Notification)

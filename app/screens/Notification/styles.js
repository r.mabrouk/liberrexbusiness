import { StyleSheet } from 'react-native';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        height:Height,
width:Width,
        // alignItems: 'center',
        backgroundColor: ColorsList.white
    },
    logo: {
        width: Width * .8,
        height: Height * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: '100%',
        height: '100%',
    },
    headerSection: {
        // backgroundColor: 'white',
         height: getHeight(67),
        width: Width,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: getWidth(15),
        // shadow: 5, 
        marginTop: -getHeight(30)
    },

    notificationContainer: {
        width: Width,
        // marginTop: getHeight(52),
        alignItems: 'center',
        // justifyContent: 'center',
    }
};

export default styles;

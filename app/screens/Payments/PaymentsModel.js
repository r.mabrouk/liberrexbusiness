import React, { Component } from 'react';
import {
    View,
    TextInput,
} from 'react-native';
import styles from './styles';
import { _Text } from '../../components/text/text';
import { Icons } from '../../config';
import { PopupModal } from '../../components/popupModal/popupModal'
import { getHeight, getWidth, Width } from '../../components/utils/dimensions';
import { Card } from '../../components/card/card';
import { Button } from '../../components/button/button';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput'
import { CheckBox } from '../../components/checkBox/checkBox';
import { _string } from '../../local';
type Props = {
    duration: number
}
class PaymentsModel extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            savePaymentMethods: false,
            cardNamber: "",
            expiry_date: "",
            cvv: ""
        }
    }

    onAnimated = (state) => {
        this.PaymentsModel.onAnimated(state)
    }
    render() {
        let { savePaymentMethods, cardNamber, cvv, expiry_date } = this.state
        let { loadingAddPaymentCard, onAddNewPaymentCard } = this.props
        return (
            <PopupModal
                inputDuration={400}
                inputHeight={getHeight(150)}
                duration={500} height={getHeight(341)} ref={(_Paymentsmodel) => this.PaymentsModel = _Paymentsmodel} >
                <Card style={{
                    height: '100%', width: '100%',
                    alignItems: 'center',

                }}>
                    <View style={styles.popupHeader}>
                        <Icons name='gridLinear' width={getWidth(28)} height={getWidth(28)} />
                        <_Text
                            style={styles.popupHeaderText}>{_string("member_ship.add_new_card").toLocaleUpperCase()}</_Text>
                    </View>
                    <CustomTextInput
                        maxLength={16}
                        marginTop={getHeight(20)}
                        onChangeText={(cardNamber) => this.setState({ cardNamber })}
                        headerIcon='paymentCardGray' headerText={_string("member_ship.card_number")}
                        placeholder={_string("pleace_holder")} leftIcon='editpen' />

                    <Card style={{
                        flexDirection: 'row',
                        alignItems: 'center', justifyContent: 'space-between',
                        width: getWidth(375),
                    }}>

                        <CustomTextInput
                            maxLength={5}
                            padding={getWidth(15)}

                            value={expiry_date}
                            Width={getWidth(180)}
                            onChangeText={(expiry_date) => {
                                if (expiry_date.length == 2)
                                    this.setState({ expiry_date: expiry_date + "/" })
                                else
                                    this.setState({ expiry_date })
                            }}
                            headerIcon='appointmentsgray' headerText={_string("member_ship.expiry_date")}
                            placeholder={_string("member_ship.mm_yy")} leftIcon='editpen' />


                        <CustomTextInput
                            maxLength={4}
                            padding={getWidth(15)}
                            Width={getWidth(180)}
                            onChangeText={(cvv) => this.setState({ cvv })}
                            headerIcon='password' headerText={_string("member_ship.cvv")}
                            placeholder={_string("pleace_holder")} leftIcon='editpen' />


                    </Card>
                    <Card style={{ flexDirection: 'row', width: Width, paddingHorizontal: getWidth(15) }}>

                        <CheckBox
                            fontSize='size15'
                            text={_string("SaveInYourPaymentMethods")} onChecked={() => {
                                this.setState({ savePaymentMethods: !savePaymentMethods })
                            }} checked={savePaymentMethods} />

                    </Card>

                    <Button
                        isLoading={loadingAddPaymentCard}
                        iconName="done"
                        iconStyle={{ width: getWidth(18), height: getWidth(18) }}
                        text={_string("member_ship.add_card")}
                        style={{
                            colors: 'blue',
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: getWidth(345),
                            height: getHeight(44),
                            borderRadius: 3,
                            textColor: 'white',
                            fontSize: 'size15',
                            fontFamily: 'bold',
                            marginTop: getHeight(20)
                        }}
                        onPress={() => onAddNewPaymentCard(cardNamber, expiry_date, cvv, savePaymentMethods, this.onAnimated)}
                    />


                </Card>
            </PopupModal>
        );
    }
}


export default PaymentsModel
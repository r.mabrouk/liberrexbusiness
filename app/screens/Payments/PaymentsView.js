import React, { Component } from 'react';
import {
    View, FlatList

} from 'react-native';
import styles from './styles';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import PaymentCard from '../../components/Payment/PaymentCard';
import PaymentsModel from './PaymentsModel';
import { getHeight, Width, getWidth } from '../../components/utils/dimensions';
import { _string } from '../../local';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import EmptyData from '../../components/emptyData/emptyData';
export default class PaymentsView extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        let { loadingGetPaymentCards, getPaymentCards, loadingDeletePaymentCard, deletePaymentCard, cardsList, onAddNewPaymentCard, loadingAddPaymentCard, onDeleteCard } = this.props
        return (
            <View
                style={styles.container}>
                <Header style={{ width: '100%', zIndex: 16 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />


                <HeaderSection

                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}
                    withButton
                    textButton={_string("payments.add_new")}
                    text={_string("payments.payment_methods")}
                    iconName='payments'
                    iconButton='addwhite'
                    onPress={() => { this.add.onAnimated(true) }}
                />
                                <EmptyData
                    onPress={() => { 
                        this.add.onAnimated(true)}}
                    show={(cardsList.length < 1) && !loadingGetPaymentCards} />
                <FlatList
                    style={{ width: Width }}
                    contentContainerStyle={{ width: '100%', alignItems: 'center', paddingTop: getHeight(20) }}
                    data={cardsList}
                    renderItem={({ item, index }) => (
                        <PaymentCard
                            isNotReomveCard={cardsList.length > 1}
                            loadingDeletePaymentCard={loadingDeletePaymentCard}
                            onDeleteCard={() => onDeleteCard(item.id, index)}
                            leftIcon={'paymentCardBlue'}
                            rightIcon={'delete'}
                            title={`${item.card.brand} Ending In ${item.card.last4}`}
                        />
                    )}
                    ListFooterComponent={() => {
                        return (<LoaderLists isLoading={loadingGetPaymentCards} />)
                    }}
                />

                <PaymentsModel
                    loadingAddPaymentCard={loadingAddPaymentCard}
                    onAddNewPaymentCard={onAddNewPaymentCard}
                    ref={(add) => this.add = add} />

            </View>
        )
    }
}

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import PaymentsView from './PaymentsView'
import { connect } from 'react-redux'
import { getPaymentCardsRequest, addnewPaymentCardRequest, deletePaymentCardRequest } from '../../actions/payment';
import { ModalToast } from '../../components/toast/toast';
import { _string } from '../../local';

class Payments extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount = () => {
    this.getPaymentCards()
  }
  validationPaymentForm = (cardNamber, expiry_date, cvv) => {
    if (!cardNamber) {
      ModalToast.open(_string("PleaseTypeYourCardNumber"), "w")
      return false
    }
   else if (cardNamber.length<16) {
    ModalToast.open(_string("PleaseEnterAValidCardNumber"), "w")
      return false
    }
    else if (!expiry_date) {
      ModalToast.open(_string("PleaseTypeExpiryDate"), "w")
      return false
    }
    else if (!cvv) {
      ModalToast.open(_string("PleaseTypeYourCvv"), "w")
      return false
    }
    else
      return true
  }

  onDeleteCard = (id, index) => {
    this.props._deletePaymentCard(id, index)
  }
  onAddNewPaymentCard = async (cardNamber, expiry_date, cvv, savePaymentMethods, CloseModal) => {
    if (this.validationPaymentForm(cardNamber, expiry_date, cvv, CloseModal)) {
      let data = {
        cardNamber,
        expMonth: expiry_date.split("/")[0],
        expYear: expiry_date.split("/")[1],
        cvv
      }
      this.props.AddNewPaymentCard(data, savePaymentMethods, CloseModal)
    }
  }
  getPaymentCards = () => {
    this.props._getPaymentCards()
  }
  render() {
    return (
      <PaymentsView {...this} {...this.state} {...this.props} />
    );
  }
}

function mapStateToProps(state) {
  return {
    cardsList: state.paymentReducer.cardsList,
    loadingAddPaymentCard: state.paymentReducer.loadingAddPaymentCard,
    loadingGetPaymentCards: state.paymentReducer.loadingGetPaymentCards,
    loadingDeletePaymentCard: state.paymentReducer.loadingDeletePaymentCard,
  }
}
function mapDispatchToProps(Dispatch) {
  return {
    _getPaymentCards: () => Dispatch(getPaymentCardsRequest()),
    AddNewPaymentCard: (data, saved, CloseModal) => Dispatch(addnewPaymentCardRequest(data, saved, CloseModal)),
    _deletePaymentCard: (id, index) => Dispatch(deletePaymentCardRequest(id, index))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Payments)


import { StyleSheet } from 'react-native';
import { getHeight, getWidth,Width, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
      width:Width,
      height:Height,
      backgroundColor:ColorsList.white
    },
    logo: {
        width: Width * .8,
        height: Height * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: '100%',
        height: '100%',
    },

   
    inputHeader:{
        fontSize: 'small13Px', fontWeight: 'bold', color: 'black', marginLeft: getWidth(5),
    },
    popupHeaderText: {

        marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily:'bold',
    },
    popupHeader: {
        flexDirection: 'row', width: getWidth(345),
        justifyContent: 'center', alignItems: 'center',
        marginTop:getHeight(20)
    },


};

export default styles;

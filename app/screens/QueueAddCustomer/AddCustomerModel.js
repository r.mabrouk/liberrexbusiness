import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import styles from './styles';
import { Button } from '../../components/button/button';
import { _Text } from '../../components/text/text';
import { Card } from '../../components/card/card';
import { images, Icons } from '../../config';
import { getHeight, getWidth } from '../../components/utils/dimensions';
import { PopupModal } from '../../components/popupModal/popupModal';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { _string } from '../../local';
type Props = {
    duration: number
}
class AddCustomereModel extends Component<Props> {
    constructor(props) {
        super(props);
    }
  
onAnimated=(state)=>{
    this.AddCustomer.onAnimated(state)
}
    render() {
        return (
        <PopupModal ref={(_AddCustomer) => this.AddCustomer = _AddCustomer} height={getHeight(520)}>
        <Card style={styles.popupContainer}>
            <View style={styles.popupHeader}>
                <Icons name='user' width={getWidth(28)} height={getHeight(28)} />
                <_Text style={styles.popupHeaderText}>{_string("queue.edit")}</_Text>
            </View>
            <CustomTextInput headerIcon={'userGray'} headerText={_string("queue.full_name")}
                placeholder={_string("pleace_holder")} leftIcon='editpen' />

            <CustomTextInput headerIcon={'email'} headerText={_string("queue.email")}
                placeholder={_string("pleace_holder")} leftIcon='editpen' />

            <CustomTextInput headerIcon={'phone'} headerText={_string("queue.phone_number")}
                placeholder={_string("pleace_holder")} leftIcon='editpen' />



            <Button
                iconName="cancel"
                iconStyle={{ width: getWidth(20),height:getHeight(20)}}
                text={_string("queue.block_this_customer")}
                style={styles.popupaddbtn}
                onPress={() => { this.AddCustomer.onAnimated() }}
            />

            <Button
                iconName="star"
                iconStyle={{ width: getWidth(22),height:getHeight(22)}}
                text={_string("queue.add_to_vip")}
                style={styles.popupaddbtn}
                onPress={() => { this.AddCustomer.onAnimated() }}
            />

            <Button
                iconName="done"
                text={_string("queue.done")}
                style={styles.popupDonebtn}
                onPress={this.props.onPress}
            />


        </Card>




    </PopupModal>


    );
  }
}
export default AddCustomereModel
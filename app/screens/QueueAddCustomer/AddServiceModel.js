import React, { Component } from 'react';
import { View,FlatList } from 'react-native';
import styles from './styles';
import { Button } from '../../components/button/button';
import { _Text } from '../../components/text/text';
import { Card } from '../../components/card/card';
import { images, Icons } from '../../config';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import { PopupModal } from '../../components/popupModal/popupModal';
import ServiceCard from '../../components/ServiceCard/ServiceCard';
import { _string } from '../../local';
import { ColorsList } from '../../components/PropsStyles';
import { CheckBox } from '../../components/checkBox/checkBox';

type Props = {
    duration: number
}
class AddServiceModel extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            selectedServicesID: [],
            selectedServicesNames: []
        }
    }

    onAnimated = (state) => {
        this.AddServiceModel.onAnimated(state)
    }

    checkService = (ID, title) => {
        let selectedServicesIDCopy = [...this.state.selectedServicesID];
        let selectedServicesNamesCopy = [...this.state.selectedServicesNames];
        let index = selectedServicesIDCopy.indexOf(ID);
        if (index > -1) {
            selectedServicesIDCopy.splice(index, 1);
            selectedServicesNamesCopy.splice(index, 1);
        }
        else{
            selectedServicesIDCopy.push(ID)
            selectedServicesNamesCopy.push(title)
        }
        this.setState({ selectedServicesID: selectedServicesIDCopy, selectedServicesNames: selectedServicesNamesCopy })
    }

    serviceSelected = (ID) => {
        let { selectedServicesID } = this.state
        let index = selectedServicesID.indexOf(ID);
        return ( index > -1 )
    }

    render() {

        return (
            <PopupModal ref={(_AddService) => this.AddServiceModel = _AddService} height={getHeight(267)} duration={500}>
                <Card style={{
                    height: '100%', width: '100%', justifyContent: 'center',
                    alignItems: 'center', padingBottom: getHeight(10),

                }}>
                    <Card style={{ flexDirection: 'row', width: getWidth(375), alignItems: 'center', justifyContent: 'center' }}>
                        <Card style={{ width: getWidth(38) }}>
                            <Icons name='user' width={getWidth(28)} height={getWidth(28)} />
                        </Card>
                        <_Text style={styles.HeaderTitle}>{_string("queue.add").toUpperCase()}</_Text>
                        <_Text style={styles.name}>{(" " + this.props.customerName + " ").toUpperCase()}</_Text>
                        <_Text style={styles.HeaderTitle}>{`${_string("to")} ${this.props.queueName}`.toUpperCase()}</_Text>
                    </Card>

                    <Card style={styles.appointmentService}>
                        <Card style={{ flexDirection: 'row', width: getWidth(345), marginTop: getHeight(15) }}>
                            <Icons name='grid' width={getWidth(18)} height={getWidth(18)} />
                            <_Text style={styles.optionTitle}>
                                {_string("QueueService")}</_Text>

                        </Card>
                        <View style={{
                            // backgroundColor: 'red',
                            height: getHeight(111)
                        }}>
                            <FlatList
                                data={this.props.selectedQueueServices}
                                extraData={this.state}
                                renderItem={({ item, index }) => {
                                    return (
                                        <ServiceItem
                                            service_name={item.title}
                                            onChecked={() => { this.checkService(item.id, item.title) }}
                                            checked={this.serviceSelected(item.id)} />
                                    )
                                }}
                            />
                        </View>
                    </Card>

                    <Button
                        isLoading={this.props.loadingAddButtone}
                        iconStyle={{ width: getWidth(18),height:getHeight(18),color: '#fff'}}
                        text={_string("queue.add_customer")}
                        iconName='done'
                        style={styles.loginButton}
                        onPress={()=>{this.props.onPress(this.state.selectedServicesID)} }
                    />
                </Card>
            </PopupModal>

        );
    }
}
const ServiceItem = ({ checked, onChecked, service_name }) => {
    return (
        <Button
            onPress={onChecked}
            style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: getHeight(15),
                paddingHorizontal: getWidth(15),
                borderBottomWidth: 1,
                borderColor: ColorsList.rgbaBlack(0.04)
            }}>
            <CheckBox
            fontSize={'size13'}
                text={service_name}
                onChecked={onChecked}
                checked={checked} />
        </Button>
    )
}

export default AddServiceModel
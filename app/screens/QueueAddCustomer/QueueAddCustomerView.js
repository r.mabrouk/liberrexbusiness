import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
    FlatList
} from 'react-native';
import styles from './styles';
import { Button } from '../../components/button/button';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { images, Icons } from '../../config';
import { Input } from '../../components/input/input';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput'
import HeaderButtons from '../../components/HeaderButtons/HeaderButtons';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import SwiperTabButtons from '../../components/swiperTabButtons/swiperTabButtons';
import { PopupModal } from '../../components/popupModal/popupModal';
import ServiceCard from '../../components/ServiceCard/ServiceCard';
import CustomerCardWithGreenButton from '../../components/CustomerCardWithGreenButton/CustomerCardWithGreenButton';
import AddServiceModel from './AddServiceModel';
import AddCustomereModel from './AddCustomerModel';
import { _string } from '../../local';
import { CustomerItem } from '../CustomerList/CustomerItem';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import ButtonWithHeaderSection from '../../components/buttonWithHeaderSection/buttonWithHeaderSection';
import SelectedQueueServiceModal from './SelectedQueueServiceModal';
import EmptyData from '../../components/emptyData/emptyData';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'

class QueueAddCustomerView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            approve: true,
            pending: false,
            Popup: false,
            name: 'Achraf Ammar',
            email: 'remon@oncloudeg.com',
            phone: '+20-128-406-5814',
            currentSelected: 0,
            selectedCustomer: 0,

            name: '',
            email: '',
            phoneNumber: '',
            selectedServicesID: [],
            selectedServicesNames: [],
        };
    }



    render() {
        let { all_customers } = this.props
        let { CustomerList, } = require("../../config/LocalDatabase")
        return (
            <View
                style={styles.container}>

                <Header style={{ width: '100%', zIndex: 40 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />
                {/* <HeaderSection
                    iconSize={getWidth(28)}
                    style={styles.headerSection}
                    text='Add a customer to'
                    iconName='signupgray'
                    withButton
                    buttonStyle={styles.headerbtn}


                /> */}
                <HeaderSection
                    onPress={() => {
                        this.props.navigation.navigate('QueueEdit')
                    }}
                    iconSize={getWidth(28)}
                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}
                    // withButton
                    // textButton={'Queue1'}
                    text={_string("queue.add_a_customer")}
                    iconName='signupgray'
                // iconButton={'arrowDown'}
                />



                <View style={{
                    alignItems: 'center',
                    width: Width.width,
                    //marginTop: getHeight(50)
                }}>
                    <SwiperTabButtons tabButons={[
                        { iconsName: 'list', label: _string("queue.customers_list") },
                        { iconsName: 'add', label: _string("queue.new_customer") }]}>

                        <Card style={{ width: Width, alignItems: 'center' }}>

<ScrollView contentContainerStyle={{width:Width,alignItems:'center',height:getHeight(71),backgroundColor:'transparent'}}>
<Input
                                onChangeText={this.props.onCustomerSearch}
                                style={styles.searchInput}
                                iconName={'search'}
                                placeholder={_string("queue.search_by_name_email_phone")}
                                iconStyle={{ width: getWidth(15), height: getHeight(15) }}
                                verticalLine={false}
                            />
</ScrollView>
                      

                            <ScrollView  style={{backgroundColor:'transparent'}} contentContainerStyle={{ height: Height, alignItems: 'center',zIndex:5,backgroundColor:'transparent' }}>
                               
                               
                                <Card style={{
                                    alignItems: 'center', justifyContent: 'center',

                                    width: '100%',
                                }}>
                                    <EmptyData

                                        height={getHeight(370)}

                                        show={(all_customers.length < 1) && !this.props.isGetAllCustomersLoading} />

                                    <FlatList
                                        extraData={this.props}
                                        keyExtractor={item => item.id}
                                        // refreshing={isRefreshing}
                                        // onRefresh={() => this.setState({ isRefreshing: true })}
                                        onEndReached={() => this.props.get_customers()}
                                        onEndReachedThreshold={0.1}
                                        style={{ height: getHeight(400) }}
                                        data={all_customers}
                                        renderItem={({ item, index }) => {
                                            return (
                                                <CustomerCardWithGreenButton
                                                firstItem={index===0}
                                                    onPress={() => {
                                                        this.setState({ selectedCustomer: index })
                                                        if (this.props.navigation.state.params) {
                                                            let { selectCustomer } = this.props.navigation.state.params
                                                            if (selectCustomer)
                                                                this.props.navigation.goBack()
                                                        }
                                                        this.AddService.onAnimated(true)
                                                    }}
                                                    name={item.fname + " " + item.lname}
                                                    email={item.email}
                                                    onEditCustomer={() => this.AddCustomer.onAnimated(true)}
                                                />
                                            )
                                        }}
                                        ListFooterComponent={() => (
                                            <LoaderLists isLoading={this.props.isGetAllCustomersLoading} />
                                        )}
                                    />
                                </Card>

                            </ScrollView>
                        </Card>


                        {/* //add Customer  */}
                        <KeyboardAwareScrollView>

                            {/* {this.state.currentSelected == 1 && */}
                            <Card
                                style={{
                                    alignItems: 'center', justifyContent: 'center', width: Width
                                }}
                            >
                                <CustomTextInput
                                    marginTop={getHeight(20)}
                                    placeholder={_string("pleace_holder")}
                                    headerIcon='userGray'
                                    headerText={_string("queue.full_name")}
                                    leftIcon='editpen'
                                    onChangeText={(name) => { this.setState({ name }) }}
                                />
                                <CustomTextInput
                                    marginTop={getHeight(5)}
                                    KeyboardType='email-address'
                                    placeholder={_string("pleace_holder")}
                                    headerIcon='email'
                                    headerText={_string("queue.email")}
                                    leftIcon='editpen'
                                    onChangeText={(email) => { this.setState({ email }) }}
                                />
                                <CustomTextInput
                                    returnKeyType='done'
                                    marginTop={getHeight(5)}
                                    KeyboardType='numeric'
                                    placeholder={_string("pleace_holder")}
                                    headerIcon='phone'
                                    headerText={_string("queue.phone_number")}
                                    leftIcon='editpen'
                                    onChangeText={(phoneNumber) => { this.setState({ phoneNumber }) }}
                                />


                                <ButtonWithHeaderSection
                                    marginTop={getHeight(5)}

                                    onPress={() => {
                                        this.selectQueueService.onAnimated(true)
                                    }}
                                    numberOfLines={1}
                                    value={this.state.selectedServicesNames.length > 0 ? this.state.selectedServicesNames.toString() : _string("SelectService")}
                                    headerText={_string("queue.queue_services")}
                                    headerIcon='grid'
                                />


                                <Button
                                    isLoading={this.props.loadingAddCutomerToQueue}
                                    onPress={() => {
                                        this.props.addNewCustomerToQueue(
                                            this.state.name,
                                            this.state.email,
                                            this.state.phoneNumber,
                                            this.state.selectedServicesID, this.AddCustomer.onAnimated
                                        )
                                    }}
                                    iconStyle={{ width: getWidth(18), height: getHeight(18), color: '#ffffff' }}
                                    text={_string("queue.add_customer")}
                                    iconName='done'
                                    style={styles.addButton} />

                            </Card>
                            {/* } */}

                            </KeyboardAwareScrollView>

                    </SwiperTabButtons>
                </View>






                {/* start add customer popup  */}
                {all_customers.length > 0 && <AddServiceModel
                    ref={(_AddService) => this.AddService = _AddService}
                    onPress={(selectedServicesID) => {
                        this.props.addExistingCustomerToQueue(all_customers[this.state.selectedCustomer].id, selectedServicesID, this.AddCustomer.onAnimated)
                        // this.props.navigation.goBack() 
                    }}
                    loadingAddButtone={this.props.loadingAddCutomerToQueue}
                    queueName={this.props.selectedQueue.queue.title}
                    customerName={all_customers[this.state.selectedCustomer].fname + ' ' + all_customers[this.state.selectedCustomer].lname}
                    selectedQueueServices={this.props.selectedQueue.services}
                />}

                <SelectedQueueServiceModal
                    ref={(selectQueueService) => this.selectQueueService = selectQueueService}
                    servicesList={this.props.selectedQueue.services}
                    selectedServicesID={this.state.selectedServicesID}
                    selectedServicesNames={this.state.selectedServicesNames}
                    onServiceAdd={(selectedServicesID, selectedServicesNames) => {
                        // this.props.servicesSelected(selectedServicesID, selectedServicesNames)
                        this.setState({ selectedServicesID, selectedServicesNames })
                        this.selectQueueService.onAnimated()
                    }}
                />

                {/* end add customer popUp */}
                <AddCustomereModel ref={(_AddCustomer) => this.AddCustomer = _AddCustomer}
                    onPress={() => { this.props.navigation.goBack() }}
                />
                {/* Start edit customer popUp */}

            </View>
        );
    }
}



export default QueueAddCustomerView;

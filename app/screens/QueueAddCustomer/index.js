import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { getAllCustomersRequest, resetCustomersList } from '../../actions/customers';
import QueueAddCustomerView from './QueueAddCustomerView'
import { ActionCreators } from '../../actions';
import { _string } from '../../local';
import { _Toast, ModalToast } from '../../components/toast/toast';
class QueueAddCustomer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      all_customers_keyword: null,
    };
  }

  onCustomerSearch = (keyword) => {
    this.setState({ all_customers_keyword: keyword }, () => {
        this.props.getAllCustomers(true, keyword, 1)
    })
}
  get_customers = () => {
    let { all_customers_keyword } = this.state
    let { cuurent_page, pages, isGetAllCustomersLoading } = this.props
    console.log("cuurent_pagecuurent_page", cuurent_page, pages)

    if (!isGetAllCustomersLoading) {
        if (cuurent_page < pages) {
            this.props.getAllCustomers(false, all_customers_keyword ? all_customers_keyword : null, cuurent_page + 1)
        }
        this.setState({ isRefreshing: false })
    }

}
componentDidMount=()=>{
  this.props.getAllCustomers(true,false,1)
}
  addExistingCustomerToQueue = (customer_id, selectedServicesID,onCloseModal) => {
    if (this.validateSelectedService(selectedServicesID)) {
      let body = {
        services: selectedServicesID.toString(),
        customer_id: customer_id,
        create_customer: 0
      }
      this.props.addCustomerToQueue(body,onCloseModal)
    }
  }

  addNewCustomerToQueue = (name, email, phoneNumber, selectedServicesID,onCloseModal) => {
    if (this.validateCustomerData(name, email, phoneNumber, selectedServicesID)) {
      let body = {
        services: selectedServicesID.toString(),
        create_customer: 1,
        fname: name.split(' ')[0],
        lname: name.split(' ')[1],
        email: email,
        phone: phoneNumber,
      }
      this.props.addCustomerToQueue(body,onCloseModal)
    }
  }

  validateCustomerData = (name, email, phoneNumber, selectedServicesID) => {
    if (name == '') {
      _Toast.open(_string('PleaseEnterAName'), 'w')
      return false
    }
    else if (email == '') {
      _Toast.open(_string('PleaseEnterAnEmail'), 'w')
      return false

    }
    else if (phoneNumber == '') {
      _Toast.open(_string('PleaseEnterAPhoneNumber'), 'w')
      return false
    }
    else if (!this.validateSelectedService(selectedServicesID)) {
      return false
    }
    return true
  }

  validateSelectedService = (selectedServicesID) => {
    if (selectedServicesID.length < 1) {
      ModalToast.open(_string("add_queue.please_select_serivece"), 'w')
      return false
    }
    return true
  }

  render() {
    return (
      <QueueAddCustomerView
      {...this}
        {...this.props}
        onCustomerSearch={this.onCustomerSearch}
        get_customers={this.get_customers}
        addExistingCustomerToQueue={this.addExistingCustomerToQueue}
        addNewCustomerToQueue={this.addNewCustomerToQueue}
      />
    );
  }
}
function mapStateToProps(state) {
  return {
    selectedQueueID: state.queueReducer.selectedQueueID,
    selectedQueue: state.queueReducer.selectedQueue,

    all_customers: state.customersReducer.all_customers,
    isGetAllCustomersLoading: state.loadingReducer.isGetAllCustomersLoading,
    cuurent_page: state.customersReducer.cuurent_page,
    pages: state.customersReducer.pages,

    loadingAddCutomerToQueue: state.queueReducer.loadingAddCutomerToQueue
  };
}
function mapDispatchToProps(Dispatch) {
  return {
    addCustomerToQueue: (body,onCloseModal) => Dispatch(ActionCreators.addCustomerToQueue(body,onCloseModal)),
    getAllCustomers: (reset, keyword, page) => Dispatch(getAllCustomersRequest(reset, keyword, page)),
    resetCustomersList: () => Dispatch(resetCustomersList()),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QueueAddCustomer);

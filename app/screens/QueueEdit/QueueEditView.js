import React, { Component } from 'react';
import {
    View,
    TouchableOpacity
} from 'react-native';
import { styles, stylesComponant } from './styles';
import { Icons } from '../../config';

import { Header } from '../../components/header/header';
import { Card } from '../../components/card/card';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import ServiceCard from '../../components/ServiceCard/ServiceCard';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import { ColorsList } from '../../components/PropsStyles';
import { Width, getHeight, getWidth } from '../../components/utils/dimensions';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';

import TimeCard from '../../components/TimeCard/TimeCard'
import { _string } from '../../local';
import TimeModal from '../NewAppointment/TimeModal';
import ServiceModal from './ServiceModal';
import ButtonWithHeaderSection from '../../components/buttonWithHeaderSection/buttonWithHeaderSection';
import LoadingView from '../Loading/loadingView';
import { Button } from '../../components/button/button';

export default class QueueListView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDay: -1
        };
    }

    render() {
        let {isUpdateOrRemoveQueue}=this.props
        console.log("loadingGetQueue",this.props.loadingGetQueue)
        // let { time } = require("../../config/LocalDatabase")
        return (
            <View
                style={styles.container}>
                <Header style={{ width: '100%', zIndex: 16 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />

                {/* <Card style={stylesComponant.HeaderContainer}>
                    <HeaderSection withButton textButton='Save' text='Queue 1'
                        iconName='queuegray' iconButton='done'  />
                </Card> */}

                <HeaderSection

                    onPress={this.props.AddQueue}

                    iconSize={getWidth(28)}
                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}
                    withButton={!isUpdateOrRemoveQueue}
                    textButton={_string("queue.save")}
                    isLoading={this.props.loadingAddQueue}
                    text={_string('AddQueue')}
                    iconName='queuegray'
                    iconButton={'done'}
                />
                <ScrollView contentContainerStyle={{ alignItems: 'center', justifyContent: 'center',paddingVertical:getHeight(20) }}>
                    <CustomTextInput
                    value={this.props.queueName}
                    leftIcon='editpen'
                    headerIcon='list' headerText={_string("queue.queue_name")}
                        placeholder={_string("pleace_holder")}
                        onChangeText={this.props.onQueueNameChange}
                    />
                    {/* <TouchableOpacity */}
                        {/* onPress={() => { this.selectService.onAnimated(true) }}> */}
                        {/* <Card style={{ flexDirection: 'row', width: getWidth(345), marginTop: getHeight(10) }}>
                            <Icons name={'grid'} width={getWidth(18)} height={getWidth(18)} />
                            <_Text style={{ fontSize: 'medium15Px', marginLeft: 5, color: 'black', fontWeight: 'bold', marginBottom: 5 }}>{_string("queue.queue_services")}</_Text>
                        </Card> */}
                        <ButtonWithHeaderSection
                        headerText={_string("queue.queue_services")}
                                onPress={() => {
                                    this.selectService.onAnimated(true)
                                }}
                                numberOfLines={1}
                                value={this.props.selectedServicesNames.length > 0 ? this.props.selectedServicesNames.toString() :_string("SelectFromYourServices")}
                                headerIcon='grid'
                            />
                    {/* </TouchableOpacity> */}
                    {/* {this.props.servicesList.length > 0 &&
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            data={this.props.servicesList}
                            style={{ width: '100%' }}
                            renderItem={({ item, index }) => {
                                return (
                                    <View style={{ height: getHeight(50), marginTop: 5, }}>
                                        <ServiceCard
                                            name={item.title}
                                            onServicePress={(status) => { this.props.onServicePress(item.id, status) }}
                                        />
                                    </View>
                                )
                            }}
                            numColumns={2}
                            columnWrapperStyle={{ justifyContent: 'space-around' }}
                        />} */}

                    <Card style={{ flexDirection: 'row', width: getWidth(345), marginTop: getHeight(10) }}>
                        <Icons name={'calendar'} width={getWidth(18)} height={getWidth(18)} />
                        <_Text style={{ fontSize: 'medium15Px', marginLeft: 5, color: 'black', fontWeight: 'bold', marginBottom: 5 }}> {_string("queue.queue_working_days")}</_Text>
                    </Card>


                    {this.props.selectedTimes.map((item, index) => {
                        return (
                            <View key={`option${index}`} >
                                <TimeCard
                                isSelected={item.active}
                                    name={item.day}
                                    onDayPress={(status) => { this.props.onDayPress(index, status) }}
                                    onPress={() => {
                                        this.AddTime.onAnimated(true)
                                       this.props.onSetTime(index)
                                        this.setState({ selectedDay: index })
                                    }}
                                    from={this.props.formatedTime(this.props.selectedTimes[index].start_time)}
                                    to={this.props.formatedTime(this.props.selectedTimes[index].end_time)}
                                />
                            </View>
                        )
                    })}


{isUpdateOrRemoveQueue&&<View style={{ width: Width, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: getWidth(15),marginTop:getHeight(20) }}>
                        <Button
                            isLoading={this.props.loadingUpdateQueue}
                        
                            onPress={this.props.onUpdateQueue}
                            backgroundColor={'blue'}
                            iconStyle={{ width: getWidth(18), height: getWidth(18), color: '#ffffff' }}

                            text={_string("app_settings.save")}
                            iconName='done'
                            style={styles.nextButton(getWidth(167.5))} />
                     <Button
                            loaderColor={'darkGray1'}
                            isLoading={this.props.loadingDeleteQueue}
                            onPress={this.props.onDeleteQueue}
                            backgroundColor={'white'}
                            iconStyle={{ width: getWidth(18), height: getHeight(18), color: '#ffffff', }}
                            text={_string("your_service.remove")}
                            iconName='delete'
                            style={styles.removeButton} />
                    </View>}
                    

                </ScrollView>
                <TimeModal
                         title={_string("QueueWorkingDaysTime")}
                         startDate={this.props.startDate}
                         endDate={this.props.endDate}
                    ref={(time) => { this.AddTime = time }}
                    onDonePress={(newTimes) => {
                        this.props.onTimeChange(this.state.selectedDay, newTimes)
                    }}
                />
                <ServiceModal
                
                    servicesList={this.props.servicesList}
                    selectedServices={this.props.selectedServices}
                    selectedServicesNames={this.props.selectedServicesNames}
                    loadingServicesList={this.props.loadingServicesList}
                    onServicesSearch={this.props.onServicesSearch}
                    onEndReachedServiceList={this.props.onEndReachedServiceList}
                    onServiceAdd={(selectedServicesID, selectedServicesNames) => {
                        this.props.servicesSelected(selectedServicesID, selectedServicesNames)
                        this.selectService.onAnimated()
                    }}
                    ref={(selectService) => this.selectService = selectService} />
            </View>
        );
    }
}

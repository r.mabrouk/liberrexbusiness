import React, { Component } from 'react';
import {
    View
} from 'react-native';
import { stylesComponant } from './styles';
import { Button } from '../../components/button/button';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { Icons } from '../../config';

import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { getHeight, getWidth } from '../../components/utils/dimensions';
import { PopupModal } from '../../components/popupModal/popupModal';
import ServiceCard from '../../components/ServiceCard/ServiceCard';
import { _string, getlanguage } from '../../local';
import { CheckBox } from '../../components/checkBox/checkBox';
import { ColorsList } from '../../components/PropsStyles';
import { InputSearch } from '../../components/inputSearch/inputSearch';
import { FlatList } from 'react-native-gesture-handler';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import { styles } from './styles';
import EmptyData from '../../components/emptyData/emptyData';

class ServiceModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time: "",
            selectedServicesID: props.selectedServices,
            selectedServicesNames: props.selectedServicesNames,
        };
    }
    onAnimated = (state) => {
        this.popupAddModal.onAnimated(state)
    }
    componentWillReceiveProps = (props) => {
        this.setState({ selectedServicesID: props.selectedServices, selectedServicesNames: props.selectedServicesNames })
    }
    checkService = (ID, title) => {
        let selectedServicesIDCopy = [...this.state.selectedServicesID];
        let selectedServicesNamesCopy = [...this.state.selectedServicesNames];
        let index = selectedServicesIDCopy.indexOf(ID);
        if (index > -1) {
            selectedServicesIDCopy.splice(index, 1);
            selectedServicesNamesCopy.splice(index, 1);
        }
        else {
            selectedServicesIDCopy.push(ID)
            selectedServicesNamesCopy.push(title)
        }
        this.setState({ selectedServicesID: selectedServicesIDCopy, selectedServicesNames: selectedServicesNamesCopy })
    }

    serviceSelected = (ID) => {
        let { selectedServicesID } = this.state
        let index = selectedServicesID.indexOf(ID);
        return (index > -1)
    }


    render() {
        let { selectedServicesID, selectedServicesNames } = this.state
        let { servicesList, loadingServicesList, onServicesSearch, onEndReachedServiceList, onServiceAdd } = this.props
        return (
            <PopupModal
                inputDuration={400}

                inputHeight={getHeight(160)}
                height={getHeight(340)}
                duration={500}
                zIndex={30}
                ref={_popupAddModal => this.popupAddModal = _popupAddModal}>
                <Card style={stylesComponant.popupContainer}>
                    <View style={styles.popupHeader}>
                        <Icons name='gridLinear' width={getWidth(28)} height={getWidth(28)} />
                        <_Text
                            style={styles.popupHeaderText}>{_string("your_service.add_a_new_service").toLocaleUpperCase()}</_Text>
                    </View>

                
                    <InputSearch
                        marginTop={getHeight(15)}
                        placeholder={_string("SearchForService")}
                        onChangeText={onServicesSearch}
                    />
                    <View style={{ height: getHeight(208), width: '100%' }}>
                        <EmptyData
                            imageHeight={getHeight(50)}
                            imageWidth={getWidth(150)}
                            show={(servicesList < 1) && !loadingServicesList} />
                        <FlatList
                            extraData={this.props}
                            data={servicesList}
                            onEndReached={onEndReachedServiceList}
                            onEndReachedThreshold={0.1}
                            renderItem={({ item, index }) => {
                                return (
                                    <ServiceItem
                                        service_name={item.title}
                                        onChecked={() => { this.checkService(item.service_id, item.title) }}
                                        checked={this.serviceSelected(item.service_id)} />
                                )
                            }}
                            ListFooterComponent={() => {
                                return (
                                    <LoaderLists isLoading={loadingServicesList} />
                                )
                            }}
                        />


                    </View>
                    <Button
                        isLoading={false}
                        iconName="done"
                        text={_string("your_service.done")}
                        style={stylesComponant.popupDonebtn}
                        iconStyle={{ width: getWidth(18), height: getHeight(18) }}
                        onPress={() => onServiceAdd(selectedServicesID, selectedServicesNames)}
                    />
                </Card>
            </PopupModal>
        );
    }
}
const ServiceItem = ({ checked, onChecked, service_name }) => {
    return (
        <Button
            onPress={onChecked}
            style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: getHeight(15),
                paddingHorizontal: getWidth(15),
                borderBottomWidth: 1,
                borderColor: ColorsList.rgbaBlack(0.04)
            }}>
            <CheckBox
                text={service_name}
                onChecked={onChecked}
                checked={checked} />
        </Button>
    )
}

export default ServiceModal
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import QueueEditView from './QueueEditView'
import moment from 'moment';
import { time } from "../../config/LocalDatabase"
import { ActionCreators } from '../../actions';
import { connect } from 'react-redux';
import { _string } from '../../local';
import { getAllServicesRequest, getIndustryServicesRequest } from '../../actions/services';
import { _Toast } from '../../components/toast/toast';

class QueueEdit extends Component {
  constructor(props) {
    super(props);
    let queueData=props.navigation.getParam("queueData")
    this.state = {
      queueName:queueData?queueData.queue.title: '',
      queueServices: '',
      selectedTimes:queueData?queueData.working_days:[
        { "day": _string("dayes.Saturday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Sunday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Monday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Tuesday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Wednesday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Thursday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 },
        { "day": _string("dayes.Friday"), "start_time": "09:00:00", "end_time": "21:00:00", "active": 0 }
      ],
      startDate:new Date(moment().format("YYYY-MM-DDT09:00:00")),
      endDate:new Date(moment().format("YYYY-MM-DDT09:00:00")),
      isUpdateOrRemoveQueue:queueData?true:false,
      // selectedTimes: [
      //   {
      //     "status": "disabled",
      //     "from": "9:00 am",
      //     "to": "9:00 pm"
      //   },
      //   {
      //     "status": "disabled",
      //     "from": "9:00 am",
      //     "to": "9:00 pm"
      //   },
      //   {
      //     "status": "disabled",
      //     "from": "9:00 am",
      //     "to": "9:00 pm"
      //   },
      //   {
      //     "status": "disabled",
      //     "from": "9:00 am",
      //     "to": "9:00 pm"
      //   },
      //   {
      //     "status": "disabled",
      //     "from": "9:00 am",
      //     "to": "9:00 pm"
      //   },
      //   {
      //     "status": "disabled",
      //     "from": "9:00 am",
      //     "to": "9:00 pm"
      //   },
      //   {
      //     "status": "disabled",
      //     "from": "9:00 am",
      //     "to": "9:00 pm"
      //   },
      // ],
      DaysOfTheWeek: time,
      selectedServices: [],
      selectedServicesNames: [],
      servicesListkeyword: ""

    };
  }

  // ----------- update the queue name ------------
  onQueueNameChange = (queueName) => {
    this.setState({ queueName })
  }

  // ------------ change the time of a specific dat -----------------
  onTimeChange = (dayOfWeek, newTimes) => {
    console.log('dayOfWeek', dayOfWeek, ' newTimes', newTimes)
    let start_time = moment(newTimes.from).format('HH:mm:00')
    let end_time = moment(newTimes.to).format('HH:mm:00')
    let selectedTimesCopy = [...this.state.selectedTimes];
    // selectedTimesCopy[dayOfWeek].status = "enabled"
    selectedTimesCopy[dayOfWeek].start_time = start_time
    selectedTimesCopy[dayOfWeek].end_time = end_time
    this.setState({ selectedTimes: selectedTimesCopy })
    console.log('selectedTimesCopy', selectedTimesCopy)
  }
  selectServices=async(services)=>{

    let selectedServices=[]
    let selectedServicesNames=[]
    await services.filter((element)=>{
      selectedServices.push(element.id)
      selectedServicesNames.push(element.title)
    })
    this.setState({selectedServicesNames,selectedServices})

    console.log("servicesservices",selectedServices)

  }
  // ----------- update the selected days -------------


  onSetTime=(dayOfWeek)=>{
    let selectedTimesCopy = [...this.state.selectedTimes];
    let startDate = new Date(moment().format("YYYY-MM-DDT" + selectedTimesCopy[dayOfWeek].start_time))
    let endDate = new Date(moment().format("YYYY-MM-DDT" + selectedTimesCopy[dayOfWeek].end_time))
    this.setState({ selectedTimes: selectedTimesCopy, startDate, endDate })
  }
  onDayPress = (dayOfWeek, status) => {
    let selectedTimesCopy = [...this.state.selectedTimes];
    selectedTimesCopy[dayOfWeek].active = status ? 1 : 0
    let startDate = new Date(moment().format("YYYY-MM-DDT" + selectedTimesCopy[dayOfWeek].start_time))
    let endDate = new Date(moment().format("YYYY-MM-DDT" + selectedTimesCopy[dayOfWeek].end_time))
    console.log(selectedTimesCopy[dayOfWeek].start_time,"dfkdngkdjghdkjghfdgdfgfdgdfgfdg")
    this.setState({ selectedTimes: selectedTimesCopy, startDate, endDate })
  }

  // ------------- format the time ------------
  formatedTime = (time) => {
    return moment(time, ["HH:mm:ss"]).format('HH:mm').toString()
  }

  // ------------- update the selected services -----------------
  // onServicePress = (id, status) => {
  //   let selectedServicesCopy = [...this.state.selectedServices];
  //   if (status) {
  //     selectedServicesCopy.push(id)
  //   }
  //   else {
  //     let index = selectedServicesCopy.indexOf(id);
  //     if (index > -1) {
  //       selectedServicesCopy.splice(index, 1);
  //     }
  //   }
  //   this.setState({ selectedServices: selectedServicesCopy })
  //   console.log('selectedServicesCopy', selectedServicesCopy)
  // }
  servicesSelected = (selectedServicesID, selectedServicesNames) => {
    console.log('QueueEdit', 'servicesSelected', 'selectedServicesID', selectedServicesID, 'selectedServicesNames', selectedServicesNames)
    this.setState({ selectedServices: selectedServicesID })
    this.setState({ selectedServicesNames })
  }

  servicesArray_to_String = (data) => {
    return data.toString()
  }

  working_days_to_String = (data) => {
    let working_days = []
    for (let i = 0; i < data.length; i += 1) {
      let dayObj = {
        "day": data[i].day,
        "start_time": moment(data[i].from, ["h:mm a"]).format('HH:mm:ss').toString(),
        "end_time": moment(data[i].to, ["h:mm a"]).format('HH:mm:ss').toString()
      }
      working_days[this.state.DaysOfTheWeek[i].name] = dayObj
    }
    return working_days
  }

  selectAtleastDay = (selectedTimes) => {
    for (let i = 0; i < selectedTimes.length; i += 1) {
      if (selectedTimes[i].active) {
        return true
      }
    }
    return false
  }

  validateData = () => {
    if (this.state.queueName == '') {
      _Toast.open(_string("add_queue.please_enter_queue_name", "e"))
      return false
    }
    else if (this.state.selectedServices.length < 1) {
      _Toast.open(_string("add_queue.please_select_serivece"), 'w')
      return false

    }
    else if (!this.selectAtleastDay(this.state.selectedTimes)) {
      _Toast.open(_string("add_queue.please_select_day"), 'w')
      return false
    }
    return true
  }
  onUpdateQueue= () => {
   let queueId=this.props.navigation.getParam("queueData").queue_id
   let body = {
    title: this.state.queueName,
    services: this.servicesArray_to_String(this.state.selectedServices),
    working_days: JSON.stringify(this.state.selectedTimes)
  }
  if (this.validateData()) {
    this.props._updateQueue(queueId,body)

  }
  }
  onDeleteQueue= () => {
    let queueId=this.props.navigation.getParam("queueData").queue_id
    this.props._deleteQueue(queueId)
  }
  AddQueue = () => {
    if (this.validateData()) {
      let body = {
        title: this.state.queueName,
        services: this.servicesArray_to_String(this.state.selectedServices),
        // working_days: this.working_days_to_String(this.state.selectedTimes),
        working_days: JSON.stringify(this.state.selectedTimes)
      }
      this.props.addQueue(body)
    }
  }

  onServicesSearch = (keyword) => {
    this.props._getAllServices(true, keyword, 1)
    this.setState({ servicesListkeyword: keyword })
  }

  onEndReachedServiceList = () => {
    let { servicesListkeyword } = this.state
    let { services_current_page, services_last_page } = this.props
    if (services_current_page < services_last_page) {
      this.props._getAllServices(null, servicesListkeyword ? servicesListkeyword : null, services_current_page + 1)
    }
  }

  componentDidMount() {
    this.props._getAllServices(true, null, 1)
    let queueData=this.props.navigation.getParam("queueData")
    console.log("selectServicesselectServices",queueData)

if (queueData)
    this.selectServices(queueData.services)
    
  }


  render() {
    return (
      <QueueEditView
      {...this}
        {...this.state}
        {...this.props}
        onQueueNameChange={this.onQueueNameChange}
        onTimeChange={this.onTimeChange}
        onDayPress={this.onDayPress}
        onServicePress={this.onServicePress}
        AddQueue={this.AddQueue}
        onServicesSearch={this.onServicesSearch}
        onEndReachedServiceList={this.onEndReachedServiceList}
        servicesSelected={this.servicesSelected}
        formatedTime={this.formatedTime}
      />
    );
  }
}
function mapStateToProps(state) {
  return {
    loadingAddQueue: state.queueReducer.loadingAddQueue,

    servicesList: state.servicesReducer.servicesList,
    loadingServicesList: state.servicesReducer.loadingServicesList,
    services_current_page: state.servicesReducer.services_current_page,
    services_last_page: state.servicesReducer.services_last_page,
    queue:state.queueReducer.queue,
    loadingGetQueue:state.queueReducer.loadingGetQueue,
    loadingUpdateQueue:state.queueReducer.loadingUpdateQueue,
    loadingDeleteQueue:state.queueReducer.loadingDeleteQueue
    // servicesList: state.servicesReducer.industryServices,
    // loadingServicesList: state.servicesReducer.loadingIndustryServices,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    _updateQueue: (id,newQueue) => dispatch(ActionCreators.updateQueueRequest(id,newQueue)),
    _deleteQueue: (id) => dispatch(ActionCreators.deleteQueueRequest(id)),
    addQueue: (body) => dispatch(ActionCreators.addQueue(body)),
    _getAllServices: (reset, keyword, page) => dispatch(getAllServicesRequest(reset, keyword, page)),
    _getIndustryServices: (reset, keyword, page) => dispatch(getIndustryServicesRequest(reset, keyword, page))
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QueueEdit);

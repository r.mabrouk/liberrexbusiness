import { StyleSheet } from 'react-native';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
      width:Width,height:Height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:ColorsList.white
    },
    nextButton: (width)=>({
        width: width,
        // marginTop: getHeight(15),
        textColor: '#fff',
        shadow: 6,
        height: getHeight(44),
        colors: 'blue',
        borderRadius: 3,
        fontFamily:'bold',
        fontSize:'size15'

    }),
    removeButton: {
        width: getWidth(167.5),
        // marginTop: getHeight(15),
        textColor:'darkGray1',
        shadow: 6,
        height: getHeight(44),
      backgroundColor:'white',
      fontFamily:'bold',
      fontSize:'size15',
        borderRadius: 3,

    },
    
    logo: {
        width: Width * .8,
        height: Height * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: '100%',
        height: '100%',
        alignItems: 'center'
    },
    popupHeader: {
        flexDirection: 'row', width: getWidth(345),
        justifyContent: 'center', alignItems: 'center',
        marginTop:getHeight(20)
    },
    popupHeaderText: {

        marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily:'bold',
    },
    popupHeaderText: {

        marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily:'bold',
    },

};

const stylesComponant = {
    card: {
        flexDirection: 'row', justifyContent: 'flex-start',
        backgroundColor: 'white',
        shadow: 5,
        height: getHeight(700),
        width: getWidth(150),
        alignItems: 'center',
        paddingLeft: getWidth(20),
        borderRadius: 2
    },
    HeaderContainer: {
       alignItems: 'center', width: Width,paddingBottom:getHeight(12)
    },
    Input: {

        width: '85%',
        height: getHeight(41),
        shadow: 2, backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: -getHeight(25),
        marginLeft: getWidth(15),
    },
    Service: {
        flexDirection: 'row',
        backgroundColor: 'white',
        width: '95%',
        height: getHeight(50),
        marginTop: 5,
        justifyContent: 'space-around'
    },
    workingSection: {

        flexDirection: 'row',
        width: getWidth(345),
        shadow: 6, height: getHeight(41), backgroundColor: 'white',
        justifyContent: 'space-around', alignItems: 'center',
        // marginTop:5,
        marginBottom: 4,


    },
    ServiceCard: {
        flexDirection: 'row', justifyContent: 'flex-start',
        backgroundColor: 'white', shadow: 6,
        height: getHeight(41),
        width: getWidth(167.5),
        alignItems: 'center',
        paddingLeft: 20,
        borderRadius: 3,
        marginVertical:7
    },
    popupContainer: {

        height: '100%', width: '100%',
        alignItems: 'center'


    },
    serviePopupHeaderText: {
        marginVertical: getHeight(10),
        // marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily:'bold',
    },
    popupDonebtn: {
        colors: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(345),
        height: getHeight(44),
        borderRadius: 3,
        textColor: 'white',
        shadow:6,
        marginTop: getHeight(15),
        marginBottom: getHeight(15),
        fontFamily:'bold',
        color:'white',
        fontSize:'size15',
        position:'absolute',
        top:getHeight(270),
        left:getWidth(15)
    },
}

export { styles, stylesComponant };

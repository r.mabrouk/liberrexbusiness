import React, { Component } from 'react';
import {
    View,
    ActivityIndicator, RefreshControl
} from 'react-native';
import styles from './styles';
import { metrics } from '../../config';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { FlatList } from 'react-native-gesture-handler';
import { QueueItem } from '../../components/queueItem/queueItem';
import { Width, getHeight, getWidth } from '../../components/utils/dimensions';
import { _string } from '../../local';
import { ColorsList } from '../../components/PropsStyles';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import EmptyData from '../../components/emptyData/emptyData';
import LoadingView from '../Loading/loadingView';


export default class QueueListView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: true
        };
    }

    render() {
        let { onEditQueue, loadingGetQueue } = this.props
        return (
            <View
                style={styles.container}>
                <Header style={{ width: '100%', zIndex: 100 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.navigate("Home") }}
                />
                <HeaderSection
                    onPress={() => {
                        this.props.navigation.navigate('QueueEdit')
                    }}
                    iconSize={getWidth(28)}
                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}

                    withButton
                    textButton={_string("queue.add_new")}
                    text={_string("queue.queues_lists")}
                    iconName='queuegray'
                    iconButton='addwhite'
                />
                <View style={{ height: getHeight(480) }}>

                    <FlatList
                        refreshControl={
                            <RefreshControl
                                refreshing={this.props.refreshing}
                                tintColor={'green'}
                                onRefresh={this.props.onPullQueueList}
                            />
                        }

                        contentContainerStyle={{ paddingBottom: getHeight(15) }}
                        style={{ flex: 1 }}
                        data={this.props.QueueList}
                        renderItem={({ item, index }) => {
                            return (
                                <QueueItem
                                    onEditQueue={() => onEditQueue(item.queue.id, index)}
                                    title={item.queue.title}
                                    description={item.queue.description}
                                    pending={item.pending}
                                    rejected={item.rejected}
                                    waiting_time={item.total_waiting}
                                    total_waiting={this.props.calculateWaitingTime(item.waiting_time)}
                                    onPress={() => {
                                        this.props.selectedQueue(item.queue.id, item)
                                    }}

                                />
                            )
                        }}
                        ListFooterComponent={() => {
                            return (
                                <LoaderLists isLoading={this.props.loadingQueueList} />
                            )
                        }}
                    />
                    <EmptyData
                    position
                        onPress={() => { this.props.navigation.navigate('QueueEdit') }}
                        show={(this.props.QueueList.length < 1) && !this.props.loadingQueueList} />
                </View>


                <LoadingView isLoading={this.props.loadingGetQueue} />

            </View>
        );
    }
}

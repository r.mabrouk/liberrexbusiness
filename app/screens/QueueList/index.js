import React, { Component } from 'react';
import { View, Text } from 'react-native';
import QueueListView from './QueueListView';
import { ActionCreators } from '../../actions';
import { connect } from 'react-redux';
import { NavigationEvents } from 'react-navigation';
import Pusher from 'pusher-js/react-native'
import { getQueueWaitingListResponse } from '../../actions/queue';


const pusherConfig = {
  "appId": "753151",
  "key": "4393daed5c7e1527a9ae",
  "secret": "bad24061dd9f66bfb8d7",
  "cluster": "eu",
  "encrypted": true
  // "restSe rver":"http://192.168.0.15:4000"
}

class QueueList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: props.loadingQueueList
    };

    this.pusher = new Pusher(pusherConfig.key, pusherConfig); // (1)
    this.chatChannel = this.pusher.subscribe('queue_90'); // (2)

    this.chatChannel.bind('pusher:subscription_succeeded', () => { // (3)
      this.chatChannel.bind('join', (data) => { // (4)
        this.handleJoin(data);
      });
      this.chatChannel.bind('exit', (data) => { // (5)
        this.handleExit(data);
      });

    });

    // this.handleSendMessage = this.onSendMessage.bind(this); // (9)

  }
  handleJoin = (data) => {
    this.props.updateWaitingList(data)
  }
  handleExit = (data) => {

  }
  calculateWaitingTime = (totalMinutes) => {

    var num = totalMinutes;
    var hour = Math.floor(num / 3600);
    var minute = Math.floor(num % 3600 / 60);
    var second = Math.floor(num % 3600 % 60);
    console.log('hours', hour, ' minutes', minute, ' seconds', second)
    return { hour, minute, second }
  }

  getQueuList = (firstTime) => {
    this.props._getQueueList(firstTime)
  }

  selectedQueue = (id, selectedQueueObj) => {
    this.props.selectedQueue(id, selectedQueueObj)
    this.props.navigation.navigate('QueueRequests', { queue_id: id })
  }
  componentWillReceiveProps=(props)=>{
this.setState({refreshing:props.loadingQueueList})
  }
  onPullQueueList=()=>{
    this.setState({refreshing:true})
    this.getQueuList(true)

  }
  componentDidMount() {
    this.getQueuList(true)
  }
  onEditQueue = (id) => {
    this.props._getQueue(id)
  }
  render() {
    return (
      <View>
        <NavigationEvents onDidFocus={(payload) => {
          this.setState({refresh:true})
          if (payload.action.routeName == 'QueueList') {
            this.getQueuList(true)
          }
        }} />
        <QueueListView
          {...this.state}
          {...this}
          {...this.props}
          calculateWaitingTime={this.calculateWaitingTime}
          selectedQueue={this.selectedQueue}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    loadingGetQueue: state.queueReducer.loadingGetQueue,
    loadingQueueList: state.queueReducer.loadingQueueList,
    QueueList: state.queueReducer.QueueList,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    _getQueue: (id) => dispatch(ActionCreators.getQueueRequest(id)),

    _getQueueList: (firstTime) => dispatch(ActionCreators.getQueueList(firstTime)),
    selectedQueue: (id, selectedQueueObj) => dispatch(ActionCreators.selectedQueue(id, selectedQueueObj)),
    updateWaitingList: (res) => dispatch(getQueueWaitingListResponse(res))
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QueueList);
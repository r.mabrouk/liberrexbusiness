import { StyleSheet } from 'react-native';
import { Width, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = StyleSheet.create({
    container: {
        width: Width,
        height: Height,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: ColorsList.white
    }
});

export default styles;

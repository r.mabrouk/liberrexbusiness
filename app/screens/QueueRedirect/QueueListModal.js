import React, { Component } from 'react';
import {
    View, FlatList
} from 'react-native';
import { styles, customizeStyle } from './styles';
import { Button } from '../../components/button/button';
import { _Text } from '../../components/text/text';
import { Card } from '../../components/card/card';
import { getHeight, getWidth, Width } from '../../components/utils/dimensions';
import { PopupModal } from '../../components/popupModal/popupModal';
import { _string, getlanguage } from '../../local';
import { CheckBox } from '../../components/checkBox/checkBox';
import { ColorsList } from '../../components/PropsStyles';
// import { FlatList } from 'react-native-gesture-handler';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import { Icons } from '../../config';

class QueueListModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    onAnimated = (state) => {
        this.popupAddModal.onAnimated(state)
    }

    render() {
        let { selectedServicesID, selectedServicesNames } = this.state
        let { queueList, loadingQueueList, onQueueSelect } = this.props
        return (
            <PopupModal
                height={getHeight(300)}
                duration={500}
                zIndex={30}
                ref={_popupAddModal => this.popupAddModal = _popupAddModal}>
                <Card style={styles.popupContainer}>

                <View style={styles.popupHeader}>
                        <Icons name='user' width={getWidth(28)} height={getWidth(28)} />
                        <_Text
                            style={styles.popupHeaderText}>{_string("SelectQueue").toLocaleUpperCase()}</_Text>
                    </View>
<View style={{width:Width,height:1,backgroundColor:ColorsList.rgbaBlack(0.04),marginTop:getHeight(10)}}></View>
                    <View style={{ height: getHeight(240), width: '100%' }}>
                        <FlatList
                            data={queueList}
                            // onEndReached={onEndReachedQueueList}
                            // onEndReachedThreshold={0.1}
                            renderItem={({ item, index }) => {
                                return (
                                    <QueueItem
                                        queue_name={item.queue.title}
                                        onChecked={() => {
                                            onQueueSelect({ id: item.queue.id, name: item.queue.title })
                                        }}
                                        checked={this.props.selectedQueueID == item.queue.id} />
                                )
                            }}
                            ListFooterComponent={() => {
                                return (
                                    <LoaderLists isLoading={loadingQueueList} />
                                )
                            }}
                        />
                    </View>
                </Card>
            </PopupModal>
        );
    }
}
const QueueItem = ({ checked, onChecked, queue_name }) => {
    return (
        <Button
            onPress={onChecked}
            style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: getHeight(15),
                paddingHorizontal: getWidth(15),
                borderBottomWidth: 1,
                borderColor: ColorsList.rgbaBlack(0.04),
            
            }}>
            <CheckBox
            
            fontSize={'size13'}
                text={queue_name}
                onChecked={onChecked}
                checked={checked} />
        </Button>
    )
}

export default QueueListModal
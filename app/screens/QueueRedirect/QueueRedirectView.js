import React, { Component } from 'react';
import {
    View,
    ActivityIndicator
} from 'react-native';
import { styles, customizeStyle } from './styles';
import { metrics, Icons } from '../../config';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { FlatList } from 'react-native-gesture-handler';
import { QueueItem } from '../../components/queueItem/queueItem';
import { Width, getHeight, getWidth } from '../../components/utils/dimensions';
import { _string } from '../../local';
import { ColorsList } from '../../components/PropsStyles';
import { Card } from '../../components/card/card';
import ButtonWithHeaderSection from '../../components/buttonWithHeaderSection/buttonWithHeaderSection';
import { Button } from '../../components/button/button';
import QueueListModal from './QueueListModal';

const TextAndIconItem = ({ iconName, text, style, fontSize }) => {
    return (
        <Card style={style}>
            <View style={styles.iconView}>
                <Icons
                    name={iconName}
                    width={getWidth(15)}  height={getWidth(15)}
                />
            </View>
            <_Text
                style={customizeStyle.textItem(fontSize)} >
                {text}
            </_Text>
        </Card>
    )
}

export default class QueueRedirectView extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        let { selectedCustomer } = this.props
        return (
            <View style={styles.container}>
                <Header style={{ width: '100%', zIndex: 100 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />
                <HeaderSection
                    iconSize={getWidth(28)}
                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}
                    text={_string("Redirect")}
                    iconName='redirect'
                />

                {/* ----------- user info -------------- */}
                <Card style={{ backgroundColor: 'white', shadow: 6, }}>
                    <View style={styles.modalHeader}>
                        
                        
                        <View style={styles.customerInfo}>
                            <View
                                style={styles.headerIconView}>
                                <Icons
                                    name='user'
                                    width={getWidth(28)}  height={getWidth(28)}
                                />
                            </View>
                            <View style={styles.headerNameView}>
                                <_Text
                                    style={customizeStyle.queueText} >
                                    {selectedCustomer.fname + ' ' + selectedCustomer.lname}
                                </_Text>
                            </View>
                        </View>


                        <View style={styles.textAndIconItemView}>
                            <TextAndIconItem
                                fontSize='small1'

                                style={customizeStyle.textAndIconItemStyle(getWidth(120))}
                                iconName='phone'
                                text={selectedCustomer.phoneNumber?selectedCustomer.phoneNumber:_string("ThereIsNotPhone")}
                            />
                            <TextAndIconItem
                                fontSize='small1'

                                style={customizeStyle.textAndIconItemStyle(145, '100%', 'flex-end')}
                                iconName='email'
                                text={selectedCustomer.email}
                            />
                        </View>
                    </View>
                </Card>

                {/* -------------- redirect ------------- */}
                <Card style={{ flexDirection: 'row', width: getWidth(345), marginTop: getHeight(15) }}>
                    <Icons name={'queuegray'} width={getWidth(22)}  height={getWidth(22)} />
                    <_Text style={customizeStyle.redirectLabel}>{_string("RedirectTo")}</_Text>
                </Card>
                <ButtonWithHeaderSection
                    onPress={() => {
                        this.selectQueue.onAnimated(true)
                    }}
                    numberOfLines={1}
                    value={this.props.selectedQueueName != '' ? this.props.selectedQueueName : _string("SelectQueue")}
                    isNotTitle
                />

                <Button
                    isLoading={this.props.loadingRedirectCustomer}
                    onPress={this.props.onRedirectCustomer}
                    iconStyle={{ width: getWidth(18),height:getHeight(18),color: '#ffffff'}}
                    text={_string("Done")}
                    iconName='done'
                    style={customizeStyle.nextButton} />


                {/* -------------- queue list model -------------- */}
                <QueueListModal
                    ref={(selectQueue) => this.selectQueue = selectQueue}
                    queueList={this.props.QueueList}
                    selectedQueueID={this.props.selectedQueueID}
                    loadingQueueList={this.props.loadingQueueList}
                    // onEndReachedServiceList={this.props.onEndReachedServiceList}
                    onQueueSelect={(selectedQueue) => {
                        this.props.queueSelected(selectedQueue)
                        this.selectQueue.onAnimated()
                    }}
                />
            </View>
        );
    }
}

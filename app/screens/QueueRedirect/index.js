import React, { Component } from 'react';
import { View, Text } from 'react-native';
import QueueRedirectView from './QueueRedirectView';
import { ActionCreators } from '../../actions';
import { connect } from 'react-redux';
import { NavigationEvents } from 'react-navigation';
import { _Toast } from '../../components/toast/toast';
import { _string } from '../../local';

class QueueRedirect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedQueueID: -1,
            selectedQueueName: '',
        };
    }

    queueSelected = (selectedQueue) => {
        console.log('selectedQueue',selectedQueue)
        this.setState({
            selectedQueueID: selectedQueue.id,
            selectedQueueName: selectedQueue.name
        })
    }

    getQueuList = (firstTime) => {
        this.props.getQueueList(firstTime)
    }

    onRedirectCustomer = () => {
        let { selectedQueueID } = this.state
        if(selectedQueueID == -1){
            _Toast.open(_string('PleaseSelectAQueue'), 'w')
            return
        }
        this.props.redirectCustomer(selectedQueueID)
    }

    componentDidMount() {
        this.getQueuList()
    }

    render() {
        return (
            <View>
                {/* <NavigationEvents onDidFocus={() => this.getQueuList(true)} /> */}
                <QueueRedirectView
                    {...this.state}
                    {...this.props}
                    queueSelected={this.queueSelected}
                    onRedirectCustomer={this.onRedirectCustomer}
                />
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        selectedCustomer: state.queueReducer.selectedCustomer,

        QueueList: state.queueReducer.QueueList,
        loadingQueueList: state.queueReducer.loadingQueueList,

        loadingRedirectCustomer: state.queueReducer.loadingRedirectCustomer
    };
}
function mapDispatchToProps(dispatch) {
    return {
        redirectCustomer: (selectedQueueID) => dispatch(ActionCreators.redirectCustomer(selectedQueueID)),
        getQueueList: (firstTime) => dispatch(ActionCreators.getQueueList(firstTime)),
        selectedQueue: (id, name) => dispatch(ActionCreators.selectedQueue(id, name)),
    };
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(QueueRedirect);
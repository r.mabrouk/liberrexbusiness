import { StyleSheet } from 'react-native';
import { Width, Height, getWidth, getHeight } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        width: Width,
        height: '100%',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: ColorsList.white
    },
    popupHeader: {
        flexDirection: 'row', width: getWidth(345),
        justifyContent: 'center', alignItems: 'center',
        marginTop:getHeight(20),
      
    },
    popupHeaderText: {

        marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily:'bold',
    },
   
    modalHeader: {
        width: getWidth(375),
        height: getHeight(65),
        marginTop: getHeight(20),
        marginBottom: getHeight(8),
        flexDirection: 'column',
        // justifyContent: 'space-between',
        alignItems: 'center'
    },
    customerInfo: {
        width: getWidth(135),
        height: getHeight(30),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerIconView: {
        height: '100%',
        width: getWidth(30),
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerNameView: {
        marginLeft: getWidth(6),
        height: '100%',
        width: getWidth(103),
        justifyContent: 'center',
        alignItems: 'center'
    },
    textAndIconItemView: {
        width: '70%',
        height: getHeight(25),
        flexDirection: 'row',
        justifyContent: 'space-between',
        // backgroundColor: 'red',
        alignItems: 'center',
        marginTop:getHeight(5)
    },
    iconView: {
        width: getWidth(25),
        height: getHeight(25),
        justifyContent: 'center',
        alignItems: 'center'
    },
    popupContainer: {
        // height: '100%',
        width: '100%',
        alignItems: 'center'
    },
}

const customizeStyle = {
    textAndIconItemStyle: (width, _height, alignItems) => ({
        // width: getWidth(width),
        height: _height,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: alignItems
    }),
    queueText: {
        fontSize: 'medium15Px',
        fontFamily: 'bold',
        color: 'darkGray1'
    },
    textItem: (fontSize) => ({
        fontSize: fontSize ? fontSize : 'medium15Px',
        fontFamily: 'regular',
        color: 'lightGray1'
    }),
    nextButton: {
        alignItems: 'center',
        justifyContent: 'center',
        width: getWidth(345),
        marginTop: getHeight(5),
        textColor: '#fff',
        shadow: 6,
        height: getHeight(44),
        colors: 'blue',
        borderRadius: 3,
        fontSize: 'size15',
        fontFamily: 'bold'
    },
    redirectLabel: {
        fontSize: 'size14',
        fontFamily: 'bold',
        marginLeft: getWidth(5),
        color: 'darkGray1',
        marginBottom: getHeight(5)
    },
    popupHeaderText: {
        marginVertical: getHeight(10),
        // marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily:'bold',
    },
}

export { styles, customizeStyle };

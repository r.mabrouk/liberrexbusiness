import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    FlatList
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { images } from '../../config';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { PendingItem } from '../../components/pendingItem/pendingItem';
import PendingModal from './pendingModal';
import UnBlockCustomerModal from './unBlockCustomerModal';
import AddNewCustomerModal from './addCustomerModal';
import CustomerListModal from './customersListModal';


class PendingView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            statusList: [
                {
                    status: 'Waiting',
                    usedValue: 35,
                    totalValue: 50
                },
                {
                    status: 'Hours Left',
                    usedValue: '05',
                    totalValue: 29
                },
                {
                    status: 'Rejected',
                    usedValue: 10,
                    totalValue: 50
                }
            ],
            openCard: false,
            currentIndex: -1
        }
    }
    async componentDidMount() {
        this.props.nextScreen()
    }
    render() {
        return (
            <View
                style={styles.container}>
                <Header style={{ width: '100%' }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />
                <View style={styles.contentContainer}>
                    <HeaderSection
                        text=' QUEUE 1 (HAIR CUT)'
                        iconName='queueIconGray'
                    />
                    <FlatList
                        extraData={this.state}
                        contentContainerStyle={styles.requestItem}
                        showsVerticalScrollIndicator={false}
                        data={this.state.statusList}
                        renderItem={({ item, index }) => {
                            return (
                                <PendingItem
                                    onPress={() => {

                                        this._CustomerListModal.onAnimated(true)
                                    }}
                                />
                            )
                        }} />
                </View>
                <CustomerListModal
                    duration={1000}
                    ref={(_CustomerListModal) => this._CustomerListModal = _CustomerListModal}
                />
            </View>
        );
    }
}


PendingView.propTypes = {
    nextScreen: PropTypes.func
};

export default PendingView;
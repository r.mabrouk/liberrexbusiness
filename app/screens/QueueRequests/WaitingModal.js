import React, { Component } from 'react';
import {
    View
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Icons } from '../../config';
import { _Text } from '../../components/text/text';
import { PopupModal } from '../../components/popupModal/popupModal';
import { getHeight, getWidth } from '../../components/utils/dimensions';
import { Card } from '../../components/card/card';
import { Button } from '../../components/button/button';
import { _string } from '../../local';


type Props = {
    duration: number,
    name: String,
    location: String,
    phoneNumber: String,
    email: String,
    date: String,
    services: String,
    onRedirectPress: Function,
    onDeclinePress: Function,
    onApprovePress: Function
}
class WaitingModal extends Component<Props> {
    constructor(props) {
        super(props);
    }
    async componentDidMount() {
    }
    onAnimated = (state) => {
        this.PendingAppointModal.onAnimated(state)
    }
    render() {
        return (
            <PopupModal
                duration={400}
                ref={_popupModal => this.PendingAppointModal = _popupModal}
                height={getHeight(290)}
            >
                <Card style={styles.modalContainer}>
                        <View style={styles.customerInfo}>
                                <Icons
                                    name='user'
                                    width={getWidth(28)}
                                    height={getWidth(28)}
                                />
                                <_Text
                                    style={styles.queueText} >
                                    {this.props.name}
                                </_Text>
                        </View>

                        {/* <TextAndIconItem
                            fontSize='small1'
                            style={styles.textAndIconItemStyle(195, getHeight(20))}
                            iconName='location'
                            text={this.props.location}

                        /> */}
                        <View style={styles.textAndIconItemView}>
                            <TextAndIconItem
                                fontSize='small1'

                                style={styles.textAndIconItemStyle(120, '100%', 'flex-start')}
                                iconName='phone'
                                text={this.props.phoneNumber?this.props.phoneNumber:_string("ThereIsNotPhone")}
                            />
                            <TextAndIconItem
                                fontSize='small1'
                                style={styles.textAndIconItemStyle(145, '100%', 'flex-end')}
                                iconName='email'
                                text={this.props.email?this.props.email:_string("ThereIsNotEmail")}
                            />
                        </View>
                    <InputItem
                    
                        iconName='calendar'
                        text={`${_string("JoinedQueueAt")} ${this.props.date}`}
                        
                    />
                    <InputItem
                        iconName='grid'
                        text={this.props.services?this.props.services:_string("ThereIsNotServices")}
                        border={1}
                    />
                    <View style={styles.buttonViewContainer}>
                        <Button
                            onPress={this.props.onRedirectPress}
                            style={styles.buttonsStyle}
                            iconName='redirect'
                            text={_string("Redirect")}
                            iconStyle={{ width: getWidth(18),height:getHeight(18) }}

                        />
                        <Button
                            isLoading={this.props.loadingCancelCustomerInWaiting}
                            loaderColor={'blue'}
                            onPress={this.props.onCancelPress}
                            style={styles.buttonsStyle}
                            iconName='cancel'
                            text={_string('CancelIt')}
                            iconStyle={{ width: getWidth(18),height:getHeight(18),color:'red' }}

                        />
                    </View>
                </Card>

            </PopupModal>
        );
    }
}
const TextAndIconItem = ({ iconName, text, style, fontSize }) => {
    return (
        <Card style={style}>
            <View style={styles.iconView}>
                <Icons
                    name={iconName}
                    width={getWidth(15)}
                    height={getWidth(15)}
                />
            </View>
            <_Text
                style={styles.textItem(fontSize)} >
                {text}
            </_Text>
        </Card>
    )
}

const InputItem = ({ iconName, text, border, style }) => {
    return (
        <Card style={style ? style : border ? styles.cardContainer(border) : styles.cardContainer()}>
            <View style={styles.iconView}>
                <Icons
                    name={iconName}
          
                    width={getWidth(18)}
                    height={getWidth(18)}
                />
            </View>
            <_Text
                style={styles.textStyle} >
                {text}
            </_Text>
        </Card>
    )
}

WaitingModal.propTypes = {
    nextScreen: PropTypes.func,
    name: "REMON NABIL",
    location: 'B2401 Smart Village, Giza, Egypt.',
    phoneNumber: '+20 128 406 5814',
    email: 'remon@oncloudeg.com',
    date: '25 June,2019 4:00PM',
    services: 'Hair Cut, Hair coloring, Shave',
    onRedirectPress: PropTypes.func,
    onDeclinePress: PropTypes.func,
    onApprovePress: PropTypes.func
};

export default WaitingModal;
import React, { Component } from 'react';
import {
    View
} from 'react-native';
import PropTypes from 'prop-types';
import { Icons } from '../../config';
import { _Text } from '../../components/text/text';
import { PopupModal } from '../../components/popupModal/popupModal';
import { getWidth, getHeight } from '../../components/utils/dimensions';
import { Card } from '../../components/card/card';
import { Button } from '../../components/button/button';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { _string } from '../../local';


type AddNewCustomerModalProps = {
    duration: number
}
class AddNewCustomerModal extends Component<AddNewCustomerModalProps> {
    constructor(props) {
        super(props);
    }
    async componentDidMount() {
    }
    onAnimated = (state) => {
        this.AddCustomerModal.onAnimated(state)
    }
    render() {
        return (
            <PopupModal
                duration={this.props.duration}
                ref={_popupModal => this.AddCustomerModal = _popupModal}
                height={getHeight(400)}  >

                <Card
                    style={styles.container}>
                    <View style={styles.iconAndText}>
                        <View
                            style={styles.iconStyle}>
                            <Icons
                                name='user'
                                width={getWidth(28)}
                                height={getHeight(28)}
                            />
                        </View>
                        <View style={styles.headerNameView}>
                            <_Text
                                style={styles.textStyle} >
                                {_string("queue.add_a_new_customer").toLocaleUpperCase()}
                            </_Text>
                        </View>
                    </View>

                    <View style={styles.inputsContainer}>
                        <CustomTextInput
                            placeholder={_string("pleace_holder")}
                            headerIcon='userGray'
                            headerText={_string("queue.full_name")}
                            leftIcon='editpen' />
                        <CustomTextInput
                            placeholder={_string("pleace_holder")}
                            headerIcon='email'
                            headerText={_string("queue.email")}
                            leftIcon='editpen' />
                        <CustomTextInput
                            placeholder={_string("pleace_holder")}
                            headerIcon='phone'
                            headerText={_string("queue.phone_number")}
                            leftIcon='editpen' />
                    </View>

                    <Button

                        iconStyle={{ width: getWidth(18), height: getHeight(18) }}
                        text={_string("queue.add_new_customer")}
                        iconName='done'
                        style={styles.buttonStyle} />

                </Card>

            </PopupModal>
        );
    }
}


const styles = {
    container: {
        width: getWidth(375),
        height: getHeight(400),
        alignItems: 'center'
    },
    iconAndText: {
        width: getWidth(208),
        height: getHeight(28),
        marginTop: getHeight(20),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    iconStyle: {
        height: '100%',
        width: getWidth(30),
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerNameView: {
        height: '100%',
        width: getWidth(170),
        justifyContent: 'center'
    },
    textStyle: {
        fontSize: 'medium15Px',
        fontFamily: 'bold',
        color: 'darkGray1'
    },
    inputsContainer: {
        height: getHeight(260),
        getWidth: getWidth(375),
        // marginBottom: getHeight(8),
        marginTop: getHeight(13),
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonStyle: {
        height: getHeight(44),
        width: getWidth(345),
        borderRadius: 3,
        marginTop: getHeight(15),

        justifyContent: 'center',
        alignItems: 'center',
        textColor: 'white',
        colors: 'blue',
        fontSize: 'medium15Px',
        fontFamily: 'bold'
    }
}

AddNewCustomerModal.propTypes = {
    nextScreen: PropTypes.func
};

export default AddNewCustomerModal;
import React, { Component } from 'react';
import {
    View
} from 'react-native';
import PropTypes from 'prop-types';
import { Icons } from '../../config';
import { _Text } from '../../components/text/text';
import { PopupModal } from '../../components/popupModal/popupModal';
import { getWidth, getHeight } from '../../components/utils/dimensions';
import { Card } from '../../components/card/card';
import { Button } from '../../components/button/button';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { ColorsList } from '../../components/PropsStyles';
import { _string } from '../../local';


type CustomerListModalProps = {
    duration: number
}
class CustomerListModal extends Component<CustomerListModalProps> {
    constructor(props) {
        super(props);
    }
    async componentDidMount() {
    }
    onAnimated = (state) => {
        this.CustomerModal.onAnimated(state)
    }
    render() {
        return (
            <PopupModal
                duration={this.props.duration}
                ref={_popupModal => this.CustomerModal = _popupModal}
                height={getHeight(520)}  >
                <Card
                    style={styles.container}>
                    <View style={styles.iconAndText}>
                        <View
                            style={styles.iconStyle}>
                            <Icons
                                name='user'
                                width={getWidth(28)} height={getWidth(28)}
                            />
                        </View>
                        <View style={styles.headerNameView}>
                            <_Text
                                style={styles.textStyle} >
                                {(_string("queue.edit") + " ACHRAF AMMAR").toLocaleUpperCase()}
                            </_Text>
                        </View>
                    </View>

                    <View
                        style={styles.inputsContainer}>
                        <CustomTextInput
                            value='Achraf Ammar'
                            headerIcon='userGray'
                            headerText={_string("queue.full_name")}
                            leftIcon='editpen' />
                        <CustomTextInput
                            value='remon@oncloudeg.com'
                            headerIcon='email'
                            headerText={_string("queue.email")}
                            leftIcon='editpen' />
                        <CustomTextInput
                            value='+20-128-406-5814'
                            headerIcon='phone'
                            headerText={_string("queue.phone_number")}
                            leftIcon='editpen' />
                    </View>

                    <Button
                         iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                        text={_string("queue.block_this_customer")}
                        iconName='cancel'
                        style={styles.normalButtonStyle} />
                    <Button
                         iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                        text={_string("queue.add_to_vip_customers")}
                        iconName='fullStar'
                        style={styles.normalButtonStyle} />
                    <Button
                               iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                        text={_string("queue.save")}
                        iconName='done'
                        style={styles.blueButtonStyle} />

                </Card>

            </PopupModal>
        );
    }
}


const styles = {
    container: {
        width: getWidth(375),
        height: getHeight(520),
        alignItems: 'center'
    },
    iconAndText: {
        width: getWidth(208),
        height: getHeight(28),
        marginTop: getHeight(20),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    iconStyle: {
        height: '100%',
        width: getWidth(30),
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerNameView: {
        height: '100%',
        width: getWidth(170),
        justifyContent: 'center'
    },
    textStyle: {
        fontSize: 'medium15Px',
        fontFamily: 'bold',
        color: 'darkGray1'
    },
    inputsContainer: {
        height: getHeight(260),
        getWidth: getWidth(375),
        marginBottom: getHeight(8),
        marginTop: getHeight(10),
        justifyContent: 'center',
        alignItems: 'center'
    },
    normalButtonStyle: {
        height: getHeight(44),
        width: getWidth(345),
        shadow: 3,
        backgroundColor: 'white',
        borderRadius: 3,
        marginTop: getHeight(15),
        justifyContent: 'center',
        alignItems: 'center',
        textColor: ColorsList.darkGray1,
        fontSize: 'medium15Px',
        fontFamily: 'bold'
    },
    blueButtonStyle: {
        height: getHeight(44),
        width: getWidth(345),
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        textColor: 'white',
        marginTop: getHeight(15),

        colors: 'blue',
        fontSize: 'medium15Px',
        fontFamily: 'bold'
    }
}

CustomerListModal.propTypes = {
    nextScreen: PropTypes.func
};

export default CustomerListModal;
import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';
import { NavigationEvents } from 'react-navigation';
import { ActionCreators } from '../../actions';
import QueueRequestsView from './queueRequestsView';
import Pusher from 'pusher-js/react-native'
import { getQueueWaitingListResponse } from '../../actions/queue';

const pusherConfig = {
  "appId" : "753151",
  "key" :"4393daed5c7e1527a9ae",
  "secret" : "bad24061dd9f66bfb8d7",
  "cluster" : "eu",
  "encrypted": true
  // "restSe rver":"http://192.168.0.15:4000"
} 
class QueueRequests extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentScrollIndex: 0,
      timeLeft: { hour: 0, minute: 0 },
      timeLeftInMinutes: 0,
      currentCountDown: -1,
    }

    this.pusher = new Pusher(pusherConfig.key, pusherConfig); // (1)
    this.chatChannel = this.pusher.subscribe(`queue_${props.navigation.getParam("queue_id")}`); // (2)
    this.chatChannel.bind('pusher:subscription_succeeded', () => { // (3)
      this.chatChannel.bind('join', (data) => { // (4)
        this.handleJoin(data);
      });
      this.chatChannel.bind('exit', (data) => { // (5)
        this.handleExit(data);
      });

    });

  }
  handleJoin=(data)=>{
    console.log(data,"update Witing list")
    this.props.updateWaitingList(data)
  }
  handleExit=(data)=>{

  }
  nextScreen = () => {

    setTimeout(() => {
    }, 2000);
  }

  currentScrollView = (index) => {
    console.log('QueueRequests', 'currentScrollView', 'index', index)
    this.setState({ currentScrollIndex: index })
    if (index == 1) {
      this.getpendingList()
    }
  }
  getWaitingList = () => {
    this.props.getQueueWaitingList()
  }

  getpendingList = () => {
    if (!this.props.loadingQueuePendingList && this.props.queuePendingList.length == 0) {
      this.props.getQueuePendingList()
    }
  }

  getFormatedDate = (date) => {
    let dateFormated = moment(date).format('DD MMM, YYYY HH:mm a')
    console.log('dateFormated', dateFormated)
    return dateFormated
  }

  getServicesNames = (services) => {
    servicesID = services.split(',')
    for (let i = 0; i < servicesID.length; i += 1) {
      this.getServiceName(servicesID[0], i == 0)
    }
  }

  getServiceName = (service_id, firstTime) => {
    this.props.getService(service_id, firstTime)
  }

  onRedirectPress = (customer) => {
    this.props.selectedCustomer(customer)
    this.props.navigation.navigate('QueueRedirect')
  }

  onDeclinePress = (queue_request_id, onCloseModal) => {
    this.props.declineQueueRequest(queue_request_id, onCloseModal)
  }

  onApprovePress = (queue_request_id, onCloseModal) => {
    this.props.approveQueueRequest(queue_request_id, onCloseModal)
  }

  onCancelWaitingPress = (customerID, onCloseModal) => {
    this.props.cancelCustomerInWaiting(customerID, onCloseModal)
  }

  onPushBackPress = (customer1_ID) => {
    // if (this.props.queueWaitingList.length > 1) {
      let nextCustomer_id=0
      if (this.props.queueWaitingList[1])
      nextCustomer_id=this.props.queueWaitingList[1].ticket_id

      this.props.swapCustomerRanks(customer1_ID, nextCustomer_id)
    // }
  }

  convertMinutesToHoursAndMinutes = (totalMinutes) => {
    if (totalMinutes < 0) {
      return { hour: 0, minute: 0 }
    }
    let num = totalMinutes;
    let hours = (num / 60);
    let rhours = Math.floor(hours);
    let minutes = (hours - rhours) * 60;
    let rminutes = Math.round(minutes);
    return { hour: rhours, minute: rminutes }
  }

  calculateTimeLeft = (statusUpdateTimestamp, cumulative_waiting_time) => {
    let expectedFinishing = moment(statusUpdateTimestamp).add(cumulative_waiting_time, 'minutes')
    console.log('calculateTimeLeft', 'expectedFinishing', expectedFinishing.format('HH:mm:ss'))
    // let minutesFromNow = moment('2019-08-29 03:38:13').add(20, 'minutes')
    let timeNow = moment()
    let timeLeftInMinutes = expectedFinishing.diff(timeNow, 'minutes')
    this.setState({ timeLeftInMinutes })
    console.log('calculateTimeLeft', 'timeNow', timeNow.format('LLL'))
    console.log('calculateTimeLeft', 'FromNow', expectedFinishing.fromNow())
    console.log('calculateTimeLeft', 'diff', timeNow.diff(expectedFinishing, 'minutes'))
    return this.convertMinutesToHoursAndMinutes(timeLeftInMinutes)

  }

  decrementTimeLeft = () => {
    let newtimeLeftInMinutes = this.state.timeLeftInMinutes - 1
    let newTimeLeft = this.convertMinutesToHoursAndMinutes(newtimeLeftInMinutes)
    this.setState({ timeLeft: newTimeLeft, timeLeftInMinutes: newtimeLeftInMinutes })
    if (newtimeLeftInMinutes == 0) {
      clearInterval(this.CountDown);
    }
  }

  makeCountDown = () => {
    this.CountDown = setInterval(() => {
      this.decrementTimeLeft();
    }, 60000);  //60000
  }


  componentWillUnmount() {
    console.log('componentWillUnmount')
    if (this.CountDown) {
      clearInterval(this.CountDown);
      console.log('componentWillUnmount', 'found interval')
    }
  }

  // componentDidMount() {
  //   this.getWaitingList()
  //   this.calculateTimeLeft()
  // }

  componentWillReceiveProps(props) {
    if (!props.loadingQueueWaitingList && props.queueWaitingList.length > 0) {
      let queueWaitingList = props.queueWaitingList
      for (let i = 0; i < queueWaitingList.length; i += 1) {
        if (queueWaitingList[i].status == 'inservice') {
          if (queueWaitingList[i].id != this.state.currentCountDown) {
            let timeLeft = this.calculateTimeLeft(queueWaitingList[i].statusUpdateTimestamp, queueWaitingList[i].cumulative_waiting_time)
            this.setState({ timeLeft })
            if (this.CountDown)
              clearInterval(this.CountDown);
            this.makeCountDown()
            this.setState({ currentCountDown: queueWaitingList[i].id })
          console.log('componentWillReceiveProps', 'timeLeft', timeLeft)
          }
        }
      }
    }
  }


  render() {

    let width = this.props.width
    return (
      <View>
        <NavigationEvents
          onWillFocus={() => {
            this.getWaitingList()
            this.getpendingList()
          }}
        // onDidFocus={() => this.getWaitingList()} 
        />
        <QueueRequestsView
          {...this.props}
          {...this.state}
          nextScreen={this.nextScreen}
          currentScrollView={this.currentScrollView}
          getFormatedDate={this.getFormatedDate}
          onApprovePress={this.onApprovePress}
          onDeclinePress={this.onDeclinePress}
          onRedirectPress={this.onRedirectPress}
          onCancelWaitingPress={this.onCancelWaitingPress}
          getServicesNames={this.getServicesNames}
          onPushBackPress={this.onPushBackPress}
          convertMinutesToHoursAndMinutes={this.convertMinutesToHoursAndMinutes}
          calculateTimeLeft={this.calculateTimeLeft}
        />
      </View>

    );
  }
}
function mapStateToProps(state) {
  return {
    selectedQueueID: state.queueReducer.selectedQueueID,
    selectedQueue: state.queueReducer.selectedQueue,
    queueWaitingList: state.queueReducer.queueWaitingList,
    loadingQueueWaitingList: state.queueReducer.loadingQueueWaitingList,
    loadingQueuePendingList: state.queueReducer.loadingQueuePendingList,
    queuePendingList: state.queueReducer.queuePendingList,
    // queuePendingList: state.queueReducer.queueWaitingList,
    servicesList: state.servicesReducer.servicesList,
    selectedServiceNames: state.servicesReducer.selectedServiceNames,

    loadingApproveQueueRequest: state.queueReducer.loadingApproveQueueRequest,
    loadingDeclineQueueRequest: state.queueReducer.loadingDeclineQueueRequest,

    loadingCancelCustomerInWaiting: state.queueReducer.loadingCancelCustomerInWaiting,

    loadingSwapCustomerRanks: state.queueReducer.loadingSwapCustomerRanks,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getQueueWaitingList: () => dispatch(ActionCreators.getQueueWaitingList()),
    updateWaitingList:(res)=>dispatch(getQueueWaitingListResponse(res)),
    getQueuePendingList: () => dispatch(ActionCreators.getQueuePendingList()),
    approveQueueRequest: (queue_request_id, onCloseModal) => dispatch(ActionCreators.approveQueueRequest(queue_request_id, onCloseModal)),
    declineQueueRequest: (queue_request_id, onCloseModal) => dispatch(ActionCreators.declineQueueRequest(queue_request_id, onCloseModal)),
    getService: (service_id, firstTime) => dispatch(ActionCreators.getService(service_id, firstTime)),
    selectedCustomer: (customer) => dispatch(ActionCreators.selectedCustomer(customer)),
    cancelCustomerInWaiting: (customer_id, onCloseModal) => dispatch(ActionCreators.cancelCustomerInWaiting(customer_id, onCloseModal)),
    swapCustomerRanks: (customer1_id, customer2_id) => dispatch(ActionCreators.swapCustomerRanks(customer1_id, customer2_id)),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QueueRequests);

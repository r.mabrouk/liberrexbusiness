import React, { Component } from 'react';
import {
    View
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Icons } from '../../config';
import { _Text } from '../../components/text/text';
import { PopupModal } from '../../components/popupModal/popupModal';
import { getHeight, getWidth } from '../../components/utils/dimensions';
import { Card } from '../../components/card/card';
import { Button } from '../../components/button/button';
import { _string } from '../../local';
import moment from 'moment'

type Props = {
    duration: number,
    name: String,
    location: String,
    phoneNumber: String,
    email: String,
    date: String,
    services: String,
    onRedirectPress: Function,
    onDeclinePress: Function,
    onApprovePress: Function
}

class PendingModal extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            customer: {

            },
            index: -1
        }
    }
    async componentDidMount() {
    }
    onAnimated = (state, item, index) => {
        if (item)
        this.setState({ customer: item, index })
        
        this.PendingAppointModal.onAnimated(state)
    }
    getFormatDate = (date) => {
        return moment(date).format("DD MMM,YYYY HH:mm")
    }
    render() {
        let { customer } = this.state
        return (
            <PopupModal duration={500} ref={_popupModal => this.PendingAppointModal = _popupModal}
                height={getHeight(320)}  >
                <Card style={styles.modalContainer}>
                    <View style={styles.customerInfo}>
                        <Icons
                            name='user'
                            width={getWidth(30)}
                            height={getWidth(30)}
                        />
                        <_Text
                            style={styles.custometName} >
                            {`${customer.fname} ${customer.lname}`.toLocaleUpperCase()}
                        </_Text>
                    </View>
                    <View style={styles.textAndIconItemView}>
                        <TextAndIconItem
                            fontSize='size12'
                            style={styles.textAndIconItemStyle()}
                            iconName='phone'
                            text={customer.phone_number ? customer.phone_number : _string("NoPhoneNumber")}
                        />
                        <TextAndIconItem
                            fontSize='size12'
                            style={styles.textAndIconItemStyle(getWidth(0))}
                            iconName='email'
                            text={customer.email ? customer.email :_string("NoCustomerEmail")}
                        />
                    </View>

                    <TextWithIcon
                        marginTop={getHeight(15)}
                        iconName='calendar'
                        text={this.getFormatDate(customer.created_at)}
                        border={1}
                    />
                    <TextWithIcon
                        iconName='grid'
                        text={customer.service_names ? customer.service_names : _string("NoServices")}
                        border={0}
                    />
                    <View style={styles.buttonViewContainer}>
                        <Button
                            onPress={this.props.onRedirectPress}
                            style={styles.buttonsStyle}
                            iconName='redirect'
                            text={_string("Redirect")}
                            iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                        />
                        <Button
                            onPress={this.props.onCancel}

                            style={styles.buttonsStyle}
                            iconName='cancel'
                            text={_string("appointments_approved.cancel_it")}
                   
                            iconStyle={{ width: getWidth(18),height:getHeight(18), color: 'red'}}
                        />

                    </View>
                    <Button
                        isLoading={this.props.loadingApproveQueueRequest}
                        onPress={this.props.onApprovePress}
                        iconName='done'
                        text={_string("queue.approve")}
                        iconStyle={{ width: getWidth(18),height:getHeight(18) }}
                        style={styles.approveButton}
                    />
                </Card>

            </PopupModal>
        );
    }
}
const TextAndIconItem = ({ iconName, text, style, fontSize }) => {
    return (
        <Card style={style}>
            <Icons
                name={iconName}
                width={getWidth(18)} height={getWidth(18)}
            />
            <_Text
                style={styles.textItem(fontSize)} >
                {text}
            </_Text>
        </Card>
    )
}

const TextWithIcon = ({ iconName, text, border, marginTop }) => {
    return (
        <Card style={styles.cardContainer(border, marginTop)}>
            <Icons
                name={iconName}
                width={getWidth(18)} height={getWidth(18)}
            />
            <_Text
                style={styles.textStyle} >
                {text}
            </_Text>
        </Card>
    )
}
export default PendingModal;

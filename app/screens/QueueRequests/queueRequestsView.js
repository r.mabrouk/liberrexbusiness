import React, { Component } from 'react';
import {
    View,
    FlatList,
    ActivityIndicator
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { ReportRequestItem } from '../../components/reportRequestItem/reportRequestItem';
import { QueueCardRequest } from '../../components/queueCardRequest/queueCardRequest';
import { Button } from '../../components/button/button';
import { ScrollView } from 'react-native-gesture-handler';
import { Card } from '../../components/card/card';
import { Width, getWidth, getHeight } from '../../components/utils/dimensions';
import { PendingItem } from '../../components/pendingItem/pendingItem';
import SwiperTabButtons from '../../components/swiperTabButtons/swiperTabButtons';
import PendingModal from './pendingModal';
import { _string } from '../../local';
import { ColorsList } from '../../components/PropsStyles';
import WaitingModal from './WaitingModal';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import EmptyData from '../../components/emptyData/emptyData';

class QueueRequestsView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            statusList: [
                {
                    status: 'Waiting',
                    usedValue: 35,
                    totalValue: 50
                },
                {
                    status: 'Hours Left',
                    usedValue: '05',
                    totalValue: 29
                },
                {
                    status: 'Rejected',
                    usedValue: 10,
                    totalValue: 50
                }
            ],
            pendingList: [

                {
                    status: 'Waiting',
                    usedValue: 35,
                    totalValue: 50
                },
                {
                    status: 'Hours Left',
                    usedValue: '05',
                    totalValue: 29
                },
                {
                    status: 'Rejected',
                    usedValue: 10,
                    totalValue: 50
                },

                {
                    status: 'Waiting',
                    usedValue: 35,
                    totalValue: 50
                },
                {
                    status: 'Hours Left',
                    usedValue: '05',
                    totalValue: 29
                },
                {
                    status: 'Rejected',
                    usedValue: 10,
                    totalValue: 50
                }


            ],
            openCard: false,
            currentIndex: -1,

            selectedPendingRequest: 0,
            selectedWaitingRequest: 0,

            popupPendingButtonPressIndex: -1,
            popupWaitingButtonPressIndex: -1,

            firstInWaiting: true,
            waitingListStatus: []
        }
    }
    async componentDidMount() {
        this.props.nextScreen()
    }

    onClickCard = (index) => {
        this.setState({ currentIndex: index }, () => {
            this.setState((prevState) => ({ openCard: !prevState.openCard }))
        })
    }

    itemStatus = (status, index) => {
        let waitingListStatusCopy = [...this.state.waitingListStatus];
        let itemStatus = ''
        if (status == 'inservice') {
            console.log('status', 'Current')
            itemStatus = _string("Current")
        }
        else if (status == 'waiting' && (index == 0 || (index > 0 && waitingListStatusCopy[index - 1] == _string('Current')))) {
            itemStatus = _string('Next')
        }
        if (waitingListStatusCopy.length > index) {
            waitingListStatusCopy[index] = itemStatus
        }
        else {
            waitingListStatusCopy.push(itemStatus)
        }
        this.setState({ waitingListStatus: waitingListStatusCopy })
        return itemStatus
    }
    render() {
        let { queuePendingList, queueWaitingList } = this.props;
        let { selectedPendingRequest, selectedWaitingRequest } = this.state
        console.log('queuePendingList', queuePendingList, ' selectedPendingRequest', selectedPendingRequest)
        console.log('this.state.waitingListStatus', this.state.waitingListStatus)
        return (
            <View
                style={styles.container}>
                <Header onBack={() => this.props.navigation.goBack()} style={{ width: '100%', alignItems: 'center', zIndex: 40 }} />
                <HeaderSection
                    onPress={() => { this.props.navigation.navigate('QueueAddCustomer') }}
                    style={{
                        zIndex: 15,
                        height: getHeight(67),
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        width: getWidth(375), shadow: 6,
                        marginTop: -getHeight(28),
                        paddingTop: getHeight(15),
                        backgroundColor: 'white',
                        paddingHorizontal: getWidth(15)
                    }}
                    textStyle={{
                        fontSize: 'size17',
                        fontFamily: 'bold',
                        color: 'darkGray1',
                        paddingHorizontal: getWidth(10),
                    }}
                    withButton
                    textButton={_string("queue.add_new")}
                    text={`${this.props.selectedQueue.queue.title}`}
                    iconName='queueIconGray'
                    iconButton='addwhite'
                />
                <View style={styles.contentContainer}>
                    <SwiperTabButtons
                        tabButons={[
                            { iconsName: 'Check', label: _string("queue.waiting_list") },
                            { iconsName: 'Pending', label: _string("queue.pending") }]}
                    // onIndexChanged={this.props.currentScrollView}
                    >
                        <View style={{ height: getHeight(465), width: Width, alignItems: 'center' }}>
                            <EmptyData
                                height={'100%'}
                                show={(queueWaitingList.length < 1) && !this.props.loadingQueueWaitingList} />
                            <FlatList
                                extraData={this.props}
                                contentContainerStyle={styles.requestItem}
                                showsVerticalScrollIndicator={false}
                                data={queueWaitingList}
                                renderItem={({ item, index }) => {
                                    return (
                                        <QueueCardRequest
                                            onPress={() => this.onClickCard(index)}
                                            onMorePress={() => {
                                                this.setState({ selectedWaitingRequest: index }, () => {
                                                    this._waitingModal.onAnimated(true)
                                                })
                                            }}
                                            index={index}
                                            currentIndex={this.state.currentIndex}
                                            openCard={this.state.openCard}

                                            number={item.id}
                                            joined={this.props.getFormatedDate(item.checkinDate + ' ' + item.checkinTime)}
                                            name={`${item.fname} ${item.lname}`}
                                            status={
                                                item.status == "inservice" ?
                                                   _string("Current")
                                                    :
                                                    index == 0 ?
                                                        _string("Next")
                                                        :
                                                        index == 1 && queueWaitingList[0].status == "inservice" ?
                                                        _string("Next")
                                                            :
                                                            ''
                                            }
                                            // status={this.itemStatus(item.status)}
                                            time={this.props.convertMinutesToHoursAndMinutes(item.cumulative_waiting_time)}
                                            service={item.service_names}
                                            timeLeft={this.props.timeLeft}
                                            onPushBackPress={() => { this.props.onPushBackPress(item.ticket_id) }}
                                            loadignPushBack={this.props.loadingSwapCustomerRanks}
                                        />
                                    )
                                }}
                                ListFooterComponent={() => {
                                    return (
                                        <LoaderLists isLoading={this.props.loadingQueueWaitingList} />
                                    )
                                }}
                            />

                        </View>

                        <View
                            style={styles.container}>
                            <View style={styles.contentContainer}>


                                <EmptyData
                                    height={getHeight(465)}
                                    show={(queuePendingList.length < 1) && !this.props.loadingQueuePendingList} />
                                <FlatList
                                    extraData={this.props}
                                    contentContainerStyle={styles.requestItem}
                                    showsVerticalScrollIndicator={false}
                                    data={queuePendingList}
                                    renderItem={({ item, index }) => {
                                        return (
                                            <PendingItem
                                                onPress={() => {
                                                    this.setState({ selectedPendingRequest: index }, () => {
                                                        this._pendingModal.onAnimated(true, item, index)
                                                        console.log('queuePendingList[selectedPendingRequest]', queuePendingList[selectedPendingRequest])
                                                    })
                                                }}
                                                name={`${item.fname} ${item.lname}`}
                                                service={item.service_names}
                                                onApprovePress={() => {
                                                    this.setState({ popupPendingButtonPressIndex: index })
                                                    this.props.onApprovePress(item.id)
                                                }
                                                }
                                                loadingApproveQueueRequest={
                                                    this.props.loadingApproveQueueRequest &&
                                                    this.state.popupPendingButtonPressIndex == index}
                                                onDeclinePress={() => {
                                                    this.setState({ popupPendingButtonPressIndex: index })
                                                    this.props.onDeclinePress(item.id)
                                                }}
                                                loadingDeclineQueueRequest={this.props.loadingDeclineQueueRequest && this.state.popupPendingButtonPressIndex == index}
                                                onRedirectPress={() => { this.props.onRedirectPress(item) }}
                                                date={this.props.getFormatedDate(item.created_at)}
                                            />
                                        )
                                    }}
                                    ListFooterComponent={() => {
                                        return (
                                            <LoaderLists isLoading={this.props.loadingQueuePendingList} />
                                        )
                                    }}
                                />

                            </View>

                        </View>

                    </SwiperTabButtons>
                </View>

                {queueWaitingList.length > 0 && <WaitingModal
                    duration={700}
                    ref={(_waitingModal) => this._waitingModal = _waitingModal}
                    name={`${queueWaitingList[selectedWaitingRequest].fname} ${queueWaitingList[selectedWaitingRequest].lname}`}
                    phoneNumber={queueWaitingList[selectedWaitingRequest].phone_number}
                    email={queueWaitingList[selectedWaitingRequest].email}
                    date={this.props.getFormatedDate(queueWaitingList[selectedWaitingRequest].checkinDate + ' ' + queueWaitingList[selectedWaitingRequest].checkinTime)}
                    services={queueWaitingList[selectedWaitingRequest].service_names}
                    onCancelPress={() => {
                        this.setState({ popupWaitingButtonPressIndex: selectedWaitingRequest })
                        this.props.onCancelWaitingPress(queueWaitingList[selectedWaitingRequest].id, this._waitingModal.onAnimated)
                        // this._waitingModal.onAnimated(false)
                    }}
                    loadingCancelCustomerInWaiting={this.props.loadingCancelCustomerInWaiting && this.state.popupWaitingButtonPressIndex == selectedWaitingRequest}
                    onRedirectPress={() => {
                        this._waitingModal.onAnimated(false)
                        this.props.onRedirectPress(queueWaitingList[selectedWaitingRequest])
                    }}
                />}
                {queuePendingList.length > 0 && <PendingModal
                    duration={700}
                    ref={(_pendingModal) => this._pendingModal = _pendingModal}
                    onApprovePress={() => {
                        this.setState({ popupPendingButtonPressIndex: selectedPendingRequest })
                        this.props.onApprovePress(queuePendingList[selectedPendingRequest].id, this._pendingModal.onAnimated)
                        // this._pendingModal.onAnimated(false)
                    }}
                    loadingApproveQueueRequest={this.props.loadingApproveQueueRequest && this.state.popupPendingButtonPressIndex == selectedPendingRequest}
                    onDeclinePress={() => {
                        this.setState({ popupPendingButtonPressIndex: selectedPendingRequest })
                        this.props.onDeclinePress(queuePendingList[selectedPendingRequest].id, this._pendingModal.onAnimated)
                        // this._pendingModal.onAnimated(false)
                    }}
                    loadingDeclineQueueRequest={this.props.loadingDeclineQueueRequest && this.state.popupPendingButtonPressIndex == selectedPendingRequest}
                    onRedirectPress={() => {
                        this._pendingModal.onAnimated(false)
                        this.props.onRedirectPress(queuePendingList[selectedPendingRequest])
                    }}
                />}
            </View>
        );
    }
}


QueueRequestsView.propTypes = {
    nextScreen: PropTypes.func
};

export default QueueRequestsView;
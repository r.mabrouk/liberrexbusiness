import { StyleSheet } from 'react-native';
import { metrics } from '../../config';
import { getHeight, getWidth, Height, Width } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        width: getWidth(375),
        height: Height,
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: ColorsList.white,

    },
    contentContainer: {
        // justifyContent: 'center',
        alignItems: 'center',
        width: Width,
    },
    imageBackground: {
        width: getWidth(375),
        height: '100%',
        alignItems: 'center'
    },
    cardStyle: {
        width:getWidth(345),
        height: getHeight(175),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    queueStyle: {
        width: '98%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    buttonStyle: {
        width: getWidth(230),
        marginTop: getHeight(15),
        textColor: '#fff',
        shadow: 6,
        colors: 'blue',
        height: getHeight(44),
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 18,
        fontFamily: 'bold'
    },
    requestItem: {
        width: '95%',
        paddingBottom: metrics.screenHeight * 0.15,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        // marginTop: '2%'
    },
    custometName: {
        fontSize: 'size15',
        fontFamily: 'bold',
        color: 'darkGray1',
        paddingHorizontal: getWidth(10)
    },
    customerInfo: {
        width: getWidth(345),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: getHeight(20)
    },
    headerIconView: {
        height: '100%',
        width: getWidth(30),
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerNameView: {
        height: '100%',
        width: getWidth(103),
        justifyContent: 'center',
        alignItems: 'center'
    },
    paragraphTextView: {
        width: getWidth(345),
        height: getHeight(45),
        justifyContent: 'center'
    },
    paragraphText: {
        fontSize: 'small13Px',
        fontFamily: 'bold',
        color: 'lightGray1',
        textAlign: 'center'
    },
    cardContainer: (border, marginTop) => ({
        width: getWidth(375),
        height: getHeight(50),
        paddingHorizontal: getWidth(15),
        flexDirection: 'row',
        alignItems: 'center',
        borderTopWidth: 1,
        borderBottomWidth: border,
        borderColor: ColorsList.rgbaBlack(.04),
        marginTop: marginTop
    }),
    iconView: {
        width: getWidth(25),
        height: getHeight(25),
        justifyContent: 'center',
        alignItems: 'center'
    },
    textStyle: {
        fontSize: 'size15',
        fontFamily: 'bold',
        color: 'darkGray1',
        paddingHorizontal: getHeight(5)
    },
    textItem: (fontSize) => ({
        fontSize: fontSize ? fontSize : 'medium15Px',
        fontFamily: 'medium',
        color: 'lightGray1', paddingHorizontal: getWidth(5)
    }),
    smallTextItem: {
        fontSize: 'small1',
        fontFamily: 'regular',
        color: 'lightGray1'
    },
    queueText: {
        fontSize: 'size15',
        fontFamily: 'bold',
        color: 'darkGray1',paddingHorizontal:getWidth(10)
    },
    ratingView: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalContainer: {
        width: getWidth(375),
        height: getHeight(320),
        alignItems: 'center'
    },
    modalHeader: {
        width: getWidth(375),
        height: getHeight(90),
        // marginTop: getHeight(9),
        // marginBottom: getHeight(8),
        // flexDirection: 'column',
        // justifyContent: 'space-between',
        alignItems: 'center',
        // backgroundColor: 'red',
    },
    textAndIconItemStyle: () => ({
        flexDirection: 'row',
        alignItems: 'center',
        alignItems:'center',
    }),
    textAndIconItemView: {
        width: getWidth(275),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: getHeight(10),
        marginBottom: getHeight(10),
    },
    buttonViewContainer: {
        width: getWidth(345),
        marginVertical: getHeight(15),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    buttonsStyle: {
        height: getHeight(44),
        width: getWidth(165),
        shadow: 6,
        borderRadius: 3,
        justifyContent: 'center',
        backgroundColor: 'white',
        alignItems: 'center',
        textColor: 'darkGray1',
        fontSize: 'size15',
        fontFamily: 'bold'
    },
    approveButton:{
        height: getHeight(44),
        width: getWidth(345),
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        textColor:'white' ,
        colors:'blue',
        fontSize: 'medium15Px',
        fontFamily: 'bold'
    }
};

export default styles;



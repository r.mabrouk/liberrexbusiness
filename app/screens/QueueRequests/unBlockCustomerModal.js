import React, { Component } from 'react';
import {
    View
} from 'react-native';
import PropTypes from 'prop-types';
import { Icons } from '../../config';
import { _Text } from '../../components/text/text';
import { PopupModal } from '../../components/popupModal/popupModal';
import { getWidth, getHeight } from '../../components/utils/dimensions';
import { Card } from '../../components/card/card';
import { Button } from '../../components/button/button';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { _string } from '../../local';


type UnBlockCustomerModalProps = {
    duration: number
}
class UnBlockCustomerModal extends Component<UnBlockCustomerModalProps> {
    constructor(props) {
        super(props);
        this.state = {
            customer:{

            },
            index:-1
        }
    }
    async componentDidMount() {
    }
    onAnimated = (state,customer_data,index) => {
        if (customer_data)
        this.setState({customer:customer_data,index:index })
        this.UnBlockModal.onAnimated(state)
        
    }
    render() {

        let {customer,index}=this.state
        let {onUnblock_customer,isremoveCustomerFromBookLoading}=this.props
        return (
            <PopupModal duration={400} ref={_popupModal => this.UnBlockModal = _popupModal}
                height={getHeight(400)}  >

                <Card
                    style={styles.container}>
                    <View style={styles.iconAndText}>
                        <View
                            style={styles.iconStyle}>
                            <Icons
                                name='user'
                                width={getWidth(28)} height={getWidth(28)}
                            />
                        </View>
                        <View style={styles.headerNameView}>
                            <_Text
                                style={styles.textStyle} >
                                {(_string("queue.edit")).toLocaleUpperCase()}
                            </_Text>
                        </View>
                    </View>

                    <View
                        style={styles.inputsContainer}>
                        <CustomTextInput
                            showonly
                            value={customer.fname+" "+customer.lname}
                            headerIcon='userGray'
                            headerText={_string("queue.full_name")}
                            leftIcon='editpen' />
                        <CustomTextInput
                            showonly
                            value={customer.email}
                            headerIcon='email'
                            headerText={_string("queue.email")}
                            leftIcon='editpen' />
                        <CustomTextInput
                            showonly
                            value={customer.phone_number}
                            headerIcon='phone'
                            headerText={_string("queue.phone_number")}
                            leftIcon='editpen' />
                    </View>

                    <Button
                    isLoading={isremoveCustomerFromBookLoading}
                    onPress={()=>onUnblock_customer(customer.id,index,"isremoveCustomerFromBookLoading",2,this.onAnimated,"blacklist_customers")}
                    iconStyle={{ width: getWidth(18),height:getHeight(18) }}

                        text={_string("queue.unblock_this_customer")}
                        iconName='done'
                        style={styles.buttonStyle} />

                </Card>

            </PopupModal>
        );
    }
}


const styles = {
    container: {
        width: getWidth(375),
        height: getHeight(400),
        alignItems: 'center'
    },
    iconAndText: {
        width: getWidth(208),
        height: getHeight(28),
        marginTop: getHeight(20),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    iconStyle: {
        height: '100%',
        width: getWidth(30),
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerNameView: {
        height: '100%',
        width: getWidth(170),
        justifyContent: 'center'
    },
    textStyle: {
        fontSize: 'medium15Px',
        fontFamily: 'bold',
        color: 'darkGray1'
    },
    inputsContainer: {
        height: getHeight(260),
        getWidth: getWidth(375),
        // marginBottom: getHeight(8),
        marginTop: getHeight(13),
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonStyle: {
        height: getHeight(44),
        width: getWidth(345),
        borderRadius: 3,
        marginTop: getHeight(15),

        justifyContent: 'center',
        alignItems: 'center',
        textColor: 'white',
        colors: 'blue',
        fontSize: 'medium15Px',
        fontFamily: 'bold'
    }
}

UnBlockCustomerModal.propTypes = {
    nextScreen: PropTypes.func
};

export default UnBlockCustomerModal;
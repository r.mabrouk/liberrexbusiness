import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text
} from 'react-native';
import styles from './styles';
import { images, metrics, Icons } from '../../config';
import { Button } from '../../components/button/button';
import { Header } from '../../components/header/header';
import { Card } from '../../components/card/card';
import { _Text } from '../../components/text/text';
import SettingsComponent from '../../components/SettingsComponent/SettingsComponent';
import { Width, getWidth, getHeight } from '../../components/utils/dimensions';
import { ScrollView } from 'react-native-gesture-handler';
import { _string } from '../../local';
import ApiConstants from '../../api/ApiConstants';
class SettingsView extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    async componentDidMount() {
        StatusBar.setHidden(true)
        this.props.nextScreen()
    }
    render() {
        let { user_profile } = this.props
        let { user, business } = user_profile
        console.log("businessbusiness", business)
        return (
            <View
                style={styles.container}>
                <Header style={{ width: '100%', zIndex: 15 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.navigate("Home") }}
                />

                <Card style={styles.Headercontainer}>
                    <Card style={{ flexDirection: "row", width: Width, justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: getWidth(15), paddingTop: getHeight(15) }}>
                        <View style={styles.headerContent}>
                            <Image source={!user.photo ? images.user_profile :
                                {
                                    uri: ApiConstants.IMAGE_BASE_URL + user.photo
                                }
                            } style={styles.avatarImg} />
                            <_Text style={{
                                fontSize: 'size15',
                                color: 'white',
                                fontFamily: 'bold',
                                marginLeft: 10,
                            }} >{user.fname + " " + user.lname}</_Text>
                        </View>
                        <Button
                            iconStyle={{ color: 'lightGray', width: getWidth(12), height: getHeight(12) }}
                            iconName={'editpen'}
                            text={_string("EditYourProfile")}
                            style={styles.headerButton}
                            onPress={() => { this.props.navigation.navigate('EditProfile') }}
                        />
                    </Card>



                </Card>




                {/* Fields */}

                <ScrollView
                    contentContainerStyle={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingBottom: getHeight(20)
                    }}
                    style={styles.optionsContainer}>


                    {/* sub sub Header */}
                    <Card
                        style={styles.subHeaderSectionContainer}>
                        <Card
                            style={{
                                width: getWidth(86),
                                height: getWidth(86),
                                borderRadius: getWidth(43),
                                shadow: 6,
                                backgroundColor: 'white'
                            }}>
                            <Image
                                source={!business.photo ? images.company_profile : { uri: ApiConstants.IMAGE_BASE_URL + business.photo }}
                                style={{
                                    width: '100%',
                                    height: '100%',
                                    borderRadius: getWidth(43)
                                }}
                                resizeMode="cover" />
                        </Card>
                        <Card
                            style={{
                                height: '100%',
                                justifyContent: 'center',
                                overflow: 'visible'


                            }} >
                            <_Text
                                style={{
                                    fontSize: 'size15',
                                    fontFamily: 'bold',
                                    color: 'darkGray1', paddingHorizontal: getWidth(5)
                                }}>{business.name}</_Text>
                            <Card
                                style={styles.addressContainer}>
                                <Icons
                                    name="location" width={getWidth(15)} height={getHeight(15)} />
                                <_Text
                                    style={styles.address}>{business.address + "," + business.city}</_Text>
                            </Card>

                            <Button
                                iconName={'editpen'}
                                text={_string("EditYourBusinessInfo")}
                                style={styles.subHeaderButton}
                                onPress={() => { this.props.navigation.navigate('EditBusiness') }}
                            />
                        </Card>


                    </Card>

                    <SettingsComponent
                        width={getWidth(345)}
                        title={_string("settings.membership_plans")}
                        leftIcon={'membership'}
                        onPress={() => { this.props.navigation.navigate('Membership') }} />


                    <SettingsComponent
                        width={getWidth(345)} title={_string("settings.payment_methods")} leftIcon={'payments'}
                        onPress={() => { this.props.navigation.navigate('Payments') }} />

                    <SettingsComponent
                        width={getWidth(345)} title={_string("settings.team_members")} leftIcon={'team'}
                        onPress={() => { this.props.navigation.navigate('TeamMembers') }} />

                    <SettingsComponent
                        width={getWidth(345)} title={_string("settings.your_services")} leftIcon={'grid'}
                        onPress={() => { this.props.navigation.navigate('YourService') }} />

                    <SettingsComponent
                        width={getWidth(345)} title={_string("settings.business_rules")} leftIcon={'rules'}
                        onPress={() => { this.props.navigation.navigate('BusinessRules') }} />

                    <SettingsComponent
                        width={getWidth(345)} title={_string("settings.customers_list")} leftIcon={'reports'}
                        onPress={() => { this.props.navigation.navigate('CustomerList') }} />


                    <SettingsComponent
                        width={getWidth(345)} title={_string("settings.customers_feedback")} leftIcon={'feedback'}
                        onPress={() => { this.props.navigation.navigate('CustomerFeedback') }} />



                    <SettingsComponent
                        width={getWidth(345)} title={_string("settings.statistics_and_reports")} leftIcon={'report'}
                        onPress={() => { this.props.navigation.navigate('Report') }} />
                    <SettingsComponent
                        width={getWidth(345)} title={_string("settings.app_settings")} leftIcon={'appsettings'}
                        onPress={() => { this.props.navigation.navigate('AppSettings') }} />
                </ScrollView>


            </View>
        );
    }
}




export default SettingsView;

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import SettingsView from './SettingsView';
import {connect} from 'react-redux'
 class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <SettingsView {...this.props} {...this.state} {...this.props}/>
        );
    }
}

function mapStateToProps(state) {
    return {
      membershipPlans:state.membershipReducer.membershipPlans,
      loadingteamMembershipPlans:state.membershipReducer.loadingteamMembershipPlans,
      user_profile:state.authReducer.user_profile
  
    };
  }
  function mapDispatchToProps(Dispatch) {
    return {
    };
  }
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Settings);

  

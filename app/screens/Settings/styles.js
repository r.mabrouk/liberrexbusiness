import { StyleSheet } from 'react-native';
import { getWidth, getHeight, Width, Height, SpaceHeight } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';
const styles = {
    container: {
        width: Width, height: Height+SpaceHeight,
        backgroundColor: ColorsList.white
    },
    logo: {
        width: Width * .8,
        height: Height * .2,
        resizeMode: 'contain'
    },

    btnHeader:
    {
        backgroundColor: 'white',
        shadow: 5, width: getWidth(75), height: getHeight(31),
        borderRadius: 3, fontSize: 10
    },
    ReportsContainer: { width: '92%', flexDirection: 'row', justifyContent: 'space-between' },
    RequestsStatusItem: {
        width: getWidth(160),
        height: Height * .18, shadow: 5,
        borderRadius: 3, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center'
    },
    Headercontainer: {
        flexDirection: 'row',
        width: Width,
        marginTop: -getHeight(30),
        justifyContent: 'space-between',
        height: getHeight(79),
        alignItems: 'center',
        colors: 'blue',
        zIndex:14,
        shadow:6


    },
    headerContent: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',




    },
    greeting: {
        color: "white",
        marginLeft: 10,
    },
    avatarImg: {
        width: 45,
        height: 45,
        borderRadius: 22.5,
    },
    name: {
        color: 'white',

    },
    address: {
        fontSize: 'size12', fontFamily: 'medium', marginLeft: 7.5, color: 'lightGray1'
    },
    addressContainer: {
        flexDirection: 'row', marginTop: 7.5
        ,paddingHorizontal:getWidth(5)
    },
    headerButton: {
        backgroundColor: 'white', width: getWidth(120), height: getHeight(31),
        fontSize: 'size11',
        borderRadius: 3, 
        alignItems: "center",
        justifyContent: 'center',
        textColor: 'darkGray1',
        fontFamily:'medium'

    },
    subHeaderSectionContainer: {
        width: Width, height: getHeight(126),
        backgroundColor: 'white', shadow: 6,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: getWidth(15)
    },
    subHeaderButton: {
        backgroundColor: 'white',
        width: getWidth(249), height: getHeight(31),
        fontSize: 'size11',
        shadow: 6,
        borderRadius: 3, alignItems: "center",
        justifyContent: 'center',
        marginLeft: getWidth(5),
        marginTop: getHeight(15),
        textColor: 'darkGray1',
        fontFamily:'medium',
       
    },
    optionsContainer: {
        width: Width,

    },
    optionCard: {

        backgroundColor: 'white', shadow: 5, width: '100%', marginTop: 20,
        height: getHeight(44), justifyContent: 'center', alignItems: 'center'

    },
    option: {

        flexDirection: 'row', width: '90%', height: '100%',
        justifyContent: 'space-between', alignItems: 'center'

    },
    optionText:
        { fontSize: "small1", fontWeight: 'bold', marginLeft: 5 }

};

export default styles;

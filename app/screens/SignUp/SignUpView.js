import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    StatusBar,
    Image, TouchableOpacity, Text, ScrollView
} from 'react-native';
import { images } from '../../config';
import { Input } from '../../components/input/input';
import { Button } from '../../components/button/button';
import styles from './styles'
import { Card } from '../../components/card/card'
import { Width, getHeight, getWidth } from '../../components/utils/dimensions';
import { _string } from '../../local';
import { _Text } from '../../components/text/text';
export default class SignUpView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            secureTextEntryPassword: true,
            secureTextEntryRetype_password: true
        };
    }

    showPassword = (key) => {
        this.setState({ [key]: !this.state[key] })
    }
    render() {
        let { secureTextEntryPassword, secureTextEntryRetype_password } = this.state
        let { email, password, reTypePassword, username, onChangeText, onSignUp, isSocialLoginLoading, socailType } = this.props

        return (
            <View
                style={styles.container}>
                <Card style={{ width: Width, height: '100%', }}>
                    <ImageBackground
                        resizeMode='cover'
                        source={images.loginbackground}
                        style={styles.imageBackground}>
                        <ScrollView scrollEnabled={false} contentContainerStyle={{ alignItems: 'center',paddingBottom:getHeight(15) }} style={{ width: '100%' }}>

                            <Image
                                source={images.WhiteLogo}
                                style={styles.logo} />

                            <View style={{ alignItems: 'center', width: '100%', marginTop: getHeight(56) }}>
                                <Input
                                onSubmitEditing={()=>{
                                  this.input2.focus()
                                }}
                                autoFocus
                                returnKeyType='next'
                                    maxLength={40}
                                    value={email}
                                    onChangeText={(text) => onChangeText(text, "email")}
                                    style={styles.input}
                                    iconName={'email'}
                                    placeholder={_string("sign_up.your_email")}
                                    iconStyle={{ width: getWidth(15), height: getHeight(15) }}
                                    verticalLine={false}
                                />
                                <Input
                                ref={(ref)=>this.input2=ref}
                                                                returnKeyType='next'
                                                                onSubmitEditing={()=>{
                                                                    this.input3.focus()
                                                                  }}
                                    maxLength={40}
                                    onRightPress={() => this.showPassword("secureTextEntryPassword")}
                                    secureTextEntry={secureTextEntryPassword}


                                    iconStyle={{ width: getWidth(15), height: getHeight(15) }}

                                    value={password}
                                    onChangeText={(text) => onChangeText(text, "password")}
                                    style={styles.input}
                                    iconName={'password'}
                                    placeholder={_string("sign_up.your_password")}
                                    verticalLine={false}
                                />

                                <Input
                                          
                                          onSubmitEditing={(text) => onSignUp(text)}
                                          returnKeyType='go'
                                                                ref={(ref)=>this.input3=ref}

                                    maxLength={40}
                                    iconStyle={{ width: getWidth(15), height: getHeight(15) }}
                                    onRightPress={() => this.showPassword("secureTextEntryRetype_password")}
                                    secureTextEntry={secureTextEntryRetype_password}

                                    value={reTypePassword}
                                    onChangeText={(text) => onChangeText(text, "reTypePassword")}
                                    style={styles.input}
                                    iconName={'password'}
                                    placeholder={_string("sign_up.retype_password")}
                                    verticalLine={false}
                                />

                                <Button
                                    iconStyle={{ color: '#fff',  width:getWidth(18), height:getWidth(18) }}
                                    text={_string("sign_up.signup")}
                                    iconName='signupgray'
                                    style={styles.sigupButtons}
                                    onPress={(text) => onSignUp(text)} />

                                <_Text style={styles.haveAccount}>{_string("sign_up.do_have_an_account").toLocaleUpperCase()}</_Text>
                                <Button
                                isLoading={this.props.loadingCheckEmail}
                                
                                    onPress={() => this.props.navigation.navigate('Login')}
                                    //iconStyle={{ color: 'red' }}
                                    iconStyle={{ width:getWidth(18), height:getWidth(18)}}
                                    text={_string("sign_up.login")}
                                    iconName='login'
                                    style={styles.loginButton}
                                />
                                <View style={styles.socailTextContainer}>
                                    <_Text style={styles.socailtext}>{_string("sign_up.or_login_with_socail_media").toLocaleUpperCase()}</_Text>
                                    <_Text style={styles.socailsubtext}>{_string("sign_up.we_dont_have_any_acceses_to_your_accounts").toLocaleUpperCase()}</_Text>

                                </View>
                                {/* socail icon buttons */}

                                <View
                                    style={styles.ButtonsContainer}>
                                    <Button
                                        isLoading={isSocialLoginLoading && socailType == 'facebook'}
                                        loaderColor={'darkGray1'}
                                        onPress={this.props.onfacebookLogin}
                                        iconStyle={{ width: getWidth(18), height: getHeight(18) }}
                                        iconName='facebook'
                                        style={styles.socialMediaButtons}
                                    />
                                    <Button
                                        isLoading={isSocialLoginLoading && socailType == 'twitter'}

                                        onPress={this.props.onTwitterLogin}

                                        iconStyle={{ width: getWidth(18), height: getHeight(18) }}
                                        iconName='twitter'
                                        style={styles.socialMediaButtons}
                                    />
                                    <Button
                                        isLoading={isSocialLoginLoading && socailType == 'google'}
                                        onPress={this.props.onGoogleLogin}
                                        iconStyle={{ width: getWidth(18), height: getHeight(18) }}
                                        iconName='googlepluse'
                                        style={styles.socialMediaButtons}
                                    />
                                </View>



                            </View>


                        </ScrollView>
                    </ImageBackground>

                </Card>
            </View>

        );
    }
}

import React, { Component } from 'react';
import { View, Text,NativeModules } from 'react-native';
import SingUpView from './SignUpView'
import { validateEmail } from '../../utils/stringUtils';
import {connect} from 'react-redux'
import { getAllIndustryRequest, socialLoginRequest, checkEmailRequest } from '../../actions/auth';
import { _string } from '../../local';

import { LoginManager, AccessToken } from "react-native-fbsdk";
// import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import { compressImages } from '../../utils/compressImages';
import { set_UserLoginData } from '../../actions/uiActions';
import { _Toast } from '../../components/toast/toast';
const { RNTwitterSignIn } = NativeModules
// var FBLoginMock = require('./facebook/FBLoginMock.js');

const Constants = {
  //Dev Parse keys
  TWITTER_COMSUMER_KEY: "O0UzoFkUY4AvlpECEjAvibBiY",
  TWITTER_CONSUMER_SECRET: "j81HqnFffZXQKnAsqvbhrpeMDMF5KKQCFvDwpzF12PVC1vyocr",
}
// GoogleSignin.configure({
//   scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
//   iosClientId: '1056402514225-efbfut40d2ld7agtbti33f69qa0lumjn.apps.googleusercontent.com', // only for iOS
//   // hostedDomain: '', // specifies a hosted domain restriction
//   // forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login
//   // accountName: '', // [Android] specifies an account name on the device that should be used
// });

class signUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      reTypePassword: "",
      socialType: 'facebook'
    };
  }
  onNext = () => {
    if (this.validationForm())
      this.props.navigation.navigate('Location')
  }
  onSignUp = () => {
    if (this.validationForm()){
      let data=new FormData()
      data.append("email",this.state.email)
      data.append("password",this.state.password)
      this.props._checkEmail(data,this.state.email)
    }
  }


  onTwitterLogin = async () => {
    RNTwitterSignIn.init(Constants.TWITTER_COMSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET)
    RNTwitterSignIn.logIn()
      .then(async twitter_data => {
        console.log(twitter_data, "twitter_data")

        let image = await compressImages("https://tinyjpg.com/images/social/website.jpg")

        let twitter_user_data = {
          provider: "twitter",
          provider_id: twitter_data.userID,
          email: twitter_data.email,
          token: twitter_data.authToken,
          first_name: twitter_data.first_name,
          last_name: twitter_data.last_name,
          user_photo: image,
          uri: image.uri,
          authTokenSecret: twitter_data.authTokenSecret
        }
        this.setState({ socialLogin: 'twitter' })
        this.props.socialLogin(twitter_user_data)
        console.log(loginData)
        const { authToken, authTokenSecret } = loginData
        if (authToken && authTokenSecret) {
          this.setState({
            isLoggedIn: true
          })
        }
      })
      .catch(error => {
        console.log(error)
      }
      )
  }


  onGoogleLogin = async () => {
    // try {
    //   await GoogleSignin.hasPlayServices();
    //   const google_data = await GoogleSignin.signIn();
    //   let image = await compressImages(google_data.user.photo)

    //   let facebook_user_data = {
    //     provider: "google",
    //     provider_id: google_data.user.id,
    //     email: google_data.user.email,
    //     token: google_data.idToken,
    //     first_name: google_data.user.givenName,
    //     last_name: google_data.user.familyName,
    //     user_photo: image,
    //     uri: image.uri

    //   }
    //   console.log("TAG", "ercvncgmnfgnfgnmfngn,fror", image)

    //   this.setState({ socialLogin: "google" })
    //   this.props.socialLogin(facebook_user_data)
    // } catch (error) {
    //   console.log("TAG", "error", error)
    //   if (error.code === statusCodes.SIGN_IN_CANCELLED) {
    //   } else if (error.code === statusCodes.IN_PROGRESS) {
    //   } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
    //   } else {
    //   }
    // }
    // this.signOut()
  }

  onfacebookLogin = async () => {
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      async (result) => {
        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {
          AccessToken.getCurrentAccessToken().then(
            async (data) => {
              var url = `https://graph.facebook.com/v3.2/${
                data.userID
                }?fields=id,first_name,last_name,email,picture.width(2000).height(2000)&access_token=${data.accessToken}`;
              let facebook_data = await fetch(url).then((res) => res.json())
              console.log(facebook_data, "facebook_datafacebook_datafacebook_datafacebook_data")
              let image = await compressImages(facebook_data.picture.data.url)
              let facebook_user_data = {
                provider: "facebook",
                provider_id: data.userID,
                email: facebook_data.email,
                token: data.accessToken,
                first_name: facebook_data.first_name,
                last_name: facebook_data.last_name,
                user_photo: image,
                uri: image.uri
              }
              this.setState({ socialLogin: "facebook" })
              this.props.socialLogin(facebook_user_data)
            }
          )
        }
        LoginManager.logOut()
      },
      function (error) {
        console.log("Login fail with error: " + error);
      }
    );
  }



componentWillMount(){
  this.props.getAllIndustry()
}
  onChangeText = (text, fieldName) => {
    this.setState({ [fieldName]: text })
  }
  validationForm = () => {
    let { email, password, reTypePassword } = this.state
    if (!email) {
      _Toast.open(_string("messages_alert.please_enter_your_email"), "w")
      return false
    }
    else if (!validateEmail(email)) {
      _Toast.open(_string("messages_alert.please_enter_valid_email"), "w")
      return false
    }
    else if (!password) {
      _Toast.open(_string("messages_alert.please_enter_your_password"), "w")
      return false
    }
    else if (!reTypePassword) {
      _Toast.open(_string("messages_alert.please_enter_reType_password"), "w")
      return false
    }
    else if (reTypePassword != password) {
      _Toast.open(_string("messages_alert.retype_password_not_match_with_password"), "w")
      return false
    }
    else return true

  }
  render() {
    return (
      <SingUpView
        {...this}
        {...this.state}
        {...this.props} />
    );
  }
}
function mapStateToProps(state) {
  return {
    isLoginLoading: state.loadingReducer.isLoginLoading,
    user_login_data: state.uiReducer.user_login_data,
    isSocialLoginLoading: state.loadingReducer.isSocialLoginLoading,
    loadingCheckEmail:state.authReducer.loadingCheckEmail,


  }
}

function mapDispatchToProps(Dispatch) {
  return {
    getAllIndustry:()=>Dispatch(getAllIndustryRequest()),
    set_UserLoginData: (user_data) => Dispatch(set_UserLoginData(user_data)),
    socialLogin: (user_data) => Dispatch(socialLoginRequest(user_data)),
    _checkEmail: (user_data,email) => Dispatch(checkEmailRequest(user_data,email))
  }
}

export default connect (mapStateToProps,mapDispatchToProps)(signUp)
import { StyleSheet } from 'react-native';
import { metrics } from '../../config';
import { getWidth, getHeight } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor:ColorsList.white
    },
    logo: {
        width: getWidth(250),
        height: getHeight(52),
        resizeMode: 'contain',
        marginTop: getHeight(100)
    },
    imageBackground: {
        width: '100%',
        height: '100%',
        alignItems: 'center',


    },
    socialMediaButtons: {
        width: getWidth(106),
        textColor: '#000',
        shadow: 6,
        backgroundColor: '#fff',
        borderRadius: 3,
        height: getHeight(38)
    },
    inputContainer: {
        marginBottom: metrics.screenHeight * (10 / 667), width: '100%'
    },
    haveAccount: {
        fontFamily: 'regular',
        fontSize: 'small1',
        marginTop: getHeight(30),
        fontFamily: 'regular',
        color: ColorsList.rgbaGray(.7)
    },
    text: {
        textTransform: "uppercase",
        marginTop: metrics.screenHeight * (15 / 667),
        fontSize: 15

    },
   
    ButtonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: getHeight(15),
        width: '90%',

    },
    socailsubtext:{
        fontFamily: 'regular',
        fontSize: 'size11',
        color:ColorsList.rgbaGray(.7)
    },
    socailtext: {
        fontFamily: 'bold',
        fontSize: 'size14',
        color: "darkGray1"
    },
    socailTextContainer: {
        marginTop: getHeight(30),
        alignItems: 'center',
    },
    loginButton: {
        width:getWidth(345),
        marginTop: getHeight(10),
        textColor: 'darkGray1',
        shadow: 6,
        height: getHeight(44),
        borderRadius: 3,
        backgroundColor: 'white',
        fontFamily:'bold',
        fontSize:'size15'
    },
    socailMediaButtons: {
        width: getWidth(106),
        textColor: '#000',
        shadow: 6,
        backgroundColor: '#fff',
        borderRadius: 3,
        height: getHeight(38)

    },
    sigupButtons:
    {
        width: getWidth(345),
        marginTop: getHeight(15),
        textColor: '#fff',
        shadow: 6,
        height:  getHeight(44),
        colors: 'blue',
        borderRadius: 3,
        fontFamily:'bold',
        fontSize:'size15'


    },
    input:
    {
        
        fontFamily: 'medium',
        borderRadius: 3,
        width: getWidth(345),
        height: getHeight(41),
        shadow: 6, backgroundColor: 'white',
        // alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: getHeight(10),
    }

};

export default styles;

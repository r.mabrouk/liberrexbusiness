import React, { Component } from 'react';
import {
  View,
  ImageBackground,
  StatusBar,
  Image, TouchableOpacity, Text, ScrollView, FlatList, KeyboardAvoidingView
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { images, metrics } from '../../config';
import { Input } from '../../components/input/input';
import { Button } from '../../components/button/button';
import { Card } from '../../components/card/card'
import { _Text } from '../../components/text/text';
import { Width, getHeight, getWidth, Height, FooterHeight } from '../../components/utils/dimensions';
import SignUpModal from './signUpModal';
import { PopupModal } from '../../components/popupModal/popupModal';
import IndustrtyModal from './industrtyModal';
import { _string } from '../../local';
import { ColorsList } from '../../components/PropsStyles';
import { _Toast } from '../../components/toast/toast';

export default class SignUpBusinessView extends Component {
  constructor(props) {
    super(props);
    this.state = {

      countrySelectedIndex: -1,
      citySelectedIndex: -1,
      postal_codeSelectedIndex: -1,
      currentModal: "",

    };
  }

  render() {


    let {
      profileImage,
      address,
      city,
      country,
      postal_code,
      industry, onNext,
      onChangeText,
      legalName, onSelectCountryAndCity,
      handleImagePickedProfile, currentModal,
      onSelectIndustry, industryIdSelected, industryd_data,
      onFilter,
      filter,
      countries

    } = this.props
    return (
      <View
        style={styles.container}>
        <Card style={{ width: '100%', height: Height + FooterHeight }}>
          <ImageBackground
            resizeMode='cover'
            source={images.loginbackground}
            style={styles.imageBackground}>
            <Button
              onPress={() => this.props.navigation.goBack()}
              iconStyle={{ color: 'white', width: getWidth(22), height: getHeight(22) }}
              style={{ position: 'absolute', left: getWidth(0), top: getHeight(25), padding: getWidth(15), zIndex: 20 }}
              iconName='arrowBack'
            />

            <KeyboardAvoidingView
              // keyboardVerticalOffset = { getHeight(40)}
              behavior='height'>
              <ScrollView contentContainerStyle={{
                width: Width,
                alignItems: 'center'
              }}>


                <Image
                  source={images.WhiteLogo}
                  style={styles.logo} />

                <_Text style={{ fontFamily: 'bold', fontSize: 'size17', color: 'white', marginTop: getHeight(5) }} >
                  {_string("sign_up.fill_your_business_information").toLocaleUpperCase()}
                </_Text>
                <Input
                  autoFocus
                  placeholderColor={ColorsList.rgbaBlack(.54)}
                  style={styles.legalInput}
                  value={legalName}
                  onChangeText={(text) => onChangeText(text, "legalName")}
                  iconName={'legal'}
                  placeholder={_string("LegalName")}
                  iconStyle={{ width: getWidth(15), height: getHeight(15) }}
                  verticalLine={false}


                />
                <Card
                  style={{
                    width: getWidth(345),
                    height: getHeight(116),
                    backgroundColor: 'white',
                    shadow: 5,
                    borderRadius: 3,
                    marginTop: getHeight(10),
                    alignItems: 'center',
                    justifyContent: 'center'
                  }} >
                  <TouchableOpacity
                    onPress={() => { handleImagePickedProfile() }}
                  >
                    <Card style={{ width: getWidth(64), height: getWidth(64), borderRadius: getWidth(32), backgroundColor: 'white', shadow: 6, justifyContent: 'center', alignItems: 'center' }}>
                      <Image source={profileImage ? { uri: profileImage } :
                        images.camera} style={{
                          width: profileImage ? '100%' : getWidth(24),
                          height: profileImage ?
                            '100%' : getWidth(24), resizeMode: 'cover'
                        }} />
                    </Card>
                  </TouchableOpacity>
                  <_Text style={{ fontSize: 'size13', marginTop: getHeight(10), fontFamily: 'medium', color: ColorsList.rgbaBlack(.54) }}>{_string("sign_up.upload_your_business_logo_or_photo")}</_Text>
                </Card>
                <Button
                  onPress={() => {
                    this.industrtyModal.onAnimated(true)
                  }}
                  iconStyle={{ width: getWidth(15), height: getHeight(15), color: '#000' }}
                  iconName='grid'
                  rightIcon='arrowDown'
                  text={industry}
                  style={{
                    textColor: 'gray',
                    borderRadius: 3,
                    width: getWidth(345),
                    height: getHeight(41),
                    shadow: 6, backgroundColor: 'white',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                    flexDirection: 'row',

                    marginTop: getHeight(10),
                    paddingHorizontal: getWidth(15)
                  }}
                />
                {/* <Input
                value={industry}
                onChangeText={(text) => onChangeText(text, "industry")}
                style={styles.input}

                iconName={'grid'}
                placeholder={'Industry'}
                iconStyle={{ size: 18 }}
                verticalLine={false} /> */}

                <Input
                  placeholderColor={ColorsList.rgbaBlack(.54)}

                  value={address}
                  onChangeText={(text) => onChangeText(text, "address")}
                  style={styles.input}
                  iconName={'location'}
                  placeholder={_string("sign_up.business_address")}
                  iconStyle={{ width: getWidth(15), height: getHeight(15) }}

                  verticalLine={false}
                // showIcon={false}
                />

                <Input
                  placeholderColor={ColorsList.rgbaBlack(.54)}
                  iconName={'location'}
                  iconStyle={{ width: getWidth(15), height: getHeight(15) }}

                  style={styles.input}
                  value={postal_code}
                  onChangeText={(text) => onChangeText(text, "postal_code")}
                  placeholder={_string("sign_up.postal_code")}
                  verticalLine={false}
                />
                {/* DropDawn */}
                <View
                  style={styles.ButtonsContainer}>
                  <Button
                    onPress={() => {

                      this.setState({ currentModal: 'country' }, () => {
                        this.signUpModel.onAnimated(true)
                      })

                    }}
                    iconStyle={{ width: getWidth(12), height: getHeight(12), color: '#000' }}
                    iconName='arrowDown'
                    text={country}
                    style={styles.socailMediaButtons}
                  />
                  <Button
                    onPress={() => {
                      this.setState({ currentModal: 'city' }, () => {
                        if (country == _string("Country"))
                          return _Toast.open(_string("PleaceSelectCountryThenSelectCity"), 'w')
                        this.signUpModel.onAnimated(true)
                      })
                    }}
                    iconStyle={{ width: getWidth(12), height: getHeight(12), color: '#000' }}
                    iconName='arrowDown'
                    text={city}
                    style={styles.socailMediaButtons}
                  />
                </View>

                <Button
                  iconStyle={{ width: getWidth(18), height: getHeight(18), color: '#ffffff' }}

                  text={_string("sign_up.next")}
                  iconName='next'
                  style={styles.nextButton}
                  onPress={onNext} />
              </ScrollView>
            </KeyboardAvoidingView >

          </ImageBackground>
        </Card >
        <SignUpModal
          selected_country={country}
          onSelect={onSelectCountryAndCity}
          data={countries}
          currentModal={this.state.currentModal}
          ref={(signUpModel) => this.signUpModel = signUpModel} />
        <IndustrtyModal
          onFilter={onFilter}
          filter={filter}
          onSelectIndustry={onSelectIndustry}
          selectItem={industryIdSelected}
          data={industryd_data}
          ref={(industrty) => this.industrtyModal = industrty} />
      </View >

    );
  }
}

//   <ScrollView contentContainerStyle={{alignItems:'center'}} style={{ width: Width }}>
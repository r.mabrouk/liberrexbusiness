import React, { Component } from 'react';
import { View, Text } from 'react-native';
import SignUpBusinessView from './SignUpBusinessView'
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux'
import { compressImages } from '../../utils/compressImages';
import { _string } from '../../local';
import { searchIndustryRequest } from '../../actions/auth';
import { _Toast } from '../../components/toast/toast';
class SignUpBusiness extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileImage: '',
      legalName: "",
      industry: _string("Industry"),
      address: "",
      country: _string("Country"),
      city:_string( "City"),
      image_file: null,
      postal_code: "",
      countrySelectedIndex: -1,
      citySelectedIndex: -1,
      currentModal: "",
      industryIdSelected: 1,
    };
  }
  onNext = () => {

    let {
      image_file,
      legalName,
      industry,
      address,
      country, city,
      postal_code
    } = this.state

    if (this.validationForm()) {
      let user_data = this.props.navigation.getParam('user_data')
      // let data = Object.assign({}, user_data, businessData)
      user_data.append("business_photo", image_file)
      user_data.append("name", legalName)
      user_data.append("address", address)
      user_data.append("postal_code", postal_code)
      user_data.append("country", country)
      user_data.append("city", city)
      user_data.append("industry", industry)
      let socialLogin_data = this.props.navigation.getParam('socialLogin_data')
      this.props.navigation.navigate('Location', { userInfoWithBusinessInfo: user_data,socialLogin_data })
    }



  }

  componentWillMount() {
  }
  onChangeText = (text, fieldName) => {
    this.setState({ [fieldName]: text })
  }

  validationForm = () => {
    let {
      address,
      postal_code,
      city,
      country,
      image_file,
      industry,
      legalName,
    } = this.state

    if (!legalName) {
      _Toast.open(_string("messages_alert.please_enter_the_legal_name"), 'w')
      return false
    }
    else if (!image_file) {
      _Toast.open(_string("messages_alert.please_choose_your_image_profile"), 'w')
      return false
    }
    else if (industry == _string('Industry')) {
      _Toast.open(_string("messages_alert.please_choose_industry"), 'w')
      return false
    }
    else if (!address) {
      _Toast.open(_string("messages_alert.please_enter_the_address"), 'w')
      return false
    }
    else if (!postal_code) {
      _Toast.open(_string("messages_alert.please_enter_the_postal_code"), 'w')
      return false
    }
    else if (country ==_string( "Country")) {
      _Toast.open(_string("messages_alert.please_chose_your_country"), 'w')
      return false
    }
    else if (city == _string("City")) {
      _Toast.open(_string("messages_alert.please_chose_your_city"), 'w')
      return false
    }
    else return true

  }
  handleImagePickedProfile = () => {

    ImagePicker.showImagePicker({}, async (response) => {
      //   console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // console.log('FLAG', 'AddProduct', 'handleImagePicked', 'response',JSON.stringify(response.data) );
        // const source = { uri: response.uri, type: response.type ? response.type : 'image/jpeg', name: response.fileName ? response.fileName : 'prof.jpg' };
        let image = await compressImages(response.uri)
        this.setState({ profileImage: image.uri, image_file: image });
      }
    });
  }
  onSelectCountryAndCity = (keyState, value) => {
    if (keyState == 'country')
      this.setState({ city: _string('City'), citySelectedIndex: -1 })

    this.setState({ [keyState]: value })
  }
  onSelectIndustry = (id, name) => {
    if (this.state.industryIdSelected == id)
      this.setState({ industryIdSelected: -1 })
    else this.setState({ industryIdSelected: id, industry: name })

  }
  onSaveSelectedIndustry = () => {

  }
  onFilter = (keyword) => {
    this.props.onFilter(keyword)
  }

  render() {

    return (
      <SignUpBusinessView

        {...this}
        {...this.state}
        {...this.props} />
    );
  }
}

function mapStateToProps(state) {

  return {
    industryies: state.authReducer.industryies,
    countries: state.authReducer.countries
  }
}

function mapDispatchToProps(Dispatch) {

  return {
    onFilter: (keyword) => Dispatch(searchIndustryRequest(keyword))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpBusiness)
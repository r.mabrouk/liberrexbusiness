import React, { Component } from 'react';
import {
    View, ActivityIndicator
} from 'react-native';
import { Icons } from '../../config';
import { _Text } from '../../components/text/text';
import { PopupModal } from '../../components/popupModal/popupModal';
import { getWidth, getHeight, Width } from '../../components/utils/dimensions';
import { Card } from '../../components/card/card';
import { Button } from '../../components/button/button';
import { CheckBox } from '../../components/checkBox/checkBox';
import { ScrollView } from 'react-native-gesture-handler';
import { Input } from '../../components/input/input';
import { connect } from 'react-redux'
import { getlanguage, _string } from '../../local';
import { ColorsList } from '../../components/PropsStyles';

type AddNewCustomerModalProps = {
    duration: number
}
class IndustrtyModal extends Component<AddNewCustomerModalProps> {
    constructor(props) {
        super(props);
    }
    async componentDidMount() {
    }
    onAnimated = (state) => {
        this.industrtyModal.onAnimated(state)
    }
    render() {

        let { selectItem, onSelectIndustry, filter, onFilter } = this.props
        return (

            <PopupModal
                inputHeight={getHeight(150)}
                inputDuration={400}
                zIndex={200}
                duration={400}
                height={getHeight(457)}
                ref={(industrty) => this.industrtyModal = industrty} >


                <View style={{ flexDirection: 'row', marginTop: getHeight(20), width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                    <Icons name='gridLinear' height={getHeight(28)} width={getWidth(28)} />
                    <_Text style={{ fontFamily: 'bold', fontSize: 'medium15Px', paddingHorizontal: getWidth(15), color: 'darkGray1' }}>{_string("sign_up.choose_your_industry").toLocaleUpperCase()}</_Text>
                </View>
                <View style={{ width: '100%', alignItems: 'center', height: getHeight(457 - 40) }}>

                    <Input
                        placeholderColor={ColorsList.rgbaBlack(.54)}
                        value={filter}
                        onChangeText={onFilter}
                        style={{
                            borderRadius: 3,
                            width: getWidth(345),
                            height: getHeight(41),
                            shadow: 6, backgroundColor: 'white',
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'row',
                            marginTop: getHeight(25),
                            marginBottom: getHeight(5),


                        }}
                        iconName={'search'}
                        placeholder={_string("sign_up.search_for_nearby_places")}
                        iconStyle={{ width: getWidth(15), height: getHeight(15) }}

                        verticalLine={false}


                    />


                    <ScrollView contentContainerStyle={{ alignItems: 'center' }} style={{ width: Width }}>

                        {
                            this.props.industryies.map((item, index) => {
                                return (
                                    <TreeSection cat_id={item.id} onSelectIndustry={onSelectIndustry} selectItem={selectItem} cat_name={item["name_" + getlanguage()]} data={item.sub_industries ? item.sub_industries : []} />

                                )
                            })
                        }
                        {this.props.isFilterIndustries && <View style={{ padding: getWidth(30) }}>
                            <ActivityIndicator size='small' color={ColorsList.darkGray1} />
                        </View>}
                    </ScrollView>

                </View>
                <Button
                    iconStyle={{ width: getWidth(18), height: getHeight(18) }}
                    onPress={() => this.industrtyModal.onAnimated()}
                    text={_string("sign_up.save")} iconName='Check'
                    style={{
                        width: getWidth(345)
                        , height: getHeight(44),
                        shadow: 6,
                        colors: 'blue', textColor: 'white',
                        borderRadius: 3, marginBottom: getHeight(15), position: 'absolute', top: getHeight(393), left: getWidth(15), fontFamily: 'bold', fontSize: 'size15'
                    }} />
            </PopupModal>

        );
    }
}
class TreeSection extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpend: false
        }
    }
    render() {
        let { isOpend } = this.state
        let { data, selectItem, onSelectIndustry, cat_name, cat_id } = this.props
        if (data.length < 1) {
            return (
                <Button style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', paddingVertical: getHeight(15), paddingHorizontal: getWidth(15), borderBottomWidth: 1, borderColor: ColorsList.rgbaBlack(0.04) }}>
                    <Card style={{ width: '100%', flexDirection: 'row' }}>

                        <CheckBox text={cat_name} onChecked={() => {
                            onSelectIndustry(cat_id, cat_name)
                        }} checked={selectItem == cat_id} />
                    </Card>
                </Button>
            )
        }
        return (
            <Card style={{ width: '100%', borderBottomWidth: 1, borderColor: ColorsList.rgbaBlack(0.04) }}>

                <Button onPress={() => {
                    this.setState({ isOpend: !isOpend })
                }} style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: getWidth(15), paddingVertical: getHeight(15) }}>
                    <Card style={{ width: '100%', flexDirection: 'row' }}>
                        <Card style={{ width: 15, height: 15, borderRadius: getWidth(15 / 2), backgroundColor: '#E5E7E8', alignItems: 'center', justifyContent: 'center' }}>
                            {isOpend && <Card style={{ width: 9, height: 3, colors: 'blue' }} />}
                        </Card>
                        <_Text style={{ color: 'darkGray1', fontSize: 'medium15Px', paddingHorizontal: getWidth(10), fontFamily: 'medium' }}>{cat_name}</_Text>
                    </Card>

                    <View style={{ transform: [{ rotate: isOpend ? '180deg' : '0deg' }] }}>
                        <Icons name='arrowDown' />
                    </View>

                </Button>

                {isOpend &&
                    data.map((item, index) => {
                        return (
                            <Button style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 10, paddingHorizontal: getWidth(40) }}>
                                <Card style={{ width: '100%', flexDirection: 'row' }}>
                                    <CheckBox text={item["name_" + getlanguage()]} onChecked={() => {
                                        onSelectIndustry(item.id, item["name_" + getlanguage()])
                                    }} checked={selectItem == item.id} />
                                </Card>
                            </Button>
                        )
                    })
                }
            </Card>

        )
    }
}


function mapStateToProps(state) {

    return {
        industryies: state.authReducer.industryies,
        isFilterIndustries: state.loadingReducer.isFilterIndustries
    }
}
export default connect(mapStateToProps, null, null, { forwardRef: true })(IndustrtyModal)
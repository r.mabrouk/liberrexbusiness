import React, { Component } from 'react';
import {
    View, FlatList, TouchableOpacity,NativeModules
} from 'react-native';
import { PopupModal } from '../../components/popupModal/popupModal';
import { Width, getHeight } from '../../components/utils/dimensions';
import styles from './styles'
import { ColorsList } from '../../components/PropsStyles';
import { _Text } from '../../components/text/text';
import { CheckBox } from '../../components/checkBox/checkBox';

type AddNewCustomerModalProps = {
    duration: number,
    currentModal: '',
   

}
class SignUpModal extends Component<AddNewCustomerModalProps> {
    constructor(props) {
        super(props);
        this.state = {
            countrySelectedIndex:-1,
            citySelectedIndex:-1,
            currentModal: ""
        }
    }
    async componentDidMount() {
        
    }
    componentWillReceiveProps = (props) => {
        this.setState({ currentModal: props.currentModal })
    }
    onAnimated = (state) => {
        this.signUpModel.onAnimated(state)
    }
    render() {

        let {
            currentModal,
        } = this.state

        let {data,onSelect,selected_country}=this.props


        let _data=[]
        if (currentModal=="country")
        _data=Object.keys(data)
        else _data=data[selected_country]

        return (


            <PopupModal
            inputHeight={getHeight(150)}
            inputDuration={400}
                backgroundColor={ColorsList.rgbaWhite(.97)}
                duration={300}
                ref={(_signUpModel) => this.signUpModel = _signUpModel}
                height={getHeight(199)} >

                <FlatList
                extraData={this.props}
                    data={_data}
                    style={{ width: Width }}
                    contentContainerStyle={styles.contentContainerStyleList}
                    renderItem={({ item, index }) => {
                        let { currentModal } = this.state
                        let _index = currentModal == 'country' ?
                            "countrySelectedIndex" : "citySelectedIndex" 
                        return (
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ [_index]: index, [currentModal]: item.name,citySelectedIndex:currentModal=='country'?-1:index})
                                    onSelect(currentModal,item)
                                    this.signUpModel.onAnimated(false)
                                }}
                                style={styles.contentContainerModal}>
                                <_Text
                                    style={styles.modaleText(this.state[_index], index)} >
                                    {item}
                                </_Text>
                                {this.state[_index] == index &&
                                    <CheckBox checked={true} onChecked={()=>{
                                        this.setState({ [_index]: index, [currentModal]: item })
                                        onSelect(currentModal,item.name)
                                        this.signUpModel.onAnimated(false)
                                        
                                    }} />}
                            </TouchableOpacity>

                        )
                    }} />
            </PopupModal>
        );
    }
}


export default SignUpModal;
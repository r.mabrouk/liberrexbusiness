import { StyleSheet } from 'react-native';
import { getHeight, getWidth, Width, Height, FooterHeight } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        height: Height+FooterHeight,
        // justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor:ColorsList.gray
    },
    logo: {
        width: getWidth(250),
        height: getHeight(52),
        marginTop: getHeight(100),
        resizeMode: 'contain',

    },
    imageBackground: {
        width: Width,
        height: Height+FooterHeight,
        alignItems: 'center',


    },
    mapContainer:
    {
        width: '90%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: getHeight(10),

    },
    mapimg: {
        borderRadius: 6,
        width: '100%',
        height: getWidth(315),

    },
    card: {
        width: '90%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,

        borderRadius: 3
    },
    ButtonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: getHeight(10),
        width: getWidth(345),

    },
    socailMediaButtons: {
        borderRadius: 3,
        width: getWidth(165),
        textColor: ColorsList.rgbaBlack(.54),
        shadow: 6,
        justifyContent: 'space-between',
        paddingHorizontal: getWidth(15),
        backgroundColor: '#fff',
        flexDirection: 'row-reverse',
        height: getHeight(41),
        fontSize: 'size13',
        fontFamily: 'medium'


    },
    resetbtn: {
        width: '90%',
        marginTop: 25,
    },
    arrivaltime: {
        fontWeight: 'bold',
        fontSize: 15
    },
    nextButton: {

        width: getWidth(345),
        marginTop: getHeight(15),
        textColor: 'white',
        shadow: 6,
        height: getHeight(44),
        colors: 'blue',
        borderRadius: 3,
        fontFamily:'bold',
        fontSize:'size15'


    },
    legalInput: {

        fontFamily: 'medium',
        borderRadius: 3,
        width: getWidth(345),
        height: getHeight(41),
        shadow: 6, backgroundColor: 'white',
        // alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: getHeight(30),
        textColor: 'red'

    },
    input: {

        fontFamily: 'medium',
        borderRadius: 3,
        width: getWidth(345),
        height: getHeight(41),
        shadow: 6, backgroundColor: 'white',
        // alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: getHeight(10),

    },
    contentContainerModal: {
        width: getWidth(375),
        height: getHeight(50),
        // backgroundColor: ColorsList.white,
        borderBottomWidth: 1,
        borderColor: ColorsList.rgbaBlack(.05),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: getWidth(15)
    },
    modaleText: (currentSelected, index) => ({
        fontFamily: 'bold',
        color: currentSelected == index ? ColorsList.rgbaGray(1) : ColorsList.rgbaGray(.5),
        fontSize: currentSelected == index ? 'size15' : 'size13'
    }),
    contentContainerStyleList: { alignItems: 'center' }


};

export default styles;

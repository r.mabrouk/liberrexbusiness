import React, { Component } from 'react';
import {
  View,
  ImageBackground,
  StatusBar,
  Image, TouchableOpacity, Text, ScrollView,KeyboardAvoidingView
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { images, metrics } from '../../config';
import { Input } from '../../components/input/input';
import { Button } from '../../components/button/button';
import { Card } from '../../components/card/card'
import { _Text } from '../../components/text/text';
import { getWidth, getHeight, Width } from '../../components/utils/dimensions';
import { _string } from '../../local';
import { ColorsList } from '../../components/PropsStyles';

export default class SignUpPersonView extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }







  render() {
    let { roleInBusiness, phoneNumber, fullname, profileImage, onRegistr, onChangeText, handleImagePickedProfile, isSignUpLoading,firstname,lastname } = this.props
    return (
      <View
        style={styles.container}>
        <Card style={{ width: getWidth(375), height: '100%', alignItems: 'center' }}>

          <ImageBackground
            resizeMode='cover'
            source={images.loginbackground}
            style={styles.imageBackground}>
           
           <Button
                onPress={() => this.props.navigation.goBack()}
                iconStyle={{ width: getWidth(22),height:getHeight(22),color: '#ffffff'}}
                style={{ position: 'absolute', left: getWidth(0), top: getHeight(25), padding: getWidth(15) }}
                iconName='arrowBack'
              />
           <KeyboardAvoidingView
              // keyboardVerticalOffset = { getHeight(40)}
              behavior='height'>
            <ScrollView contentContainerStyle={{ alignItems: 'center' }} style={{ width: getWidth(375) }}>


              <Image
                source={images.WhiteLogo}
                style={styles.logo} />

              <_Text style={{ fontFamily: 'bold', fontSize: 'size17', color: 'white', marginTop: getHeight(5) }} >
                {_string("sign_up.fill_your_personal_information").toLocaleUpperCase()}
              </_Text>

              <View style={{ flexDirection: 'row', width: Width, justifyContent: 'space-between', paddingHorizontal: getWidth(15), }}>
                <Input
                
                  onChangeText={(text) => onChangeText(text, "firstname")}
                  value={firstname}
                  style={styles.inputLegalName}

                  iconName={'userGray'}
                  placeholder={_string("sign_up.first_name")}
                  iconStyle={{ width: getWidth(15),height:getHeight(15) }}
                  
                  verticalLine={false}


                />
                <Input
                  onChangeText={(text) => onChangeText(text, "lastname")}
                  value={lastname}
                  style={styles.inputLegalName}

                  iconName={'userGray'}
                  placeholder={_string("sign_up.last_name")}
                  iconStyle={{ width: getWidth(15),height:getHeight(15) }}
                  verticalLine={false}


                />
              </View>

              <Card
                style={styles.coverImg} >
                <TouchableOpacity onPress={handleImagePickedProfile}>
                  <Card style={{ width: getWidth(64), height: getWidth(64), borderRadius: getWidth(32), backgroundColor: 'white', shadow: 6, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={profileImage ? { uri: profileImage } :
                      images.camera} style={{
                        width: profileImage ? '100%' : getWidth(24),
                        height: profileImage ?
                          '100%' : getWidth(24), resizeMode: 'cover'
                      }} />


                  </Card>
                </TouchableOpacity>
                <_Text style={{ fontSize: 'size13', marginTop: getHeight(10), fontFamily: 'medium', color: ColorsList.rgbaBlack(.54) }}> {_string("sign_up.upload_your_profile_photo")}</_Text>
              </Card>

              <Input
                onChangeText={(text) => onChangeText(text, "phoneNumber")}
                value={phoneNumber}
                style={styles.input}
                keyboardType='numeric'
                returnKeyType='done'
                iconName={'phone'}
                placeholder={_string("sign_up.phone_number")}
                iconStyle={{ width: getWidth(15),height:getHeight(15) }}
                verticalLine={false} />

              <Input
                onChangeText={(text) => onChangeText(text, "roleInBusiness")}
                value={roleInBusiness}
                style={styles.input}
                iconName={'case'}
                placeholder={_string("sign_up.role_in_your_business")}
                iconStyle={{ width: getWidth(15),height:getHeight(15) }}
                verticalLine={false} />
              <Button
                isLoading={isSignUpLoading}
                onPress={onRegistr}
                iconStyle={{ width: getWidth(18),height:getHeight(18) , color: '#ffffff',}}
                text={_string("sign_up.done")}
                iconName='done'
                style={styles.donebtn} />
            </ScrollView>
            </KeyboardAvoidingView>
          </ImageBackground>


        </Card>
      </View>

    );
  }
}

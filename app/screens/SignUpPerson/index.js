import React, { Component } from 'react';
import { View, Text } from 'react-native';
import SignUpPeronView from './SignUpPersonView'
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux'
import { requestSignUp } from '../../actions/auth';
import { compressImages } from '../../utils/compressImages';
import { _string } from '../../local';
import { _Toast } from '../../components/toast/toast';
class SignUpPeron extends Component {
  constructor(props) {
    let socialLogin_data = props.navigation.getParam('socialLogin_data')
    super(props);
    this.state = {
      profileImage: socialLogin_data?socialLogin_data.uri:"",
      firstname: socialLogin_data ? socialLogin_data.first_name : "",
      lastname: socialLogin_data ?socialLogin_data.last_name : "",
      phoneNumber: '',
      roleInBusiness: "",
      image_file: socialLogin_data?socialLogin_data.user_photo:null
    };
  }

  onRegistr = () => {
    if (this.validationForm()) {

      let {
        profileImage,
        firstname,
        phoneNumber,
        lastname,
        roleInBusiness,
        image_file
      } = this.state
      // let personInfo = {
      //   user_photo: image_file,
      //   fname: fullname.split(' ')[0],
      //   lname: fullname.split(' ')[1] ? fullname.split(' ')[1] : "",
      //   phone: phoneNumber,
      //   name: roleInBusiness
      // }
      let userAndBusinessInfo = this.props.navigation.getParam('userAndBusinessInfo')
      let socialLogin_datathis=this.props.navigation.getParam('socialLogin_data')

      // let data = Object.assign({}, userAndBusinessInfo, personInfo)
      userAndBusinessInfo.append("fname", firstname)
      userAndBusinessInfo.append("lname", lastname)
      userAndBusinessInfo.append("user_photo", image_file)
      userAndBusinessInfo.append("position", roleInBusiness)
      userAndBusinessInfo.append("phone", phoneNumber)
      userAndBusinessInfo.append("_method", "PUT")
      if (socialLogin_datathis) {
        let { provider, provider_id, token } = socialLogin_datathis
        userAndBusinessInfo.append("provider", provider)
        userAndBusinessInfo.append("provider_id", provider_id)
        userAndBusinessInfo.append("token", token)
       return this.props.registr(userAndBusinessInfo,"/social")

      }
      this.props.registr(userAndBusinessInfo)

    }
    // this.props.navigation.navigate('Home')

  }


  handleImagePickedProfile = () => {

    ImagePicker.showImagePicker({}, async (response) => {
      //   console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // console.log('FLAG', 'AddProduct', 'handleImagePicked', 'response',JSON.stringify(response.data) );
        // const source = { uri: response.uri, type: response.type ? response.type : 'image/jpeg', name: response.fileName ? response.fileName : 'prof.jpg' };
        // this.setState({ profileImage: 'data:image/jpeg;base64,' + response.data });
        let image = await compressImages(response.uri)
        this.setState({ profileImage: image.uri, image_file: image });
      }
    });
  }




  onChangeText = (text, fieldName) => {
    this.setState({ [fieldName]: text })
  }
  validationForm = () => {
    let {
      profileImage,
      firstname, lastname,
      phoneNumber,
      roleInBusiness
    } = this.state
    if (!firstname) {
      _Toast.open(_string("messages_alert.Please_enter_the_first_name"), 'w')
      return false
    }
    if (!lastname) {
      _Toast.open(_string("messages_alert.Please_enter_the_last_name"), 'w')
      return false
    }
    else if (!profileImage) {
      _Toast.open(_string("messages_alert.please_choose_your_image_profile"), 'w')
      return false
    }
    else if (!phoneNumber) {
      _Toast.open(_string("messages_alert.please_enter_phone_number"), 'w')
      return false
    }
    else if (!roleInBusiness) {
      _Toast.open(_string("messages_alert.please_enter_the_role_in_your_business"), 'w')
      return false
    }
    else return true

  }


  render() {
    return (
      <SignUpPeronView {...this} {...this.state} {...this.props} />
    );
  }
}

function mapStateToProps(state) {
  return {
    isSignUpLoading: state.loadingReducer.isSignUpLoading
  }
}
function mapDispatchToProps(Dispatch) {
  return {
    registr: (user_data,social) => Dispatch(requestSignUp(user_data,social))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpPeron)
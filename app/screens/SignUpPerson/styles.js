import { StyleSheet } from 'react-native';
import { getHeight, getWidth, Width } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    inputLegalName:{
        borderRadius: 3,
        width: getWidth(165),
        height:getHeight(41) ,
        shadow: 6, backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop:getHeight(35) 
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor:ColorsList.white
    },
    logo: {
        width: getWidth(250),
        height: getHeight(52),
        marginTop: getHeight(100),
        resizeMode: 'contain',

    },
    imageBackground: {
        width: '100%',
        height: '100%',
        alignItems: 'center',


    },
    mapContainer:
    {
        width: '90%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: getHeight(10),

    },
    mapimg: {
        borderRadius: 3,
        width: '100%',
        height: getHeight(315),

    },
    card: {
        width: '90%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: getHeight(10),

        borderRadius: 3
    },
    input: {


        borderRadius: 5,
        width: getWidth(345),
        height:getHeight(41) ,
        shadow: 6, backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop:getHeight(10) 
    },
    coverImg:
    {
        width:getWidth(345),
        height:getHeight(116),
        backgroundColor: 'white',
        shadow: 6,
        borderRadius: 3,
        marginTop: getHeight(10),
        alignItems: 'center',
        justifyContent: 'center'
    },
    donebtn: {

       

        width: getWidth(345),
        marginTop: getHeight(15),
        textColor: 'white',
        shadow: 6,
        height: getHeight(44),
        colors: 'blue',
        borderRadius: 3,
        fontFamily:'bold'

    }


};

export default styles;

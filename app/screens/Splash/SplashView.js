import React, { Component } from 'react';
import {
  View,
  ImageBackground,
  StatusBar,
  Image
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { images } from '../../config';
import { persistStore,persistReducer,Persistor } from 'redux-persist';


class SplashView extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  async componentDidMount() {

    StatusBar.setHidden(true)
    this.props.nextScreen()


  }
  render() {
    return (
      <View
        style={styles.container}>
        <ImageBackground
          resizeMode='cover'
          source={images.splash}
          style={styles.imageBackground}>
          <Image
            source={images.logo}
            style={styles.logo} />
        </ImageBackground>
      </View>
    );
  }
}
SplashView.propTypes = {
  nextScreen: PropTypes.func
};

export default SplashView;

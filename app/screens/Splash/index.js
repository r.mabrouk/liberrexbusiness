import React, { Component } from 'react';
import { connect } from 'react-redux';
import SplashView from './SplashView';
import { getAllCustomersRequest, getAllCustomersVipRequest, getAllCustomersBlacklistRequest } from '../../actions/customers';
import { setlanguage } from '../../local';
import { getCountriesRequest } from '../../actions/auth';
import { getAllServicesRequest } from '../../actions/services';
import { getHomeStatsRequest } from '../../actions/home';
import firebase from 'react-native-firebase'
class Splash extends Component {
  constructor(props) {
    super(props);
  }

  nextScreen = async () => {

    try {
      await firebase.messaging().requestPermission();
    } catch (error) {
    }

    let { user_profile, lang } = this.props
    if (user_profile.token) {
      this.props.getHomeStats()
    }
    else this.props.getCountries()

    let navigateTo = 'Language'
    if (lang) {
      setlanguage(lang)
      if (user_profile.token)
        navigateTo = "Tabs"
      else navigateTo = "Login"
    }
    if (!user_profile.token)
      this.props.navigation.replace(navigateTo);
  }

  render() {
    return (
      <SplashView
        {...this.props}
        nextScreen={this.nextScreen}
      />
    );
  }
}
function mapStateToProps(state) {
  return {
    lang: state.uiReducer.language,
    user_profile: state.authReducer.user_profile
  };
}
function mapDispatchToProps(dispatch) {
  return {

    getCountries: () => dispatch(getCountriesRequest()),
    getHomeStats: () => dispatch(getHomeStatsRequest())
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Splash);

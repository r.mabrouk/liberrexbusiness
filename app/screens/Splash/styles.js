import { StyleSheet } from 'react-native';
import { metrics } from '../../config';
import { ColorsList } from '../../components/PropsStyles';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:ColorsList.white
    },
    logo: {
        width: metrics.screenWidth * .8,
        height: metrics.screenHeight * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: '100%',
        height: '100%',
        alignItems: 'center', justifyContent: 'center'
    }
});

export default styles;

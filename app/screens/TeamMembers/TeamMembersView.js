import React, { Component } from 'react';
import {
    View
} from 'react-native';
import styles from './styles';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { Icons } from '../../config';
import { Width, getHeight, getWidth } from '../../components/utils/dimensions';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import { Button } from '../../components/button/button';
import { _string } from '../../local';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import EmptyData from '../../components/emptyData/emptyData';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class TeamMembersView extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    render() {
        let { teamMembers, loadingteamMembersList } = this.props
        return (
            <View
                style={styles.container}>
                <Header style={{ width: '100%', zIndex: 16 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />
                <HeaderSection
                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}
                    text={_string("team_members.team_members").toUpperCase()} iconName='team'
                    withButton
                    textButton={_string("team_members.add_new")}
                    iconButton='addwhite'

                    onPress={() => { this.props.navigation.navigate('TeamMembersAdd') }}


                />

                <FlatList
                    data={teamMembers}
                    renderItem={({ item, index }) => {
                        return (
                            <TeamMembersItem
                                item={item}
                                onPress={() => {
                                    this.props.navigation.navigate('TeamMembersAdd', { team: item, index: index, update: true })
                                }}
                            />
                        )
                    }}
                    ListFooterComponent={() => {
                        return (
                            <LoaderLists
                                isLoading={loadingteamMembersList} />
                        )
                    }}
                />


                <EmptyData
                    onPress={() => { this.props.navigation.navigate('TeamMembersAdd') }}
                    show={(teamMembers.length < 1) && !loadingteamMembersList} />
            </View >

        );
    }
}


const TeamMembersItem = ({ onPress, item }) => (
    <Button
        onPress={onPress}
        style={styles.memberCard}>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
            <Icons name='user' width={getWidth(28)} height={getWidth(28)} />

            <Card style={styles.TeamMemberInfo}>
                <_Text style={styles.memberName}>{item.fname + " " + item.lname}</_Text>
                <_Text style={styles.memberPosition}>{item.position}</_Text>
            </Card>
        </View>

        <Button
            iconStyle={{ width: getWidth(18), height: getHeight(18) }}

            iconName='more'
            style={{ alignItems: 'center', width: getWidth(18) }}
            onPress={onPress}
        >
        </Button>

    </Button>
)


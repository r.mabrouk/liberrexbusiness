import React, { Component } from 'react';
import TeamMembersView from './TeamMembersView'
import { getAllTeamMembersRequest } from '../../actions/teamMembers';
import {connect} from 'react-redux'
class TeamMembers extends Component {
  constructor(props) {


    super(props);
    this.state = {
    };
  }

  componentWillMount=()=>{
    this.getAllTeamMembers()
  }
  getAllTeamMembers = () => {
    this.props.getAllTeamMeambers()
  }
  render() {
    return (
      <TeamMembersView {...this.state} {...this} {...this} {...this.props} />
    );
  }
}


function mapStateToProps(state) {

  return {
    teamMembers:state.teamMembersReducers.teamMembers,
    loadingteamMembersList:state.teamMembersReducers.loadingteamMembersList,
  }
}

function mapDispatchToProps(Dispatch) {

  return {
    getAllTeamMeambers: () => Dispatch(getAllTeamMembersRequest())

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamMembers)


import { StyleSheet } from 'react-native';
import { metrics } from '../../config';
import { getHeight, Width, Height, getWidth } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
        width:Width,
        height:Height,
        backgroundColor: ColorsList.white,
    },
    logo: {
        width: metrics.screenWidth * .8,
        height: metrics.screenHeight * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: '100%',
        height: '100%',
    },

    headerSection: {
        backgroundColor: 'white', height: 67,
        width: metrics.screenWidth,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: metrics.screenWidth * .05,
        shadow: 5, top: 80,
        position: 'absolute',
        paddingTop: 13
    },
    memberCard: {
        marginTop: getHeight(15),
        width: Width,
        flexDirection: 'row',
        height: getHeight(58),
        shadow: 6,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal:getWidth(15)
    },
    TeamMemberInfo: {
        flexDirection: 'row',
        height: '100%',
        alignItems: 'center',paddingHorizontal:getWidth(15)
    },
    memberName: {
        fontSize: 'size15', fontFamily: 'bold',
        color: 'DarkGray1'
    },
    memberPosition:{
        fontSize: 'small1',
        fontFamily:'medium',
        marginLeft: getWidth(8),
        fontSize:'size12',
        color:'lightGray1'
    }

};

export default styles;

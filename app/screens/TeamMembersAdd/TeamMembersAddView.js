import React, { Component } from 'react';
import {
    View
} from 'react-native';
import styles from './styles';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { Icons } from '../../config';
import { ScrollView } from 'react-native-gesture-handler';
import { getHeight, Width, getWidth } from '../../components/utils/dimensions';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { Button } from '../../components/button/button';
import ServiceCard from '../../components/ServiceCard/ServiceCard';
import { _string } from '../../local';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class TeamMembersAddView extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    getPermissionState = (id) => {
        let { permissions } = this.props
        for (let i = 0; i < permissions.length; i++)
            if (permissions[i] == id) return true
    }
    render() {
        let { fullname,
            email,
            phone,
            job_title,
            permissions, onSelectPermission, onChangeText, isUpdate, onUpdateTeamMember, onCreateTeamMember, onRemoveTeamMember, loadingDeleteTeamMember, loadingCreateAndEditTeamMember } = this.props

        return (
            <View
                style={styles.container}>

                <Header style={{ width: '100%', zIndex: 16 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />
                <HeaderSection
                    style={{
                        shadow: 6,
                        zIndex: 15,
                        width: Width,
                        height: getHeight(67),
                        marginTop: -getHeight(30),
                        paddingTop: getHeight(15),
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        paddingHorizontal: getWidth(15)
                    }}
                    text={_string("team_members.team_members")}
                    iconName='team'

                />
                <KeyboardAwareScrollView
                    style={{ width: Width }}
                    contentContainerStyle={{
                        paddingTop: getHeight(15),
                        justifyContent: 'center',
                        alignItems: 'center'
                        , paddingBottom: getHeight(15)
                    }}
                >




                    <CustomTextInput
                        onSubmitEditing={() => this.input2.focus()}
                        returnKeyType='next'
                        marginTop={getHeight(5)}
                        onChangeText={(text) => onChangeText(text, 'fullname')}
                        value={fullname}
                        placeholder={_string("pleace_holder")}
                        leftIcon={'editpen'}
                        headerText={_string("team_members.full_name")}
                        headerIcon={'userGray'}
                    />

                    <CustomTextInput
                        ref={(input) => this.input2 = input}

                        onSubmitEditing={() => this.input3.focus()}
                        returnKeyType='next'
                        marginTop={getHeight(5)}

                        value={email}
                        onChangeText={(text) => onChangeText(text, 'email')}

                        placeholder={_string("pleace_holder")}
                        leftIcon={'editpen'}
                        headerText={_string("team_members.email")}
                        headerIcon={'email'}
                    />

                    <CustomTextInput
                        ref={(input) => this.input3 = input}
                        // onSubmitEditing={() => this.input4.focus()}
                        KeyboardType='numeric'
                        marginTop={getHeight(5)}
                        returnKeyType='done'
                        value={phone}
                        onChangeText={(text) => onChangeText(text, 'phone')}

                        placeholder={_string("pleace_holder")}
                        leftIcon={'editpen'}
                        headerText={_string("team_members.phone_number")}
                        headerIcon={'phone'} />
                    <CustomTextInput

                        ref={(input) => this.input4 = input}

                        marginTop={getHeight(5)}

                        onChangeText={(text) => onChangeText(text, 'job_title')}
                        value={job_title}
                        placeholder={_string("pleace_holder")}
                        leftIcon={'editpen'}
                        headerText={_string("team_members.job_title")}
                        headerIcon={'case'} />


                    <Card
                        style={{
                            justifyContent: 'center', alignItems: 'center',
                            marginTop: getHeight(5)
                        }}>
                        <Card
                            style={{ flexDirection: 'row', width: getWidth(345) }}>
                            <Icons
                                name={'rules'} width={getWidth(18)} height={getWidth(18)} />
                            <_Text
                                style={styles.headerTiltle}>{_string("team_members.permissions")}</_Text>
                        </Card>


                        <Card style={styles.choocies(0)}>
                            <ServiceCard

                                onChecked={(state) => {
                                    onSelectPermission(1, state)

                                }}
                                checked={this.getPermissionState(1)}
                                text={_string("team_members.queues")} />
                            <ServiceCard

                                onChecked={(state) => {
                                    onSelectPermission(2, state)

                                }}
                                checked={this.getPermissionState(2)}
                                text={_string("team_members.appointments")} />
                        </Card>
                        <View
                            style={styles.choocies(11)}>
                            <ServiceCard
                                onChecked={(state) => {
                                    onSelectPermission(3, state)

                                }}
                                checked={this.getPermissionState(3)}
                                text={_string("team_members.statistics")} />
                            <ServiceCard
                                onChecked={(state) => {
                                    onSelectPermission(4, state)

                                }}
                                checked={this.getPermissionState(4)}
                                text={_string("team_members.customer_list")} />
                        </View>
                    </Card>
                    <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: getWidth(15) }}>
                        <Button
                            isLoading={loadingCreateAndEditTeamMember}

                            onPress={() => {
                                if (isUpdate)
                                    onUpdateTeamMember()
                                else
                                    onCreateTeamMember()
                            }}
                            backgroundColor={'blue'}
                            iconStyle={{ width: getWidth(18), height: getWidth(18), color: '#ffffff' }}

                            text={isUpdate ? _string("app_settings.save") : _string('team_members.create')}
                            iconName='done'
                            style={styles.nextButton(getWidth(isUpdate ? 167.5 : 345))} />
                        {isUpdate && <Button
                            loaderColor={'darkGray1'}
                            isLoading={loadingDeleteTeamMember}
                            onPress={() => onRemoveTeamMember()}
                            backgroundColor={'white'}
                            iconStyle={{ width: getWidth(18), height: getHeight(18), color: '#ffffff', }}
                            text={_string("your_service.remove")}
                            iconName='delete'
                            style={styles.removeButton} />}
                    </View>

                </KeyboardAwareScrollView>


            </View >

        );
    }
}


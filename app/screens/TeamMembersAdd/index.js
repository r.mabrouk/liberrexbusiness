import React, { Component } from 'react';
import { View, Text } from 'react-native';
import TeamMembersAddView from './TeamMembersAddView'
import { connect } from 'react-redux'
import { updateMemberRequest, deleteMemberRequest, createTeamMemberRequest } from '../../actions/teamMembers';
import { validateEmail } from '../../utils/stringUtils';
import { _Toast } from '../../components/toast/toast';
import { _string } from '../../local';
class TeamMembersAdd extends Component {
  constructor(props) {

    const teamMembersData = props.navigation.getParam("team")
    super(props);
    this.state = {
      fullname: teamMembersData ? (teamMembersData.fname + " " + teamMembersData.lname) : "",
      email: teamMembersData ? teamMembersData.email : "",
      phone: teamMembersData ? teamMembersData.telephone : "",
      job_title: teamMembersData ? teamMembersData.position : "",
      permissions: [],
      isUpdate: props.navigation.getParam("update")
    };
  }
  onChangeText = (text, key) => {
    this.setState({ [key]: text })
  }

  onUpdateTeamMember = () => {
    let team_id = this.props.navigation.getParam("team").id
    let index = this.props.navigation.getParam("index")
    this.props.updateTeamMeambers(team_id, this.teamMembersData(), index)
  }
  getPermations = () => {
    let team = this.props.navigation.getParam("team", {})
    let permissions = []
    if (team.queue == 1) permissions.push(1)
    if (team.booking == 1)  permissions.push(2)
    if (team.team == 1) permissions.push(4)
    if (team.settings == 1) permissions.push(3)
    this.setState({ permissions })
  }
ValidationForm=()=>{
  let {email,fullname,isUpdate,job_title,permissions,phone}=this.state
  if (!fullname)
  {

    _Toast.open(_string("PleaseEnterYourFullname"),'w')
    return false
  }
  else if (!email)
  {
    _Toast.open(_string("PleaseEnterYourEmail"),'w')
    return false
  }
  else if (!validateEmail(email))
  {
    _Toast.open(_string("PleaseEnterAValidEmailAddressr"),'w')
    return false
  }
  else if (!phone)
  {
    _Toast.open(_string("PleaseEnterYourPhoneNumber"),'w')
    return false
  }
  else if (!job_title)
  {
    _Toast.open(_string("PleaseEnterYourJobTitle"),'w')
    return false
  }
  else if (permissions.length<1)
  {
    _Toast.open(_string("PleaseSelectThePermissions"),'w')
    return false
  }
  else return true
}

  teamMembersData = () => {
    let { fullname, email, phone, job_title, permissions } = this.state
    let _permissions = {}
    permissions.filter((element, index) => {
      if (element == 1) _permissions.queue = 1
      else if (element == 2) _permissions.booking = 1
      else if (element == 4) _permissions.team = 1
      else if (element == 3) _permissions.settings = 1
    })

    return {
      fname: fullname.split(" ")[0],
      lname: fullname.split(" ")[1] ? fullname.split(" ")[1] : "",
      email,
      position: job_title,
      phone,
      ..._permissions
    }
  }
  onCreateTeamMember = () => {
    if (this.ValidationForm())
    this.props.createTeamMeambers(this.teamMembersData())

  }
  onRemoveTeamMember = () => {
    let team_id = this.props.navigation.getParam("team").id
    let index = this.props.navigation.getParam("index")
    this.props.removeTeamMeambers(team_id, index)
  }


  onSelectPermission = (id, state) => {
    let _permissions = JSON.parse(JSON.stringify(this.state.permissions))
    let new_per = []
    if (!state) {
      _permissions.push(id)
      this.setState({ permissions: _permissions })

    }
    else {
      for (let i = 0; i < _permissions.length; i++)
        if (_permissions[i] != id) {
          new_per.push(_permissions[i])
        }
      this.setState({ permissions: new_per })

    }
  }

  componentDidMount = () => {
    this.props.navigation.getParam("team")
    this.getPermations()
  }
  render() {
    return (
      <TeamMembersAddView {...this.state} {...this} {...this.props} />
    );
  }
}

function mapStateToProps(state) {

  return {
    loadingDeleteTeamMember: state.teamMembersReducers.loadingDeleteTeamMember,
    loadingCreateAndEditTeamMember: state.teamMembersReducers.loadingCreateAndEditTeamMember,
  }
}

function mapDispatchToProps(Dispatch) {

  return {
    updateTeamMeambers: (id, team_data, index) => Dispatch(updateMemberRequest(id, team_data, index)),
    removeTeamMeambers: (id, index) => Dispatch(deleteMemberRequest(id, index)),
    createTeamMeambers: (team_data) => Dispatch(createTeamMemberRequest(team_data)),



  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamMembersAdd)


import { StyleSheet } from 'react-native';
import { metrics } from '../../config';
import { getHeight, Width, Height, getWidth, SpaceHeight } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
      width:Width,
      height:Height+SpaceHeight,
      backgroundColor:ColorsList.white
    },
    logo: {
        width: metrics.screenWidth * .8,
        height: metrics.screenHeight * .2,
        resizeMode: 'contain'
    },
    imageBackground: {
        width: '100%',
        height: '100%',
    },

    headerSection: {
        backgroundColor: 'white', height: 67,
        width: metrics.screenWidth,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: metrics.screenWidth * .05,
        shadow: 5, top: 80,
        position: 'absolute',
        paddingTop: 13
    },
    memberCard: {
        marginTop: 15,
        width: metrics.screenWidth,
        flexDirection: 'row',
        height: getHeight(58),
        shadow: 3,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    TeamMemberInfo: {
        flexDirection: 'row',
        width: metrics.screenWidth * .75,
        height: '100%',
        alignItems: 'center',
    },
    memberName: {
        fontSize: 'medium15Px', fontWeight: 'bold',
        color: 'black'
    },
    memberPosition: {
        fontSize: 'small1',
        fontWeight: '500',
        marginLeft: 8,
    },
    nextButton: (width)=>({
        width: width,
        // marginTop: getHeight(15),
        textColor: '#fff',
        shadow: 6,
        height: getHeight(44),
        colors: 'blue',
        borderRadius: 3,
        fontFamily:'bold',
        fontSize:'size15'

    }),
    removeButton: {
        width: getWidth(167.5),
        // marginTop: getHeight(15),
        textColor:'darkGray1',
        shadow: 6,
        height: getHeight(44),
      backgroundColor:'white',
      fontFamily:'bold',
      fontSize:'size15',
        borderRadius: 3,

    },
    headerTiltle: {

        fontSize: 'medium15Px', marginLeft: 5, color: 'black',
        fontWeight: 'bold', marginBottom: 5
    },
    choocies:(marginBottom)=>(
      {
        width: Width,
        flexDirection: 'row', height: getHeight(41),
        justifyContent: 'space-between',
        alignItems: 'center',
        height:getHeight(51),
        paddingHorizontal:getWidth(15),
        marginBottom:marginBottom
        ,overflow:'visible',
  
        
    }  
    ) 

};

export default styles;

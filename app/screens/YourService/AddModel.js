import React, { Component } from 'react';
import {
    View
} from 'react-native';
import styles from './styles';
import { Button } from '../../components/button/button';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { Icons } from '../../config';

import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { getHeight, getWidth } from '../../components/utils/dimensions';
import { PopupModal } from '../../components/popupModal/popupModal';
import ServiceCard from '../../components/ServiceCard/ServiceCard';
import { _string, getlanguage } from '../../local';
import { CheckBox } from '../../components/checkBox/checkBox';
import { ColorsList } from '../../components/PropsStyles';
import { InputSearch } from '../../components/inputSearch/inputSearch';
import { FlatList } from 'react-native-gesture-handler';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import { Toast, ModalToast } from '../../components/toast/toast';
class AddModel extends Component {
    constructor(props) {
        super(props);
        this.state = {

            time: "",
            selectedServiceId: -1

        };
    }
    onAnimated = (state) => {
        this.popupAddModal.onAnimated(state)
    }
    render() {
        let { selectedServiceId, time } = this.state
        let { onAddService, loadingCreateService, loadingIndustryServices, industryServices, onSearchIndustryServices } = this.props
        return (
            <PopupModal
                inputHeight={getHeight(120)}
                inputDuration={400}

                height={getHeight(500)}
                duration={500}
                zIndex={30}
                ref={_popupAddModal => this.popupAddModal = _popupAddModal}>
                <Card style={styles.popupContainer}>
                    <View style={styles.popupHeader}>
                        <Icons name='gridLinear' width={getWidth(28)} height={getWidth(28)} />
                        <_Text
                            style={styles.popupHeaderText}>{_string("your_service.add_a_new_service").toLocaleUpperCase()}</_Text>
                    </View>

                    <CustomTextInput
                        returnKeyType='done'

                        onChangeText={(text) => {
                            this.setState({ time: text })
                        }

                        }
                        value={time}
                        KeyboardType='numeric'
                        marginTop={getHeight(20)}
                        headerIcon={'clockTime'}
                        headerText={_string("your_service.avarge_time_per_customer")}
                        placeholder={_string("pleace_holder")}
                        leftIcon='editpen' />


                    <HeaderSection
                        style={{
                            height: getHeight(29),
                            width: getWidth(345),
                            justifyContent: 'flex-start', alignItems: 'flex-start'

                        }}
                        text={_string("ChooseYourService")}
                        iconName='grid'
                        textStyle={{
                            fontSize: 'medium15Px', fontFamily: 'bold', color: 'darkGray1', marginLeft: getWidth(5)
                        }}
                        iconSize={getWidth(18)}
                        iconStyle={{ width: getWidth(15), height: getHeight(15) }}


                    />

                    <InputSearch marginTop={getHeight(5)} placeholder={_string("SearchForService")}
                        onChangeText={onSearchIndustryServices} />
                    <FlatList
                        contentContainerStyle={{ paddingBottom: getHeight(70) }}
                        data={industryServices}
                        renderItem={({ item, index }) => {
                            return (
                                <ServiceItem
                                    service_name={item.title}
                                    onChecked={() => this.setState({ selectedServiceId: item.id })}
                                    checked={item.id == selectedServiceId} />
                            )
                        }}
                        ListFooterComponent={() => {
                            return (
                                <LoaderLists isLoading={loadingIndustryServices} />
                            )
                        }}
                    />
                    <Button
                        isLoading={loadingCreateService}
                        iconName="done"
                        text={_string("your_service.done")}
                        style={styles.popupDonebtn}
                        iconStyle={{ width: getWidth(18), height: getHeight(18) }}
                        onPress={() => {
                            if (this.state.time === "")
                                return ModalToast.open(_string("PleaseEnterTheAvatgeTime"))
                            else if (this.state.selectedServiceId ===-1)
                                return ModalToast.open(_string("PleaseSelectTheService"))

                            else {
                                onAddService({ service_id: selectedServiceId, duration: time }, this.onAnimated)

                            }
                        }}
                    />
                </Card>

            </PopupModal>
        );
    }
}
const ServiceItem = ({ checked, onChecked, service_name }) => {
    return (
        <Button
            onPress={onChecked}
            style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: getHeight(15),
                paddingHorizontal: getWidth(15),
                borderBottomWidth: 1,
                borderColor: ColorsList.rgbaBlack(0.04)
            }}>
            <CheckBox
                fontSize='size13'
                text={service_name}
                onChecked={onChecked}
                checked={checked} />
        </Button>
    )
}

export default AddModel
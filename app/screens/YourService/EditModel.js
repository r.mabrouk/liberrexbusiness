import React, { Component } from 'react';
import {
    View
} from 'react-native';
import styles from './styles';
import { Button } from '../../components/button/button';
import { _Text } from '../../components/text/text';
import { Card } from '../../components/card/card';
import { Icons } from '../../config';
import CustomTextInput from '../../components/CustomTextInput/CustomTextInput';
import { getHeight, getWidth } from '../../components/utils/dimensions';
import { PopupModal } from '../../components/popupModal/popupModal';
import ServiceCard from '../../components/ServiceCard/ServiceCard';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { _string } from '../../local';
import { ModalToast } from '../../components/toast/toast';


export default class EditModel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: -1,
            duration: "",
            index: -1,
            service: {},
            avarge_time: ""


        };
    }
    onAnimated = (state, item, index) => {
        if (item) {
            this.setState({ duration: item.duration.toString(), id: item.id, index, service: item }, () => {
            })
        }

        this.editServiceModal.onAnimated(state)

    }

    render() {
        let { onUpdateService, onRemoveService, loadingremoveService, loadingUpdateService } = this.props
        let { id, duration, index, service } = this.state
        return (
            <PopupModal

                inputHeight={getHeight(170)}
                inputDuration={400}
                duration={500} ref={(editModel) => this.editServiceModal = editModel} height={getHeight(280)}>
                <Card style={styles.popupContainer}>
                    <View style={styles.popupHeader}>
                        <Icons name='gridLinear' width={getWidth(28)} height={getWidth(28)} />
                        <_Text style={styles.popupHeaderText1}>{_string("your_service.edit").toLocaleUpperCase()}</_Text>
                        <_Text style={styles.popupHeaderEditText}>{"HairCut".toLocaleUpperCase()}</_Text>
                        <_Text style={styles.popupHeaderText}>{_string("your_service.service").toLocaleUpperCase()}</_Text>
                    </View>
                    {/* <CustomTextInput headerIcon={'grid'} headerText={'Service Name:'}
                        placeholder="Type here..." leftIcon='editpen' /> */}
                    <CustomTextInput
                        returnKeyType='done'
                        marginTop={getHeight(20)}
                        KeyboardType='numeric'
                        value={duration}
                        onChangeText={(text) => this.setState({ duration: text })}
                        headerIcon={'clockTime'}
                        headerText={_string("your_service.avarge_time_per_customer_in_minutes")}
                        placeholder={_string("pleace_holder")}
                        leftIcon='editpen' />
                    <Button
                        loaderColor={'darkGray1'}
                        isLoading={loadingremoveService}
                        iconName="delete"
                        text={_string("your_service.remove")}
                        style={styles.popupRemovebtn}
                        onPress={() => onRemoveService(id, index, this.onAnimated)}
                        iconStyle={{ width: getWidth(18), height: getHeight(18) }}

                    />

                    <Button
                        isLoading={loadingUpdateService}
                        iconName="done"
                        text={_string("your_service.done")}
                        style={styles.editDoneButton}
                        iconStyle={{ width: getWidth(18), height: getHeight(18) }}

                        onPress={() => {
                            if (this.state.duration === "")
                                return ModalToast.open(_string("PleaseEnterTheAvatgeTime"), "w")
                            let serivers_data = JSON.parse(JSON.stringify(service))
                            serivers_data.duration = duration
                            onUpdateService(id, serivers_data, index, this.onAnimated)

                        }}
                    />
                </Card>




            </PopupModal>
        );
    }
}



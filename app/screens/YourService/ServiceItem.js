import  React from "react";
import styles from "./styles";
import { Card } from "../../components/card/card";
import { _Text } from "../../components/text/text";

import {TouchableOpacity} from 'react-native'
import { Icons } from "../../config";
import { getWidth } from "../../components/utils/dimensions";
import { _string } from "../../local";


type ServiceItemProps={
    duration:String,
    title:String,
    onPress:Function
}



const get_duration=(Duration)=> {
    if (Duration >= 60)
        return  Math.floor(Duration/60)+':'+Duration%60  + ` ${_string("HourPerCustomer")}`
    else
        return (Math.floor(Duration*10)/10) +` ${_string("MinutePerCustomer")}`
}
export const ServiceItem = (props:ServiceItemProps) => {
    return (
        <Card style={styles.serviceContainer}>
            {/* <Card style={styles.cardInfo}> */}
                
                
                <Card style={{
                    flexDirection: 'row',
                    width: getWidth(310),
                    alignItems:'center'
                }}>
             
                        <Icons name='gridLinear' width={getWidth(28)} height={getWidth(28)} />
           
                    <Card style={styles.textContainer}>
                        <_Text style={styles.serviceName}>{props.title}</_Text>
                        <_Text style={styles.duration}>{"( "+get_duration(props.duration)+" )"}</_Text>
                    </Card>
                </Card>


                
                <TouchableOpacity  style={styles.leftButton}
                 onPress={props.onPress}
                >
                    <Icons name={'more'} width={getWidth(18)} height={getWidth(18)} />
                </TouchableOpacity>

            {/* </Card> */}
        </Card>
    )
}
import React, { Component } from 'react';
import {
    View,
    TouchableOpacity
} from 'react-native';
import styles from './styles';
import { Header } from '../../components/header/header';
import { _Text } from '../../components/text/text';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { Card } from '../../components/card/card';
import { Icons, AppStyles } from '../../config';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import { getHeight, getWidth, Width } from '../../components/utils/dimensions';
import { Input } from '../../components/input/input';
import AddModel from './AddModel'
import EditModel from './EditModel';
import { _string } from '../../local';
import { ServiceItem } from './ServiceItem';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import { InputSearch } from '../../components/inputSearch/inputSearch';
import EmptyData from '../../components/emptyData/emptyData';

export default class YourService extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {
        let { servicesList, loadingServicesList,
            onUpdateService,
            onRemoveService,
            loadingremoveService,
            loadingUpdateService,
            refreshing_list,
            onRefreshServiceList,
            onEndReachedServiceList,
            onServicesSearch,
            onAddService,
            loadingCreateService,
            loadingIndustryServices,
            industryServices,
            onSearchIndustryServices
        } = this.props
        return (
            <View
                style={styles.container}>
                <Header style={{ width: '100%', zIndex: 16 }}
                    onNotification={() => { this.props.navigation.navigate('Notification') }}
                    onBack={() => { this.props.navigation.goBack() }}
                />
                <HeaderSection
                    style={AppStyles.headerSection.container}
                    text={_string("your_service.your_service")}
                    iconName='grid'
                    withButton
                    iconSize={getWidth(28)}
                    iconButton={'addwhite'}
                    textButton={_string("your_service.add_new")}
                    iconStyle={{ width: getWidth(15),height:getHeight(15) }}
                    //buttonStyle={styles.headerbtn}
                    buttonStyle={{
                        colors: 'blue',
                        width: getWidth(110), height: getHeight(35),
                        borderRadius: 3,
                        textColor: 'white',
                        fontFamily:'bold',
                        fontSize:'size13'
                    }}
                    onPress={() => { this.add.onAnimated(true) }}
                />
                <InputSearch
                    onChangeText={onServicesSearch}
                    placeholder={_string("your_service.search_by_service_name")}
                />
             <FlatList
             contentContainerStyle={{paddingBottom:getHeight(15)}}
                    extraData={this.state}
                    keyExtractor={item => item.id}
                    refreshing={refreshing_list}
                    onRefresh={onRefreshServiceList}
                    onEndReached={onEndReachedServiceList}
                    onEndReachedThreshold={0.1}
                    data={servicesList}
                    renderItem={({ item, index }) => {
                        return (
                            <ServiceItem
                                onPress={() => { this.edit.onAnimated(true, item, index) }}
                                duration={item.duration} title={item.title} />

                        )
                    }}

                    ListFooterComponent={() => {
                        return <LoaderLists isLoading={loadingServicesList} />
                    }}
                />



                <EditModel
                    loadingremoveService={loadingremoveService}
                    loadingUpdateService={loadingUpdateService}
                    onUpdateService={onUpdateService}
                    onRemoveService={onRemoveService}
                    ref={(edit) => this.edit = edit} />
                <AddModel
                    onSearchIndustryServices={onSearchIndustryServices}
                    industryServices={industryServices}
                    loadingIndustryServices={loadingIndustryServices}
                    loadingCreateService={loadingCreateService}
                    onAddService={onAddService}
                    ref={(add) => this.add = add} />

                <EmptyData
                    onPress={() => { this.add.onAnimated(true) }}
                    show={(servicesList.length < 1)&&!loadingServicesList} />
            </View >

        );
    }
}



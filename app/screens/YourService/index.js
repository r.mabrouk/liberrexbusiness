import React, { Component } from 'react';
import { View, Text } from 'react-native';
import YourServiceView from './YourServiceView'
import { connect } from 'react-redux'
import { getAllServicesRequest, updateServiceRequest, deleteServiceRequest, createServiceRequest, getIndustryServicesRequest } from '../../actions/services';
import { getIndustryServices } from '../../api/methods/services';
class YourService extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing_list: false,
      servicesListkeyword:""
    };
  }
  onUpdateService = (serivrs_id, serivers_data, index, onCloseModal) => {
    console.log("onUpdateServiceonRemovcdfdfeService", serivrs_id, serivers_data, index, onCloseModal)

    this.props._onUpdateService(serivrs_id, serivers_data, index, onCloseModal)
  }
  onRemoveService = (serivrs_id, index, onCloseModal) => {
    console.log("onRemoveServiceonUpdateServiceonRemoveService", serivrs_id, index, onCloseModal)
    this.props._onDeleteService(serivrs_id, index, onCloseModal)
  }
  componentWillMount = () => {
   this.props._getAllServices(true,null,1)
    this.onGetIndustryServices()
  }

  onRefreshServiceList = () => {
    this.setState({ refreshing_list: true })
  }

  onEndReachedServiceList = () => {
    let {servicesListkeyword}=this.state
    let { services_current_page, services_last_page,loadingServicesList } = this.props
    if (services_current_page < services_last_page){
      this.props._getAllServices(null,servicesListkeyword?servicesListkeyword:null,services_current_page+1)
    }
  }
  
  onServicesSearch = (keyword) => {
    this.props._getAllServices(true, keyword, 1)
    this.setState({servicesListkeyword:keyword})
  }
  onAddService = (service_data, onCloseModal) => {
    this.props.onCreateService(service_data, onCloseModal)
  }
  onSearchIndustryServices = (keyword) => {
    this.props._getIndustryServices(true, keyword, 1)
  }
  onGetIndustryServices = () => {
    this.props._getIndustryServices()
  }

  render() {
    return (
      <YourServiceView {...this.state} {...this} {...this.props} />
    );
  }
}



function mapStateToProps(state) {

  return {
    loadingServicesList: state.servicesReducer.loadingServicesList,
    loadingIndustryServices: state.servicesReducer.loadingIndustryServices,
    loadingUpdateService: state.servicesReducer.loadingUpdateService,
    loadingremoveService: state.servicesReducer.loadingremoveService,
    loadingrAddService: state.servicesReducer.loadingrAddService,
    industryServices: state.servicesReducer.industryServices,
    loadingCreateService: state.servicesReducer.loadingCreateService,
    servicesList: state.servicesReducer.servicesList,
    services_current_page: state.servicesReducer.services_current_page,
    services_last_page: state.servicesReducer.services_last_page,

  }
}
function mapDispatchToProps(Dispatch) {
  return {
    _getAllServices: (reset, keyword, page) => Dispatch(getAllServicesRequest(reset, keyword, page)),
    _onUpdateService: (service_id, serivers_data, index, onCloseModal) =>
      Dispatch(updateServiceRequest(service_id, serivers_data, index, onCloseModal)),
    _onDeleteService: (service_id, index, onCloseModal) =>
      Dispatch(deleteServiceRequest(service_id, index, onCloseModal)),
    onCreateService: (service_data, onClodeModal) =>
      Dispatch(createServiceRequest(service_data, onClodeModal)),
    _getIndustryServices: (reset, keyword, page) => Dispatch(getIndustryServicesRequest(reset, keyword, page))

  }
}


export default connect(mapStateToProps, mapDispatchToProps)(YourService)


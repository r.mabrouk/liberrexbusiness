import { StyleSheet } from 'react-native';
import { metrics } from '../../config';
import { getHeight, getWidth, Width, Height } from '../../components/utils/dimensions';
import { ColorsList } from '../../components/PropsStyles';

const styles = {
    container: {
       width:Width,
       height:Height,
       color:ColorsList.white,
       alignItems:'center'
    },
    logo: {
        width: getWidth(345),
        height: getHeight(667*.2),
        resizeMode: 'contain'
    },
    imageBackground: {
        width: '100%',
        height: '100%',
    },
    headerSection: {
        backgroundColor: 'white', height: getHeight(67),
        width:Width,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: Width*.05,
        shadow: 5, top: getHeight(80),
        position: 'absolute',
        paddingTop: getHeight(13)
    },
    notificationCard: {


        width: metrics.screenWidth,
        backgroundColor: 'white',
        shadow: 4,
        height: getHeight(86),
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: getHeight(67),
        flexDirection: 'row'


    },
    Icon: {
        //width: metrics.screenWidth * .2,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: 'red'
    },
    title: {
        fontSize: 'small13Px',
        fontWeight: 'bold',
        color: 'black'
    },
    subtitle: {

        fontSize: 'small1',
        fontWeight: '500',
        color: 'black',
        marginTop: getHeight(2),
    },
    popupHeaderText: {

        marginLeft: getWidth(10),
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily:'bold',
    },

    popupHeaderText1: {
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily:'bold',
        marginLeft: getWidth(8),
    },


    popupHeaderEditText: {
        fontSize: 'size15',
        color: 'darkGray1',
        fontFamily:'bold',
        color: 'lightBlue',
        marginLeft: getWidth(3)
      

    },

  
    cardInfo: {
        width: metrics.screenWidth,
        height: getHeight(58),
        backgroundColor: 'white',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row'
    },
    serviceContainer: {

        width: Width, 
        justifyContent: 'center',
         alignItems: 'center',
        shadow: 6, 
        marginTop: getHeight(15),
         height: getHeight(58),
          backgroundColor: 'white',
          flexDirection:'row',
          justifyContent:'space-between',
          paddingHorizontal:getWidth(15)

    },
    infoContainer:
    {
        flexDirection: 'row',
        width: getWidth(345),
        // shadow: 5,
        alignSelf: 'center',
        // justifyContent: 'space-between',
        //backgroundColor:'green'
    },
    textContainer: {
        flexDirection: 'row', marginLeft: getWidth(10),alignItems:'center'
    },
    serviceName: {
        fontSize: 'size15',
        fontFamily: 'bold', color: 'darkGray1'
    },
    duration: {
        fontSize: 'size12', 
        fontFamily:'medium', marginLeft: getWidth(5),
        color:"lightGray1"
    },
    leftButton: { justifyContent: 'center', alignItems: 'flex-end' },
    input: {
        borderRadius: 3,
        width: getWidth(345 ),
        height: getHeight(41),
        shadow: 6,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: getHeight(10)
    },
    popupHeader: {
        flexDirection: 'row', width: getWidth(345),
        justifyContent: 'center', alignItems: 'center',
        marginTop:getHeight(20)
    },
    popupDonebtn: {
        colors: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(345),
        height: getHeight(44),
        borderRadius: 3,
        textColor: 'white',
        shadow:6,
        marginTop: getHeight(15),
        marginBottom: getHeight(15),
        fontFamily:'bold',
        color:'white',
        fontSize:'size15',
        position:'absolute',top:getHeight(420),left:getWidth(15)
    },
    editDoneButton: {
        colors: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        width: getWidth(345),
        height: getHeight(44),
        borderRadius: 3,
        textColor: 'white',
        shadow:6,
        marginTop: getHeight(15),
        marginBottom: getHeight(15),
        fontFamily:'bold',
        color:'white',
        fontSize:'size15',

    },
    popupContainer: {

        height: '100%', width: '100%',
        alignItems: 'center'


    },
    popupRemovebtn: {
        backgroundColor: 'white',
        shadow: 6,
        width: getWidth(345),
        height: getHeight(44),
        borderRadius: 3,
        textColor: 'darkGray1',
        fontFamily:'bold',
        color:'darkGray1',
        fontSize:'size15'

    },
    ServiceCard: {
        flexDirection: 'row', justifyContent: 'flex-start',
        backgroundColor: 'white', shadow: 5,
        height: 41,
        width: '45%',
        alignItems: 'center',
        paddingLeft: 20,
        borderRadius: 3

    },
    Service: {
        backgroundColor: 'white',
        width: '95%',
        height: 300,
        marginTop: 5,
    },
};

export default styles;

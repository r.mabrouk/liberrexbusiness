import React, { Component } from 'react';
import {
    View, FlatList, TouchableOpacity
} from 'react-native';
import { PopupModal } from '../../../components/popupModal/popupModal';
import { ColorsList } from '../../../components/PropsStyles';
import { getHeight, Width } from '../../../components/utils/dimensions';
import { _Text } from '../../../components/text/text';
import styles from './styles';
import { _string } from '../../../local';
import { CheckBox } from '../../../components/checkBox/checkBox';

type AddNewCustomerModalProps = {
    duration: number,
    currentModal: ''

}
class FilterByDateModal extends Component<AddNewCustomerModalProps> {
    constructor(props) {
        super(props);
        this.state = {
            currentSelected: ""
        }
    }
    async componentDidMount() {
    }
    componentWillReceiveProps = (props) => {
        this.setState({ currentModal: props.currentModal })
    }
    onAnimated = (state) => {
        this.reportByModal.onAnimated(state)
    }
    render() {


        let data = [
            { date:_string("modals.report_by.daily") },
            { date: _string("modals.report_by.weekly")},
            { date: _string("modals.report_by.monthly") },
            { date: _string("modals.report_by.yearly") }
        ]
        let {onFilter}=this.props
        return (


            <PopupModal
            zIndex={300}
                backgroundColor={ColorsList.rgbaWhite(.97)}
                duration={500}
                ref={(_reportByModal) => this.reportByModal = _reportByModal}
                height={getHeight(199)} >

                <FlatList
                    data={data}
                    style={{ width: Width }}
                    contentContainerStyle={styles.contentContainerStyleList}
                    renderItem={({ item, index }) => {    
                        return (
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ currentSelected: index })
                                    this.reportByModal.onAnimated(false)
                                    onFilter(item,"filter_date")
                                }}
                                style={styles.contentContainerModal}>
                                <_Text
                                    style={styles.modaleText(this.state.currentSelected, index)} >
                                    {item.date}
                                </_Text>
                                {this.state.currentSelected== index &&
                                    <CheckBox checked={true} />}
                            </TouchableOpacity>

                        )
                    }} />
            </PopupModal>
        );
    }
}


export default FilterByDateModal;
import { getWidth, getHeight } from "../../../components/utils/dimensions";
import { ColorsList } from "../../../components/PropsStyles";

export default Styles={
    contentContainerModal: {
        width: getWidth(375),
        height: getHeight(50),
        // backgroundColor: ColorsList.white,
        borderBottomWidth: 1,
        borderColor: ColorsList.rgbaBlack(.05),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: getWidth(15)
    },
    modaleText: (currentSelected, index) => ({
        fontFamily: 'darkGray1',
        color: currentSelected == index ? 'darkGary1' : 'gray0',
        fontSize: currentSelected == index ? 'size15' : 'size13'
    }),
}
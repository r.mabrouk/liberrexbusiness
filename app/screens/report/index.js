import React, { Component } from 'react';
import {
    View
} from 'react-native';
import ReportView from './reportView';
import { getReportsRequest, filterReportRequest } from '../../actions/report';
import { connect } from 'react-redux'
import { getAllServicesRequest } from '../../actions/services';
import { _string } from '../../local';
class Report extends Component {
    constructor(props) {
        super(props);
        this.state = {
        
            filter_date: _string("modals.report_by.weekly"),
            selectedServices: [],
            selectedServicesNames: [],
            servicesListkeyword: ""
        };
    }
    async componentDidMount() {
    }


    onFilter = (value, key) => {
        let { filter_date, selectedServices } = this.state
        this.setState({ [key]: value.date }, () => {
            let data = {
                services: selectedServices.toString(),
                date: filter_date
            }
            this.props._filterReport(data)
        })
    }
    render() {
        return (
            <ReportView {...this} {...this.state} {...this.props} />
        );
    }

    componentDidMount = () => {
        this.props._getReportList()
        this.props._getAllServices(true, null, 1)
    }

    onRefreshServiceList = () => {
        this.setState({ refreshing_list: true })
    }

    onEndReachedServiceList = () => {
        let { servicesListkeyword } = this.state
        let { services_current_page, services_last_page, loadingServicesList } = this.props
        if (services_current_page < services_last_page) {
            this.props._getAllServices(null, servicesListkeyword ? servicesListkeyword : null, services_current_page + 1)
        }
    }
    onServicesSearch = (keyword) => {
        this.props._getAllServices(true, keyword, 1)
        this.setState({ servicesListkeyword: keyword })
    }

    servicesSelected = (selectedServicesID, selectedServicesNames) => {
        let { filter_date, selectedServices } = this.state
        this.setState({selectedServices: selectedServicesID, selectedServicesNames},()=>{
            let data = {
                services: selectedServices.toString(),
                date: filter_date
            }
            this.props._filterReport(data)
        })
    }
}

function mapStateToProps(state) {
    return {
        reports: state.reportReducer.reports,
        loadingReportList: state.reportReducer.loadingReportList,

        services_current_page: state.servicesReducer.services_current_page,
        services_last_page: state.servicesReducer.services_last_page,
        servicesList: state.servicesReducer.servicesList,
        loadingServicesList: state.servicesReducer.loadingServicesList,

    };
}

function mapDispatchToProps(Dispatch) {
    return {
        _getReportList: () => Dispatch(getReportsRequest()),
        _filterReport: (keywords) => Dispatch(filterReportRequest(keywords)),
        _getAllServices: (reset, keyword, page) => Dispatch(getAllServicesRequest(reset, keyword, page)),

    };
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Report);


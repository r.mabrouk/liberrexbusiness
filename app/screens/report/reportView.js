import React, { Component } from 'react';
import {
  View, ScrollView
} from 'react-native';
import styles from './styles';

import HomeHeader from '../../components/HomeHeader/HomeHeader';
import { Header } from '../../components/header/header';
import { Width, getWidth, getHeight } from '../../components/utils/dimensions';
import { images, Icons } from '../../config';
import { Card } from '../../components/card/card';
import { ReportRequestItem } from '../../components/reportRequestItem/reportRequestItem';
import { HeaderSection } from '../../components/headerSection/headerSection';
import { _Text } from '../../components/text/text';
import { Rating } from '../../components/rating/rating';
// import {LineChart} from 'react-native-charts-wrapper';
import StarRating from 'react-native-star-rating';
import { ColorsList } from '../../components/PropsStyles';
import { _string } from '../../local';
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from 'react-native-chart-kit'
import { Button } from '../../components/button/button';
import FilterByDateModal from './filterByDateModal/filterByDateModal';
import { LoaderLists } from '../../components/LoaderList/loaderLists';
import ServiceModal from '../QueueEdit/ServiceModal';
class ReportView extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  async componentDidMount() {
  }

  render() {
    let { reports, onFilter, filter_date, loadingReportList, selectedServicesNames } = this.props

    return (
      <View
        style={styles.container}>
        <Header
          onBack={() => this.props.navigation.goBack()}
          onNotification={() => this.props.navigation.navigate('Notification')}
          iconName='arrowBack'
          style={{ width: Width, zIndex: 15 }} />

        <Card style={{ width: Width, height: getHeight(77), colors: 'blue', shadow: 6, zIndex: 14, marginTop: -getHeight(30), justifyContent: 'center', alignItems: 'flex-start', paddingTop: getHeight(10) }}>
          <View style={{ width: getWidth(290), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: getWidth(15) }}>
            <Icons name='filter' width={getWidth(18)} height={getWidth(18)} />
            <Button
              onPress={() => {
                this.filterByDateModal.onAnimated(true)
              }}
              text={filter_date}
              iconName='arrowDown'
              style={{
                width: getWidth(107),
                height: getHeight(31),
                backgroundColor: 'white',
                flexDirection: 'row-reverse',
                justifyContent: 'space-between',
                paddingHorizontal: getWidth(10),
                fontSize: 'size11',
                fontFamily: 'medium',
                borderRadius: 3,
                shadow: 6
              }}
            />
            <Button
              onPress={() => {
                this.selectService.onAnimated(true)
              }}
              text={selectedServicesNames.length < 1 ? _string("AllServices") : selectedServicesNames.toString()}
              iconName='arrowDown'
              style={{
                width: getWidth(107),
                height: getHeight(31),
                backgroundColor: 'white',
                flexDirection: 'row-reverse',
                justifyContent: 'space-between',
                paddingHorizontal: getWidth(10),
                fontSize: 'size11',
                fontFamily: 'medium',
                borderRadius: 3,
                shadow: 6
              }} />
          </View>
        </Card>
        <ScrollView >
          {Object.keys(reports).map((key, index) => {
            if (reports[key].type == "linechart")
              return <Linechar
                labels={reports[key].labels}
                data={reports[key].data}
                title={key.replace("_", " ")}
                zIndex={Object.keys(reports).length - index} />
            if (reports[key].type == "piechart")
              return <BarChar
                labels={reports[key].labels}
                data={reports[key].data}
                title={key.replace("_", " ")}
                zIndex={Object.keys(reports).length - index} />

            if (reports[key].type == "star")
              return <StarChart

                labels={reports[key].labels}
                data={reports[key].data}
                title={key.replace("_", " ")}
                zIndex={Object.keys(reports).length - index} />
          })}
          {/* start chart */}
          <LoaderLists isLoading={loadingReportList} />
        </ScrollView>

        <ServiceModal
          servicesList={this.props.servicesList}
          selectedServices={this.props.selectedServices}
          selectedServicesNames={this.props.selectedServicesNames}
          loadingServicesList={this.props.loadingServicesList}
          onServicesSearch={this.props.onServicesSearch}
          onEndReachedServiceList={this.props.onEndReachedServiceList}
          onServiceAdd={(selectedServicesID, selectedServicesNames) => {
            this.props.servicesSelected(selectedServicesID, selectedServicesNames)
            this.selectService.onAnimated()
          }}
          ref={(selectService) => this.selectService = selectService} />

        <FilterByDateModal
          onFilter={onFilter}
          ref={(_filterByDateModal) => this.filterByDateModal = _filterByDateModal} />

      </View>
    );
  }
}
const CustomerSatisfaction = ({ rating, value }) => {
  return (
    <Card style={{ flexDirection: 'row', width: Width, paddingHorizontal: getWidth(15), paddingVertical: getHeight(10), justifyContent: 'space-between' }}>

      <StarRating
        disabled={true}
        maxStars={5}
        rating={rating}
        starSize={getHeight(19)}
        emptyStar={'star'}
        fullStar={'star'}
        iconSet={'FontAwesome'}
        fullStarColor='#FBBC05'
        emptyStarColor={ColorsList.rgbaOrange(.2)}
        containerStyle={{ width: ((Width * 0.4) / 5) * 5 }}
        starStyle={{}}
        selectedStar={{}}
      />
      <_Text style={{ fontSize: 'size17', fontFamily: 'black', paddingHorizontal: getWidth(10), color: 'darkGray1' }}>{value}  <_Text style={{ fontSize: 'small', fontFamily: 'black', color: 'gray0' }}>{_string("report.customers")}</_Text></_Text>

    </Card>
  )
}


const CircalChart = () => {
  return (
    <Card style={{ width: Width, height: getHeight(235), shadow: 6, backgroundColor: 'white', alignItems: 'center', zIndex: 10 }} >
      <HeaderSection
        onPress={() => {
          this.reportByModal.onAnimated(true)
        }}
        style={{
          width: getWidth(345),
          height: getHeight(70),
          justifyContent: 'space-between',
          flexDirection: 'row',
          alignItems: 'center'
        }}
        iconStyle={{ width: getWidth(20),height:getHeight(20),color: 'gray0'}}

        buttonStyle={styles.btnHeader}
        text={_string("report.requests_report").toLocaleLowerCase()}
        iconName='appointmentsgray'
        iconButton='arrowDown'
      />
      <View
        style={{ width: Width, alignItems: 'center' }}>
        <View
          style={{
            width: '92%',
            flexDirection: 'row',
            justifyContent: 'space-between'
          }}>
          {chartData.map((item) => (
            <ReportRequestItem
              withStatusDot
              requests
              totalValue={item.totalValue}
              usedValue={item.usedValue}
              status={item.status} />
          ))}
        </View>
      </View>
    </Card>
  )
}
const Linechar = ({ zIndex, title, labels, data }) => {
  return (
    <Card style={{ width: Width, height: getHeight(280), shadow: 6, backgroundColor: 'white', alignItems: 'center', zIndex: zIndex }} >
      <HeaderSection
        onPress={() => {
          this.reportByModal.onAnimated(true)
        }}
        style={{
          width: getWidth(345),
          height: getHeight(70),
          justifyContent: 'space-between',
          flexDirection: 'row',
          alignItems: 'center'
        }}
        iconStyle={{ width: getWidth(20),height:getHeight(20),color: 'gray0'}}

        buttonStyle={styles.btnHeader}
        text={(title + " Report").toLocaleUpperCase()}
        iconName='clockTime'
        iconButton='arrowDown'
      />


      {/* //line chart */}
      <View
        style={{ width: Width, alignItems: 'center' }}>
        <View
          style={{
            width: '92%',
            flexDirection: 'row',
            justifyContent: 'space-between'

          }}>

          <LineChart
          fromZero
            onDataPointClick={() => {
            }}
            data={{
              labels: labels,
              datasets: [{
                data: data
              }]
            }}
            withShadow
            width={getWidth(345)} // from react-native
            height={getHeight(200)}
            // yAxisLabel={'$'}
            chartConfig={{
              strokeWidth: 1,
              backgroundColor: 'green',
              backgroundGradientFrom: 'white',
              backgroundGradientTo: 'white',
              decimalPlaces: 0, // optional, defaults to 2dp
              color: (opacity = 1) => ColorsList.blue,
            }}
            bezier
          />

        </View>
      </View>
    </Card>
  )
}

const StarChart = ({ zIndex, title, labels, data }) => {
  return (
    <Card style={{ width: Width, shadow: 6, backgroundColor: 'white', alignItems: 'center' }} >
      <HeaderSection
        onPress={() => {
          this.reportByModal.onAnimated(true)
        }}
        style={{
          width: getWidth(345),
          height: getHeight(60),
          justifyContent: 'space-between',
          flexDirection: 'row',
          alignItems: 'center',
        }}
        iconStyle={{ width: getWidth(20),height:getHeight(20),color: 'gray0'}}

        text={(title + " Report").toLocaleUpperCase()}
        iconName='feedback'
      />

      {labels.map((item, index) => {
        return <CustomerSatisfaction value={data[index]} rating={item} />
      })}
    </Card>
  )

}
const BarChar = ({ zIndex, title, labels, data }) => {
  return (
    <Card style={{ width: Width, height: getHeight(280), shadow: 6, backgroundColor: 'white', alignItems: 'center', zIndex: zIndex }} >
      <HeaderSection
        onPress={() => {
          this.reportByModal.onAnimated(true)
        }}
        style={{
          width: getWidth(345),
          height: getHeight(70),
          justifyContent: 'space-between',
          flexDirection: 'row',
          alignItems: 'center'
        }}
        iconStyle={{ width: getWidth(20),height:getHeight(20),color: 'gray0'}}

        buttonStyle={styles.btnHeader}
        text={(title + " Report").toLocaleUpperCase()}
        iconName='reports'
        iconButton='arrowDown'
      />


      {/* //BarChart */}
      <View
        style={{ width: Width, alignItems: 'center' }}>
        <View
          style={{
            width: '92%',
            flexDirection: 'row',
            justifyContent: 'space-between'

          }}>

          <BarChart
          fromZero
            onDataPointClick={() => {
            }}
            data={{
              labels: labels,
              datasets: [{
                data: data
              }]
            }}
            withShadow
            width={getWidth(345)} // from react-native
            height={getHeight(200)}
            // yAxisLabel={'$'}
            chartConfig={{
              strokeWidth: 1,
              backgroundColor: 'gray',
              backgroundGradientFrom: 'white',
              backgroundGradientTo: 'white',
              decimalPlaces: 0, // optional, defaults to 2dp
              color: (opacity = 1) => ColorsList.blue,
            }}
            bezier
          />
        </View>
      </View>
    </Card>
  )
}
export default ReportView;

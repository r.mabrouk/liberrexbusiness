import ImageResizer from 'react-native-image-resizer';


export const compressImages = async (imageUri) => {
    let response = await ImageResizer.createResizedImage(imageUri, 300, 300, 'JPEG', 10);
    return {
        uri: response.uri,
        type: 'image/jpeg',
        name: response.name,
    }
}

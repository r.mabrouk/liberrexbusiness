module.exports = {
    dependencies: {
      'react-native-firebase': {
        platforms: {
          android: null, // disable Android platform, other platforms will still autolink if provided
        },
      },
      'react-native-date-picker': {
        platforms: {
          android: null, // disable Android platform, other platforms will still autolink if provided
        },
      },
      'tipsi-stripe': {
        platforms: {
          android: null, // disable Android platform, other platforms will still autolink if provided
        },
      }
      ,
      'react-native-google-signin': {
        platforms: {
          android: null, // disable Android platform, other platforms will still autolink if provided
        },
      }
      
    },
  };

  